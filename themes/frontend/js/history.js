$(document).ready(function () {
    $(".nav-tabs a").click(function () {
        $(this).tab('show');
    });
    
    $(".exercise__").on("change", function(){
        if (this.value == "1") {
            $(".exercise___").show();
        }
        else {
            $(".exercise___").hide();
        }
    });  
    if ($('input[name=HaveExercise]:checked').val() == "1") {
        $(".exercise___").show();
    }
    else {
        $(".exercise___").hide();
    }
    $(".tobacco__").on("change", function () {
        if (this.value == "1") {
            $(".tobacco___").show();
        }
        else {
            $(".tobacco___").hide();
        }
    });
    if ($('input[name=HaveTobaccouse]:checked').val() == "1") {
            $(".tobacco___").show();
        }
        else {
            $(".tobacco___").hide();
        }
    $(".beverage__").on("change", function () {
        if (this.value == "1") {
            $(".beverage___").show();
        }
        else {
            $(".beverage___").hide();
        }
    });
    if ($('input[name=Havedrink]:checked').val() == "1") {
            $(".beverage___").show();
        }
        else {
            $(".beverage___").hide();
        }
    $(".drug__").on("change", function () {
        if (this.value == "1") {
            $(".drug___").show();
        }
        else {
            $(".drug___").hide();
        }
    });
     if ($('input[name=HaveDruguse]:checked').val() == "1") {
            $(".drug___").show();
        }
        else {
            $(".drug___").hide();
        }
    if ($("#success").val().length > 0)
    {   toastr.options = {"positionClass": "toast-top-center"};
        toastr["success"]($("#success").val());
        
    }
    if ($("#error").val().length > 0)
    {
        toastr["error"]($("#error").val());
    }
});
// function medications1() {
//     var html = '<div class="del_elem"><div class="row">'
//             + '<div class="col-lg-6 col-xs-12"><label>Name of Medication </label></div>'
//             + '<div class="col-lg-5 col-xs-12"><input type="text" name="medicationname[]" ></div>'
//             + '</div>'

//             + '<div class="row">'
//             + '<div class="col-lg-6 col-xs-12"><label>Dosage </label></div>'
//             + '<div class="col-lg-5 col-xs-12"><input type="text" name="madicationdose[]" ></div>'
//             + '</div>'
//             + '<div class="row">'
//             + '<div class="col-lg-6 col-xs-12"><label>Frequency</label></div>'
//             + '<div class="col-lg-5 col-xs-12"><input type="text" name="madicationoftentaken[]" ></div>'
//             + '</div>'

//             + '<div class="row">'
//             + '<div class="col-lg-6 col-xs-12"><label>Reason</label></div>'
//             + '<div class="col-lg-5 col-xs-12"><input type="text" name="medicationprescribed[]" ></div>'
//             + '<div class="col-lg-1 col-xs-12">'
//             + '<a href="javascript:;" onclick="medications1()"><span class="glyphicon glyphicon-plus" style="top:11px"></span></a>'
//             + '<a href="javascript:;" onclick="remove1(this)"><span class="glyphicon glyphicon-minus" style="top:11px"></span></a>'
//             + '</div>'
//             + '</div><br><div>';
//     //alert(html);
//     $("#medications__").append(html);
// }
function medic(){
    var html='<tr>'
            +'<td><input type="text" name="medicationname[]" ></td>'
            +'<td><input type="text" name="madicationdose[]" ></td>'
            +'<td><input type="text" name="madicationoftentaken[]" ></td>'
            +'<td><input type="text" name="medicationprescribed[]" ></td>'
            +'<td>'
            + '<a href="javascript:;" class="btn btn-primary" onclick="medic()"><i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove_tr(this)"><i class="fa fa-minus" aria-hidden="true"></i></a>'
            +'</td>'
            +'</tr>';
     $('#Medication tbody').append(html);
}
function remove_tr(e){
    //$(this).parent('tr').remove();
    e.closest("tr").remove();
}
function add_fam() {
    var html = '<div class="row">'
            + '<div class="col-lg-offset-4  col-lg-7 col-xs-11 text-center">'
            + '<input type="text" placeholder="Other" name="FDIGNOOTHER[]">'
            + '</div>'
            + '<div class="col-lg-2 col-xs-1 text-left">'
            + '<a href="javascript:;" class="btn btn-primary" onclick="add_fam()"> <i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove(this)"> <i class="fa fa-minus" aria-hidden="true"></i></a>'
            + '</div>'
            + '</div>';
    $("#fam_hist_").append(html);
}
function add_p() {
    var html = '<div class="row">'
            + '<div class="col-lg-offset-1  col-lg-5 col-xs-5 text-center">'
            + '<input type="text" placeholder="Other" name="PMHO[]">'
            + '</div>'
            + '<div class="col-lg-2 col-xs-1 text-left">'
            + '<a href="javascript:;" class="btn btn-primary" onclick="add_p()"> <i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove(this)"> <i class="fa fa-minus" aria-hidden="true"></i></a>'
            + '</div>'
            + '</div>';
    $("#past_med_").append(html);
}
function add_s_o() {
    var html = '<div class="row">'
            + '<div class="col-lg-offset-2  col-lg-5 col-xs-5 text-center">'
            + '<input type="text" placeholder="Other" name="other_S[]">'
            + '</div>'
            + '<div class="col-lg-2 col-xs-1 text-left">'
            + '<a href="javascript:;" class="btn btn-primary" onclick="add_s_o()"> <i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove(this)"> <i class="fa fa-minus" aria-hidden="true"></i></a>'
            + '</div>'
            + '</div>';
    $("#PastSurgicalHistoryother").append(html);
}
function add_f_o() {
    var html = '<div class="row">'
            + '<div class="col-lg-offset-1  col-lg-5 col-xs-5 text-center">'
            + '<input type="text" placeholder="Other" name="other_f[]">'
            + '</div>'
            + '<div class="col-lg-2 col-xs-1 text-left">'
            + '<a href="javascript:;" class="btn btn-primary" onclick="add_f_o()"> <i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove(this)"> <i class="fa fa-minus" aria-hidden="true"></i></a>'
            + '</div>'
            + '</div>';
    $("#f_other").append(html);
}
function add() {
    var html = '<div class="row"><div class="col-lg-2 col-xs-2 text-center">'
            + '</div>'
            + '<div class="col-lg-5 col-xs-5 text-center">'
            + '<input type="text" placeholder="Type" name="PSHTYPE[]">'
            + '</div>'
            + '<div class="col-lg-3 col-xs-3 text-center">'
            + '<input type="text" placeholder="Year" name="PSHYEAR[]" onkeypress="return isNumberKey(event)">'
            + '</div>'
            + '<div class="col-lg-2 col-xs-2 text-left">'
            + '<a href="javascript:;" class="btn btn-primary" onclick="add()"> <i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove(this)"> <i class="fa fa-minus" aria-hidden="true"></i></a>'
            + '</div></div>';
    $("#surg").append(html);
}

function remove(e) {
    e.closest("div.row").remove();
}
function remove1(e) {
    e.closest("div.del_elem").remove();
}
function add1() {
    var html = '<div class="row"><div class="col-lg-2 col-xs-2 text-center">'
            + '</div>'
            + '<div class="col-lg-5 col-xs-5 text-center">'
            + '<input type="text" placeholder="Allergy" name="medication[]">'
            + '</div>'
            + '<div class="col-lg-3 col-xs-3 text-center">'
            + '<input type="text" placeholder="Reaction" name="reaction[]">'
            + '</div>'
            + '<div class="col-lg-2 col-xs-2 text-left">'
            + '<a href="javascript:;" class="btn btn-primary" onclick="add1()"> <i class="fa fa-plus" aria-hidden="true"></i></a> '
            + '<a href="javascript:;" class="btn btn-warning" onclick="remove(this)"> <i class="fa fa-minus" aria-hidden="true"></i></a>'
            + '</div></div>';
    $("#surg1").append(html);
}
function add2() {    
    var html = '<div class="row top-buffer">'
            + '<div class="col-lg-3 col-xs-6 text-left">'
            + '<input type="text" placeholder="Name" name="si_name[]">'
            + '</div>'
            + '<div class="col-lg-3 col-xs-6 text-center">'
            + '<select name="si_drug[]">'
            + '<option value="-1" disabled selected style="display: none;">Health Status</option>'
            + '<option value="1">Good</option>'
            + '<option value="2" >Fair</option>'
            + '<option value="3" >Poor</option>'
            + '</select>'
            + '</div>'
            + '<div class="col-lg-3 col-xs-6 text-center">'
            + '<input type="text" placeholder="Cause of death" name="si_death_cause[]">  '
            + '</div>'
            + '<div class="col-lg-2 col-xs-6 text-center">'
            + '<input type="text" placeholder="Year" name="si_year[]" onkeypress="return isNumberKey(event)">'
            + '</div>  '
            + '<div class="col-lg-1 col-xs-6 text-center">'
            + '<a href="javascript:;" onclick="add2()"><span class="glyphicon glyphicon-plus" style="top:11px"></span></a>'
            + '<a href="javascript:;" onclick="remove(this)"><span class="glyphicon glyphicon-minus" style="top:11px"></span></a>'
            + '</div>'
            + '</div>';
    $("#sib").append(html);
}
function add3() {    
    var html = '<div class="row top-buffer">'
            + '<div class="col-lg-3 col-xs-6 text-left">'
            + '<input type="text" placeholder="Name" name="chl_name[]">'
            + '</div>'
            + '<div class="col-lg-3 col-xs-6 text-center">'
            + '<select name="chl_drug[]">'
            + '<option value="-1" disabled selected style="display: none;">Health Status</option>'
            + '<option value="1">Good</option>'
            + '<option value="2" >Fair</option>'
            + '<option value="3" >Poor</option>'
            + '</select>'
            + '</div>'
            + '<div class="col-lg-3 col-xs-6 text-center">'
            + '<input type="text" placeholder="Cause of death" name="chl_death_cause[]">  '
            + '</div>'
            + '<div class="col-lg-2 col-xs-6 text-center">'
            + '<input type="text" placeholder="Year" name="chl_year[]" onkeypress="return isNumberKey(event)">'
            + '</div>  '
            + '<div class="col-lg-1 col-xs-6 text-center">'
            + '<a href="javascript:;" onclick="add3()"><span class="glyphicon glyphicon-plus" style="top:11px"></span></a>'
            + '<a href="javascript:;" onclick="remove(this)"><span class="glyphicon glyphicon-minus" style="top:11px"></span></a>'
            + '</div>'
            + '</div>';
    $("#children").append(html);
}


