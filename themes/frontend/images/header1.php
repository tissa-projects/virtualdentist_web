<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>My way med</title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>themes/frontend/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url();?>themes/frontend/css/custom.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo base_url();?>themes/frontend/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Muli:400,normal,normalitalic,700normal,700italic|Open+Sans:800,300,700,400,300italic,700normal,700italic,400italic|PT+Serif:400italic"/>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url();?>themes/frontend/js/jquery.min.js"></script> 

</head>
  <?php 
      $page = $this->uri->segment(2);
      $page_menu = ''; if(isset($selected_menu)) { if(strlen($selected_menu) > 0) { $page_menu = trim($selected_menu); } }
      ?>
<style type="text/css">
.active{
  background-color: rgba(0, 0, 0, 0.6);
}
</style>
<body>
<nav class="navbar navbar-default nav-height">
  <div class="container-fluid">
      <div class="col-md-12">
        <span>
        <img src="<?php echo base_url();?>themes/frontend/images/logo.jpg" alt="logo" class="headernew" ></span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span><strong>Book an appointment with doctor</strong> <span style="color:#e30707">(281) 895-3560 </span>/<span style="color:#e30707">1(844) 895-3560 </span> </span>
        <div class="col-lg-4 col-md-4" style="float:right; margin-top:27px;"><a style="background-color:#0C8799; color:#fff;padding:10px 35px; border-radius:15px" href="<?php echo base_url();?>home/signup" <?php if($page=="media"){?> class="active" <?php }?>>Sign Up</a><a style="padding:10px ; border-radius:15px; color:#000" href="#">Not a member? Sign up
</a></div>

        
      </div>

    </div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="row headGrad" >
      <div class="col-md-10" id="grad">
      <div class="col-md-4 col-sm-3">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> 
          <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
           <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
           
           <!-- <p class="logo_text"><span class="name1">N. M. WADIA</span><span>Institute of Cardiology</span></p>  -->
            </div>
      </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="col-md-8 col-sm-9 nav-nav" >  
        <div class="collapse navbar-collapse nav-bottom" id="bs-example-navbar-collapse-1" >
          <ul class="nav navbar-nav custom-padding" >     
           <li class="dropdown"> <a href="<?php echo base_url();?>" class="<?php if($page_menu=="home"){?> active <?php }?>" role="button" aria-haspopup="true" aria-expanded="false">Home <!--<span class="caret"></span>--></a>
          <!--  <ul class="dropdown-menu">
                <li><a href="<?php echo base_url();?>home/chairman_message">Message from Chairman</a></li>
                <li><a href="<?php echo base_url();?>home/vision_mission">Vision mission &amp; Quality Statement</a></li>
                <li><a href="<?php echo base_url();?>home/value_ethics">Values &amp; ethics</a></li>
                <li><a href="<?php echo base_url();?>home/board_of_trustees">Board of trustees</a></li>
                <li><a href="<?php echo base_url();?>home/senior_leadership_team">Senior Leadership team</a></li>
                <li><a href="<?php echo base_url();?>home/hospital_history">Hospital history &amp; Milestones </a></li>
                <li><a href="<?php echo base_url();?>home/donation">Donation</a></li>
              </ul> -->
            </li> <li> <a href="<?php echo base_url();?>home/services" <?php if($page=="services"){?> class="active" <?php }?>>Membership</a></li>
            <li><a href="<?php echo base_url();?>home/doctors" <?php if($page=="doctors"){?> class="active" <?php }?>>About </a></li>
            <li><a href="<?php echo base_url();?>home/patient_visitors" <?php if($page=="patient_visitors"){?> class="active" <?php }?>>Pricing</a></li>
            <!-- <li><a href="<?php echo base_url();?>home/signup" <?php if($page=="media"){?> class="active" <?php }?>>Signup</a></li> -->
            <li><a href="<?php echo base_url();?>home/contact_us" <?php if($page=="contact_us"){?> class="active" <?php }?>>Contact Us</a></li>
          </ul>
        </div>
      </div>
        <!-- /.navbar-collapse --> 
      </div>
       <div class="col-md-2 col-sm-4">
        <div class="navbar-header">
      
          <!-- <a class="navbar-brand-right" href="index.html"><img src="images/logo.jpg" alt="logo" height="80" width="80"></a>-->
           <!-- <p class="logo_text_right"><span class="name1">N. M. WADIA</span><span>Institute of Cardiology</span></p> --> 
            </div>
            </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</nav>


