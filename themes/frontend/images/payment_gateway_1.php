<link rel="stylesheet" href="<?php echo base_url();?>themes/frontend/ext_css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>themes/frontend/ext_css/toastr.min.css">
<style>
    .line-separator {
        height: .2px;
        background: #717171;
        border-bottom: 1px solid #d3d3d3;
    }
    body{
        /*border:1px dashed gray;*/
        /*background-color: white;*/
        background:url("<?php echo base_url(); ?>themes/frontend/images/blue_abstract_background_310971.jpg");
        background-size: cover;    
    }
    .bold{
        font-size:19px;
        color: crimson;
    }
    .big{
        height: 17px;
        width: 17px;
    }
    .blink_me {
        color: white;
        font-weight: bold;
      }
</style>
<input type="hidden" id="success" value="<?php
if (isset($success)) {
    echo $success;
}
?>">
<input type="hidden" id="error" value="<?php
if (isset($error)) {
    echo $error;
}
?>">
<input type="hidden" id="error1" value="<?php
if ($this->session->flashdata('error')) {
    echo $this->session->flashdata('error');
}
?>">
<!-- Payeezy JS and jquery reference Java-script files   -->
<br></br>
<form method="post" action="<?php echo base_url(); ?>home/payment_confirm/<?php if (isset($details[0]->unique_id)) echo $details[0]->unique_id; ?>/<?php if(isset($appt) && $appt==1) echo $appt;?>">     
    <div id="one-time-pay">            
        <div class="container">
<!--             <div class="row text-center"><h3 style='color: brown;'><?php if (isset($details)) echo $details[0]->name . ' - Pay $' . $details[0]->amount . '/Year'; ?></h3></div>-->
            <div class="row text-center"><h3>Enter Payment Information</h3></div>
            <br></br>
            <div class="row text-center"> 
                <input type="hidden" name="membership_pay" value="<?php if(isset($membership_pay) && !empty($membership_pay)) echo $membership_pay; else echo 0; ?>">
                <div class = "col-lg-6">
                    <div class="row"> 
                        <div class = "col-lg-6 text-left">
                            <label>Card Type :</label>
                        </div>
                        <div class="col-lg-6 text-right"> 
                            <select payeezy-data="card_type" class="form-control" name="card_type">
                                <option value="visa">Visa</option>
                                <option value="mastercard">Master Card</option>
                                <option value="American Express">American Express</option>
                                <option value="discover">Discover</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>Name on card :</label>
                        </div>
                        <div class="col-lg-6 text-right">               
                            <input required style="text-transform: capitalize;" id="alphaonly" class="form-control " title="please enter the valid name" type="text" name="name" placeholder="Card Holder Name" payeezy-data="cardholder_name">
                        </div>
                    </div>
                    <div class="row"> 
                        <div class = "col-lg-6 text-left">
                            <label>Card Number :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
<!--                            <input id="account2" class="form-control" type="hidden" name="cc_number">                                                       -->
                            <input id="account" name="cc_number" class="form-control numeric" type="text">
                        </div>                        
                    </div>   
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>CVV Number :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
                            <input required class="form-control numeric" maxlength="3" type="text" name="cvv_code" placeholder="3 digit number" payeezy-data="cvv_code">                            
                        </div>                        
                    </div> 
                    <div class="row"> 
                        <div class = "col-lg-6 text-left">
                            <label>Expiration Date :</label>
                        </div>
                        <div class="col-lg-3 text-right"> 
                            <select class="form-control" payeezy-data="exp_month" name="exp_month">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12" selected>12</option>
                            </select> 
                        </div>
                        <div class="col-lg-3 text-right"> 
                            <select class="form-control" payeezy-data="exp_year" name="exp_year">
                                <?php for($i=17;$i<41;$i++){ 
                                echo "<option value='".$i."'>20".$i."</option>";
                                }?>
                            </select>
                        </div>                          
                    </div>
                </div>
                <div class = "col-lg-6">                    
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>Billing Address :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
                            <input required class="form-control" type="text" name="address" placeholder="Address" payeezy-data="cvv_code">
                        </div>                        
                    </div>
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>City :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
                            <input required class="form-control" type="text" name="city" placeholder="City" payeezy-data="cvv_code">
                        </div>                        
                    </div>
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>State :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
                            <input required class="form-control" type="text" name="state" placeholder="State" payeezy-data="cvv_code">
                        </div>                        
                    </div>
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>Zip Code :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
                            <input required class="form-control" type="text" name="zipcode" placeholder="Zip Code" payeezy-data="cvv_code">
                        </div>                        
                    </div>
                    <div class="row">
                        <div class = "col-lg-6 text-left">
                            <label>Email Address :</label>
                        </div>
                        <div class="col-lg-6 text-right">              
                            <input required class="form-control" type="text" name="email" placeholder="Email Address" payeezy-data="cvv_code">
                        </div>                        
                    </div>                    
                </div>
            </div>  
            <br>
            <?php if(isset($appt) && $appt=='1' && $details[0]->type == 1) {?>
            <div class="row">
                <div class = "col-lg-12 text-center">
                    <div class="row bold">
                        <div class = "col-lg-1"></div>
                        <div class = "col-lg-10 text-center">
                            <input class="" id="type1" required type="radio" name="package" value="1" <?php if (isset($details[0]->type) && $details[0]->type == 1) echo "checked"; ?>>   Monthly Members Appointment Fee $40.00
                        </div>
                    </div>
                </div>
            </div>
            <?php } else if(isset($appt) && $appt=='1' && $details[0]->type == 2) {?>
            <div class="row">
                <div class = "col-lg-12 text-center">
                    <div class="row bold">
                        <div class = "col-lg-1"></div>
                        <div class = "col-lg-10 text-center">
                            <input class="" id="type1" required type="radio" name="package" value="2" <?php if (isset($details[0]->type) && $details[0]->type == 2) echo "checked"; ?>>   Annual Members Appointment Fee $40.00
                        </div>
                    </div>
                </div>
            </div>
            <?php } else if(isset($membership_pacakge) && $membership_pacakge==1) {?>
            <div class="row">
                <div class = "col-lg-12 text-center">
                    <div class="row bold">
                        <div class = "col-lg-1"></div>
                        <div class = "col-lg-10 text-center">
                            <input class="" id="type1" required type="radio" name="package1" value="1" <?php if (isset($membership_pacakge) && $membership_pacakge == 1) echo "checked"; ?>>   Monthly Members plan Fee $10.00
                            <input class="" id="type1" required type="radio" name="package1" value="2" <?php if (isset($membership_pacakge) && $membership_pacakge == 2) echo "checked"; ?>>   Annual Members Appointment plan $100.00
                        </div>
                    </div>
                </div>
            </div>
            <?php } else{?>
             <?php if(isset($chk_type) && $chk_type !=2) {?>
                <div class="col-lg-12 text-center"> 
                    <div class="row bold">
                        <div class = "col-lg-1"></div>
                        <div class = "col-lg-10 text-left">
                            <input class="big" id="type0" style="padding:0" required type="radio" name="package"  value="0" <?php if (isset($details[0]->type) && $details[0]->type == 0) echo "checked"; ?>>  Basic Membership plan $75.00    
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class = "col-lg-12 text-center">
                    <div class="row bold">
                        <div class = "col-lg-1"></div>
                        <div class = "col-lg-10 text-left">
                            <input class="big" id="type1" required type="radio" name="package" value="1" <?php if (isset($details[0]->type) && $details[0]->type == 1) echo "checked"; ?>>   Monthly Membership:    $10.00 (Month Fee) $40.00 (Per Appointment) 
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center"> 
                    <div class="row bold">
                        <div class = "col-lg-1"></div>
                        <div class = "col-lg-10 text-left">
                            <input class="big" id="type0" required type="radio" name="package"  value="2" <?php if (isset($details[0]->type) && $details[0]->type == 2) echo "checked"; ?>>  Annual Membership:       $100.00 (Annual) $40.00 (Per Appointment)     
                        </div>
                    </div>
                </div>                        
            </div>
            <?php }?>
            <br><br>
            <div class="row">
                <div class = "col-lg-6 col-xs-6 text-right">
                    <a class="btn btn-default" href='<?php echo base_url() ?>home/dashboard'>Cancel</a>
                </div>
                <div class = "col-lg-6 col-xs-6 text-left">
                    <button id="generate-token" class="btn btn-primary" type="submit" name="submit">Pay Now</button>
                </div>
            </div>                      
            <br></br>
        </div>
    </div>
    <br>
</form>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cancel" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Membership Change</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to change the membership ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary cancel"  data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="confirm_clicked" value="0">
<script src="<?php echo base_url(); ?>asset/admin/js/plugins/toastr-master/toastr.js"></script>
<script>
$(function () {
    var string = "";
    //$( "#datepicker" ).datepicker({dateformat:'mm-dd-yy'});
    if ($("#success").val().length > 0)
    {
        toastr["success"]($("#success").val());
    }
    if ($("#error").val().length > 0)
    {
        toastr["error"]($("#error").val());
    }
    if ($("#error1").val().length > 0)
    {
        toastr["error"]($("#error1").val());
    }
    jQuery('.numeric').keyup(function () { 
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });
    document.getElementById("alphaonly").onkeypress=function(e){ 
        var e=window.event || e 
        var keyunicode=e.charCode || e.keyCode 
        return (keyunicode>=65 && keyunicode<=122 || keyunicode==8 || keyunicode==32)? true : false 
        } 
     /*$('#account').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
   $('#account').on('keyup', function (e) {
        var KeyID = event.keyCode;
        switch (KeyID)
        {
            case 8:
                $('#account').val("");
                string = "";
                break;
            case 46:
                $('#account').val("");
                string = "";
                break;
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
                var account2 = document.getElementById('account2');
                var account = document.getElementById('account');
                string += account.value.substr(account.value.length - 1);
                account2.value = string;
                var str = account2.value;
                var n = str.replace(/.(?=.{4})/g, 'X');
                account.value = n;
                break;
            default:
                $('#account').val("");
                string = "";
                break;
        }
    });
    $("#account").click(function (evt) {
        var searchInput = $('#account');
        var strLength = searchInput.val().length * 2;
        searchInput.focus();
        searchInput[0].setSelectionRange(strLength, strLength);
    });*/
    $('.big').on("click", function () {
        $('#myModal').modal('show');
    });
    $('#myModal').on('hidden.bs.modal', function () {
        if ($('#confirm_clicked').val() != "1")
        {
            if ($('input[name=package]:checked').val() == 1)
            {
                $("#type0").prop("checked", true);
            } else
            {
                $("#type1").prop("checked", true);
            }
        }
        $('#confirm_clicked').val(0);
    });
    $('#confirm').on("click", function () {
        $('#confirm_clicked').val(1);
    });
});
(function blink() { 
  $('.blink_me').fadeOut(2000).fadeIn(1000, blink); 
})();
</script>


