<?php
/**
 * Month View Template
 * The wrapper template for month view.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$show_top_bar = givingwalk_opt_events_top_bar_view();
 
do_action( 'tribe_events_before_template' );

if( in_array('month', $show_top_bar))
	tribe_get_template_part( 'modules/bar' );

tribe_get_template_part( 'month/content' );

do_action( 'tribe_events_after_template' );