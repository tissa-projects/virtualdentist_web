<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onlinecourses extends CI_Controller {
	public function __construct()
    {
	  	parent::__construct();
	  	// $this->load->model('home_model');
	  	$this->load->model('onlinecourses_model');
	  	$this->load->helper('url');
        $this->load->library('session');
	}
	public function checkurl() 
    {
       $data['url'] = base_url()."Onlinecourses_Api/";
       $this->session->set_userdata($data);
    }

    public function termsofuse() {
		$this->load->view('onlinecourses/header');
    	$this->load->view('onlinecourses/myaccount/termsofuse');
		$this->load->view('onlinecourses/footer');
    }

    public function privacypolicy() {
		$this->load->view('onlinecourses/header');
    	$this->load->view('onlinecourses/myaccount/privacypolicy');
		$this->load->view('onlinecourses/footer');
    }

/* Web panel Functions */
	public function index()
	{
		$this->checkurl();
		$api = $this->session->userdata('url');
		if(isset($_SESSION['Local_SESS']) && $_SESSION['Local_SESS'] != "" && isset($_SESSION['VIR_SESSION']) && $_SESSION['VIR_SESSION']['user_id'] != 0) {
			$data = array("cartdata"=>$_SESSION['Local_SESS'], "cart_id"=>$_SESSION['VIR_SESSION']['cart_id']);
			$method = 'POST';
	        $url = $api.'addbeforelogincartitem';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($data));
	        $getdata = json_decode($details);
			if($getdata->success == 1) {
				unset($_SESSION['Local_SESS']);
			}
		}

		$method = 'GET';
        $url = $api.'courselist';
        $header = array(
	       'Content-Type: application/json'
        );
        $details = $this->onlinecourses_model->CallAPI($method, $url, $header);
        $data['courselist'] = json_decode($details);
		$this->load->view('onlinecourses/header');
		$this->load->view('onlinecourses/index',$data);
		$this->load->view('onlinecourses/footer');
	}

	public function coursedetail($course_id)
	{
		$this->checkurl();
		$api = $this->session->userdata('url');
		$course_id = base64_decode($course_id);
		$method = 'POST';
        $url = $api.'getcourse';
        $header = array(
	       'Content-Type: application/json'
        );
        $apidata = array('course_id'=>$course_id);
        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
        $data['getcourse'] = json_decode($details);
		$this->load->view('onlinecourses/header');
		$this->load->view('onlinecourses/coursedetail',$data);
	}

	public function checkout()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'getcartlist';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $cart_id = $_SESSION['VIR_SESSION']['cart_id'];
	        $apidata = array("cart_id" => $cart_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['cartitemlist'] = json_decode($details);

	        $method = 'POST';
	        $url = $api.'getbillingdata';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $user_id = $_SESSION['VIR_SESSION']['user_id'];
	        $apidata = array("user_id" => $user_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['billdata'] = json_decode($details);
	        // $data['txid'] = base64_decode($txid);
	        // $data['status'] = base64_decode($status);
			$this->load->view('onlinecourses/header');
			$this->load->view('onlinecourses/checkout',$data);
			$this->load->view('onlinecourses/footer', $data);
		} else {
			redirect('onlinecourses');
		}
	}

	public function login()
	{
		$this->load->view('onlinecourses/header');
		$this->load->view('onlinecourses/login');
	}

	public function register()
	{
		$this->load->view('onlinecourses/header');
		$this->load->view('onlinecourses/register');
	}

	public function dashboard()
	{
		$totalsale = $this->onlinecourses_model->getallsale();
		$data['totalsale'] = 0;
		if(isset($totalsale['success']) && $totalsale['success'] == 1) 
			$data['totalsale'] = count($totalsale['data']);

		$newsignups = $this->onlinecourses_model->newsignups();
		$data['newsignups'] = 0;
		if(isset($newsignups['success']) && $newsignups['success'] == 1) 
			$data['newsignups'] = $newsignups['data']->totalusers;
			
		$this->load->view('onlinecourses/dashboard',$data);
	}

	public function sales()
	{
		$data['totalsale'] = $this->onlinecourses_model->getallsale();
		$this->load->view('onlinecourses/myaccount/sales',$data);
	}

	public function setcartlist() {
		if(isset($_SESSION['Local_SESS']) && $_SESSION['Local_SESS'] != array()) {
			$i = 0;
			$arr = $_SESSION['Local_SESS'];
			$list = [];
			foreach ($arr as $row) {
				array_push($list, $row['ci']);
			}
			if(!in_array($_POST['ci'], $list)) {
				array_push($arr, $_POST);
				$sessArr['Local_SESS'] = $arr;
				$this->session->set_userdata($sessArr);
			}
		} else {
			$sessArr['Local_SESS'] = [];
			array_push($sessArr['Local_SESS'], $_POST);
			$this->session->set_userdata($sessArr);
		}
		if(isset($_SESSION['Local_SESS']) && $_SESSION['Local_SESS'] != array()) {
		  	$cart_list = $_SESSION['Local_SESS'];
		  	$cart_list_html = "";
		  	$sum = 0;
            if($cart_list) {
            	$cart_list_html .= '<table class="table table-image">
		            <thead>
		              <tr>
		                <th scope="col"></th>
		                <th scope="col">Product</th>
		                <th scope="col">Price</th>
		                <th scope="col">Actions</th>
		              </tr>
		            </thead>
		            <tbody>';
	            	foreach($cart_list as $row) {
		            	if($row['price'] == 0) { $price = "FREE"; } else { $price = "₹".$row['price']; }
		            	$cart_list_html .= '<tr><td class="w-25"><img src="'.base_url().'uploads/course_cover/'.$row['course_cover'].'" alt="course cover" style="height: 84px;width: 10rem;max-width: none!important;"></td><td>
		                	'.$row['title'].' ';
		                	if($row['course_access']) {
			                	$cart_list_html .= '<div style="color: #0006;">Course Access | Till '.$row['course_access'].' </div>';
		                	}
		                $cart_list_html .= '</td>
		                <td>'.$price.'</td>
		                <td>
		                  <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="return deletecartitem('.$row['ci'].');">
		                    <i class="fa fa-times"></i>
		                  </a>
		                </td>
		              </tr>';
		              	$sum += $row['price'];
	            	}
            	$cart_list_html .= '</tbody>
		          </table> 
		          <div class="d-flex justify-content-end">
		            <h5>Total Amount: <span class="price text-success">₹'.$sum.'</span></h5>
		          </div>
		        </div>
		        <div class="modal-footer border-top-0 d-flex justify-content-between">
		          <button class="btn btn-secondary" data-dismiss="modal">Add More</button>
		          <button type="button" class="btn btn-success" onclick="return checkoutfunction();">Checkout</button>
		        </div>';
            }
            $response['success'] = 1;
            $response['cart_list_html'] = $cart_list_html;
            // $response['sum'] = $sum;
            $response['cart_count'] = count($cart_list);
		} else {
			$response['success'] = 0;
		}
		echo json_encode($response);
	}

	public function deletecartitem() {
		if(isset($_SESSION['Local_SESS']) && $_SESSION['Local_SESS'] != array()) {
			foreach ($_SESSION['Local_SESS'] as $key => $val) {
				if($_POST['cartitem'] == $val['ci']) {
					// print_r($arr[$i]);
					unset($_SESSION["Local_SESS"][$key]);
				}
			}
		}
		if(isset($_SESSION['Local_SESS']) && $_SESSION['Local_SESS'] != array()) {
		  	$cart_list = $_SESSION['Local_SESS'];
		  	$cart_list_html = "";
		  	$sum = 0;
            if($cart_list) {
            	$cart_list_html .= '<table class="table table-image">
		            <thead>
		              <tr>
		                <th scope="col"></th>
		                <th scope="col">Product</th>
		                <th scope="col">Price</th>
		                <th scope="col">Actions</th>
		              </tr>
		            </thead>
		            <tbody>';
	            	foreach($cart_list as $row) {
		            		if(isset($row['price']) && $row['price'] != 0) { $price = "₹".$row['price']; } else { $price = "FREE"; }
		            		$cart_list_html .= '<tr><td class="w-25"><img src="'.base_url().'uploads/course_cover/'.$row['course_cover'].'" alt="course cover" style="height: 84px;width: 10rem;max-width: none!important;"></td><td>
		                	'.$row['title'].'';
		                	if($row['course_access']) {
		                		$cart_list_html .= '<div style="color: #0006;">Course Access | Till '.$row['course_access'].' </div>';
		                	}
		                $cart_list_html .= '</td>
		                <td>'.$price.'</td>
		                <td>
		                  <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="return deletecartitem('.$row['ci'].');">
		                    <i class="fa fa-times"></i>
		                  </a>
		                </td>
		              </tr>';
		              	$sum += $row['price'];
	            	}
	            	$cart_list_html .= '</tbody>
		          </table> 
		          <div class="d-flex justify-content-end">
		            <h5>Total Amount: <span class="price text-success">₹'.$sum.'</span></h5>
		          </div>
		        </div>
		        <div class="modal-footer border-top-0 d-flex justify-content-between">
		          <button class="btn btn-secondary" data-dismiss="modal">Add More</button>
		          <button type="button" class="btn btn-success" onclick="return checkoutfunction();">Checkout</button>
		        </div>';
            }
            $response['success'] = 1;
            $response['cart_list_html'] = $cart_list_html;
            $response['sum'] = $sum;
            $response['cart_count'] = count($cart_list);
		} else {
			$cart_list_html = '<div class="modal-body">
                    <p style="text-align: center;"><b>There are no items in your cart</b><br/><br/>

                    <button class="btn btn-secondary" data-dismiss="modal" style="text-align: center;">Add More</button></p>
                  </div>
                  <div class="modal-footer border-top-0 d-flex justify-content-between">
                  </div>';
		  	$sum = 0;
			$response['success'] = 1;
            $response['cart_list_html'] = $cart_list_html;
            $response['sum'] = $sum;
            $response['cart_count'] = 0;
		}
		echo json_encode($response);
	}

	public function addcartlist() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'addtocart';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $cart_id = $_SESSION['VIR_SESSION']['cart_id'];
	        $apidata = $_POST;
	        $apidata['cart_id'] = $cart_id;
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $adddata = json_decode($details);
			
			$cart_list = "";
			$cart_count = 0;
			if($adddata->success == 1) {
	  			$cart_list = $adddata->data;
	  			$_SESSION['VIR_SESSION']['cart_sum'] = $adddata->cart_sum;
			}

			$cart_list_html = "";
		  	$sum = 0;
	        if($cart_list) {
	        	$cart_count = count($cart_list);
	        	$cart_list_html .= '<table class="table table-image">
		            <thead>
		              <tr>
		                <th scope="col"></th>
		                <th scope="col">Product</th>
		                <th scope="col">Price</th>
		                <th scope="col">Actions</th>
		              </tr>
		            </thead>
		            <tbody>';
	            	foreach($cart_list as $row) {
		            	if($row->price == 0) { $price = "FREE"; } else { $price = "₹".$row->price; }
		            	$cart_list_html .= '<tr><td class="w-25"><img src="'.base_url().'uploads/course_cover/'.$row->course_cover.'" alt="course cover" style="height: 84px;width: 10rem;max-width: none!important;"></td><td>
		                	'.$row->title.'';
		                if($row->course_access) {
		                	$cart_list_html .= '<div style="color: #0006;">Course Access | Till '.$row->course_access.' </div>';
		                }
		                $cart_list_html .= '</td>
		                <td>'.$price.'</td>
		                <td>
		                  <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="return deletecartitem('.$row->ci.');">
		                    <i class="fa fa-times"></i>
		                  </a>
		                </td>
		              </tr>';
		              	$sum += $row->price;
	            	}
	        	$cart_list_html .= '</tbody>
		          </table> 
		          <div class="d-flex justify-content-end">
		            <h5>Total Amount: <span class="price text-success">₹'.$sum.'</span></h5>
		          </div>
		        </div>
		        <div class="modal-footer border-top-0 d-flex justify-content-between">
		          <button class="btn btn-secondary" data-dismiss="modal">Add More</button>
		          <button type="button" class="btn btn-success" onclick="return checkoutfunction();">Checkout</button>
		        </div>';
	        } else {
	        	$cart_list_html .= '<div class="modal-body">
	                    <p style="text-align: center;"><b>There are no items in your cart</b><br/><br/>

	                    <button class="btn btn-secondary" data-dismiss="modal" style="text-align: center;">Add More</button></p>
	                  </div>
	                  <div class="modal-footer border-top-0 d-flex justify-content-between">
	                  </div>';
	        }
	        $response['success'] = 1;
	        $response['cart_list_html'] = $cart_list_html;
	        $response['cart_count'] = $cart_count;
			echo json_encode($response);
		} else {
			redirect('onlinecourses');
		}
	}

	public function deletemaincartitem() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'deletemaincartitem';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $cart_id = $_SESSION['VIR_SESSION']['cart_id'];
	        $apidata = array("course_id"=>$_POST['cartitem'], "cart_id"=>$cart_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $editdata = json_decode($details);
			// $deletedata = $this->onlinecourses_model->deletemaincartitem($_POST['cartitem']);
			// $getdata = $this->onlinecourses_model->getcartlist();
			$cart_count = 0;
			$cart_list = "";
			if($editdata->success == 1) {
	  			$cart_list = $editdata->data;
	  			$_SESSION['VIR_SESSION']['cart_sum'] = $editdata->cart_sum;
			}
			$cart_list_html = "";
			$cart_list_checkout_html = "";
		  	$sum = 0;
	        if($cart_list) {
	        	$cart_count = count($cart_list);
	        	$cart_list_html .= '<table class="table table-image">
		            <thead>
		              <tr>
		                <th scope="col"></th>
		                <th scope="col">Product</th>
		                <th scope="col">Price</th>
		                <th scope="col">Actions</th>
		              </tr>
		            </thead>
		            <tbody>';
		            $cart_list_checkout_html .= '<table class="table table-image">
	                                  <thead>
	                                    <tr>
	                                      <th scope="col"></th>
	                                      <th scope="col">Product</th>
	                                      <th scope="col">Price</th>
	                                      <th scope="col">Actions</th>
	                                    </tr>
	                                  </thead>
	                                  <tbody>';
	            	foreach($cart_list as $row) {
		            	if(isset($row->price) && $row->price != 0) { $price = "₹".$row->price; } else { $price = "FREE"; }
		            	$cart_list_html .= '<tr><td class="w-25"><img src="'.base_url().'uploads/course_cover/'.$row->course_cover.'" alt="course cover" style="height: 84px;width: 10rem;max-width: none!important;"></td><td>
		                	'.$row->title.'';
		                	if($row->course_access) {
		                	$cart_list_html .= '<div style="color: #0006;">Course Access | Till '.$row->course_access.' </div>';
		                	}
		                $cart_list_html .= '</td>
		                <td>'.$price.'</td>
		                <td>
		                  <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="return deletecartitem('.$row->ci.');">
		                    <i class="fa fa-times"></i>
		                  </a>
		                </td>
		              </tr>';
		              $cart_list_checkout_html .= '<tr>
	                                      <td class="w-25">
	                                        <img src="'.base_url().'uploads/course_cover/'.$row->course_cover.'" alt="course cover" style="height: 84px;width: 10rem;max-width: none!important;">
	                                      </td>
	                                      <td>
	                                        '.$row->title.'';
	                                    if($row->course_access) {
	                                       $cart_list_checkout_html .=  '<div style="color: #0006;">Course Access | Till '.$row->course_access.' </div>';
	                                   	}
	                                      $cart_list_checkout_html .= '</td>
	                                      <td>'.$price.'</td>
	                                      <td>
	                                        <a href="#" class="btn btn-danger btn-sm" onclick="return deletecartitem('.$row->ci.',"checkout");">
	                                          <i class="fa fa-times"></i>
	                                        </a>
	                                      </td>
	                                    </tr>';
		              	$sum += $row->price;
	            	}
	        	$cart_list_html .= '</tbody>
		          </table> 
		          <div class="d-flex justify-content-end">
		            <h5>Total Amount: <span class="price text-success">₹'.$sum.'</span></h5>
		          </div>
		        </div>
		        <div class="modal-footer border-top-0 d-flex justify-content-between">
		          <button class="btn btn-secondary" data-dismiss="modal">Add More</button>
		          <button type="button" class="btn btn-success" onclick="return checkoutfunction();">Checkout</button>
		        </div>';
		        $cart_list_checkout_html .= '</tbody>
	                                </table>';
	        } else {
	        	$cart_list_html .= '<div class="modal-body">
	                    <p style="text-align: center;"><b>There are no items in your cart</b><br/><br/>

	                    <button class="btn btn-secondary" data-dismiss="modal" style="text-align: center;">Add More</button></p>
	                  </div>
	                  <div class="modal-footer border-top-0 d-flex justify-content-between">
	                  </div>';
	        }
	        $response['success'] = 1;
	        $response['cart_list_html'] = $cart_list_html;
	        $response['cart_count'] = $cart_count;
	        $response['cart_list_checkout_html'] = $cart_list_checkout_html;
	        $response['sum'] = $sum;
			echo json_encode($response);
		} else {
			redirect('onlinecourses');
		}
	}

	public function addbillingdetails() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'addbillingdetails';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $apidata = $_POST;
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $adddata = json_decode($details);
	        if($adddata->success == 1) {
	        	echo 1;
	        } else {
	        	echo 0;
	        }
	    } else {
	    	redirect("onlinecourses");
	    }
	}

	public function forgotpassword() {
		$this->load->view('onlinecourses/header');
		$this->load->view('onlinecourses/forgotpassword');
	}

	public function forgot($email) {
		$data['email'] = base64_decode($email);
		$this->load->view('onlinecourses/header');
		$this->load->view('onlinecourses/forgot',$data);
	}

	public function verify($email) {
		$email = base64_decode($email);
		$this->checkurl();
		$api = $this->session->userdata('url');
		$method = 'POST';
        $url = $api.'check_email';
        $header = array(
	       'Content-Type: application/json'
        );
		$apidata = array('email' => $email,'user_role' => 'Learner');
        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
        $isemail = json_decode($details);
        $data['is_verify'] = 0;
        if($isemail->success == 1) {
        	$method = 'POST';
	        $url = $api.'edituser_isverify';
	        $header = array(
		       'Content-Type: application/json'
	        );
			$apidata = array('email' => $email);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $verifyemail = json_decode($details);
	        if($verifyemail->success == 1) {
        		$data['is_verify'] = 1;
        	}
        }
		$this->load->view('onlinecourses/verify',$data);
	}

	public function pay_now($amount, $email, $name, $mobile, $cart_id) {
		if($amount != '' && $email != '' && $name != '' && $mobile != '' && $cart_id != '') {
			$amount = base64_decode($amount); 
			$customer_email = base64_decode($email); 
			$customer_name = base64_decode($name); 
			$customer_mobile = base64_decode($mobile);
			$product_info = base64_decode($cart_id);
			// $product_info = 'Online Courses';
			
			//payumoney details

			$MERCHANT_KEY = "rLtGWaHK"; //change  merchant with yours
	        $SALT = "SiKMPQ8ykc";  //change salt with yours 

	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
	        $hash = strtolower(hash('sha512', $hashstring));
	        $success = base_url() . 'OnlineCoursesStatus';  
	        $fail = base_url() . 'OnlineCoursesStatus';
	        $cancel = base_url() . 'OnlineCoursesStatus';
	        
	        $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'productinfo' => $product_info,
	            'mailid' => $customer_email,
	            'phoneno' => $customer_mobile,
	            'address' => '',
	            'action' => "https://secure.payu.in/_payment", //for live change action  https://secure.payu.in
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel,          
	            'status' => ''         
	        );
	        $this->load->view('onlinecourses/payment/confirmation', $data);
	    } else {
	    	redirect("onlinecourses/checkout");
	    }
	}

	public function payment($amount, $email, $name, $mobile, $cart_id) {
		if($amount != '' && $email != '' && $name != '' && $mobile != '' && $cart_id != '') {
			$amount = base64_decode($amount); 
			$customer_email = base64_decode($email); 
			$customer_name = base64_decode($name); 
			$customer_mobile = base64_decode($mobile);
			$product_info = base64_decode($cart_id);
			// $product_info = 'Online Courses';
			
			//payumoney details

			$MERCHANT_KEY = "rLtGWaHK"; //change  merchant with yours
	        $SALT = "SiKMPQ8ykc";  //change salt with yours 

	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
	        $hash = strtolower(hash('sha512', $hashstring));
	        $success = base_url() . 'OnlineCoursesStatus';  
	        $fail = base_url() . 'OnlineCoursesStatus';
	        $cancel = base_url() . 'OnlineCoursesStatus';
	        
	        $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'productinfo' => $product_info,
	            'mailid' => $customer_email,
	            'phoneno' => $customer_mobile,
	            'address' => '',
	            'action' => base_url()."OnlineCoursesStatus", 
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel,            
	            'status' => 'success'            
	        );
	        $this->load->view('onlinecourses/payment/confirmation', $data);
	    } else {
	    	redirect("onlinecourses/checkout");
	    }
	}
/* /. Web panel Functions */

/* Admin panel Functions */
	public function courses()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data['courselist'] = $this->onlinecourses_model->admincourselist();
			$this->load->view('onlinecourses/courses/list',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addcourses()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data['languages'] = $this->onlinecourses_model->languagelist();
			$data['instructors'] = $this->onlinecourses_model->instructorlist();
			$course_sequence = $this->onlinecourses_model->get_course_sequence();
			$data['course_sequence'] = 0;
			if($course_sequence['success'] == 1) {
				$data['course_sequence'] = $course_sequence['seq'];
			}
			$this->load->view('onlinecourses/courses/add',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function check_course_sequence() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$sequence = $this->input->post('sequence');
			$course_id = $this->input->post('course_id');
			$data = array('sequence'=>$sequence,'course_id'=>$course_id);
			$sequencedata = $this->onlinecourses_model->check_course_sequence($data);
			if($sequencedata['success'] == 0) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addcourseaction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data = $_POST;
			$img = file_get_contents($_FILES['course_cover']['tmp_name']);
			  
			// Encode the image string data into base64
			$data['course_cover'] = base64_encode($img);
			$getdata = $this->onlinecourses_model->addcourse($data);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/courses');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function ratingaction()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data = $_POST;
			$getdata = $this->onlinecourses_model->ratingaction($data);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/courses');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editcourse($course_id)
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$course_id = base64_decode($course_id);
			$data['languages'] = $this->onlinecourses_model->languagelist();
			$data['instructors'] = $this->onlinecourses_model->instructorlist();
			$course_sequence = $this->onlinecourses_model->get_course_sequence();
			$data['course_sequence'] = 0;
			if($course_sequence['success'] == 1) {
				$data['course_sequence'] = $course_sequence['seq'];
			}
			$data['getcourse'] = $this->onlinecourses_model->admingetcourse($course_id);
			$data['course_plan'] = $this->onlinecourses_model->getcourseplans($course_id);
			$data['coursecontentlist'] = $this->onlinecourses_model->coursecontentlist($course_id);
			$this->load->view('onlinecourses/courses/edit',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editcourseaction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data = $_POST;
			$getdata = $this->onlinecourses_model->editcourse($data);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/courses');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function deletecourse() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->deletecourse($_POST['delete_id']);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/courses');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function publishcourse() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->publishcourse($_POST['delete_id'],$_POST['status']);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/courses');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addcourseplan() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data = $_POST;
			$getdata = $this->onlinecourses_model->addcourseplan($data);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/editcourse/'.base64_encode($_POST['course_id']).'/pricing');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editcourseplan() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data = $_POST;
			$getdata = $this->onlinecourses_model->editcourseplan($data);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/editcourse/'.base64_encode($_POST['course_id']).'/pricing');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function deletecourseplan($course_id) {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->deletecourseplan($_POST['delete_id']);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/editcourse/'.base64_encode($course_id).'/pricing');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function coursecontent($course_id,$coursecontentid=0,$subcontentid='')
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$course_id = base64_decode($course_id);
			$coursecontentid = base64_decode($coursecontentid);
			$subcontentid = base64_decode($subcontentid);
			$data['getcourse'] = $this->onlinecourses_model->admingetcourse($course_id);
			$data['coursecontentid'] = $coursecontentid;
			$data['subcontentid'] = $subcontentid;
			$data['course_id'] = $course_id;
			if($coursecontentid == 0 && $subcontentid != '') {
				$data['getcoursecontent'] = $this->onlinecourses_model->getcoursesubcontent($subcontentid);
			} else {
				$data['getcoursecontent'] = $this->onlinecourses_model->getcoursecontent($coursecontentid);
			}
			$data['coursecontentlist'] = $this->onlinecourses_model->coursecontentlist($course_id);
			// print_r($data['coursecontentlist']);exit;
			$this->load->view('onlinecourses/courses/coursecontent',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function previewcoursecontent($course_id)
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$course_id = base64_decode($course_id);
			$data['getcourse'] = $this->onlinecourses_model->admingetcourse($course_id);
			$data['course_id'] = $course_id;
			$data['coursecontentlist'] = $this->onlinecourses_model->coursecontentlist($course_id);
			$this->load->view('onlinecourses/courses/previewcoursecontent',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function profile() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'getuserdata';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $user_id = $_SESSION['VIR_SESSION']['user_id'];
	        $apidata = array("user_id" => $user_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['userdata'] = json_decode($details);

			$method = 'GET';
	        $url = $api.'countrylist';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header);
	        $data['countrylist'] = json_decode($details);

			$method = 'GET';
	        $url = $api.'statelist';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header);
	        $data['statelist'] = json_decode($details);

			$method = 'GET';
	        $url = $api.'languagelist';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header);
	        $data['languagelist'] = json_decode($details);
			$this->load->view('onlinecourses/myaccount/profile',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function profileaction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'updateuserdata';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $data = $_POST;
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($data));
	        $getdata = json_decode($details);
			if($getdata->success == 1) {
				$this->session->set_flashdata('success', $getdata->message);
			} else {
				$this->session->set_flashdata('error', $getdata->message);
			}
			redirect('onlinecourses/profile');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function checkcurrentpassword() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'checkcurrentpassword';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $data = $_POST;
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($data));
	        $getdata = json_decode($details);
			if($getdata->success == 1) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function changepassword($page='') {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'changepassword';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $data = $_POST;
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($data));
	        $getdata = json_decode($details);
			if($getdata->success == 1) {
				$this->session->set_flashdata('success', $getdata->message);
			} else {
				$this->session->set_flashdata('error', $getdata->message);
			}
			if(!$page){
				redirect('onlinecourses/profile');
			}
			else{
				redirect('onlinecourses/'.$page);
			}
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addchapterfile() {
		if(isset($_SESSION['VIR_SESSION']))
		{
	        $getdata = $this->onlinecourses_model->addchapterfile($_POST,$_FILES);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/coursecontent/'.base64_encode($_POST['course_id']).'/'.base64_encode($getdata['data']));
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function updatecoursecontent() {
		if(isset($_SESSION['VIR_SESSION']))
		{
	        $getdata = $this->onlinecourses_model->updatecoursecontent($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/coursecontent/'.base64_encode($_POST['course_id']));
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addsubchapterfile() {
		if(isset($_SESSION['VIR_SESSION']))
		{
	        $getdata = $this->onlinecourses_model->addsubchapterfile($_POST,$_FILES);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/coursecontent/'.base64_encode($_POST['course_id']).'/'.base64_encode(0).'/'.base64_encode($getdata['data']));
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function updatecoursecontentassign() {
		if(isset($_SESSION['VIR_SESSION']))
		{
	        $getdata = $this->onlinecourses_model->updatecoursecontentassign($_POST,$_FILES);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/coursecontent/'.base64_encode($_POST['course_id']));
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function getchaptercontents() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$chapter_id = $_POST['chapter_id'];
			$sub_chapter_id = $_POST['sub_chapter_id'];
			if($sub_chapter_id == '') {
				$getdata = $this->onlinecourses_model->getcoursecontent($chapter_id);
			} else {
				$getdata = $this->onlinecourses_model->getcoursesubcontent($sub_chapter_id);
			}
			echo json_encode($getdata);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function learners()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data['learnerlist'] = $this->onlinecourses_model->learnerlist();
			$this->load->view('onlinecourses/learner/list',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addlearner()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$this->load->view('onlinecourses/learner/add');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function check_email() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$email = $this->input->post('email');
			$user_role = $this->input->post('user_role');
			$user_id = $this->input->post('user_id');
			$data = array('email'=>$email,'user_role'=>$user_role,'user_id'=>$user_id);
			$checkdata = $this->onlinecourses_model->check_email($data);
			if($checkdata['success'] == 0) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function check_mobile() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$mobile = $this->input->post('mobile');
			$user_role = $this->input->post('user_role');
			$user_id = $this->input->post('user_id');
			$data = array('mobile'=>$mobile,'user_role'=>$user_role,'user_id'=>$user_id);
			$checkdata = $this->onlinecourses_model->check_mobile($data);
			if($checkdata['success'] == 0) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addlearneraction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->adduseraction($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/learners');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editlearner($user_id,$page='')
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$user_id = base64_decode($user_id);
			$adddata = array('user_id'=>$user_id);
			$data['getusers'] = $this->onlinecourses_model->getuserdata($adddata);
			$data['languages'] = $this->onlinecourses_model->languagelist();
			$data['states'] = $this->onlinecourses_model->statelist();
			$data['courselist'] = $this->onlinecourses_model->courselist();
			$arr = array('user_id' => $user_id);
			$data['enrollcourseslist'] = $this->onlinecourses_model->getenrollcourseslist($arr);
			$data['page'] = $page;
			
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'purchasehistorylist';
	        $header = array(
		       'Content-Type: application/json'
	        );
			$apidata = array('user_id' => $user_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['purchaselist'] = json_decode($details);
			$this->load->view('onlinecourses/learner/edit',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editlearneraction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->edituseraction($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/learners');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function instructors()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data['instructorlist'] = $this->onlinecourses_model->instructorlist();
			$this->load->view('onlinecourses/instructor/list',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addinstructor()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$this->load->view('onlinecourses/instructor/add');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function addinstructoraction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->adduseraction($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/instructors');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editinstructor($user_id)
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$user_id = base64_decode($user_id);
			$adddata = array('user_id'=>$user_id);
			$data['getusers'] = $this->onlinecourses_model->getuserdata($adddata);
			$data['languages'] = $this->onlinecourses_model->languagelist();
			$data['states'] = $this->onlinecourses_model->statelist();
			$this->load->view('onlinecourses/instructor/edit',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function editinstructoraction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->edituseraction($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/instructors');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function upload_course_cover() {
		if(isset($_SESSION['VIR_SESSION']))
		{
	        $getdata = $this->onlinecourses_model->updatecoursecover($_POST,$_FILES);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/editcourse/'.base64_encode($_POST['course_id']));
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function upload_profile() {
		if(isset($_SESSION['VIR_SESSION']))
		{
	        $getdata = $this->onlinecourses_model->upload_profile($_POST,$_FILES);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/profile');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function changestatus($page) {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->changestatus($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/'.$page);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function enrollcourseaction() {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->enrollcourseaction($_POST);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/editlearner/'.base64_encode($_POST['user_id']).'/enroll');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function deleteenrollcourse($user_id) {
		if(isset($_SESSION['VIR_SESSION']))
		{
			$getdata = $this->onlinecourses_model->deleteenrollcourse($_POST['delete_id']);
			if($getdata['success'] == 1) {
				$this->session->set_flashdata('success', $getdata['message']);
			} else {
				$this->session->set_flashdata('error', $getdata['message']);
			}
			redirect('onlinecourses/editlearner/'.base64_encode($user_id).'/enroll');
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function getassignmenthistory()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$url = $api.'getassignmenthistory';
			$method = 'POST';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $assigndata = array('course_id' => $_POST['course_id'],'user_id' => $_POST['user_id'],'enroll_id' => $_POST['enroll_id']);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($assigndata));
	        $getdata = json_decode($details);
			if($getdata->success == 1) {
				$response['success'] = 1;
				$msg = "";

				$msg .= "<table id='basic-datatable-new' class='table dt-responsive w-100 table-assignment'>
	                        <thead>
	                            <tr>
	                                <th>Sr. No.</th>
	                                <th>Date</th>
	                                <th>Message</th>
	                                <th>Status</th>
	                                <th>File/Notes</th>
	                            </tr>
	                        </thead>
	                        <tbody>";
	            $i = 1;
				foreach ($getdata->data as $row) {
					$status = '-';
					if($row->status == 1) {
						$status = '<span class="text text-success">REVIEWED</span>';
					} else if($row->status == 2) {
						$status = '<span class="text text-warning">AVAILABLE FOR REVIEW</span>';
					} else if($row->status == 3) {
						$status = '<span class="text text-danger">REJECTED</span>';
					}
					$filenote = "";
					if($row->file) { 
						$filenote .= '<a href="'.base_url().'uploads/assignment_upload/'.$row->file.'" target="_blank">'.$row->file.'</a>'; 
					} 
					if($row->notes) { 
						$filenote .= '<br/><button type="button" class="btn btn-info" onclick="return showNote(\''.$row->notes.'\');" data-notes="'.$row->notes.'">View Note</button>'; 
					} 
					$msg .= "<tr><td>".$i."</td>
	                    <td>".date("d/m/Y", strtotime($row->created_at))."<br/>".date("h:i A", strtotime($row->created_at))."</td>
	                    <td>".$row->message."</td>
	                    <td>".$status."</td>
	                    <td>".$filenote."</td></tr>";
	                $i++;
				}
				$msg .= "</tbody></table>";
				$response['message'] = $msg;
				$course = $this->onlinecourses_model->getcourse(array('course_id' => $_POST['course_id']));
				$response['title'] = $course['data']->title;
			} else {
				$response['success'] = 0;
				$response['message'] = "";
				$response['title'] = "";
			}
			echo json_encode($response);
		} else {
	    	redirect("onlinecourses");
	    }
	}
/* /. Admin Panel Functions */
/* 	Learner Panel Functions */
	public function mycourses()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'getenrollcourseslist';
	        $header = array(
		       'Content-Type: application/json'
	        );
			$apidata = array('user_id' => $_SESSION['VIR_SESSION']['user_id']);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['enrollcourseslist'] = json_decode($details);
			$this->load->view('onlinecourses/mycourses/list',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function purchasehistory()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'purchasehistorylist';
	        $header = array(
		       'Content-Type: application/json'
	        );
			$apidata = array('user_id' => $_SESSION['VIR_SESSION']['user_id']);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['purchaselist'] = json_decode($details);
			$this->load->view('onlinecourses/myaccount/purchasehistory',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function purchasecourselist($order_id, $transaction_id, $page='')
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$order_id = base64_decode($order_id);
			$transaction_id = base64_decode($transaction_id);
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'purchasecourselist';
	        $header = array(
		       'Content-Type: application/json'
	        );
			$apidata = array('order_id' => $order_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['orderlist'] = json_decode($details);
	        $data['order_id'] = $order_id;
	        $data['transaction_id'] = $transaction_id;
	        $data['page'] = $page;
			$this->load->view('onlinecourses/myaccount/purchasecourselist',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function uploadassignmentreply($page='')
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$data = $_POST;
			if($_FILES['assignment_upload']['tmp_name'] != "") {
				$img = file_get_contents($_FILES['assignment_upload']['tmp_name']);
				$data['assignment_upload'] = base64_encode($img);
			}
			  
			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'uploadassignmentreply';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($data));
	        $getdata = json_decode($details);
	        if($getdata->success == 1) {
				$this->session->set_flashdata('success', $getdata->message);
			} else {
				$this->session->set_flashdata('error', $getdata->message);
			}
			if($page) {
				redirect('onlinecourses/assignment/'.base64_encode($_POST['course_id']).'/'.$_POST['status']);
			} else {
				redirect('onlinecourses/mycourseview/'.base64_encode($_POST['enroll_id']).'/'.base64_encode($_POST['course_id']).'/'.base64_encode('assignment'));
			}
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function mycourseview($enroll_id,$course_id,$page='')
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$enroll_id = base64_decode($enroll_id);
			$course_id = base64_decode($course_id);
			$user_id = $_SESSION['VIR_SESSION']['user_id'];
			$data['page'] = base64_decode($page);

			$api = $this->session->userdata('url');
			$method = 'POST';
	        $url = $api.'getcoursebyid';
	        $header = array(
		       'Content-Type: application/json'
	        );
			$apidata = array('course_id' => $course_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['getcourse'] = json_decode($details);
			// $data['getcourse'] = $this->onlinecourses_model->admingetcourse($course_id);

			$url = $api.'coursecontentlist';
			$method = 'POST';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
	        $data['coursecontentlist'] = json_decode($details);

			$url = $api.'getenrolldatabyid';
			$method = 'POST';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $enrolldata = array('enroll_id' => $enroll_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($enrolldata));
	        $data['getenrolldatabyid'] = json_decode($details);

			$url = $api.'getassignmenthistory';
			$method = 'POST';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $assigndata = array('course_id' => $course_id,'user_id' => $user_id,'enroll_id' => $enroll_id);
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($assigndata));
	        $data['assignlist'] = json_decode($details);

			$data['course_id'] = $course_id;
			$data['enroll_id'] = $enroll_id;
			$this->load->view('onlinecourses/mycourses/mycourseview',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function assignments()
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$api = $this->session->userdata('url');
			$method = 'GET';
	        $url = $api.'assignmenthistorybytype';
	        $header = array(
		       'Content-Type: application/json'
	        );
	        $details = $this->onlinecourses_model->CallAPI($method, $url, $header);
	        $data['assignlist'] = json_decode($details);
	        
			$this->load->view('onlinecourses/assignment/list',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}

	public function assignment($course_id, $status)
	{
		if(isset($_SESSION['VIR_SESSION']))
		{
			$course_id = base64_decode($course_id);
			$data['status'] = $status;
			$data['course_id'] = $course_id;
			$data['getdata'] = $this->onlinecourses_model->getassignmentlist($course_id, $status);
			$data['course'] = $this->onlinecourses_model->getcourse(array('course_id' => $course_id));
			$this->load->view('onlinecourses/assignment/view',$data);
		} else {
	    	redirect("onlinecourses");
	    }
	}
/* /. Learner Panel Functions */

	public function logout()
	{
		$this->session->unset_userdata($_SESSION['VIR_SESSION']);
		$this->session->sess_destroy();
        redirect('onlinecourses');
	}
}
?>