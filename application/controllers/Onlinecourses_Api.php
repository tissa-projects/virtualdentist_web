<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class Onlinecourses_Api extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('onlinecourses_model');
		$this->load->library('pagination'); 
		$this->load->helper('form'); 
		$this->load->library('session');
		$this->load->library('email');
		$this->load->helper('cookie');   
		$this->load->library('encryption');
	}

	public function login() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->login($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function register() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->register($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function instructorlist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->instructorlist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function languagelist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->languagelist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function courselist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->courselist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function addcourse() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->addcourse($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function check_course_sequence() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->check_course_sequence($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function check_email() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->check_email($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function getcartlist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getcartlist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function addbeforelogincartitem() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->addbeforelogincartitem($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function getcourse() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getcourse($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function addtocart() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->addtocart($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function deletemaincartitem() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->deletemaincartitem($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function addbillingdetails() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->addbillingdetails($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function getbillingdata() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getbillingdata($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function getuserdata() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getuserdata($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function statelist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->statelist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function countrylist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->countrylist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function updateuserdata() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->updateuserdata($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function checkcurrentpassword() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->checkcurrentpassword($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function changepassword() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->changepassword($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function addorders() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->addorders($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function forgotpassword() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->forgotpassword($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function changeforgotpassword() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->changeforgotpassword($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function getcourseplans() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getcourseplans($obj['course_id']);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function getenrollcourseslist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getenrollcourseslist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function purchasehistorylist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->purchasehistorylist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function edituser_isverify() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->edituser_isverify($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function getcoursebyid() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->admingetcourse($obj['course_id']);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function coursecontentlist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->coursecontentlist($obj['course_id']);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function getenrolldatabyid() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getenrollcoursesbyid($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
	
	public function updateprogress() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->updateprogress($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function uploadassignmentreply() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->uploadassignmentreply($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function getassignmenthistory() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->getassignmenthistory($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function assignmenthistorybytype() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->assignmenthistorybytype($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function purchasecourselist() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->onlinecourses_model->purchasecourselist($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}
}
?>