<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	 {
	  	parent::__construct();
	  	$this->load->model('home_model');
	  	$this->load->helper('url');
	 }

	 public function Checklogin() 
    {
        if ($this->session->userdata('admin_id') == '') 
        {
            redirect('admin/');
        }
    }
	
	public function index()
	{
		$data['page'] = 'index';
 
		if($this->input->post('submit')){

			$admin = $this->input->post('username');
			$password = $this->input->post('password');
			
			
			if($admin == ''){
			   $this->session->set_flashdata('message', "Enter Username Please."); 
			    
			}else if($password == ''){
			   $this->session->set_flashdata('message', "Enter Your Password."); 
			    
			}else{

			$row = $this->db->query("SELECT * FROM `vd_admin` WHERE `username` = '$admin' AND `password` = '$password' ")->row();

			if($row)
			{
				$data['admin_id'] = $row->username;
				$this->session->set_userdata($data);
				 redirect('admin/doctorslist');

			}
			else
			{
			    $this->session->set_flashdata('message', "Invalid Credential. Please try again.");
				redirect('admin/');
			}
		}

		}
		$this->load->view('frontend/header');
		$this->load->view('admin/index',$data);
		$this->load->view('frontend/footer');
	}

	public function doctorslist()
	{
		 $this->Checklogin();
		$data['page'] = 'doctorslist';
		$this->load->view('frontend/header1');
		$this->load->view('admin/doctorslist',$data);
		$this->load->view('frontend/footer');
	}
	public function subscribedlist(){
		$this->Checklogin();
		$data['page'] = 'subscribedlist';
		$this->load->view('frontend/header1');
		$this->load->view('admin/subscribedlist',$data);
		$this->load->view('frontend/footer');

	}
	public function change_pass(){
		$this->Checklogin();
		$data['page'] = 'change_pass';
		$this->load->view('frontend/header1');
		$this->load->view('admin/change_pass',$data);
		$this->load->view('frontend/footer');

	}

	public function renew_notify(){
		$this->Checklogin();
		$data['page'] = 'renew_notify';
		$this->load->view('frontend/header1');
		$this->load->view('admin/renew_notify',$data);
		$this->load->view('frontend/footer');

	}
	public function logout() {

        $data = array('admin_id');

        $this->session->unset_userdata($data);

        redirect('admin/');
    }

	



}
