<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Statusdoc extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('home_model');     
    }

	public function index() {
       $status = $this->input->post('status');
      if (empty($status)) {
            redirect('Welcome');
        }
       
         $firstname = $this->input->post('firstname');
        $amount = $this->input->post('amount');
        $txnid = $this->input->post('txnid');
        $posted_hash = $this->input->post('hash');
        $key = $this->input->post('key');
        $productinfo = $this->input->post('productinfo');
        $email = $this->input->post('email');
        $salt = "SiKMPQ8ykc"; //  Your salt
        //$add = $this->input->post('additionalCharges');
        If (isset($add)) {
            $additionalCharges = $this->input->post('additionalCharges');
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {

            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
         $data['hash'] = hash("sha512", $retHashSeq);
          $data['amount'] = $amount;
          $data['txnid'] = $txnid;
          $data['posted_hash'] = $posted_hash;
          $data['status'] = $status;

          $date = date('Y-m-d');

      $date1 = strtotime($date);
      $yr = 1;
      if($amount == 5000) {
        $yr = 500;
      }
      $new_date1 = strtotime('+ '.$yr.' year', $date1);
        $new_date = date('Y-m-d', $new_date1);


           $data1 = array(
                    'pay_status' => $status,
                    'is_verified' =>'1',
                    'acc_status' => '1',
                    'starts_on' => $date,
                    'ends_on' => $new_date,
                    'pay_id' => $txnid
            );

             $this->db->where("reg_number",$productinfo); 
           $update = $this->db->update("vd_doctors",$data1);

          
          if($status == 'success'){
            $data['reg'] = $productinfo;
            $this->load->view('frontend/header');
          $this->load->view('doctor/success',$data);
          $this->load->view('frontend/footer');
         }
         else{
          $data['country'] = $this->home_model->fetch_country();
            $data['state'] = $this->home_model->get_state();

            $this->load->view('frontend/header',$data);
              $this->load->view('frontend/failure', $data); 
              $this->load->view('frontend/footer',$data);
         }
     
    }
 
    
   }
