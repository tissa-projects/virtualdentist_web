<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	 public function __construct()
	 {
	  	parent::__construct();
	  	$this->load->model('home_model');
	  	$this->load->helper('url');
	 }

	 public function fetch_state()
	 {
	  if($this->input->post('country_id'))
	  {
	   echo $this->home_model->fetch_state($this->input->post('country_id'));
	  }
	 }

	 public function fetch_city()
	 {
	  if($this->input->post('state_id'))
	  {
	   echo $this->home_model->fetch_city($this->input->post('state_id'));
	  }
	 }
	 
	  public function sub_fetch_state()
	 {
	  if($this->input->post('country_id'))
	  {
	   echo $this->home_model->sub_fetch_state($this->input->post('country_id'));
	  }
	 }

	 public function sub_fetch_city()
	 {
	  if($this->input->post('state_id'))
	  {
	   echo $this->home_model->sub_fetch_city($this->input->post('state_id'));
	  }
	 }

	 
	public function index()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
        $data['page'] = "home";
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/index',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function search()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/search',$data);
		$this->load->view('frontend/footer');
	}
	public function booking()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/booking',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function about()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['page'] = "about";
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/about',$data);
		$this->load->view('frontend/footer',$data);
	}
	public function contact()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['page'] = "contact";
		$this->load->view('frontend/header');
		$this->load->view('frontend/contact');
		$this->load->view('frontend/footer');
	}
	public function services()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['page'] = "services";
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/services',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function consultation()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['page'] = "consultation";
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/counsultation',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function doctor()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/index',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function login()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['page'] = "doctorlogin";
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/login',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function subscription()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/subscription',$data);
		$this->load->view('frontend/footer',$data);
	}

	public function patient()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/index',$data);
		$this->load->view('frontend/footer',$data);
	}
	public function view_doctor()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/view_doctor',$data);
		$this->load->view('frontend/footer',$data);
	}
	public function add_feedback()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/add_feedback',$data);
		$this->load->view('frontend/footer',$data);
	}
	
	public function terms(){
		$this->load->view('frontend/terms');
	}

	public function pay_now($id){

		$result = $this->db->query("SELECT * FROM vd_doctors WHERE doctor_id = '".$id."' ")->row();
		$product_info =  $result->reg_number;
		$customer_emial =  $result->email_id;
		$customer_name =  $result->name;
		$mobile =  $result->mobile_number;
	    $plan_id =  $result->plan_id;
		if($plan_id == 1) {
	    	$amount = '5000';
	    	$plan = "Lifetime";
		}else {
	    	$amount = '2000';
	    	$plan = "Yearly";
		}

	      $PAYU_BASE_URL = "";

		$action = '';
	    $action = "https://secure.payu.in/_payment";

	 		$MERCHANT_KEY = "rLtGWaHK"; //change  merchant with yours
	        $SALT = "SiKMPQ8ykc";  //change salt with yours 


	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	        $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|||||||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() . 'Status';  
	        $fail = base_url() . 'Status';
	        $cancel = base_url() . 'Status';
	        
	        
	         $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'reg_number' => $product_info,
	            'email' => $customer_emial,
	            'mobile' => $mobile,
	            'action' => $action, 
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel,
	            'plan' => $plan,               
	        );

	        $this->load->view('frontend/confirmation', $data);   

	}
	
		public function renew_now($id){

		$result = $this->db->query("SELECT * FROM vd_doctors WHERE doctor_id = '".$id."' ")->row();
		$product_info =  $result->reg_number;
		$customer_emial =  $result->email_id;
		$customer_name =  $result->name;
		$mobile =  $result->mobile_number;
	    $plan_id =  $result->plan_id;
		if($plan_id == 1) {
	    	$amount = '5000';
	    	$plan = "Lifetime";
		}else {
	    	$amount = '2000';
	    	$plan = "Yearly";
		}

	      $PAYU_BASE_URL = "";

		$action = '';
	    $action = "https://secure.payu.in/_payment";

	 		$MERCHANT_KEY = "rLtGWaHK"; //change  merchant with yours
	        $SALT = "SiKMPQ8ykc";  //change salt with yours 


	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	        $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|||||||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() . 'Statusdoc';  
	        $fail = base_url() . 'Statusdoc';
	        $cancel = base_url() . 'Statusdoc';
	        
	        
	         $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'reg_number' => $product_info,
	            'email' => $customer_emial,
	            'mobile' => $mobile,
	            'action' => $action, 
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel,
	            'plan' => $plan,                
	        );

	        $this->load->view('frontend/renew_now', $data);   

	}

	public function set_password()
	{
		$data['page'] = 'set_password';
		$this->load->view('frontend/header');
		$this->load->view('frontend/set_password',$data);
		$this->load->view('frontend/footer');
	}

	public function changepassword($email)
	{

		$date = date('Y-m-d H:i:s');
        $check_otp = $this->db->query("SELECT name FROM vd_doctors WHERE email_id = '".$email."' AND ('".$date."' < DATE_ADD(token , INTERVAL 15 MINUTE))")->row();
        if($check_otp){
      
		$data['page'] = 'changepassword';
		$data['email'] = $email;
		$this->load->view('frontend/header');
		$this->load->view('frontend/changepassword',$data);
		$this->load->view('frontend/footer');

		}else{
			$data['msg'] = 'Sorry..!! Link is expired';
		
		$this->load->view('frontend/header');
		$this->load->view('frontend/expird',$data);
		$this->load->view('frontend/footer');
		}


	}

	
}
