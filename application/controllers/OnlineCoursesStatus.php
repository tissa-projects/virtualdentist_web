<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class OnlineCoursesStatus extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');  
        $this->load->model('onlinecourses_model');    
    }

    public function checkurl() 
    {
       $data['url'] = base_url()."Onlinecourses_Api/";
       $this->session->set_userdata($data);
    }

    public function index() {
        $this->checkurl();
        $status = $this->input->post('status');
        if (empty($status)) {
          redirect('Onlinecourses');
        }
           
        $firstname = $this->input->post('firstname');
        $amount = $this->input->post('amount');
        $txnid = $this->input->post('txnid');
        $posted_hash = $this->input->post('hash');
        $key = $this->input->post('key');
        $productinfo = $this->input->post('productinfo');
        $email = $this->input->post('email');
        $PG_TYPE = $this->input->post('PG_TYPE');
        $salt = "SiKMPQ8ykc"; //  Your salt
        // $add = $this->input->post('additionalCharges');
        $add = '';
        If (isset($add)) {
            $additionalCharges = $this->input->post('additionalCharges');
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $data['hash'] = hash("sha512", $retHashSeq);
        $data['amount'] = $amount;
        $data['txnid'] = $txnid;
        $data['posted_hash'] = $posted_hash;
        $data['status'] = $status;
        if($status == 'success'){
            $api = $this->session->userdata('url');

            $method = 'POST';
            $url = $api.'addorders';
            $header = array(
               'Content-Type: application/json'
            );
            $apidata = array("cart_id" => $productinfo, 'txnid' => $txnid, 'status' => $status, 'amount' => $amount, 'PG_TYPE' => $PG_TYPE);
            $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
            $addorders = json_decode($details);
            $this->load->view('onlinecourses/payment/success', $data);   
        }
        else{
            $this->load->view('onlinecourses/payment/fail', $data); 
        }
    }
}
?>