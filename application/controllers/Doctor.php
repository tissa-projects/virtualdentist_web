<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

 public function __construct()
	 {
	  	parent::__construct();
	  	$this->load->model('home_model');
	  	$this->load->helper('url');
	 }

	
	public function index()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['city'] = $this->home_model->get_city();
		$data['page'] = 'index';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/index',$data);
		$this->load->view('frontend/footer');
	}
	
	public function counsultant()
	{
		$data['country'] = $this->home_model->fetch_country();
		$data['state'] = $this->home_model->get_state();
		$data['city'] = $this->home_model->get_city();
		$data['page'] = 'counsultant';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/counsultant',$data);
		$this->load->view('frontend/footer');
	}

	public function practitioner()
	{
		$data['page'] = 'practitioner';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/practitioner',$data);
		$this->load->view('frontend/footer');
	}

	public function patintlist()
	{
		$data['page'] = 'patintlist';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/patintlist',$data);
		$this->load->view('frontend/footer');
	}

	public function appointmentlist()
	{
		$data['page'] = 'appointmentlist';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/appointmentlist',$data);
		$this->load->view('frontend/footer');
	}

	public function counsultantlist()
	{
		$data['page'] = 'counsultantlist';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/counsultantlist',$data);
		$this->load->view('frontend/footer');
	}
	public function subscription()
	{
		$data['page'] = 'subscription';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/subscription',$data);
		$this->load->view('frontend/footer');
	}
	
    public function consultsubscription()
	{
		$data['page'] = 'consultsubscription';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/consultsubscription',$data);
		$this->load->view('frontend/footer');
	}
	
	public function counsultchangepass(){
		$data['page'] = 'changepass';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/counsultchangepass',$data);
		$this->load->view('frontend/footer');

	}
	
	public function changepass(){
		$data['page'] = 'changepass';
		$this->load->view('frontend/header_doctor');
		$this->load->view('doctor/changepass',$data);
		$this->load->view('frontend/footer');

	}


		public function pay_now(){

			$id=$this->input->post('doctor_id');

		$result = $this->db->query("SELECT * FROM vd_doctors WHERE doctor_id = '".$id."' ")->row();
		$product_info =  $result->reg_number;
		$customer_emial =  $result->email_id;
		$customer_name =  $result->name;
		$mobile =  $result->mobile_number;
	    $plan_id =  $result->plan_id;
		if($plan_id == 1) {
	    	$amount = '5000';
	    	$plan = "Lifetime";
		}else {
	    	$amount = '2000';
	    	$plan = "Yearly";
		}

	      $PAYU_BASE_URL = "";

		$action = '';
	    $action = "https://secure.payu.in/_payment";

	 		$MERCHANT_KEY = "rLtGWaHK"; //change  merchant with yours
	        $SALT = "SiKMPQ8ykc";  //change salt with yours 


	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	        $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|||||||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() . 'Statusdoc';  
	        $fail = base_url() . 'Statusdoc';
	        $cancel = base_url() . 'Statusdoc';
	        
	        
	         $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'reg_number' => $product_info,
	            'email' => $customer_emial,
	            'mobile' => $mobile,
	            'action' => $action, 
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel,
	            'plan' => $plan,               
	        );

	        $this->load->view('doctor/confirmation', $data);   

	}

}
