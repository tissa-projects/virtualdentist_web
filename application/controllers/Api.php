<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class Api extends CI_Controller
{
public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		$this->load->library('pagination'); 
		$this->load->helper('form'); 
		$this->load->library('session');
		$this->load->library('email');
		$this->load->helper('cookie');   
	
		$this->load->library('encryption');
		
	}
	// private function is_valid_apikey($api_key, $user_id)
	// {
	// 	if($api_key == '246c1044-c75f-d866-901c-246c1044-c75f-d866-901c-cbaa7f52a07a')
	// 	{
	// 		return true;
	// 	}
	// 	return false;
	// }
	// private function invalid_api_msg()
	// {
	// 	return 'Invalid request..!';
	// }

    	public function country(){
	    $json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->api_model->fetch_country();
		header('Content-type: application/json');
		echo json_encode($result);	
       
     }

      public function update_profile(){
		//$json=file_get_contents("php://input");
		//$obj=json_decode($json,true);
		$posts=$this->api_model->update_profile();	
		//header('Content-type: application/json');
        echo $posts;
	}

     public function update_document(){
		//$json=file_get_contents("php://input");
		//$obj=json_decode($json,true);
		$posts=$this->api_model->update_document();	
		//header('Content-type: application/json');
        echo json_encode($posts);
	}

	 public function get_subscription(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_subscription($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	}

	 public function setpassword(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->setpassword($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	}

	public function search(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->search($obj);	
		header('Content-type: application/json');
       
		echo json_encode($posts);


	}

	public function gettimeslot(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->api_model->gettimeslot($obj);
		header('Content-type: application/json');
		echo json_encode($result);	

	}

	public function doctor_login() {
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$result= $this->api_model->doctor_login($obj);
		header('Content-type: application/json');
		echo json_encode($result);	
	}


	public function update_doctor(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->update_doctor($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	}
	
	public function update_consultant(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->update_consultant($obj);	
		header('Content-type: application/json',true);
        echo json_encode($posts);
	}


	public function get_doctor(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_doctor($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	}

	public function request_otp(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->request_otp($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	}

	public function book_appointment(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->book_appointment($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	}

	public function add_appointment(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->add_appointment($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);		
	}

	public function get_appointment(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_appointment($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function edit_appointment_status(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->edit_appointment_status($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	
	public function decline_status(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->decline_status($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	
	public function attend_patient(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->attend_patient($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);


	}

	public function add_schedule(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->add_schedule($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	public function update_schedule(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->update_schedule($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_schedule(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_schedule($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function add_feedback(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->add_feedback($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_feedback(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_feedback($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_patient(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_patient($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_counsultant(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_counsultant();	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_all_doctors(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_all_doctors($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_all_subscriber(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_all_subscriber();	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function get_all_subscribed(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_all_subscribed();	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function add_holiday(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->add_holiday($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	public function get_holiday(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_holiday($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}


	public function update_holiday(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->update_holiday($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function delete_doctor(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->delete_doctor($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function delete_patient(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->delete_patient($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function delete_holiday(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->delete_holiday($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function delete_schedule(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->delete_schedule($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function approved(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->approved($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function active(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->active($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function send_notification(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->send_notification($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);


	}

	public function getlink(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->getlink($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}

	public function changepass(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->changepass($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	
	public function c_changepass(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->c_changepass($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	
	
	public function admin_change_pass(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->admin_change_pass($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);

	}
	
	
    public function getcounsultant(){

		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->getcounsultant($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);


	}

	public function get_payinfo(){
		$json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->get_payinfo($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);


	}
	
	public function lastlogout(){
	    
	    $json=file_get_contents("php://input");
		$obj=json_decode($json,true);
		$posts=$this->api_model->lastlogout($obj);	
		header('Content-type: application/json');
        echo json_encode($posts);
	    
	    
	    
	}


 
}
