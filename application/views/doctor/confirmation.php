<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
<!--    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt--->
<!--color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>-->
  <script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
  <body>
  
<table  width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
    <tbody>
      <tr>
        <td valign="top" align="center" bgcolor="" style="padding:0 15px">
         
          <table cellpadding="0" width="100%" cellspacing="0" border="0" align="center" style="margin:0 auto;table-layout:fixed;border-collapse:collapse!important;max-width:600px">
            <tbody>
              <tr>
                <td align="center" valign="top" width="100%">
                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                      <tr><td align="center" valign="top" style="padding:30px 0 ; font-size: 30px;color:blue;">
                     <img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" />
                      </td></tr>
                    </tbody>
                  </table>                
                  <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-radius:4px">
                    <tbody> 
                      <tr>
                        <td height="40"></td>
                      </tr>
                      <tr style="font-family:-apple-system,BlinkMacSystemFont,'Segoe UI','Roboto','Oxygen','Ubuntu','Cantarell','Fira Sans','Droid Sans','Helvetica Neue',sans-serif;color:#4e5c6e;font-size:14px;line-height:20px;margin-top:20px">
                        <td colspan="2" valign="top" align="center" style="padding-left:30px;padding-right:30px">

                          <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="">
                          <tbody>
                            
                            

                            <tr>
                              <td align="center">
        
                                <table style="width:100%;border-collapse:collapse">
                                  <tbody style="border:0;padding:0;margin-top:20px">
                                    <tr>
                                      <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                        Registration Number
                                      </td>
                                      <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;"><span style="font-family:'Monaco',monospace;border:1px solid #dae1e9;letter-spacing:2px;padding:5px 8px;border-radius:4px;background-color:#f4f7fa;color:#2e7bc4 "><?php echo $reg_number; ?></span></td>
                                    </tr>
                                    <tr>
                                      <td width="50%" valign="top" style="padding-bottom:10px;padding-top:10px ;font-size:14px;">Email</td>
                                      <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;"><?php echo $email; ?></td>
                                    </tr>
                                      <tr>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">Name</td>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;"><?php echo $name; ?></td>
                                      </tr>
                                      <tr>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                          Subscription Plan<br>
                                        </td>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                          <?php echo $plan; ?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                          Subscription Amount<br>
                                        </td>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                          <strong>&#8377;&nbsp;<?php echo $amount; ?></strong><br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                         Mobile
                                        </td>
                                        <td style="padding-bottom:10px;padding-top:10px ;font-size:14px;">
                                         <?php echo $mobile; ?>
                                        </td>
                                      </tr>
                                     
                                  </tbody>
                                </table>

                            </td>
                          </tr>

                         

                         
                          <tr>
                          <td></td>
                          </tr>

                          <tr><td height="24"></td></tr>
                          <tr>
                            <td height="1" bgcolor="#DAE1E9"></td>
                          </tr>



                          <tr>
                            <td height="24">
                              <form action="<?php echo $action; ?>" method="post" id="payuForm" name="payuForm">
                    <input type="hidden" name="key" value="<?php echo $mkey; ?>" />
                    <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
                    <input type="hidden" name="txnid" value="<?php echo $tid; ?>" />
                    <div class="form-group">
                       
                        <input type="hidden" class="form-control" name="amount" value="<?php echo $amount; ?>"  readonly/>
                    </div>
                    <div class="form-group">
                      
                        <input  type="hidden" class="form-control" name="firstname" id="firstname" value="<?php echo $name; ?>" readonly/>
                    </div>
                    <div class="form-group">
                        
                        <input type="hidden" class="form-control" name="email" id="email" value="<?php echo $email; ?>" readonly/>
                    </div>
                    <div class="form-group">
                       
                        <input type="hidden" class="form-control" name="phone" value="<?php echo $mobile; ?>" readonly />
                    </div>
                     <div class="form-group">
                       
                        <input type="hidden" class="form-control" name="productinfo" value="<?php echo $reg_number; ?>" readonly />
                    </div>
                   
                    <div class="form-group">
                        <input name="surl" value="<?php echo $sucess; ?>" size="64" type="hidden" />
                        <input name="furl" value="<?php echo $failure; ?>" size="64" type="hidden" />  
                        <!--for test environment comment  service provider   -->
                        <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                       <!--  <input name="udf1" type="hidden" value="">
                <input name="udf2" type="hidden" value="">
                <input name="udf3" type="hidden" value="">
                <input name="udf4" type="hidden" value="">
                <input name="udf5" type="hidden" value=""> -->
                       <!--  <input name="surl" type="text" value="<?php echo base_url('doctor/success'); ?>" size="64" />
          <input name="furl" type="hidden" value="<?php echo base_url('doctor/fail'); ?>" size="64" />
          <input name="curl" type="hidden" value="<?php echo base_url('doctor/cancel'); ?>" />
 -->
                    </div>
                    <div class="form-group float-right">
                     <input type="submit" name="submit_form" value="Click Here for Payment" class="btn btn-info btn-block" >
                    </div>
                </form> 
                            </td>
                          </tr>
                          <tr>
                            <td align="center">                             
                            <span style="color:#9ba6b2;font-size:12px;line-height:19px">
                            

                            </span>

                              </td>
                            </tr>
                          </tbody>
                        </table>
                        </td>
                      </tr>
                      <tr>
                        <td height="40"></td>
                      </tr>
                    </tbody>
                  </table>
                                   
                    <table  width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px">
                      <tbody>
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                        
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                      
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                      </tbody>
                    </table>
                
                 <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                      <tr>
                        <td height="10">&nbsp;</td>
                      </tr>
                     
                      <tr>
                        <td height="50">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>
        </font></td></tr></tbody></table>

  </body>
</html>




