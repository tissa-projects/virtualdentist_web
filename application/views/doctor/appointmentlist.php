<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 28px;
    font-size: 13px;
    color: #495057;
}

.logo-header{
  margin-left: -29px;
}

.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}

</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/css/font_style.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/responsive.dataTables.min.css" type="text/css" /> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      
                 if(sessionStorage.getItem('myprofile') == null){
                    
                     window.location.href = "<?php echo base_url();?>home/index";
                 }

             });
  </script>
<style type="text/css">

    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

  .btn-group-lg>.btn, .btn-lg {
    padding: 2px 35px;
    font-size: 13px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.checked {
  color: black;
}

.badge1 {
    display: inline-block;
   /* padding: .25em .4em;*/
    font-size: 83%;
    /* font-weight: 700; */
    line-height: 1;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25rem;
    background-color: #ffff !important;
    border:1px solid gray;
    color:gray;
    padding:8px;
    /*padding-left: 20px;
    padding-bottom:  20px;*/
}

.badge-dark[href]:focus, .badge-dark[href]:hover {
    color: #ffff;
    text-decoration: none;
    background-color: blue !important;
}



#outer
{
    width:100%;
    text-align: center;
}
.inner
{
    display: inline-block;
}
.radio-inline {
    border: 2px solid gray;
    border-radius: 30px;
    font-size: 12px;
    background-color: #ccc;
    padding-left: 22px;
    padding-right: 6px;
}


</style>
<?php $this->load->view('frontend/leftsidebar'); ?>

<script type="text/javascript">
    $(document).ready(function(){

        if(sessionStorage.getItem('myprofile') == null){
                    //alert("hii");
                     window.location.href = "<?php echo base_url();?>home/index";
                 }else{


                var data = sessionStorage.getItem('myprofile');

                 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                         $('#doctortime').val(json[0].doctor_id);

                         // $('#proimg').html('<img src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"  alt="Avatar" style="border-radius: 32px;height:50px;width:50px;margin-top:-5px;">') ;
                         $('#log').html('<i class="fa fa-sign-out" aria-hidden="true" style="font-size:30px;color:#2E55FA;margin-top: 6px; cursor:pointer;" id="logout" onclick=logout(this);></i>');

                         $("#dlogin").hide();
                             $("#dpanel").show();


                }  

    }               

               
});

</script>

<script>
$(document).ready(function (){
   var doctor_id = document.getElementById('doctortime').value;
   $.ajax({
            url: "<?php echo base_url();?>api/get_appointment",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                     var data1 = JSON.stringify(response.data);

                    //alert(data1);
                     var obj = JSON.parse(data1);
                    var html = ''; 
                    var i;

                for(i=0; i< obj.length; i++){

                    if(obj[i].status == 1){

                       var disabled = "disabled";
                       var st1 = "display:none";
                        var st = "display:block";
                        var text = "Accepted";

                    }

                    if(obj[i].status == 3){

                      var disabled1 = "disabled";
                        var st = "display:none";
                        var st1 = "display:block";
                        var text1 = "Rejected";

                    }

                     if(obj[i].status == 0){

                     var disabled = "";
                       var disabled1 = "";
                      var st = "display:block";
                      var st1 = "display:block";
                      var text = "Accept";
                      var text1 = "Reject";
                    }
                    

                  
                    var mor = obj[i].schedule_id;
                    var eve = obj[i].schedule_eve_id;

                    if(mor == undefined){

                        mor = '';
                    }else{
                        eve = '';
                    }

                 j = i+1;  
                 
                 
                 var date = new Date(obj[i].appointment_date) ;

                    var d = date.toDateString();

                    d = d.split(' ');                  


                  html += '<tr><td>'+j+'</td><td>'+d[2] + ' ' + d[1] + ' ' + d[3]+'</td><td>'+mor+''+eve+'</td><td>'+obj[i].patient_name+'</td><td>'+obj[i].location+'</td><td>'+obj[i].mobile+'</td><td><div id="outer"><div class="inner"><button class="btn btn-success white btn-block" id="success_'+obj[i].appointment_id+'" onclick="accept('+obj[i].appointment_id+');" style="display: inline-block;vertical-align: top;'+st+'" '+disabled+'>'+text+'</button></div>&nbsp;&nbsp;<div class="inner"><button class="btn btn-danger white btn-block" id="danger_'+obj[i].appointment_id+'" onclick="decline('+obj[i].appointment_id+');" style="display: inline-block;vertical-align: top;'+st1+'" '+disabled1+'>'+text1+'</button></div></div></td><input type="hidden" id="appintmentid_'+j+'" value="'+obj[i].appointment_id+'"><input type="hidden" id="statussucess_'+j+'" value="1"><input type="hidden" id="statusdecline_'+j+'" value="2"></tr>';                 
                }

                   
            $('#tbody').html(html);
                   
            }

                
            });
        


            $.ajax({
            url: "<?php echo base_url();?>api/get_holiday",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            data: JSON.stringify({doctor_id:doctor_id}),
            success: function(response){
                var data = response.data;
               //var unavailableDates = Array [];
               var unavailableDates = new Array();
                var i;
               for(i=0; i< data.length; i++){
                var date = data[i].holiday_date;
                    unavailableDates.push(date);
                }
               function unavailable(date) {
                   dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
                  if ($.inArray(dmy, unavailableDates) < 0) {
                    return [true,"","Book Now"];
                  } else {
                    return [false,"","Booked Out"];
                  }
                }
                $("#datepicker").datepicker({ 
                dateFormat: 'yy-mm-dd',
                minDate:+1,
                beforeShowDay: function(date){
                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                        return [ unavailableDates.indexOf(string) == -1 ]
                    }
              });
                                            
        }         
        });
        
});
</script>
<script type="text/javascript">
    
    function accept(a){

        var appointment_id = a;
       

        $.ajax({
        url: "<?php echo base_url();?>api/edit_appointment_status",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({appointment_id:appointment_id}),
            success: function(response){

              //  var data = JSON.stringify(response.is_verified);
               //  var obj = JSON.parse(data);

             location.reload();
                  
          }
    });
    

    }

    function decline(a){

       var status = 3;
       var appointment_id = a;

        $.ajax({
        url: "<?php echo base_url();?>api/decline_status",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({appointment_id:appointment_id,status:status}),
            success: function(response){

              //  var data = JSON.stringify(response.is_verified);
               //  var obj = JSON.parse(data);
               location.reload();
                
          }
    });
    

    }


</script>

  <input type="hidden" id="doctortime" >
<div class="col-md-10 dshbrd" >
            <div class="row" style="">
                <div class="col-md-12"><center><h2><b style="color:#27ABD7;"></b><h2></center></div>
                <div class="col-md-12" style="padding: 10px; text-align: right;"><a class="btn btn-info" data-toggle="modal" data-target="#myModal" style="background-color: #0B4FE8;color: white;">Add Appointment</a></div>
                <div class="col-md-12">
                    <div class="table-responsive"> 
                         <table class="table table-bordered display" id="example2" cellspacing="0" width="100%" style="font-family: Lustria;">
                                <thead style="border: 1px solid rgb(12,11,11); ">
                                    <th>Sr.No</th>
                                    <th>Appointment Date</th>
                                    <th>Time</th>
                                    <th>Patient Name</th>
                                    <th>Location</th>
                                    <th>Mobile Number</th>
                                    <th class="text-center" >Action</th>
                                </thead>
                                
                                <tbody id="tbody">             
                                      
                                                         
                                </tbody>
                            </table>
                       
                    </div>
                    
                </div>
               
                <br>
                
            </div>
       
</div>
</div>
</div>

<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
       
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title apptheading">Add Appointment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
               
            </div>
            <div class="modal-body" id="details_body">
                <center><span id="message"></span></center>
                <div class="row">
                    <div class="col-md-3">Select Date: </div>
                 <div class="col-md-9 form-group"><input type="text" name="datepicker" id="datepicker"  class="form-control"></div>
                    <div class="col-md-3"><button class="btn-sm btn btn-primary" onclick="show_time();"> Show Available Time</button></div>
                    <input type="hidden" name="mid" id="mid"><input type="hidden" name="eid" id="eid">
                    <div class="col-md-9" ><div id="mortime"></div><hr><div id="evetime"></div><hr></div>
                    <div class="col-md-3">Patient Name: </div>
                 <div class="col-md-9 form-group"><input type="text" name="pname" id="pname"  class="form-control"></div>
                 <div class="col-md-3">Location: </div>
                 <div class="col-md-9 form-group"><input type="text" name="plocation" id="plocation"  class="form-control"></div>
                 <div class="col-md-3">Mobile: </div>
                 <div class="col-md-9 form-group"><input type="text"  maxlength="10"  pattern="[0-9]{10}" title="Enter your mobile number" name="pmobile" id="pmobile"  class="form-control"></div>
                 <div class="col-md-12"><center><button class="btn-sm btn btn-success" onclick="book_appointment();"> Add Appointment For Patient </button></center></div>
                </div>            
               
            </div>
        </div>
            
    </div>
    </div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example2').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>

<script type="text/javascript">
    

    function show_time(){
        var date = document.getElementById('datepicker').value;
        var doctor_id = document.getElementById('doctortime').value;
        
       Date.prototype.addTime = addTime;
        Date.prototype.showTime = showTime;
        var cur_date = new Date();

        var time = new Date().addTime({hours: 1, minutes: 20}).showTime();
        //alert(time);

        if(date == ''){

            Swal.fire({
                              title: "<i style='color:orange;'>Sorry...!!</i>", 
                              html: "Select Date First",  
                              confirmButtonText: "OK", 
                            });


        }else{

        $.ajax({
        url: "<?php echo base_url();?>api/gettimeslot",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id,date:date}),
            success: function(response){

               if(response.status == true){

              var unavailabletime = new Array();
              var morningtime = new Array();
               var eveningtime = new Array();

                var html ='';
                 var html1 ='';

             var data1 = response.morning.morningslot;
              var data2 = response.evening.eveningslot;
              var data = response.schedule;

              var dArr = data1.split(",");
              var dArr2 = data2.split(",");

                var i;
               for(i=0; i< data.length; i++){
                var date = data[i].schedule_id;
                    unavailabletime.push(date);
                }



                var j;
               for(j=0; j< dArr.length; j++){
                var date = dArr[j];
                    morningtime.push(date);
                }


                var k;
               for(k=0; k< dArr2.length; k++){
                var date = dArr2[k];
                    eveningtime.push(date);
                }


                 for(var l=0; l< morningtime.length; l++) {
             
               
                  var found = $.inArray(morningtime[l], unavailabletime);
                  if( !(-1 == found) ) {
                       html +='<div class="radio-inline"><input type="radio" style="background-color:white;margin-top: -3px;" name="time_morning" disabled>Booked</div>';
                  }else{
                    html +='<div class="radio-inline"><input type="radio" style="background-color:white;margin-top: -3px;" name="time_morning" value="'+morningtime[l]+'">'+morningtime[l]+'</div>';
                  }
               
              }

            
                 $("#mortime").html(html);


                  for(var m=0; m < eveningtime.length; m++) {
                      
                
                  var found1 = $.inArray(eveningtime[m], unavailabletime);
                  
                  if( !(-1 == found1) ) {
                       html1 +='<div class="radio-inline"><input type="radio" style="background-color:white;margin-top: -3px;" name="time_morning" disabled>Booked</div>';
                  }else{
                     
                     html1 +='<div class="radio-inline"><input type="radio" style="background-color:white;margin-top: -3px;" name="time_morning" value="'+eveningtime[m]+'">'+eveningtime[m]+'</div>';
                  }
             
              }

                 $("#evetime").html(html1);

        }



                if(response.status == false){

                    var msg = JSON.stringify(response.msg);

                        Swal.fire({
                              title: "<i style='color:orange;'>Sorry...!!</i>", 
                              html: msg,  
                              confirmButtonText: "OK", 
                            });
                        
            }          
        }           
    });

    }
 }

    
    function addTime(values) {
      for (var l in values) {
        var unit = l.substr(0,1).toUpperCase() + l.substr(1);
        this['set' + unit](this['get' + unit]() + values[l]);
      }
      return this;
    }
    
    function showTime(military) {
      var zeroPad = function () {
        return this < 10 ? '0' + this : this;
      };
      
      if (military) {
        return [ zeroPad.call(this.getHours()),
                 zeroPad.call(this.getMinutes())].join(':');
      }
      var isPM = this.getHours() >= 12;
      var isMidday = this.getHours() == 12;
      return time = [ zeroPad.call(this.getHours() - (isPM && !isMidday ? 12 : 0)),
                      zeroPad.call(this.getMinutes())].join(':') +
                    (isPM ? 'PM' : 'AM');
    
      
     
    }

  function book_appointment(){
    var appointment_date = document.getElementById('datepicker').value;
        var  doctor_id = document.getElementById('doctortime').value;
         var name = document.getElementById('pname').value;
          var location = document.getElementById('plocation').value;
           var mobile = document.getElementById('pmobile').value;
            var timeid =  $("input[name='time_morning']:checked").val();

        var timeeveid =  $("input[name='time_morning']:checked").val();

        $("#mid").val(timeid);
         $("#eid").val(timeeveid);

         var mid = document.getElementById('mid').value;
         var eid = document.getElementById('eid').value;


         if(name == ''){
            $('#message').html("<i>Please enter patient name</i>").css('color', 'red');
            return false;
         }else if(mobile == ''){
             $('#message').html("<i>Please enter mobile</i>").css('color', 'red');
            return false;
         }else if(mid == ''){

            $('#message').html("<i>Please select time/i>").css('color', 'red');
            return false;
         }else{    
             $('#message').html("");
             
        $.ajax({
        url: "<?php echo base_url();?>api/add_appointment",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id,name:name,location:location,mobile:mobile,timeid:mid,timeeveid:eid,appointment_date:appointment_date}),
            success: function(response){

            if(response.status == true){
                var msg = response.msg;
                
                $("#myModal").modal("hide");
                $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();

            Swal.fire({
                  title: "<i style='color:#ADD8E6'>Congratulation...!!</i>", 
                  html: msg,  
                  confirmButtonText: "OK", 
                });

                 window.location.href = "<?php echo base_url();?>doctor/appointmentlist";
                
                      
                    }


                      if(response.status == false){

                    var msg = JSON.stringify(response.msg);

                        Swal.fire({
                              title: "<i style='color:orange'>Sorry...!!</i>", 
                              html: msg,  
                              confirmButtonText: "OK", 
                            });
                        

                    }          
                
        }           
    });


}


  }

</script>