<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 28px;
    font-size: 13px;
    color: #495057;
}

.logo-header{
  margin-left: -30px;
}
.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      
                 if(sessionStorage.getItem('myprofile') == null){
                    
                     window.location.href = "<?php echo base_url();?>home/index";
                 }

             });
  </script>
  <script>
      $( function() {

        // var unavailableDates = ["30-5-2020","14-5-2020","15-5-2020"];

        // function unavailable(date) {
        //   dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
        //   if ($.inArray(dmy, unavailableDates) < 0) {
        //     return [true,"","Book Now"];
        //   } else {
        //     return [false,"","Booked Out"];
        //   }
        // }

       $("#datepicker").datepicker({ 
        dateFormat: 'yy-mm-dd',
        minDate: 0
        //beforeShowDay: unavailable
    });
      });
  </script>
<style type="text/css">

    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

.badge {
    padding: 7px 7px;
    font-size: 10px;
    color: #fff;
    background-color: #2E55FA;
}

.panel {
    width:380px;
}
  .btn-group-lg>.btn, .btn-lg {
    padding: 2px 35px;
    font-size: 13px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.fa-close{
    color:red;
} 
</style>
<?php $this->load->view('frontend/consuleftsidebar'); ?>


<script type="text/javascript">
    $(document).ready(function(){

        if(sessionStorage.getItem('myprofile') == null){
                    //alert("hii");
                     window.location.href = "<?php echo base_url();?>home/index";
                 }else{


                var data = sessionStorage.getItem('myprofile');

                 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                         $('#doctortime').val(json[0].doctor_id);
                       

                          //$('#proimg').html('<a href="<?php echo base_url(); ?>doctor/index"><img src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"  alt="Avatar" style="border-radius: 32px;height:50px;width:50px;margin-top: -17px;"></a>') ;
                         $('#log').html('<i class="fa fa-sign-out" aria-hidden="true" style="font-size:30px;color:#2E55FA;margin-top: 6px; cursor:pointer;" id="logout" onclick=logout(this);></i>');

                            $("#dlogin").hide();
                             $("#dcpanel").show();
                }  

    }               

               
});

</script>



<script>
$(document).ready(function (){
   var doctor_id = document.getElementById('doctortime').value;
  

        $.ajax({
            url: "<?php echo base_url();?>api/getcounsultant",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                    var data = JSON.stringify(response.data);

                     var json = $.parseJSON(data)                
                if(json.length>0)
                {

                     $('#doctortime').val(json[0].doctor_id);
                        $('#dname').html(json[0].name);
                         $('#regnum').html(json[0].reg_number); 
                         $('#regemail').html(json[0].email_id); 
                         $('#passing').html(json[0].passing_year); 
                         $('#mobile').html(json[0].mobile_number);
                         $('#speci').html(json[0].specialization);

                         var curyear = new Date().getFullYear();

                         var exp =  curyear - json[0].passing_year;

                         $('#year').html(exp);
                         
                         $('#address').html(json[0].c_address);
                        
                         $('#city').html(json[0].c_city);
                         $('#pincode').html(json[0].c_pincode);

                          $('#avtarpic').html('<img class="mx-auto img-fluid img-circle d-block" alt="avatar" style="height:100px;width:100px" id="avtar" src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'">') ;


                    $('#type_e').val(json[0].doctor_type);
                    $('#name_e').val(json[0].name); 
                     $('#spacial').val(json[0].specialization);
                    $('#phone_e').val(json[0].mobile_number); 
                    $('#pass_y_e').val(json[0].passing_year); 
                  
                    $('#clinic_add_e').val(json[0].c_address); 
                  
                    $('#country_e').val(json[0].c_country); 
                     $('#state_e').val(json[0].c_state); 
                    $('#city_e').val(json[0].c_city); 
                    $('#pincode_e').val(json[0].c_pincode);
                   

                }

            }

                
            });




});
</script> 
<!--content part-->
<div class="col-lg-10 dshbrd">
     
    <input type="hidden" name="doctortime" id="doctortime">
     <!--<input type="text" name="regemail" id="regemail">-->
    <div class="row">
        <div class="col-md-12"><center><h2><b style="color:#FFFFFF;">Change Password</b><h2></center></div>
            <div class="col-md-3"></div>
        <div class="col-md-5" >
           
        <div class="forms">
           
            <form action="" id="forgot" class = "form-horizontal" role = "form">
			      	<div class="input-field">

			      		<center><span id="message"></span></center>
			        	<div class="col-md-12 form-group">
					        	<label for="email">Password</label>
					        	<input type="password" class="form-control" id="pass" name="pass"  />
					    </div>

					    <div class="col-md-12 form-group">
					        	<label for="email">Confirm Password</label>
					        	<input type="password" class="form-control" id="cpass" name="pass"  />
					    </div>
				        
			        	 <div class="col-md-12 form-group">
			        		<input type="submit" value="Change your password" id="changepass" class="btn btn-success" style=" height:30px!important; color: white; font-size: 15px; border-radius: 20px; ">
			        	</div><br>
			        		 
				    </div>
				 </form>
        </div>
            
        </div>
        <div class="col-md-3" >
           
             
        </div>
        <!--<div class="col-md-1"></div>-->

    </div>
    <br><br><br><br><br><br>
</div>

    
</div>
 
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#changepass').click(function(e) {
        e.preventDefault();

		 	 var doctor_id = document.getElementById('doctortime').value;
		 var pass = document.getElementById('pass').value;
		  var cpass = document.getElementById('cpass').value;

		  if(pass == ''){

		  	$('#message').html("<i>Please Enter Password</i>").css('color', 'red');
		        	return false;
		  }else if(pass != cpass){

		  	$('#message').html("<i>Entered Password Doesn't Match With Confirm Password</i>").css('color', 'red');
		        	return false;

		  }else{
       $('#message').html("");
	        $.ajax({
	        url: "<?php echo base_url();?>api/c_changepass",
	        type:"POST",
	        processData:false,
	        contentType: 'application/json',
	        dataType: 'json',
	        async: false,
	        //data :form_data,
	        data: JSON.stringify({pass:pass,doctor_id:doctor_id}),
		    	success: function(response){
		    		var data = JSON.stringify(response.data);


		    		 Swal.fire({
                        title: "<i>Congratulations...!!</i>", 
                        html: data,  
                        confirmButtonText: "OK", 
                      });

                      
		    	}

                
      		});
	    }
        });
	});
</script>






