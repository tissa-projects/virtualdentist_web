<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 30px !important;
    font-size: 13px;
    color: #495057;
}

select{
  height: 30px !important;
}

.logo-header{
  margin-left: -29px;
}
.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}

select.bs-select-hidden, select.selectpicker {
    display: block!important;
}
.bootstrap-select{
   display: none!important;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      
                 if(sessionStorage.getItem('myprofile') == null){
                    
                     window.location.href = "<?php echo base_url();?>home/index";
                 }

             });
  </script>
  <script>
      $( function() {
       $("#datepicker").datepicker({ 
        dateFormat: 'yy-mm-dd',
        minDate: 0
        //beforeShowDay: unavailable
    });
      });
  </script>
<style type="text/css">

    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

.badge {
    padding: 7px 7px;
    font-size: 10px;
    color: #fff;
    background-color: #2E55FA;
}

.panel {
    width:380px;
}
  .btn-group-lg>.btn, .btn-lg {
    padding: 2px 35px;
    font-size: 13px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.fa-close{
    color:red;
} 
</style>
<?php $this->load->view('frontend/consuleftsidebar'); ?>


<script type="text/javascript">
    $(document).ready(function(){

        if(sessionStorage.getItem('myprofile') == null){
                    //alert("hii");
                     window.location.href = "<?php echo base_url();?>home/index";
                 }else{


                var data = sessionStorage.getItem('myprofile');

                 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                         $('#doctortime').val(json[0].doctor_id);
                       

                          //$('#proimg').html('<a href="<?php echo base_url(); ?>doctor/index"><img src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"  alt="Avatar" style="border-radius: 32px;height:50px;width:50px;margin-top: -17px;"></a>') ;
                         $('#log').html('<i class="fa fa-sign-out" aria-hidden="true" style="font-size:30px;color:#2E55FA;margin-top: 6px; cursor:pointer;" id="logout" onclick=logout(this);></i>');

                            $("#dlogin").hide();
                             $("#dcpanel").show();
                }  

    }               

               
});

</script>



<script>
$(document).ready(function (){
   var doctor_id = document.getElementById('doctortime').value;
  

        $.ajax({
            url: "<?php echo base_url();?>api/getcounsultant",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                    var data = JSON.stringify(response.data);

                     var json = $.parseJSON(data)                
                if(json.length>0)
                {

                     $('#doctortime').val(json[0].doctor_id);
                        $('#dname').html(json[0].name);
                         $('#regnum').html(json[0].reg_number); 
                         $('#regemail').html(json[0].email_id); 
                         $('#passing').html(json[0].passing_year); 
                         $('#mobile').html(json[0].mobile_number);
                         $('#speci').html(json[0].specialization);

                         var curyear = new Date().getFullYear();

                         var exp =  curyear - json[0].passing_year;

                         $('#year').html(exp);
                         
                         $('#address').html(json[0].c_address);
                        
                         $('#city').html(json[0].c_city);
                         $('#pincode').html(json[0].c_pincode);

                          $('#avtarpic').html('<img class="mx-auto img-fluid img-circle d-block" alt="avatar" style="height:100px;width:100px" id="avtar" src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'">') ;


                    $('#type_e').val(json[0].doctor_type);
                    $('#name_e').val(json[0].name); 
                     $('#spacial').val(json[0].specialization);
                    $('#phone_e').val(json[0].mobile_number); 
                    $('#pass_y_e').val(json[0].passing_year); 
                  
                    $('#clinic_add_e').val(json[0].c_address); 
                  
                    $('#country_e').val(json[0].c_country); 
                     $('#state_e').val(json[0].c_state); 
                    $('#city_e').val(json[0].c_city);  
                    $('#pincode_e').val(json[0].c_pincode);
                   

                }

            }

                
            });


    var state_id = document.getElementById('state_e').value;

    // $.ajax({
    //     url:"<?php echo base_url(); ?>home/sub_fetch_city",
    //     method:"POST",
    //     data:{state_id:state_id},
    //     success:function(data)
    //     {
    //       //alert(data);
    //      $('#city_e').html(data);
    //     }
    //   });

});
</script> 
<!--content part-->
<div class="col-md-10 dshbrd">
     
    <div class="row">
        <div class="col-md-12">
           <center style="color: #0B4FE8;"><h4><i>Welcome Doctor</i></h4>
            <span id="avtarpic"></span>
                               <label class="custom-file" style=" cursor: pointer; color:blue;">
                                    
                                    <input type="file" id="file" class="custom-file-input">
                                    <span class="custom-file-control" id="changepic">Change photo</span>
                                   
                                </label></center>

          <center><ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Profile</a></li>
           
            <li><a data-toggle="tab" href="#menu2">Edit profile</a></li>
          </ul> </center>

          <div class="tab-content">
            <div id="home" class="tab-pane fade in active" style="padding:20px;">

              <h4><b id="dname"></b></h4><input type="hidden" name="doctortime" id="doctortime">

              <div class="col-md-2"> <a href="#" class="badge badge-dark badge-pill" style="width: 200px;">Registration number</a><br>
                <h6 id="regnum" style="font-weight: bolder;padding-left: 70px;"></h6></div>
             <div class="col-md-2"> <a href="#" class="badge badge-dark badge-pill" style="width: 200px;">&nbsp;&nbsp;&nbsp;Your Email id&nbsp;&nbsp;&nbsp;</a><br>
                <h6 id="regemail" style="font-weight: bolder;padding-left: 5px;"></h6></div>
            <div class="col-md-2"><a href="#" class="badge badge-dark badge-pill" style="width: 200px;"> Your Mobile number</a><br>
                <h6 id="mobile" style="font-weight: bolder;padding-left: 50px;"></h6>
            </div>
            <div class="col-md-6"><br></div>
            
            <br><br>
              <div class="col-md-12" style="text-align: left;"><h4><b>About</b></h4></div>
              <div style="font-weight: bolder;"><span id="speci" ></span></div>
              <div style="font-weight: bolder;"><span id="passing" ></span>(<span id="year" ></span>&nbsp;Years Experience Overall)</div><br>

              <h4><b>Address</b></h4>
             <br><sapn id="address" style="font-weight: bolder;"></sapn><br><sapn id="city" style="font-weight: bolder;"></sapn>,&nbsp;<sapn id="pincode" style="font-weight: bolder;"></sapn><br></div><br>


              
            <!-- </div> -->
           
            <div id="menu2" class="tab-pane fade" style="padding:20px;">
              <form action="" id="editc" class="form-horizontal " role ="form"  style="display: block;padding:20px;margin-right: 100px;">
                  <div class="row">
                      <center><span id="message"></span></center>

                             <input type="hidden" class="form-control" id="clinic_id_e" name="clinic_id_e"  />
                             <input type="hidden" class="form-control" id="type_e" name="type_e"  />
                            
                            <div class="col-md-4 form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" id="name_e" name="name_e"  />
                            </div>
                            
                            <div class="col-md-4 form-group">
                                <label for="Type">Mobile Number</label>
                                <input type="text" class="form-control" id="phone_e" name="phone_e" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  />
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">Specialization</label>
                                <select id="spacial" name="spacial" >
                                    <option value="Orthodontist">Orthodontist</option>
                                    <option value="Implantalogist">Implantalogist</option>
                                    <option value="Cosmetic Dentist">Cosmetic Dentist</option>
                                    <option value="Prosthodontist">Prosthodontist</option>
                                    <option value="Endodontist">Endodontist</option>
                                    <option value="Pedodontist">Pedodontist</option>
                                    <option value="Periodontist">Periodontist</option>
                                    <option value="Community Dentistry">Community Dentistry</option>
                                    <option value="Oral Pathologist">Oral Pathologist</option>
                                    <option value="Oral Medicine and Diagnostic Radiology">Oral Medicine and Diagnostic Radiology</option>
                                    <option value="Oral Maxillofacial Surgeon">Oral Maxillofacial Surgeon</option>
                                    <option value="Forensic Odontology">Forensic Odontology</option>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="passing_year">Passing year</label>
                                <input type="text" class="form-control" id="pass_y_e" name="pass_y_e" onkeypress="return isNum(event)" maxlength="4" minlength="4" pattern="\d{4}" />
                            </div>
                          
                            <div class="col-md-4 form-group" id="addc">
                                <label for="email"> Address</label>
                                <input type="text" class="form-control"  id="clinic_add_e"  name="clinic_add_e" />
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">Country</label>
                               <select name="country_e" id="country_e" class="form-control">
                                    <option value="0">Select Country</option>
                                    <?php
                                    foreach($country as $row)
                                    {
                                     echo '<option value="'.$row->country_name.'">'.$row->country_name.'</option>';
                                    }
                                    ?>
                                   </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">State</label>
                                  <select name="state_e" id="state_e" class="form-control">
                                         <?php
                                    foreach($state as $row)
                                    {
                                     echo '<option value="'.$row->state_name.'">'.$row->state_name.'</option>';
                                    }
                                    ?>
                                      </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">City</label>
                               <select name="city_e" id="city_e" class="form-control"  >
                                    <?php
                                    foreach($city as $row)
                                    {
                                     echo '<option value="'.$row->city_name.'">'.$row->city_name.'</option>';
                                    }
                                    ?>
                  
                    </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="zipcode">Pin code</label>
                                <input type="text" class="form-control" id="pincode_e" name="pincode_e" onkeypress="return isNum(event)"  />
                            </div>
                   
                    </div>
                        <input type="submit" id="submit"  value="submit" class="button btn-success btn-lg" style=" height:40px; color: white; font-size: 15px; border-radius: 20px;"/>
                            
             </form>
            </div>
            
          </div>
       

        </div>
        
    </div>
</div>

    
</div>
 
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script type="text/javascript">
    $(document).ready(function(){
       
        $('#submit').click(function(e) {
        e.preventDefault();
      
        var type = document.getElementById('type_e').value;
        var name = document.getElementById('name_e').value;
        var phone = document.getElementById('phone_e').value;
        var pass_y = document.getElementById('pass_y_e').value;
        
        var clinic_add = document.getElementById('clinic_add_e').value;

        var specialization = document.getElementById('spacial').value;
       
        var country1 = document.getElementById('country_e').value;
        var state1 = document.getElementById('state_e').value;
        var city1 = document.getElementById('city_e').value;
        var zipcode = document.getElementById('pincode_e').value;
        var doctor_id = document.getElementById('doctortime').value;
       

         $.ajax({
            url: "<?php echo base_url();?>api/update_consultant",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({type:type,name:name,phone:phone,specialization:specialization,pass_y:pass_y,clinic_add:clinic_add,country1:country1,state1:state1,city1:city1,zipcode:zipcode,doctor_id:doctor_id}),
                success: function(response){
                    
                    	Swal.fire({
			                  title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
			                  html: "Profile has been updated successfully",  
			                  confirmButtonText: "OK", 
			                });
                    var data = JSON.stringify(response.data);

                    var json = $.parseJSON(data);
                
                if(json.length>0)
                {
                     
                    $('#type_e').val(json[0].doctor_type);

                     $('#name_e').val(json[0].name); 
                    $('#phone_e').val(json[0].mobile_number); 
                    $('#pass_y_e').val(json[0].passing_year);   
                    $('#clinic_add_e').val(json[0].address); 
                   $('#country_e').val(json[0].c_country); 
                     $('#state_e').val(json[0].c_state); 
                    $('#city_e').html('<option>'+json[0].c_city+'</option>');  
                    $('#pincode_e').val(json[0].pincode); 
                    
                   // $('#message')html("Profile has been updated successfully").css('color:green');
                   
                     
                }
               
                 

            }

                
            });





             });


    });

</script>




<script>
$(document).ready(function(){
  $('.btn-group.bootstrap-select').hide();
 $('#country_e').change(function(){
  
  var country_id = $('#country_e').val();
 
  if(country_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>home/sub_fetch_state",
    method:"POST",
    data:{country_id:country_id},
    success:function(data)
    {
      
     $('#state_e').html(data);
     $('#city_e').html('<option value="">Select City</option>');
    }
   });
  }
  else
  {
   $('#state_e').html('<option value="">Select State</option>');
   $('#city_e').html('<option value="">Select City</option>');
  }
 });

 $('#state_e').change(function(){
  var state_id = $('#state_e').val();
  if(state_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>home/sub_fetch_city",
    method:"POST",
    data:{state_id:state_id},
    success:function(data)
    {
      //alert(data);
     $('#city_e').html(data);
    }
   });
  }
  else
  {
   $('#city_e').html('<option value="">Select City</option>');
  }
 });
 
});
</script>

<script>
$(document).ready(function(){
 $(document).on('change', '#file', function(){
     //alert("hii");
    var doctor_id = document.getElementById('doctortime').value;
  var name = document.getElementById("file").files[0].name;
  var form_data = new FormData();
  form_data.append('doctor_id', doctor_id);
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
   alert("Invalid Image File");
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("file").files[0]);
  var f = document.getElementById("file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 2000000)
  {
   alert("Image File Size is very big");
  }
  else
  {
   form_data.append("file", document.getElementById('file').files[0]);
   $.ajax({
    url:"<?php echo base_url(); ?>api/update_profile",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    dataType: "html",
    success:function(response)
    {
                     
       $('#avtarpic').html('<img class="mx-auto img-fluid img-circle d-block" alt="avatar" id="avtar" style="height:100px;width:100px" src="<?php echo base_url(); ?>uploads/profile_pics/'+response+'">') ;
             
    }
   });
  }
 });
});
</script>

<script type="text/javascript">
  function isNum(evt){

  evt =(evt)? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode<48 || charCode >57)){

    return false;
  }
return true;
}
</script>

<script>
$( document ).ready(function() {
    $( "#name" ).keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
});
</script>