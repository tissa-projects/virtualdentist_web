
<?php $this->load->view('frontend/leftsidebar'); ?> 
<link rel="stylesheet" href="<?php echo base_url();?>themes/frontend/css/font_style.css">
<link rel="stylesheet" href="<?php echo base_url();?>themes/frontend/ext_css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>themes/frontend/ext_css/jquery.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>themes/frontend/ext_css/responsive.dataTables.min.css" type="text/css" />
<style>
    .price-plan-name{
        margin-bottom: 25px;
        font-family: Gotham SSm,sans-serif;
        font-size: 22px;
    }
    .currency-symbol{
        font-family: Gotham,sans-serif;
        font-size: 36px;
        line-height: 1;
        font-weight: 300;
    }
    .amount
    {
        font-family: Gotham,sans-serif;
        font-size: 76px;
        font-weight: 100;
        line-height: .7;
    }
    .price-plan-lockup-schedule{
        -webkit-flex-basis: 130px;
        -ms-flex-preferred-size: 130px;
        flex-basis: 130px;
        font-family: Gotham SSm,sans-serif;
        font-size: 15px;
        line-height: 1.5;
        flex: 0 0 90px;
        color: #999;
    }
    .price-plan-summary {
        height: 63px;
        /*margin-bottom: 50px;*/
        font-family: Gotham SSm,sans-serif;
        font-size: 18px;
    }
    .price-plan-feature-list{
        font-family: Gotham SSm,sans-serif;
        font-size: 14px;
        padding-left: 0;
        line-height: 1.75;
        list-style: none;
    }
    .price-plan-feature-item {
        position: relative;
        margin-bottom: 10px;
        padding-left: 30px;
        cursor: help;
    }
    svg {
        position: absolute;
        left: 5px;
        width: 12px;
        height: 9px;
        top: 7px;
    }
    /*.boxes:hover{
      background-color: #038195;
      color: white;
    }*/
    .boxes{
        /*background-color: #437dcc;*/
        background-color: #038195;
        color: white;
    }
    .subscribe{
        font-family: Gotham SSm,sans-serif;
        margin: auto;
        width: 45%;
        padding: 10px;
        font-weight: bold;
        font-size: larger;
        color: #038195 ;
    }
    .subscribe:hover{
        background-color: white;
        color:#038195;
    }
    .lftsdbr{
        background-color: #038195;
    }
    .menu{
        border-bottom: 1px solid beige;
        border-right: 1px solid beige;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .menu a{
        font-size:16px;
        color:white;
    }
    .menu i{
        color: white;
        width: 26px;
    }
    .content{
        margin-right: 15px;
        margin-left: 15px;
    }
    .dshbrd{
        background-color: beige;
        height:100%;
    }
    .nav-tabs>li{
        border: 1px solid white;
    }
    label{
        font-family: serif;
        font-size: 18px;
        font-weight: normal;
        text-align: justify;
    }
    .nav-tabs{
        border-bottom: 0px solid #ddd;
    }
    table th{
        text-align: center;
        color: #993300;
    }
    td{
        padding: 10px;
    }
    .clrheading{
        color: #993300;
        width: 55%;
    }
    .clrtext{
        color: #007FA6;
    }
    .apptheading{
        color: #007FA6;
        font-weight: bold;
        font-size: 21px;
    }
    .dataTables_length,.dataTables_filter {
        padding:15px;
    }
    .dataTables_info {
        padding:0 15px;
    }
    .dataTables_filter {
        float:right;
    }
    .dataTables_length select {
        width:65px;
        padding:5px 8px;
    }
    .dataTables_length label,.dataTables_filter label {
        font-weight:300;
    }
    .dataTables_filter label {
        width:100%;
    }
    .dataTables_filter label input {
        width:78%;
    }
    .border-top {
        border-top:1px solid #ddd;
    }
    .dataTables_paginate.paging_bootstrap.pagination li {
        float:left;
        margin:0 1px;
        border:1px solid #ddd;
        border-radius:3px;
        -webkit-border-radius:3px;
    }
    .dataTables_paginate.paging_bootstrap.pagination li.disabled a {
        color:#c7c7c7;
    }
    .dataTables_paginate.paging_bootstrap.pagination li a {
        color:#797979;
        padding:5px 10px;
        display:inline-block;
    }
    .dataTables_paginate.paging_bootstrap.pagination li:hover a,.dataTables_paginate.paging_bootstrap.pagination li.active a {
        color:#797979;
        background:#eee;
        border-radius:3px;
        -webkit-border-radius:3px;
    }
    .dataTables_paginate.paging_bootstrap.pagination {
        float:right;
        margin-top:-5px;
        margin-bottom:15px;
    }
    .dataTable tr:last-child {
        border-bottom: 1px solid #ddd;
    }
    .pagination ul{
      list-style-type: none;
    }
    .modal-content{
            background-color: white;
    }
    
</style>
<div class="col-lg-10 dshbrd">
    <section class="panel dshbrd">
            <div class="panel-body dshbrd">
      
            <div class="row">
                <h3 class="text-center apptheading">Patient Information</h3>
                <form action="" method="post" style="float:left">
                    <input placeholder="search" type="text" name="name" id="name" style="float:left;height: 34px;padding: 10px;"/>
                    <button type="submit" name="search" id="search" style="height: 34px; "/>
                        <i class="fa fa-search" aria-hidden="true" style="width: 29px;"></i>
                    </button>
                </form>
                <table class="table table-bordered table-responsive" id="example" cellspacing="0" width="100%">
                    <thead>
                    <th>Sr.No</th>
                    <th>Patient ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                            <?php 
                            if(!empty($patients))
                            {
                                $page_sr = intval($page_sr);
                            foreach($patients as $pts)
                            {
                                ?>
                            <tr class="clrtext">
                            <td align="center"><?php echo $page_sr;?></td>
                            <td align="center"><?php echo "P-".$pts->user_id;?></td>
                            <td align="center"><?php echo $pts->firstname.' '.$pts->lastname ; ?></td>
                            <td align="center"><?php echo $pts->email; ?></td>
                            <td align="center"><?php echo $pts->userContact; ?></td>
                            <td align="center">
                                <button class="btn btn-default" onclick="view('<?php echo $pts->unique_id; ?>')">VIEW</button>
                            </td>
                             </tr>
                            <?php  
                            $page_sr++;
                            }
                            }
                            ?>
                            
                    </tbody>
                </table>
                <br>
                <div class="dataTables_paginate paging_bootstrap pagination"><ul><?php echo $links; ?></ul></div>
            </div>
        </div>
    </section>
</div></div></div>
</div>
    
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title apptheading">Patient Details</h4>
            </div>
            <div class="modal-body" id="details_body">
                <div class="row">
                    <div class="col-lg-6"><strong>Patient Name</strong></div>
                    <div class="col-lg-6" id="name_"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><strong>Gender</strong></div>
                    <div class="col-lg-6" id="gender_"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><strong>DOB ( MM-DD-YYYY )</strong></div>
                    <div class="col-lg-6" id="dob_"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><strong>Contact Number</strong></div>
                    <div class="col-lg-6" id="number_"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><strong>Email-ID</strong></div>
                    <div class="col-lg-6" id="email_"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><strong>Preferred Pharmacy Used</strong></div>
                    <div class="col-lg-6" id="pharmacy_" style="word-break: break-all;"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><strong>Pharmacy Phone Number</strong></div>
                    <div class="col-lg-6" id="pharmacy_no" style="word-break: break-all;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
            </div>
        </div>
            
    </div>
</div>
        
<script src="<?php echo base_url();?>themes/frontend/ext_js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>themes/frontend/ext_js/dataTables.responsive.min.js"></script>
<script>  
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
            paging:   false,
            ordering: false,
            info:     false,
            searching: false
        });        
    } );
    function view(id)
    {
        $.ajax({
            method:'GET',
            url:"<?php echo base_url();?>"+"home/get_user/"+id,
            success: function(result){
                var json = $.parseJSON(result)                
                if(json.length>0)
                {
                    $('#name_').html("");
                    $('#gender_').html("");
                    $('#dob_').html("");
                   $('#number_').html("");
                    $('#email_').html("");
                    $('#pharmacy_').html(""); 
                    $('#name_').html(json[0].firstname+' '+json[0].lastname );
                    $('#gender_').html(json[0].user_gender);
                    $('#dob_').html(json[0].user_dob);
                    $('#status_').html(json[0].user_status);
                    $('#number_').html(json[0].usercontact);
                    $('#email_').html(json[0].email);
                    $('#pharmacy_').html(json[0].pharmacy_ph_no);      
                     $('#pharmacy_no').html(json[0].p_ph_no);      
                    $('#myModal').modal("show");
                }
            }           
        });
    }
</script>
