<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>


<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 28px;
    font-size: 13px;
    color: #495057;
}
.logo-header{
  margin-left: -29px;
}

.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/css/font_style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/font-awesome-4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" /> 
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/responsive.dataTables.min.css" type="text/css" /> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      
                 if(sessionStorage.getItem('myprofile') == null){
                    
                     window.location.href = "<?php echo base_url();?>home/index";
                 }

             });
  </script>
  <script>
      $( function() {

        // var unavailableDates = ["30-5-2020","14-5-2020","15-5-2020"];

        // function unavailable(date) {
        //   dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
        //   if ($.inArray(dmy, unavailableDates) < 0) {
        //     return [true,"","Book Now"];
        //   } else {
        //     return [false,"","Booked Out"];
        //   }
        // }

       $("#datepicker").datepicker({ 
        dateFormat: 'yy-mm-dd',
        minDate: 0
        //beforeShowDay: unavailable
    });
      });
  </script>
<style type="text/css">

    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

  .btn-group-lg>.btn, .btn-lg {
    padding: 2px 35px;
    font-size: 13px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.fa-close{
    color:red;
} 

.img{
    height:80px !important;
    width:100px !important;
}
</style>
<?php $this->load->view('frontend/leftsidebar'); ?>

<script type="text/javascript">
    $(document).ready(function(){

        if(sessionStorage.getItem('myprofile') == null){
                    //alert("hii");
                     window.location.href = "<?php echo base_url();?>home/index";
                 }else{


                var data = sessionStorage.getItem('myprofile');

                 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                         $('#doctortime').val(json[0].doctor_id);

                          //$('#proimg').html('<img src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"  alt="Avatar" style="border-radius: 32px;height:50px;width:50px;margin-top: -5px;">') ;
                         $('#log').html('<i class="fa fa-sign-out" aria-hidden="true" style="font-size:30px;color:#2E55FA;margin-top: 6px; cursor:pointer;" id="logout" onclick=logout(this);></i>');
                         $("#dlogin").hide();
                             $("#dpanel").show();

                }  

    }               

               
});

</script>

<script>
$(document).ready(function (){
   //var doctor_id = document.getElementById('doctortime').value;
   $.ajax({
            url: "<?php echo base_url();?>api/get_counsultant",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            //data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                    var data = JSON.stringify(response.data);

                   // alert(data);

                     var obj = JSON.parse(data);
                    var html = ''; 
                    var i;

                for(i=0; i< obj.length; i++){

                j = i+1;           
                  html += '<tr><td>'+j+'</td><td>'+obj[i].reg_number+'</td><td>'+obj[i].name+'</td><td>'+obj[i].email_id+'</td><td>'+obj[i].specialization+'</td><td>'+obj[i].mobile_number+'</td><td>'+obj[i].c_city+'</td><td align="center"><button class="btn btn-primary white btn-block" data-toggle="modal" data-target="#myModal" onclick="view('+obj[i].doctor_id+')">VIEW</button></td></tr>';                 
                }

                   
           $('#tbody').html(html);
                   
            }

                
            });
        
});
</script>
  <input type="hidden" id="doctortime" >
<div class="col-md-10 dshbrd" >
    
            <div class="row" style="">
                <div class="col-md-12"><center><h2><b style="color:#FFFFFF;">Consultant list</b><h2></center></div>
                <div class="col-md-12">
                    <div class="table-responsive"> 
                         <table class="table table-bordered display" id="example2" cellspacing="0" width="100%" style="font-family: Lustria;">
                                <thead style="border: 1px solid rgb(12,11,11); ">
                                    <th>Sr.No</th>
                                    <th>Reg Number</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Specialization</th>
                                    <th>Mobile</th>
                                     <th>City</th>
                                    <th>Action</th>
                                    
                                </thead>
                                </thead>
                                <tbody id="tbody">             
                                      
                                    
                                                             
                                </tbody>
                            </table>
                       
                    </div>
                    
                </div>
               
                <br>
                
            </div>
       
</div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
       
        <div class="modal-content">
            <!-- <div class="modal-header">
                
                
            </div> -->
            <div class="modal-body">
                <!-- <div class="row">
                   
                    <div class="col-lg-6" id="name_"></div>
                    <div class="col-lg-6" id="email_"></div>
                </div> -->

                <div class="row">
                                    <div class="col-xs-12 col-sm-3 center" style="background-color: #ccc">
                                        <span class="profile-picture" id="avtar">
                                           
                                        </span><span>Profile Picture</span><hr>

                                        <span class="profile-picture" id="avtar2">
                                        
                                           
                                        </span><span>Degree</span><hr>
                                        <span class="profile-picture" id="avtar3">
                                        
                                           
                                        </span><span>Certificate</span><hr>

                                        <div class="space space-4"></div>



                                       
                                    </div><!-- /.col -->

                                    <div class="col-xs-12 col-sm-9"><button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="blue">
                                            <span class="middle" id="name_"></span> 
                                            <!--(<span id="doctor_type"></span>)-->
                                        </h4>

                                        <div class="profile-user-info">

                                          <div class="profile-info-row">
                                                <div class="profile-info-name col-md-5" ><b> Contact </b></div>

                                                <div class="profile-info-value col-md-7">
                                                    <span id="mobile_">789456123</span>
                                                    <span id="email_"></span><br> <br>
                                                </div>
                                            </div>
                                            <br> <br>
                                            
                                            
                                            <div class="profile-info-row">
                                                <div class="profile-info-name col-md-5"><b>Passing Year</b></div>

                                                <div class="profile-info-value col-md-7">
                                                    
                                                    <span id="passing_year"></span>(<span id="year" ></span>&nbsp;Years of Experience)<br> <br>
                                                </div>
                                            </div> <br> <br>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name col-md-5"><b> Address</b> </div>

                                                <div class="profile-info-value col-md-7">
                                                    <span id="clinic_address"></span>,
                                                    <span id="country"></span>,
                                                    <span id="state"></span>,
                                                    <span id="city"></span>,
                                                    <span id="pincode"></span><br> <br>
                                                    
                                                </div>
                                            </div><br> <br>

                                             <div class="profile-info-row">
                                                <div class="profile-info-name col-md-5"><b>Specialization</b></div>

                                                <div class="profile-info-value col-md-7">
                                                    
                                                    <span id="specialization"></span><br> <br>
                                                   
                                                </div>
                                            </div><br> <br>

                                            
                                            
                                        </div>

                                        <div class="hr hr-8 dotted"></div>

                                       
                                    </div><!-- /.col -->
                                </div>
               
                
            </div>
           
        </div>
            
    </div>
</div>


<script>  
    

    function view(id)
    {
            var doctor_id = id;

            $.ajax({
            url: "<?php echo base_url();?>api/getcounsultant",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){

                var data = JSON.stringify(response.data); 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                   $('#reg').html(json[0].reg_number);
                    $('#name_').html(json[0].name);
                    $('#email_').html(json[0].email_id);
                    $('#mobile_').html(json[0].mobile_number);
                    $('#doctor_type').html(json[0].doctor_type);
                    $('#clinic_address').html(json[0].c_address);
                    $('#fees').html(json[0].counsult_fee);
                    $('#establish_year').html(json[0].establish_year);      
                     $('#country').html(json[0].c_country);  
                     $('#state').html(json[0].c_state);  
                     $('#city').html(json[0].c_city); 
                     $('#pincode').html(json[0].c_pincode); 
                     $('#passing_year').html(json[0].passing_year);
                     var curyear = new Date().getFullYear();

                         var exp =  curyear - json[0].passing_year;

                         $('#year').html(exp);
                     $('#doctor_type').html(json[0].doctor_type); 
                      $('#specialization').html(json[0].specialization); 
                      $('#avtar').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'" data-lightbox="example-1"><img class="example-image img img-responsive" alt=" Avatar" id="avatar2" src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"></a>') ;
                      $('#avtar2').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/degree/'+json[0].degree_cert+'" data-lightbox="example-2"><img class="example-image img img-responsive" alt=" Avatar"  src="<?php echo base_url(); ?>uploads/degree/'+json[0].degree_cert+'"></a>') ;
                      $('#avtar3').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/degree/'+json[0].reg_cert+'" data-lightbox="example-3"><img class="example-image img img-responsive" alt=" Avatar"  src="<?php echo base_url(); ?>uploads/degree/'+json[0].reg_cert+'"></a>') ; 
                    $('#myModal').modal("show");
                }

                
            }           
        });
    }
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#example2').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>

<script src="<?php echo base_url();?>assets/js/lightbox.min.js"></script> 
