<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 30px !important;
    font-size: 13px;
    color: #495057;
}

select{
  height: 30px !important;
}

.logo-header{
  margin-left: -29px;
}
.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}

select.bs-select-hidden, select.selectpicker {
    display: block!important;
}
.bootstrap-select{
   display: none!important;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      
                 if(sessionStorage.getItem('myprofile') == null){
                    
                     window.location.href = "<?php echo base_url();?>home/index";
                 }

             });
  </script>
  <script>
      $( function() {

       $("#datepicker").datepicker({ 
        dateFormat: 'yy-mm-dd',
        minDate: 0
        //beforeShowDay: unavailable
    });
      });
  </script>
  
<style type="text/css">

    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

.badge {
    padding: 7px 7px;
    font-size: 10px;
    color: #fff;
    background-color: #2E55FA;
}
.panel-body {
    padding: 7px;
    margin-bottom:3px;
}
.panel {
    width:380px;
}
  .btn-group-lg>.btn, .btn-lg {
    padding: 2px 35px;
    font-size: 13px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.fa-close{
    color:red;
} 
</style>
<?php $this->load->view('frontend/leftsidebar'); ?>



<script type="text/javascript">
    $(document).ready(function(){

        if(sessionStorage.getItem('myprofile') == null){
                    //alert("hii");
                     window.location.href = "<?php echo base_url();?>home/index";
                 }else{


                var data = sessionStorage.getItem('myprofile');
                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                         $('#doctortime').val(json[0].doctor_id);
                       

                          // $('#proimg').html('<a href="<?php echo base_url(); ?>doctor/index"><img src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"  alt="Avatar" style="border-radius: 32px;height:50px;width:50px;margin-top: -17px;"></a>') ;
                         $('#log').html('<i class="fa fa-sign-out" aria-hidden="true" style="font-size:30px;color:#2E55FA;margin-top: 6px; cursor:pointer;" id="logout" onclick=logout(this);></i>');

                            $("#dlogin").hide();
                             $("#dpanel").show();
                }  

    }               

               
});

</script>



<script>
$(document).ready(function (){
   var doctor_id = document.getElementById('doctortime').value;
   $.ajax({
            url: "<?php echo base_url();?>api/get_holiday",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                    var data = JSON.stringify(response.data);

                    // document.getElementById("p2").innerHTML = 'toDateString: '+ date.toDateString();
                     var obj = JSON.parse(data);
                    var html = ''; 
                    var i;

                for(i=0; i< obj.length; i++){
                    var date = new Date(obj[i].holiday_date) ;

                    var d = date.toDateString();

                    d = d.split(' ');                  

                  html += '<div id="day_'+obj[i].holiday_id+'"><div class="col-md-6" ><span id="p2">'+d[2] + ' ' + d[1] + ' ' + d[3]+'</span></div><div class="col-md-6" style="text-align:right;"><i onclick="myFunction('+obj[i].holiday_id+')" class="fa fa-close"></i></div><hr style="border-top: 1px solid gray;"></div>';                 
                }

                   
            $('#holy').html(html);
                   
            }

                
            });


        $.ajax({
            url: "<?php echo base_url();?>api/get_schedule",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                    var data = JSON.stringify(response.data);
                    
                     var obj = JSON.parse(data);
                    var html = ''; 
                    var i;
                    
                    if(obj.length == 0){
                        
                        html += ' No Schedule Available';
                        
                    }else{

                for(i=0; i< obj.length; i++){
                                    

                  html += '<div class="col-md-6"><b>Morning Time</b></div><div class="col-md-6"><br></div><div class="col-md-6">Start Time: <span id="smtime">'+obj[i].mor_start+'</span></div><div class="col-md-6">End Time: <span id="emtime">'+obj[i].mor_end+'</span></div><hr style="border-top: 1px solid gray;margin: 6px;padding: 4px;"><div class="col-md-6"><b>Evening Time</b></div><div class="col-md-6"><br></div><div class="col-md-6">Start Time: <span id="setime">'+obj[i].eve_start+'</span></div><div class="col-md-6">End Time: <span id="eetime">'+obj[i].eve_end+'</span></div><hr style="border-top: 1px solid gray;margin: 6px;padding: 4px;"><div class="col-md-12"><b>Time Slot:</b>&nbsp;&nbsp;<span id="allt">'+obj[i].time_slote+'</span>&nbsp;&nbsp;Minutes</div>';
                    
                }

                    }  
            $('#schedule').html(html);

            }

                
            });


        $.ajax({
            url: "<?php echo base_url();?>api/get_doctor",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){
                    var data = JSON.stringify(response.data);

                     var json = $.parseJSON(data)                
                if(json.length>0)
                {

                     $('#doctortime').val(json[0].doctor_id);
                        $('#dname').html(json[0].name);
                         $('#regnum').html(json[0].reg_number); 
                         $('#regemail').html(json[0].email_id); 
                         $('#passing').html(json[0].passing_year); 
                         $('#mobile').html(json[0].mobile_number);
                         $('#specializationlabel').html(json[0].specialization);

                         var curyear = new Date().getFullYear();

                         var exp =  curyear - json[0].passing_year;

                         $('#year').html(exp);
                         $('#clinic').html(json[0].clinic_name);
                         $('#address').html(json[0].address);
                         $('#esatblish').html(json[0].establish_year);
                         $('#city').html(json[0].city);
                         $('#pincode').html(json[0].pincode);

                          $('#avtarpic').html('<img class="mx-auto img-fluid img-circle d-block" style="height:100px;width:100px" alt="avatar" id="avtar" src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'">') ;


                    $('#type_e').val(json[0].doctor_type);
                    $('#name_e').val(json[0].name); 
                    $('#phone_e').val(json[0].mobile_number); 
                    $('#pass_y_e').val(json[0].passing_year); 
                    $('#clinic_name_e').val(json[0].clinic_name); 
                     $('#clinic_year_e').val(json[0].establish_year); 
                    $('#clinic_add_e').val(json[0].address); 
                    $('#fee_e').val(json[0].counsult_fee); 
                    $('#country_e').val(json[0].country); 
                   $('#state_e').val(json[0].state); 
                    $('#city_e').val(json[0].city);  
                    $('#pincode_e').val(json[0].pincode);
                    $('#clinic_id_e').val(json[0].clinic_id);
                    $('#specialization').val(json[0].specialization);

                }

            }

                
            });


     var state_id = document.getElementById('state_e').value;

    // $.ajax({
    //     url:"<?php echo base_url(); ?>home/sub_fetch_city",
    //     method:"POST",
    //     data:{state_id:state_id},
    //     success:function(data)
    //     {
    //       //alert(data);
    //      $('#city_e').html(data);
    //     }
    //   });


});
</script> 
<!--content part-->
<div class="col-md-10 dshbrd">
     
    <div class="row">
        <div class="col-md-7">
           <center style="padding: 5px;color: blue;"><h4><i>Welcome Doctor</i></h4>
            <span id="avtarpic"></span>
                                <label class="custom-file" style="cursor: pointer; color:blue;">
                                    
                                    <input type="file" id="file" class="custom-file-input">
                                    <span class="custom-file-control" id="changepic">Change Photo</span>
                                   
                                </label>
              </center>

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Profile</a></li>
            <li><a data-toggle="tab" href="#menu1">Add schedule </a></li>
            <li><a data-toggle="tab" href="#menu2">Edit profile</a></li>
          </ul>

          <div class="tab-content">
            <div id="home" class="tab-pane fade in active" style="padding:20px;">

              <h4><b>Dr. </b><b id="dname"></b></h4>

              <div class="col-md-4"> <a href="#" class="badge badge-dark badge-pill" style="width: 196px;">Registration number</a><br>
                <h6 id="regnum" style="font-weight: bolder;padding-left: 70px;"></h6></div>
             <div class="col-md-4"> <a href="#" class="badge badge-dark badge-pill" style="width: 196px;">&nbsp;&nbsp;&nbsp;Your Email id&nbsp;&nbsp;&nbsp;</a><br>
                <h6 id="regemail" style="font-weight: bolder;padding-left: 5px;"></h6></div>
            <div class="col-md-4"><a href="#" class="badge badge-dark badge-pill" style="width: 196px;"> Your Mobile number</a><br>
                <h6 id="mobile" style="font-weight: bolder;padding-left: 50px;"></h6>
            </div>
            
            <br><br>
              <h4><b>About</b></h4>
              <div style="font-weight: bolder;">
                <sapn id="dctype">Private Practitioner</sapn><br>
                <span id="passing" ></span>(<span id="year" ></span>&nbsp;Years Experience Overall)<br>
                Specialization : <sapn id="specializationlabel"></span><br>
              </div><br>

              <h4><b>Address</b></h4>
              <div style="font-weight: bolder;">Clinic name:&nbsp;&nbsp;<span id="clinic" ></span>(<span id="esatblish" ></span>)<br>Clinic Address:&nbsp;&nbsp;<sapn id="address" ></sapn><br><sapn id="city" ></sapn>,&nbsp;<sapn id="pincode" ></sapn><br></div><br>


              <br><br><br>
            </div>
            <form id="menu1" class="tab-pane fade" method="post" style="padding:20px;"> 
                <!-- <form method="post" action="" class="form-forizontal"> -->
                    <input type="hidden" id="doctortime" >
                   <center> <span id="message"></span></center>
              <h5><b>Morning Time</b></h5>
              <div class="col-md-2">Start Time:</div>
              <div class="col-md-4 form-group">
                <select class="form-control" id="mor_start" name="mor_start">
                    <option value="0">Select Time</option>
                     <option value="07:00 AM">07:00 AM</option>
                     <option value="08:00 AM">08:00 AM</option>
                     <option value="09:00 AM">09:00 AM</option>
                      <option value="10:00 AM">10:00 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
               </select>
              </div>
              <div class="col-md-2">End Time:</div>
              <div class="col-md-4 form-group" > 
                <select class="form-control" id="mor_end" name="mor_end" >
                     <option value="0">Select Time</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="01:00 PM">01:00 PM</option>
                    <option value="02:00 PM">02:00 PM</option>
                    <option value="03:00 PM">03:00 PM</option>
               </select>
              </div>
               <h5><b>Evening Time</b></h5>
              <div class="col-md-2">Start Time:</div>
              <div class="col-md-4 form-group" >
                <select class="form-control" id="eve_start" name="eve_start">
                     <option value="0">Select Time</option>
                    <option value="03:00 PM">03:00 PM</option>
                    <option value="04:00 PM">04:00 PM</option>
                    <option value="05:00 PM">05:00 PM</option>
                    <option value="06:00 PM">06:00 PM</option>
                    <option value="07:00 PM">07:00 PM</option>
               </select>
              </div>
              <div class="col-md-2">End Time:</div>
              <div class="col-md-4 form-group" >
                <select class="form-control" id="eve_end" name="eve_end">
                     <option value="0">Select Time</option>
                    <option value="08:00 PM">08:00 PM</option>
                    <option value="09:00 PM">09:00 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
               </select>
              </div>
              <div class="col-md-2">Time Slot:</div> 
              <div class="col-md-4 form-group"><input type="text" name="time_slote" id="time_slote" class="form-control" placeholder="ex.30 minutes" onkeypress="return isNum(event)" maxlength="2" pattern="\d{2}"></div>
               <div class="col-md-6"></div>
                <div class="col-md-3"></div> <div class="col-md-3"></div> <div class="col-md-2"></div>
                 <div class="col-md-4"><input type="button" value="Save" id="saveworktime" class="button btn-success btn-lg" onclick="addSchedule();"></div><br>
            
                 <hr style="border-top: 1px solid blue;">
                  <center><div id="message1"></div></center>
                 <div class="col-md-4">Enter Your Holiday: </div>
                 <div class="col-md-4 form-group"><input type="text" name="datepicker" id="datepicker"  class="form-control"></div><div class="col-md-4"></div>
                 <div class="col-md-3"></div> <div class="col-md-3"></div> <div class="col-md-2"></div>
                 <div class="col-md-4"><a id="saveworktime1" class="button btn-success btn-lg" onclick="adddate();" style="color:white;padding: 5px;padding-left: 39px;padding-right: 36px;cursor:pointer"> Save</a></div><br><br>
            <br><br><br>
            </form>
            <div id="menu2" class="tab-pane fade" style="padding:20px;">
              <form action="" id="edit" class="form-horizontal " role ="form"  style="display: block;padding:20px;">
                  <div class="row">

                             <input type="hidden" class="form-control" id="clinic_id_e" name="clinic_id_e"  />
                             <input type="hidden" class="form-control" id="type_e" name="type_e"  />
                            
                            <div class="col-md-4 form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" id="name_e" name="name_e"  />
                            </div>
                            
                            <div class="col-md-4 form-group">
                                <label for="Type">Mobile Number</label>
                                <input type="text" class="form-control" id="phone_e" name="phone_e" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  />
                            </div>
                           
                            <div class="col-md-4 form-group">
                                <label for="passing_year">Passing year</label>
                                <input type="text" class="form-control" id="pass_y_e" name="pass_y_e" onkeypress="return isNum(event)" />
                            </div>
                           <!--  <div class="col-md-4 form-group" id="counsultantadd">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" />
                            </div> -->
                             <div class="col-md-4 form-group" id="namec" >
                                <label for="email">Clinic Name</label>
                                <input type="text" class="form-control" id="clinic_name_e" name="clinic_name_e" />
                            </div>
                            <div class="col-md-5 form-group" id="yearc" >
                                <label for="email">Clinic Establishment Year</label>
                                <input type="text" class="form-control" id="clinic_year_e" name="clinic_year_e" onkeypress="return isNum(event)"/>
                            </div>
                             <div class="col-md-4 form-group" id="fees">
                                <label for="email"> Counsultation Fees</label>
                                <input type="text" class="form-control" id="fee_e" name="fee_e"  />
                            </div>
                            <div class="col-md-4 form-group" id="addc">
                                <label for="email">Clinic Address</label>
                                <input type="text" class="form-control"  id="clinic_add_e"  name="clinic_add_e" />
                            </div>
                        <div class="col-md-4 form-group">
                                <label for="specialization">Specialization</label>
                                <select name="specialization" id="specialization" class="form-control" required>
                                  <option value="">Select Specialization</option>
                                  <option value="Oral Pathology">Oral Pathology</option>
                                  <option value="Forensic Odontology">Forensic Odontology</option>
                                  <option value="Oral Physiotherapy">Oral Physiotherapy</option>
                                  <option value="Oral Nutrition And Detects">Oral Nutrition And Detects</option>
                                  <option value="Oral Medicine">Oral Medicine</option>
                                  <option value="Oral Radiology">Oral Radiology</option>
                                  <option value="Cranio Oro Maxillofacial Surgery">Cranio Oro Maxillofacial Surgery</option>
                                  <option value="Periodontics">Periodontics</option>
                                  <option value="Operative Dentistry And Endodontics">Operative Dentistry And Endodontics</option>
                                  <option value="Orthodontics">Orthodontics</option>
                                  <option value="Prosthodontics">Prosthodontics</option>
                                  <option value="Implantology">Implantology</option>
                                  <option value="Aesthetic And Cosmetic Dentistry">Aesthetic And Cosmetic Dentistry</option>
                                  <option value="Dental Lasers">Dental Lasers</option>
                                  <option value="Minimal Intervention Dentistry">Minimal Intervention Dentistry</option>
                                  <option value="Sedative Dentistry">Sedative Dentistry</option>
                                  <option value="Dental Sleep Medicine">Dental Sleep Medicine</option>
                                  <option value="Pedodontics">Pedodontics</option>
                                  <option value="Public Health Dentistry">Public Health Dentistry</option>
                                  <option value="General Dentistry">General Dentistry</option>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">Country</label>
                               
                                   <select name="country_e" id="country_e" class="form-control">
                                    <option value="0">Select Country</option>
                                    <?php
                                    foreach($country as $row)
                                    {
                                     echo '<option value="'.$row->country_name.'">'.$row->country_name.'</option>';
                                    }
                                    ?>
                                   </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">State</label>
                               
                                      <select name="state_e" id="state_e" class="form-control">
                                         <?php
                                    foreach($state as $row)
                                    {
                                     echo '<option value="'.$row->state_name.'">'.$row->state_name.'</option>';
                                    }
                                    ?>
                                      </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">City</label>
                               
                                        <select name="city_e" id="city_e" class="form-control"  >
                                            
                                            <?php
                                    foreach($city as $row)
                                    {
                                     echo '<option value="'.$row->city_name.'">'.$row->city_name.'</option>';
                                    }
                                    ?>
                  
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="zipcode">Pin code</label>
                                <input type="text" class="form-control" id="pincode_e" name="pincode_e" onkeypress="return isNum(event)"  />
                            </div>
                    </div>
                        <input type="button" id="submit"  value="submit" class="button btn-success btn-lg" style=" height:40px; color: white; font-size: 15px; border-radius: 20px;" onclick="editProfile();"/>
                            
             </form>
            </div>
            
          </div>
        

        </div>
        <div class="col-md-5" style="padding: 18px;">
            <div class="panel panel-default" style="border:1px solid blue;">
                <div class="panel-heading" style="background-color: #2E55FA; color: white;"><center>Time Schedule</center></div>
                <div class="panel-body" id="schedule">
                   <!--<div class="col-md-6"><b>Morning Time</b></div>-->
                   <!-- <div class="col-md-6"><br></div><br>-->
                   <!-- <div class="col-md-6">Start Time: <span id="smtime"></span></div>-->
                   <!-- <div class="col-md-6">End Time: <span id="emtime"></span></div><br>-->
                   <!-- <hr style="border-top: 1px solid gray;">-->
                   <!-- <div class="col-md-6"><b>Evening Time</b></div>-->
                   <!-- <div class="col-md-6"><br></div><br>-->
                   <!-- <div class="col-md-6">Start Time: <span id="setime"></span></div>-->
                   <!-- <div class="col-md-6">End Time: <span id="eetime"></span></div><br>-->
                   <!--  <hr style="border-top: 1px solid gray;">-->
                   <!--  <div class="col-md-12"><b>Time Slot:</b>&nbsp;&nbsp;<span id="allt"></span>&nbsp;&nbsp;Minutes</div>-->
                </div>
            </div>

             <div class="panel panel-default" style="border:1px solid blue;">
                <div class="panel-heading" style="background-color: #2E55FA; color: white;"><center>Holidays</center></div>
                <div class="panel-body scrollable" id="holy">
                  
                    
                    
                </div>
            </div>

        </div>
    </div>
</div>

    
</div>
 
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script type="text/javascript">
    function addSchedule() {
        var mor_start = document.getElementById('mor_start').value;
        var mor_end = document.getElementById('mor_end').value;
        var eve_start = document.getElementById('eve_start').value;
        var eve_end = document.getElementById('eve_end').value;
        var time_slote = document.getElementById('time_slote').value;
        var doctor_id = document.getElementById('doctortime').value;

        if(mor_start == '0'){
          $('#message').html("<i>Please enter start time</i>").css('color', 'red');
          return false;
        }else if(mor_end == '0'){
          $('#message').html("<i>Please enter end time</i>").css('color', 'red');
          return false;
        }else if(eve_start == '0'){
           $('#message').html("<i>Please enter start time</i>").css('color', 'red');
          return false;
        }else if(eve_end == '0'){
           $('#message').html("<i>Please enter end time</i>").css('color', 'red');
          return false;
        }else if(time_slote == ''){
           $('#message').html("<i>Please enter time slot</i>").css('color', 'red');
          return false;
        }else{
            $('#message').html("");
         $.ajax({
            url: "<?php echo base_url();?>api/add_schedule",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({mor_start:mor_start,mor_end:mor_end,eve_start:eve_start,eve_end:eve_end,time_slote:time_slote,doctor_id:doctor_id}),
                 success: function(response){
                    var data = JSON.stringify(response.data);
                    
                     var obj = JSON.parse(data);
                    var html = ''; 
                    var i;
                    
                    if(obj.length == 0){
                        
                        html += ' No Schedule Available';
                        
                    }else{

                for(i=0; i< obj.length; i++){
                                    

                 html += '<div class="col-md-6"><b>Morning Time</b></div><div class="col-md-6"><br></div><div class="col-md-6">Start Time: <span id="smtime">'+obj[i].mor_start+'</span></div><div class="col-md-6">End Time: <span id="emtime">'+obj[i].mor_end+'</span></div><hr style="border-top: 1px solid gray;margin: 6px;padding: 4px;"><div class="col-md-6"><b>Evening Time</b></div><div class="col-md-6"><br></div><div class="col-md-6">Start Time: <span id="setime">'+obj[i].eve_start+'</span></div><div class="col-md-6">End Time: <span id="eetime">'+obj[i].eve_end+'</span></div><hr style="border-top: 1px solid gray;margin: 6px;padding: 4px;"><div class="col-md-12"><b>Time Slot:</b>&nbsp;&nbsp;<span id="allt">'+obj[i].time_slote+'</span>&nbsp;&nbsp;Minutes</div>';
                    
                }

                    }  
            $('#schedule').html(html);



            
                        
                         Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: "Time schedule has been added successfully",  
                        confirmButtonText: "OK", 
                      });
                     

            }

                
            });
        }
    }

    function editProfile() {
        var type = document.getElementById('type_e').value;
        var name = document.getElementById('name_e').value;
        var phone = document.getElementById('phone_e').value;
        var pass_y = document.getElementById('pass_y_e').value;
        var clinic_name = document.getElementById('clinic_name_e').value;
        var clinic_year = document.getElementById('clinic_year_e').value;
        var clinic_add = document.getElementById('clinic_add_e').value;
        var fee = document.getElementById('fee_e').value;
        var country1 = document.getElementById('country_e').value;
        var state1 = document.getElementById('state_e').value;
        var city1 = document.getElementById('city_e').value;
        var zipcode = document.getElementById('pincode_e').value;
        var doctor_id = document.getElementById('doctortime').value;
        var clinic_id = document.getElementById('clinic_id_e').value;
        var specialization = document.getElementById('specialization').value;

        $.ajax({
            url: "<?php echo base_url();?>api/update_doctor",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({type:type,name:name,phone:phone,pass_y:pass_y, clinic_name:clinic_name,clinic_year:clinic_year,fee:fee,clinic_add:clinic_add,country1:country1,state1:state1,city1:city1,zipcode:zipcode,doctor_id:doctor_id,clinic_id:clinic_id,specialization:specialization}),
                success: function(response){
                    
                    Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: "Profile has been updated successfully",  
                        confirmButtonText: "OK", 
                      });
                      
                    var data = JSON.stringify(response.data);


                    var json = $.parseJSON(data)                
                if(json.length>0)
                {
                    $('#type_e').val(json[0].doctor_type);

                     $('#name_e').val(json[0].name); 
                    $('#phone_e').val(json[0].mobile_number); 
                    $('#pass_y_e').val(json[0].passing_year); 
                    $('#clinic_name_e').val(json[0].clinic_name); 
                     $('#clinic_year_e').val(json[0].establish_year); 
                    $('#clinic_add_e').val(json[0].address); 
                    $('#fee_e').val(json[0].counsult_fee); 
                    $('#country_e').val(json[0].country); 
                    $('#state_e').val(json[0].state);  
                    $('#city_e').val(json[0].city);  
                    $('#pincode_e').val(json[0].pincode); 
                    $('#specialization').val(json[0].specialization); 

                }
                
                 Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: "Profile has been updated successfully",  
                        confirmButtonText: "OK", 
                      });

            }

                
            });
    }
</script>

<script type="text/javascript">
    function adddate(){
       var doctor_id = document.getElementById('doctortime').value;
        var holiday_date = document.getElementById('datepicker').value;
        
        if(holiday_date == ''){
          $('#message1').html("<i>Please enter holiday date</i>").css('color', 'red');
          return false;
        }else{
            $('#message1').html("");
        $.ajax({
            url: "<?php echo base_url();?>api/add_holiday",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({holiday_date:holiday_date,doctor_id:doctor_id}),
                success: function(response){
                    
                     if(response.status == true){
                    var date = JSON.stringify(response.data);

                    
                var data = JSON.stringify(response.data);

                    // document.getElementById("p2").innerHTML = 'toDateString: '+ date.toDateString();
                     var obj = JSON.parse(data);
                    var html = ''; 
                    var i;

                for(i=0; i< obj.length; i++){
                    var date = new Date(obj[i].holiday_date) ;

                    var d = date.toDateString();

                    d = d.split(' ');                  

                  html += '<div id="day_'+obj[i].holiday_id+'"><div class="col-md-6" ><span id="p2">'+d[2] + ' ' + d[1] + ' ' + d[3]+'</span></div><div class="col-md-6" style="text-align:right;"><i onclick="myFunction('+obj[i].holiday_id+')" class="fa fa-close"></i></div><hr style="border-top: 1px solid gray;"></div>';                 
                }

                   
            $('#holy').html(html);
                   
            $('#datepicker').val('');
            
            
             Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: "Holiday date has been added successfully",  
                        confirmButtonText: "OK", 
                      });
                } 
                      
                     if(response.status == false){

                  var msg = JSON.stringify(response.msg);
        
                      Swal.fire({
                                title: "<i style='color:orange'>Sorry...!!</i>", 
                                html: msg,  
                                confirmButtonText: "OK", 
                              });
                              
                               $('#datepicker').val('');
                      
              }          

            }

                
            });
    }

    }

</script>
<script>
function myFunction(x) {
    var holiday_id = x;

     $.ajax({
            url: "<?php echo base_url();?>api/delete_holiday",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({holiday_id:holiday_id}),
                success: function(response){
                    var date = JSON.stringify(response.data);

                   // alert(date);

                    $('#day_'+x).remove();
                    
                    Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: "Holiday has been deleted successfully",  
                        confirmButtonText: "OK", 
                      });

            }

                
            });

  
}
</script>

<script>
$(document).ready(function(){
  $('.btn-group.bootstrap-select').hide();
 $('#country_e').change(function(){
  
  var country_id = $('#country_e').val();
 
  if(country_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>home/sub_fetch_state",
    method:"POST",
    data:{country_id:country_id},
    success:function(data)
    {
      
     $('#state_e').html(data);
     $('#city_e').html('<option value="">Select City</option>');
    }
   });
  }
  else
  {
   $('#state_e').html('<option value="">Select State</option>');
   $('#city_e').html('<option value="">Select City</option>');
  }
 });

 $('#state_e').change(function(){
  var state_id = $('#state_e').val();
  if(state_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>home/sub_fetch_city",
    method:"POST",
    data:{state_id:state_id},
    success:function(data)
    {
      
     $('#city_e').html(data);
    }
   });
  }
  else
  {
   $('#city_e').html('<option value="">Select City</option>');
  }
 });
 
});
</script>

<script>
$(document).ready(function(){
 $(document).on('change', '#file', function(){
    var doctor_id = document.getElementById('doctortime').value;
  var name = document.getElementById("file").files[0].name;
  var form_data = new FormData();
  form_data.append('doctor_id', doctor_id);
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
   alert("Invalid Image File");
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("file").files[0]);
  var f = document.getElementById("file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 2000000)
  {
   alert("Image File Size is very big");
  }
  else
  {
    //alert(form_data);
   form_data.append("file", document.getElementById('file').files[0]);
   $.ajax({
    url:"<?php echo base_url(); ?>api/update_profile",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    dataType: "html",
    success:function(response)
    {
         $('#avtarpic').html('<img class="mx-auto img-fluid img-circle d-block" alt="avatar" id="avtar" style="height:100px;width:100px" src="<?php echo base_url(); ?>uploads/profile_pics/'+response+'">') ;
             
    }
   });
  }
 });
});
</script>



<script type="text/javascript">
  function isNum(evt){

  evt =(evt)? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode<48 || charCode >57)){

    return false;
  }
return true;
}
</script>

<script>
$( document ).ready(function() {
    $( "#name" ).keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
});
</script>