<head>
<meta charset="UTF-8">
<title>Online Dental Services | Oral Care | Virtual Dentist</title>

<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="page-content bg-white"><!-- inner page banner -->
	<div class="dez-bnr-inr dez-bnr-inr-md" style="background-image:url(<?php echo base_url();?>assets/images/main-slider/slide2.jpg);">
		<div class="container">
			<div class="dez-bnr-inr-entry align-m ">
				<div class="find-job-bx">
					<h2>Online Consultation</h2>

					<p style="font-size:20px; color:#000;">Welcome to Virtual Dentist. To make the painful experience of getting the dental implant or root canal easier for you, we create a comfortable environment for all our patients. ..</p>

					<form class="dezPlaceAni">
						<div class="row">
							<div class="col-lg-4 col-md-6">
								<div class="form-group"><label>Enter State</label>

									<div class="input-group"><input class="form-control" placeholder="" type="text" />
										<div class="input-group-append"></div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6">
								<div class="form-group"><label>Enter City</label>
									<div class="input-group"><input class="form-control" placeholder="" type="text" />
										<div class="input-group-append"></div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6">
								<div class="form-group"><select><option>Select Services</option><option>Cosmetic Dentistry</option><option>Dental Implant</option><option>Laser Dentistry</option><option>Full Mouth Rehabilitation</option><option>Teeth Whitening</option><option>Painless Root Canal Treatment</option><option>Orthodontics Emphasis</option><option>General Dentistry</option><option>Others</option> </select></div>
							</div>

							<div class="col-lg-2 col-md-6">
								<button class="site-button btn-block" type="submit"><a href="https://www.payumoney.com/paybypayumoney/#/BDCF7E9C2DCBE9B83554C3B7FA786DAD" style="color:#fff;" target="_blank">Pay Now</a></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- inner page banner END -->

	<div class="content-block">
		<div class="section-full content-inner overlay-white-middle">
			<div class="container">
				<div class="row align-items-center m-b50">
					<div class="col-md-12 col-lg-8 m-b20">
						<p class="m-b15">We always believe in second opinion, We at Virtual Dentist Provide Online Consultation related to your all dental issues or disease.<br />
						<br />
						We believe in providing a comfortable environment for all our patients. We offer a wide range of dental services at a cost-effective price that would help our patients get the confidence and a bright smile. We offer best dental care and oral care services through our online portal. You can book an instant appointment with the dentists and have a telephonic conversation with them.<br />
						<br />
						You require to fill up your details Above form and then our Doctor will call you on the given timing.</p>
					</div>

					<div class="col-md-12 col-lg-4 about"><img alt="" src="<?php echo base_url();?>assets/images/our-work/pic1.jpg" /></div>

				</div>

				<h5>Terms and conditions: Our Do&#39;s and Don&rsquo;ts.</h5>

				<h6>Do&#39;s:</h6>

				<p>What we provide? - We provide Dental counselling.&nbsp;We educate the patient about dental treatment that is needed.&nbsp;<br />
				We Relieve patient&#39;s dental anxiety and apprehension.<br />
				Will be providing second opinion.<br />
				Will guide them to the right dentist.<br />
				Answer parent&#39;s worrying dental questions about their child&#39;s teeth falling and new teeth coming up in the mouth.<br />
				Also provide instant elderly dental care.<br />
				Working towards future caries/cavity free generation.</p>

				<h6>Don&rsquo;ts:</h6>
				No prescription or medical advice given through phone or online.<br />
				Also, this service is not in case of emergency dental treatments.
			</div>
		</div>
	</div>
<!-- contact area END -->
</div>