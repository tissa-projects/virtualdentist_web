<head>
<meta charset="UTF-8">
<title>Online Portal for Doctors| Doctors Login | Virtual Dentist</title>
<meta name="Description" content="Virtual Dentist is ultimate online portal doctors, where doctors can login and give their dental care virtually.">
<meta name="keywords" contents="online portal for doctors, doctors login, dental services, emergency dentist, dentist office near me, cheap dentist near me, dental care near me, nearest dentist">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<div class="page-content bg-white"><!-- inner page banner -->
<div class="content-block" >
   	<div class="row" style="margin: 35px;">	
   		<div class="col-md-4" style="padding: 25px;"></div>			
		<div class="col-md-4" style="padding: 25px;">	
			<center><h6>Login</h6></center>
			
			<div class="forms">
				<center><span id="message"></span></center>
				<form action="" id="login" class = "form-horizontal" role = "form">
			      	<div class="input-field">
			        	<div class="col-md-12 form-group">
				        	<label for="email">Email Id</label>
				        	<input type="email" class="form-control" id="email_id" name="email_id" required="email" />
				    	</div>
				        <div class="col-md-12 form-group">
					        	<label for="email">Password</label>
					        	<input type="password" class="form-control" id="pass" name="pass" required="email" />
					    </div>
			        	<center>
			        		<input type="submit" value="Login" id="logindoctor" class="button btn-success btn-lg" style=" height:30px; color: white; font-size: 15px; border-radius: 20px; ">
			        	</center><br>
			        		<p class="text-p" style="text-align: center;"> <a href="<?php echo base_url();?>home/set_password">Forgot Password?</a> <br><b>Or</b> if you don't have account then click on below </p>

			        		<p class="text-p" style="text-align: center; "> <a href="<?php echo base_url();?>home/subscription" style="color:blue;font-size: 20px;"><i>Get Subscription</i></a> </p>
				    </div>
				 </form>
			</div>
		</div>
		<div class="col-md-4" style="padding: 25px;"></div>	
	</div>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#logindoctor').click(function(e) {
        e.preventDefault();

		 var email = document.getElementById('email_id').value;
        var password = document.getElementById('pass').value;

        if(email == ''){

        	$('#message').html("<i>Please Enter Email</i>").css('color', 'red');
		        	return false;

        }else if(password == ''){

        	$('#message').html("<i>Please Enter password</i>").css('color', 'red');
		        	return false;

        }else{

	        $.ajax({
	        url: "<?php echo base_url();?>api/doctor_login",
	        type:"POST",
	        processData:false,
	        contentType: 'application/json',
	        dataType: 'json',
	        async: false,
	        //data :form_data,
	        data: JSON.stringify({email:email,password:password}),
		    	success: function(response){
		    		var data = JSON.stringify(response.data);
                    var type = response.doctor_type;
		    		if(response.status == true){
		    			if(type.doctor_type == 'doctor'){
			    		sessionStorage.setItem('myprofile', data);
						window.location.href = "<?php echo base_url();?>doctor/index";

						}else{
							sessionStorage.setItem('myprofile', data);
							window.location.href = "<?php echo base_url();?>doctor/counsultant";
						}

				}

					if(response.status == false){

					var msg = JSON.stringify(response.msg);

    					Swal.fire({
			                  title: "<i style='color:orange'>Sorry...!!</i>", 
			                  html: msg,  
			                  confirmButtonText: "OK", 
			                });
    					
						}          


		    	}

                
      		});
			}
        });
	});
</script>