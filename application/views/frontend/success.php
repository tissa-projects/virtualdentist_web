
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>

<div class="page-content bg-white"><!-- inner page banner -->
  
    </div>
   	<div class="row" style="margin-bottom: 60px;">	
   		<div class="col-md-2" style="padding: 25px;"></div>			
			<div class="col-md-8" style="padding: 25px;">		

				<div class="container mt-5">
					
        				 
        				
        					<div class="card">
        						<h4 class="card-header">Transaction <label for="Success" class="badge badge-success">Success</label></h4>
		        				<div class="card-body">
		        					<?php 
				                echo "<p>Thank you. Your transaction has been Successful. ";
				                echo "Your Transaction ID for this transaction is ".$txnid."</p>";
				                
				            		?>
		        				</div>
        					</div>
	
    			</div>
    		</div>
    		<div class="col-md-2" style="padding: 25px;"></div>

    		<div class="col-md-4" style="padding: 25px;"></div>			
		<div class="col-md-4" style="padding: 25px;">	
			<center><h6>Set Your Login Password Here</h6></center>
			
			<div class="forms">
				<form action="#" id="login" class = "form-horizontal" role = "form">
			      	<div class="input-field">
			        	<div class="col-md-12 form-group">

			        		<input type="hidden" class="form-control" id="pay_id" name="pay_id" required="" value="<?php echo $txnid ; ?>" />

			        		<input type="hidden" class="form-control" id="acc_status" name="acc_status" required="" value="<?php echo $status ; ?>" />
				        	
				        	<input type="hidden" class="form-control" id="reg_num" name="reg_num" required="" value="<?php echo $reg ; ?>" />
				    	</div>
				        <div class="col-md-12 form-group">
					        	<label for="email">Password</label>
					        	<input type="password" class="form-control" id="pass" name="pass" minlength="5" maxlength="10" />
					        	<span id="message"></span>
					    </div>
					     <div class="col-md-12 form-group">
					        	<label for="email">Confirm Password</label>
					        	<input type="password" class="form-control" id="conpass" name="conpass"  />
					        	<span id="message1"></span>
					    </div>
			        	<center>
			        		<input type="submit" id="setpassword" value="Submit" class="button btn-success btn-lg" style=" height:30px; color: white; font-size: 15px; border-radius: 20px;  ">
			        	</center><br>
			        		
				    </div>
				 </form>
			</div>
		</div>
		<div class="col-md-4" style="padding: 25px;"></div>	


    	</div>
	<!-- Footer -->

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
	
$(document).ready(function(){
    $(function() {
		   $('#setpassword').click(function(e) {
		        e.preventDefault();

		        var reg_num = document.getElementById('reg_num').value;
		        var password = document.getElementById('pass').value;
		         var conpassword = document.getElementById('conpass').value;
		        var acc_status = document.getElementById('acc_status').value;
		        var pay_id = document.getElementById('pay_id').value;

		        if(password == ''){

		        	$('#message').html("Enter your password").css('color', 'red');
		        	return false;
		        
		        }else if(conpassword == ''){
		        	$('#message1').html("Enter cnfirm password").css('color', 'red');
		        	return false;
		        }else if(password != conpassword){
		        	$('#message1').html("Entered password doesn't match with confirm password").css('color', 'red');
		        	return false;
		        }else{
		        
		        $('#message1').html("");
		         $('#message').html("");
		      
		        $.ajax({
		        url: "<?php echo base_url();?>api/setpassword",
		        type:"POST",
		        processData:false,
		        contentType: 'application/json',
		        dataType: 'json',
		        async: false,
		        //data :form_data,
		        data: JSON.stringify({reg_num:reg_num, password:password ,acc_status:acc_status,pay_id:pay_id}),
			    	success: function(response){

			    		var data = JSON.stringify(response.data);

			    		var msg = JSON.stringify(response.msg);

			    		if(response.status == true){

			    		Swal.fire({
			                  title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
			                  html: msg    
			                });
    					
						     

    					window.location.href = "<?php echo base_url();?>home/login";

    					}
    					if(response.status == false){



    					Swal.fire({
			                  title: "<i style='color:orange'>Sorry...!!</i>", 
			                  html: msg,  
			                  confirmButtonText: "OK", 
			                });
    					
						}           
    			}
		    });
		  }
    	});
	});
});
</script>
	
	