
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="page-content bg-white"><!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(<?php echo base_url();?>assets/images/about-banner.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">About Us</h1>
    <!-- Breadcrumb row -->

                <div class="breadcrumb-row">
                    <ul class="list-inline">
    	               <li><a href="<?php echo base_url();?>home/index">Home</a></li>
    	               <li>About Us</li>
                    </ul>
                </div>
<!-- Breadcrumb row END -->
            </div>
        </div>
    </div>
<!-- inner page banner END -->

    <div class="content-block">
        <div class="section-full content-inner overlay-white-middle">
            <div class="container">
                <div class="row align-items-center m-b50">
                    <div class="col-md-8  m-b20">
                        <h2 class="m-b5">About Us</h2>

                        <p>Welcome to Virtual Dentist. To make the painful experience of getting the dental implant easier for you, we help you find the best medical help. Owned by Dr Sarika Sonawane. We believe in providing a comfortable environment for all our patients. We offer a wide range of dental services at a cost-effective price that would help our patients get the confidence and a bright smile. We offer 24/7 dental care and oral care services through our online portal. You can book an instant appointment with the dentists and have a telephonic conversation with them.</p>

                        <p>Our professional team of doctors is led by Dr. Sarika Sonawane BDS, from K. L. E Institute of health sciences Rajiv Gandhi University Bangalore. She is a former IDA Kharghar secretary who earned her Post graduate diploma in clinical research and clinical data management from Panvel. With more than 11years of experience, Dr. Sarika had been associated with the well-reputed Suman Dental Clinic, Tata Consultancy services, IKS US healthcare, and IQVIA as well. She also had her own private practice Sparkle Dental Clinic for more than 6 years.</p>

                        <p>Her team of bright and young minds is committed to offering a comprehensive range of all the dental services under one roof. Dr. Sarika is always ready to explore new and innovative ideas that could make the dental procedures simpler and less time consuming for her patients. She also provides guidance to the junior dentists who are just starting their careers in the medical field.</p>

                        <p>At Virtual Dentist, every patient is treated with absolute care and patience. We understand how important your work and family is for you, and that&rsquo;s why we help you book the online appointment.</p>
                    </div>

                    <div class="col-md-4 about"><img alt="" src="<?php echo base_url();?>assets/images/our-work/pic1.jpg" /></div>
                </div>
            </div>
        </div>
    </div>
<!-- contact area END -->
</div>

        <!-- footer bottom part -->