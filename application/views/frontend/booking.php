
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<style type="text/css">
   .dez-bnr-inr {
    height: 250px;
}
.contact-style-1 .border-1 {
    border: none;
}
.checkboxes label {

  display: inline-block;

  padding-right: 10px;

  white-space: nowrap;

}

.checkboxes input {

  vertical-align: middle;

}

.checkboxes label span {

  vertical-align: middle;

}

</style>


<script type="text/javascript">
    $(document).ready(function(){
        //var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        var date = sessionStorage.getItem('date');
        var date1 = sessionStorage.getItem('date');
        // var newdate = date.toLocaleDateString("en-US", options);
        // alert(newdate);
        var time = sessionStorage.getItem('time');
        var timeid = sessionStorage.getItem('timeid');
        var timeeve = sessionStorage.getItem('timeeve');
        var timeeveid = sessionStorage.getItem('timeeveid');

        var data = sessionStorage.getItem('mybooking'); 
        var obj = JSON.parse(data);
        var html = '';var html1 = '';var html2 = '';
        var i;var j;
        var date1 = new Date(date1) ;

                    var d = date1.toDateString();

                    d = d.split(' ');                  

                for(i=0; i< obj.length; i++){
        html +='<div class="p-a30 border m-b30 contact-area border-1 align-self-stretch radius-sm" style="width:360px;"><h4 class="m-b10">'+obj[i].name+'</h4><img src="<?php echo base_url(); ?>uploads/profile_pics/'+obj[i].profile_pic+'" style="height: 150px;width: 150px;" class="mx-auto img-fluid img-circle d-block" alt="avatar"><ul class="no-margin"><li class="icon-bx-wraper left m-b30"><div class="icon-bx-xs border-1"><i class="fa fa-medkit" style="font-size:35px;color:#1D46F5;" aria-hidden="true"></i></div><div class="icon-content"><h6 class="text-uppercase m-tb0 dez-tilte">Clinic:</h6><p>'+obj[i].clinic_name+'</p></div></li><li class="icon-bx-wraper left m-b30"><div class="icon-bx-xs border-1"><i class="fa fa-graduation-cap" style="font-size:35px;color:#1D46F5;" aria-hidden="true"></i></div><div class="icon-content"><h6 class="text-uppercase m-tb0 dez-tilte">Specialization:</h6><p>'+obj[i].specialization+'</p></div></li><li class="icon-bx-wraper left m-b30"><div class="icon-bx-xs border-1"><i class="fa fa-hospital-o" style="font-size:35px;color:#1D46F5;" aria-hidden="true"></i></div><div class="icon-content"><h6 class="text-uppercase m-tb0 dez-tilte">Address:</h6><p>'+obj[i].address+'&nbsp;'+obj[i].city+'&nbsp;'+obj[i].pincode+'</p></div></li><li class="icon-bx-wraper left  m-b30"><div class="icon-bx-xs border-1"><i class="fa fa-envelope" style=" font-size:35px;color:#1D46F5;"aria-hidden="true"></i></div><div class="icon-content"><h6 class="text-uppercase m-tb0 dez-tilte">Email:</h6><p>'+obj[i].email_id+'</p></div></li></ul></div>';

        html1 +='<input type="hidden" id="did" name="dname" value="'+obj[i].doctor_id+'"><input type="hidden" id="tid" name="tname" value="'+timeid+'"><input type="hidden" id="teveid" name="tevename" value="'+timeeveid+'"><input type="hidden" id="date" name="tname" value="'+date+'">';
            
        html2 +='<div class="m-t20"><div> <i class="fa fa-calendar" aria-hidden="true"></i> &nbsp;&nbsp;'+d[2] + ' ' + d[1] + ' ' + d[3]+'</div><br><div><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;At '+timeid+' </div></div>';
            
                }
         $('#details').html(html);
         $('#bookingdetails').html(html1);
          $('#dt').html(html2);

    });
</script>


<div class="page-content bg-white"><!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" >
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">Appointment Booking</h1>
    <!-- Breadcrumb row -->

                <div class="breadcrumb-row">
                    <ul class="list-inline">
                     <li><a href="<?php echo base_url();?>home/search">Doctors</a></li>
                     <li>Appointment Booking</li>
                    </ul>
                </div>
<!-- Breadcrumb row END -->
            </div>
        </div>
    </div>

    <div class="section-full content-inner bg-white contact-style-1">
        <div class="container">
            <div class="row"><!-- right part start -->
                <div class="col-lg-4 col-md-6 d-lg-flex d-md-flex" id="details" >
                    
                </div>
<!-- right part END --><!-- Left part start -->

                <div class="col-lg-4 col-md-6">
                    <i> <span id="message" style="font-size: 15px;"></span></i>
                    <div class="p-a30 m-b30 radius-sm bg-gray clearfix">
                        <h6 class="m-b10">Fill Your Details & Book Your Appointment</h6>

                        <form action="" class="dzForm" method="post">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"><input class="form-control" id="patientname" name="name" placeholder="Your Name"  type="text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"><input class="form-control" id="patientlocation" name="email" placeholder="Your Location"  type="text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"><input class="form-control" id="patientcontact" name="contact" placeholder="Your Contact No" maxlength="10" minlength="10" pattern="\d{10}" type="text" onkeypress="return isNum(event)" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12"><button class="site-button " name="button" type="submit" value="Submit" onclick="request();"><span>Request For OTP</span></button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                    <div class="alert alert-info alert-dismissable" id="otpmsg">
                       
                    </div>
                </div>
<!-- Left part END -->
                <div class="col-lg-4 col-md-6">
                    <div class="p-a30 m-b30 radius-sm bg-gray clearfix">
                       <i> <span id="message1" style="font-size: 15px;"></span></i>
                            <form action="" class="dzForm" method="post">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="input-group"><input class="form-control" id="otp" name="name" placeholder="Enter OTP" required="" type="text" onkeypress="return isNum(event)" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkboxes">
                                        <div for="x" style="padding-left: 17px;"><input type="checkbox" name="agree" id="agree"  value="1"> <span style="line-height: 1.5em;font-size:13px;">I agree to Virtual Dentist's  <a href="<?php echo base_url()?>home/terms" target="_blank"> Terms and Privacy Policy </a> </span></div>
                                    </div>
                                    <div id="bookingdetails"></div>

                                    <div class="col-lg-12"><button class="site-button " name="button" type="submit" value="Submit" onclick="get_appointment();"><span>Get Appointment</span></button>
                                    </div>
                                </div>
                            </form>
                            
                    </div>
                    
                     <div class="p-a30 m-b30 radius-sm bg-gray clearfix">
                          <h6 class="m-b10">Appointment Date & Time</h6>
                        
                         <div id="dt"></div>
                               
                    </div>
                <!--<iframe allowfullscreen="" frameborder="0"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3771.2761116110014!2d73.07205631490083!3d19.05159408710182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c21b62c2f00d%3A0x8e105aa55b631e94!2sBhumiraj+woods+CHS!5e0!3m2!1sen!2sin!4v1544764108821" style="border:1px ;width: 361px;height: 400px;" ></iframe>-->
            
            </div>
        </div>
    </div>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">

     $('#otpmsg').hide();
    function request(){

        var name = $('#patientname').val();
         var location = $('#patientlocation').val();
          var contact = $('#patientcontact').val();

          if(name == '')
          {
                $('#message').html("Enter your name").css('color', 'red');
            return false;
          }else if(location == ''){
                $('#message').html("Please tell us your location ").css('color', 'red');
            return false;
        }else if(contact == ''){
                $('#message').html("Please provide us you mobile number").css('color', 'red');
              return false;
        }else{

          $.ajax({
        url: "<?php echo base_url();?>api/request_otp",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({name:name,location:location,contact:contact}),
            success: function(response){
               

           var data = JSON.stringify(response.msg);

        var obj = JSON.parse(data);

        var html = '';

         html += '<span style=" font-size:15px;">'+obj+'</span>';
          $('#otpmsg').show();
          $('#otpmsg').html(html);
                
        } 

    });
      }

    }


    function get_appointment(){
        var checkBox = document.getElementById("agree");
         var doctor_id = $('#did').val();
         var scedule_id = $('#tid').val();
          var schedule_eve_id = $('#teveid').val();
          var appointment_date = $('#date').val();
          var otp = $('#otp').val();

            if(otp == ''){
            $('#message1').html("<i>Please enter OTP</i>").css('color', 'red');
            return false;
          }else if(checkBox.checked == false){
          $('#message1').html("<i>Please agree terms and privacy policy of VirtualDentist</i>").css('color', 'red');
          return false;
            }else{
            $('#message1').html("");
          $.ajax({
        url: "<?php echo base_url();?>api/book_appointment",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id,scedule_id:scedule_id,schedule_eve_id:schedule_eve_id,appointment_date:appointment_date,otp:otp}),
            success: function(response){


            if(response.status == true){
                var data = response.msg; 

            Swal.fire({
                  title: "<i style='color:#ADD8E6'>Congratulation...!!</i>", 
                  html: data,  
                  confirmButtonText: "OK", 
                });
                      window.setTimeout(function() {
                       window.location.href = "<?php echo base_url();?>home/index";
                    }, 3000);


                    }


                      if(response.status == false){

                    var msg = JSON.stringify(response.msg);

                        Swal.fire({
                              title: "<i style='color:orange'>Sorry...!!</i>", 
                              html: msg,  
                              confirmButtonText: "OK", 
                            });
                        
                    }          
                
        }           
    });
    }
    }

function isNum(evt){

  evt =(evt)? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode<48 || charCode >57)){

    return false;
  }
return true;
}
</script>