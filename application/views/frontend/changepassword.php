
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>

<style type="text/css">
	.page-content {
    margin-top: -50px;
    padding-bottom: 50px;
}
</style>


<div class="page-content bg-white"><!-- inner page banner -->
  
    </div>
   	<div class="row" style="margin: 35px;">	
   		<div class="col-md-4" style="padding: 25px;"></div>			
		<div class="col-md-4" style="padding: 25px;">	
			<!-- <center><h6>Forgot Password ?</h6></center> -->
			
			<div class="forms">
				<center><span id="message"></span></center>
				<form action="" id="forgot" class = "form-horizontal" role = "form">
			      	<div class="input-field">

			      		<input type="hidden" id="cemail" name="cemail" value="<?php echo $email;?>">
			        	<div class="col-md-12 form-group">
					        	<label for="email">Password</label>
					        	<input type="password" class="form-control" id="pass" name="pass"  />
					    </div>

					    <div class="col-md-12 form-group">
					        	<label for="email">Confirm Password</label>
					        	<input type="password" class="form-control" id="cpass" name="pass" />
					    </div>
				        
			        	<center>
			        		<input type="submit" value="Reset Password" id="changepass" class="button btn-success btn-lg" style=" height:30px; color: white; font-size: 15px; border-radius: 20px; ">
			        	</center><br>
			        		
				    </div>
				 </form>
			</div>
		</div>
		<div class="col-md-4" style="padding: 25px;"></div>	
	</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#changepass').click(function(e) {
        e.preventDefault();

        	 var email = document.getElementById('cemail').value;
		 var pass = document.getElementById('pass').value;
		  var cpass = document.getElementById('cpass').value;

		  if(pass == ''){

		  	$('#message').html("<i>Please Enter Password</i>").css('color', 'red');
		        	return false;
		  }else if(pass != cpass){

		  	$('#message').html("<i>Entered Password Doesn't Match With Confirm Password</i>").css('color', 'red');
		        	return false;

		  }else{
       $('#message').html("");
	        $.ajax({
	        url: "<?php echo base_url();?>api/changepass",
	        type:"POST",
	        processData:false,
	        contentType: 'application/json',
	        dataType: 'json',
	        async: false,
	        //data :form_data,
	        data: JSON.stringify({pass:pass,email:email}),
		    	success: function(response){
		    		var data = JSON.stringify(response.data);


		    		 Swal.fire({
                        title: "<i>Congratulations...!!</i>", 
                        html: data,  
                        confirmButtonText: "OK", 
                      });

                      window.setTimeout(function() {
                      window.location.href = "<?php echo base_url();?>home/login";
                    }, 3000);
		    		
		    	}

                
      		});
	    }
        });
	});
</script>