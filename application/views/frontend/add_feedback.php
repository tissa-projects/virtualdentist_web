
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<style type="text/css">
	div.stars {
  width: 270px;
  display: inline-block;
}

input.star { display: none; }

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content: '\f005';
  color: #FD4;
  transition: all .25s;
}

input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before { color: #F62; }

label.star:hover { transform: rotate(-15deg) scale(1.3); }

label.star:before {
  content: '\f006';
  font-family: FontAwesome;
}
.dez-bnr-inr {
    height: 250px;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
       
        var data = sessionStorage.getItem('myid');

       $("#feedid").val(data);


      });
</script>

<div class="page-content bg-white">
     <div class="dez-bnr-inr overlay-black-middle" >
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">Feedback</h1>
    <!-- Breadcrumb row -->

                <div class="breadcrumb-row">
                    <ul class="list-inline">
    	               <li><a href="<?php echo base_url();?>home/view_doctor">Doctor Panel</a></li>
    	               <li>Feedback</li>
                    </ul>
                </div>
<!-- Breadcrumb row END -->
            </div>
        </div>
    </div>
	<div class="section-full content-inner bg-white contact-style-1">
        <div class="container">
            <div class="row">
               <div class="col-md-12 alert alert-primary" role="alert" id="feed"></div>
            	<div class="col-lg-4 col-md-3 d-lg-flex d-md-flex"></div>
                <div class="col-lg-4 col-md-6 d-lg-flex d-md-flex">
                	<div class="p-a30 m-b30 radius-sm bg-gray clearfix" id="showform">
                        <h6 class="m-b10" style="color: blue;align:center;">Rate Your Doctor</h6>
                        <i> <span id="message" style="font-size: 15px;"></span></i>
                        <form action="" class="dzForm" method="post">
                            <div class="row">
                            	<div class="col-lg-12">
                        			<div class="stars">

            									    <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
            									    <label class="star star-5" for="star-5"></label>
            									    <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
            									    <label class="star star-4" for="star-4"></label>
            									    <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
            									    <label class="star star-3" for="star-3"></label>
            									    <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
            									    <label class="star star-2" for="star-2"></label>
            									    <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
            									    <label class="star star-1" for="star-1"></label>
  									 
									             </div>
                            		
                            	</div>
                              <input type="hidden" name="feedid" id="feedid">
                            	 <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"><textarea class="form-control" id="comment" name="name" placeholder="Your comment"  ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"><input class="form-control" id="pname" name="pname" placeholder="Your Name"  type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"><input class="form-control" id="pcontact" name="pcontact" placeholder="Your Contact No"  type="text" onkeypress="return isNum(event)" maxlength="10" minlength="10" pattern="\d{10}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12"><button class="site-button" name="button" type="submit" value="Submit"  onclick="addfeedback()"><span>Submit</span></button>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                    
                </div>

               <div class="col-lg-4 col-md-3 d-lg-flex d-md-flex"></div>

                
            </div>
            
        </div>
    </div>
</div>

<script type="text/javascript">
   $('#feed').hide();
  function addfeedback(){

    var doctor_id = $("#feedid").val();
     var feed_desc = $("#comment").val();
      var name = $("#pname").val();
       var mobile = $("#pcontact").val();
        var rating = 0 ;
         rating = $('input:radio:checked').val();  

          if(rating == undefined)
          {
                $('#message').html("Please rate this doctor ").css('color', 'red');
            return false;
          }else if(name == ''){
                $('#message').html("Please tell us your name ").css('color', 'red');
            return false;
        }else if(mobile == ''){
                $('#message').html("Please provide us you mobile number").css('color', 'red');
              return false;
        }else{
        
         $.ajax({
        url: "<?php echo base_url();?>api/add_feedback",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id,feed_desc:feed_desc,mobile:mobile,rating:rating}),
            success: function(response){

                var data = JSON.stringify(response.data);

        var obj = JSON.parse(data);
        
         if(response.status == true){
                var data = response.data; 

            Swal.fire({
                  title: "<i style='color:#ADD8E6'>Congratulation...!!</i>", 
                  html: data,  
                  confirmButtonText: "OK", 
                });
                     

                    }

        if(response.status == false){

                    var msg = JSON.stringify(response.data);

                        Swal.fire({
                              title: "<i style='color:orange'>Sorry...!!</i>", 
                              html: msg,  
                              confirmButtonText: "OK", 
                            });
                        
                    }         
                
        }           
    });

}

  }



  function isNum(evt){

  evt =(evt)? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode<48 || charCode >57)){

    return false;
  }
return true;
}
</script>