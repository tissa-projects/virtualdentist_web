<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Virtual Dentist | Dental Implant Treatment | Dentist in Kharghar Navi Mumbai</title>
	<meta name="keywords" content="virtual Dentist, Dental Implant Treatment in navhi Mumbai, Dentist in Kharghar "/><meta name="description" content="Are you looking for the right dentist in Navi Mumbai? The Virtual dentist is one of the best treatment providers; every patient is treated with absolute care and patience."/><meta name="google-site-verification" content="GBnAVie-37eQTEbcXXocTB4wGfGV3YQEYXDHdNaVhTQ" /><meta name="page-type" content="Dental Implant Treatment - Virtual Dentist Kharghar" /><meta name="distribution" content="Global" /><meta name="robots" content="index, follow" /><meta name="googlebot" content="index, follow" /><meta name="classification" content="Virtual Dentist Kharghar, Navi Mumbai" /><meta name="resource-type" content="Dental Implant Treatment - Virtual Dentist Kharghar, Navi Mumbai" /><meta name="revisit-after" content="5 days" /><meta name="rating" content="General" /><meta name="copyright" content="2019" /><meta name="Voluntary Content Rating" content="general" /><meta name="ObjectType" content="Document" /><meta name="doc-class" content="Living Document" /><meta name="MSSmartTagsPreventParsing" content="true" /><meta name="language" content="English" /><meta name="author" content="Virtual Dentist"><!-- Facebook Og Tags --><meta property="og:title" content="Virtual Dentist Navi Mumbai"><meta property="og:site_name" content="Virtual Dentist"><meta property="og:url" content="https://www.virtualdentist.in/"><meta property="og:description" content="We are one of the best Dental in Navi Mumbai, India"><meta property="fb:app_id" content=""><meta property="og:type" content="Website"><!-- End Facebook Og Tags --><!-- Facebook Insights --><meta property="fb:admins" content="" /><!-- End Facebook Insights --><!-- Twitter Cards --><meta name="twitter:card" content="summary_large_image"><meta name="twitter:site" content="Virtual Dentist Navi Mumbai"><meta name="twitter:title" content="Best Dental Implant Treatment In Navi Mumbai"><meta name="twitter:description" content="We are one of the best Dental in Navi Mumbai, India"><!-- End Twitter cards --><!-- Address Meta Info --><meta name="contactstreetaddress1"
		content="Sector 20, Kharghar, Navi Mumbai, 410210" /><meta name="contactcity" content="Navi Mumai" /><meta name="contactstate" content="Maharashtra" /><meta name="contactzipcode" content="410210"/><meta name="format-detection" content="telephone=no"><!-- FAVICONS ICON -->
	<link href="<?php echo base_url();?>assets/images/favicon.png" rel="icon" type="image/x-icon" />
	<link href="<?php echo base_url();?>assetsimages/favicon.png" rel="shortcut icon" type="image/x-icon" /><!-- PAGE TITLE HERE --><!-- MOBILE SPECIFIC --><meta name="viewport" content="width=device-width, initial-scale=1"><!-- STYLESHEETS -->
	<link href="<?php echo base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/templete.css" rel="stylesheet" type="text/css" />
	<link class="skin" href="<?php echo base_url();?>assets/css/skin/skin-1.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" /><!-- Revolution Slider Css -->
	<link href="<?php echo base_url();?>assets/css/layers.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/settings.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/navigation.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Revolution Navigation Style --><!-- Start of  Zendesk Widget script --><script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=4ca2b977-8d21-4ef5-a994-5ac1756555fa"> </script><!-- End of  Zendesk Widget script -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
</head>
<style>ul#menu li {
  display:inline;
}
.logo-header {
    margin-left: 10px;
}
.checked {
  color: orange;
}
.fullwidth .logo-header {
    margin-right: 25px;
}
		.forms {
    background: #fff;
    box-shadow: 0 0 10px #28531f;
    margin: 10px auto;
    max-width: 1000px;
    overflow: hidden;
    position: relative;
    padding: 0;
    border-radius: 15px;
}

.forms form { padding: 30px; }
#signup { display: none; }
.forms .tab-group {
	list-style: none;
	padding: 0;
	margin: 0;
}
.forms .tab-group:after {
	content: "";
	display: table;
	clear: both;
}
.forms .tab-group li a {
	display: block;
	text-decoration: none;
	padding: 4px;
	background: #e5e6e7;
	color: #888;
	font-size: 15px;
	float: left;
	width: 50%;
	text-align: center;
	border-top: 3px solid transparent;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms .tab-group li a:hover {
	background: #dedfdf;
	color: #666;
}
.forms .tab-group .active a {
	background: #fff;
	color: #444;
	border-top: 3px solid #73cf41;
}
.forms input {
	font-size: 12px;
	display: block;
	width: 100%;
	padding: 4px 10px;
	border: 1px solid #ddd;
	color: #666;
	border-radius: 2px;
	margin-bottom: 10px;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms select {
	font-size: 12px;
	display: block;
	width: 100%;
	padding: 4px 10px;
	border: 1px solid #ddd;
	color: #666;
	border-radius: 2px;
	margin-bottom: 10px;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms input:focus {
	outline: 0;
	border-color: #2e5ed7;
}
.forms label {
	font-size: 12px;
	font-weight: normal;
	color: #666;
	margin-bottom: 5px;
	display: block;
}
.modal-content {
	border-radius: 23px;
}
.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 31px;
    font-size: 12px;
    color: #495057;
}
.navbar-fixed-top {
    top: 0;
    border-width: 0 0 1px;
}

.top-bar-transparent { background-color: rgba(0, 0, 0, 0.1); padding-top: 10px; padding-bottom: 10px; color: #fff; }

@media (max-width:767px) {
    .top-message{display: none;}
    .top-link {padding-left: 270px;}
}

	</style>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script type="text/javascript">
		$(document).ready(function(){
			  $('.tab a').on('click', function (e) {
			  e.preventDefault();
			  
			  $(this).parent().addClass('active');
			  $(this).parent().siblings().removeClass('active');
			  
			  var href = $(this).attr('href');
			  $('.forms > form').hide();
			  $(href).fadeIn(500);
			});
		});
	</script>


  
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#seeAnotherFieldGroup").change(function () {
            
            var selectedValue = $(this).val();

            if(selectedValue == 'Counsultation'){

            	$("#namec").hide();
            	$("#yearc").hide();
            	$("#addc").hide();
            		$("#specialization").show();
            	$("#counsultantadd").show();
            }

            if(selectedValue == 'doctor'){
                	$("#namec").show();
            	$("#yearc").show();
            	$("#addc").show();
            	$("#specialization").hide();
            	$("#counsultantadd").hide();
            }
            
        });
    });
</script>

<body id="bg"><!--<div id="loading-area"></div>-->
	<div class="page-wraper"><!-- header -->
	 <!--   <header class=" site-header header fullwidth" style="height:30px;">-->
		<!--	<div class="sticky-header main-bar-wraper navbar-expand-lg header-v2 navbar-fixed-top">-->
		<!--		<div class="main-bar clearfix top-bar-transparent" style="">-->
		<!--			<div class="container clearfix" style="max-width:1325px">-->
		<!--				<div class="row">-->
		<!--					<div class="col-md-8 top-message">-->
		<!--					    <span style="font-size: 13px; color:white;">-->
                                    
  <!--                                      <i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;<a href="info@virtualdentist.in" style="color:white;text-transform: lowercase;">info@virtualdentist.in</a>-->
  <!--                              </span>-->
		<!--					</div>-->
		<!--					<div class="col-md-4 top-link">-->
							
		<!--							<a  data-toggle="modal" data-target="#myModal" style="border-radius: 10px;font-size: 13px;  font-weight:bold">Doctor Login</a>-->
								
		<!--					</div>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</header>-->
		<header class="site-header mo-left header fullwidth">
			<div class="sticky-header main-bar-wraper navbar-expand-lg">
				<div class="main-bar clearfix">
					<div class="container clearfix" style="max-width:1325px"><!-- website logo -->
						<div class="logo-header mostion">
							<a href="<?php echo base_url();?>home"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" /></a>
						</div>
						
<!-- nav toggle button --><!-- nav toggle button -->
						<button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler collapsed navicon justify-content-end" data-target="#navbarNavDropdown" data-toggle="collapse" data-dismiss="modal" type="button" >
							<!--<span class="icon-bar"></span>-->
		    				<span class="icon-bar"></span>
		    				<span class="icon-bar"></span>
		    				<span class="icon-bar"></span>
		    			</button><!-- extra nav --><!-- Quik search -->

						<div class="dez-quik-search bg-primary">
							<form action="#">
								<input class="form-control" name="search" placeholder="Type to search" type="text" value="" />
							</form>
						</div>
<!-- main nav -->

						<div class="header-nav navbar-collapse collapse justify-content-start" id="navbarNavDropdown" style="padding-left:60px;">
							<ul class="nav navbar-nav">
								<li class="active"><a href="<?php echo base_url();?>home">Home </a></li>
								<li><a href="<?php echo base_url();?>home/about">About Us</a></li>
								<li><a href="<?php echo base_url();?>home/services">Service&#39;s</a></li>
								<!--<li><a href="<?php echo base_url();?>home/doctor_panel">Doctor Panel</a></li>-->
								<li><a href="<?php echo base_url();?>home/consultation">Online Consultation</a></li>
								<li><a href="https://onlinecourses.virtualdentist.in/s/store">Online Courses</a></li>
								<li><a href="<?php echo base_url();?>blog">Blog</a></li>
								<li><a href="<?php echo base_url();?>home/contact">Contact Us</a></li>
							</ul>
							<!--<ul id="menu" style="align-self: right; margin-left: 100px;">-->
						
							<!--	 <li> <button type="button" class="btn btn-default" id="open"  data-toggle="modal" data-dismiss="modal" href="#myModal" style="border-radius: 15px;font-size: 13px; font-weight:bold">Doctor Login</button></li>-->
								<!-- <li > <button type="button" class="btn btn-default"  data-toggle="modal" data-target="#myModalpatient" style="border-radius: 15px;font-size: 11px;">Patient Panel</button></li> -->
							<!--</ul>-->
						</div>    
					</div>
			</div>
		</div>
		<!-- main header END -->
	</header>
		<!-- header END --><!-- Content -->

                        <div class="modal hide fade" id="myModal" role="dialog" >
   							<div class="modal-dialog modal-lg" style="">
    							<div class="modal-content">
		   							<div class="modal-body">
		    	 						<button type="button" class="close" data-dismiss="modal" style="font-size:30px;"><b>&times;</b></button>
		        						<div class="forms">
											<ul class="tab-group">
												<li class="tab active"><a href="#login">Login </a></li>
												<li class="tab "><a href="#signup">Get Subscription</a></li>
											</ul>
											<form action="<?php echo base_url();?>doctor" id="login" class = "form-horizontal" role = "form">
										      <div class="input-field">
										        <div class="col-md-12 form-group">
											        	<label for="email">Email Id</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											    </div>
										        <div class="col-md-12 form-group">
											        	<label for="email">Password</label>
											        	<input type="password" class="form-control" id="password" name="password" required="email" />
											    </div>
										        <input type="submit" value="Login" class="button btn-success btn-lg" style=" height:30px; color: white; font-size: 15px; border-radius: 20px;"><br>
										        <p class="text-p"> <a href="#">Forgot Password?</a> </p>
											     </div>
											 </form>
											  <form action="#" id="signup" class = "form-horizontal" role = "form">
											      <div class="row">
											        <div class="col-md-3 form-group">
											        	<label for="Type">Select Type</label>
											        	<select class="form-control" id="seeAnotherFieldGroup">
											        	    <option>Select Type</option>
														    <option value="doctor">Private Practitioner</option>
														    <option value="Counsultation">Consultant</option>
														 </select>
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Registration Number</label>
											       		<input type="email" class="form-control" id="email" name="email" required="email" />
											    	</div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Name</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Email Id</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											    	</div>
											        <div class="col-md-3 form-group">
											        	<label for="Type">Mobile Number</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group" id="specialization">
											        	<label for="email">Specialization</label>
											        	<select><option>Orthodontist</option><option>Implantalogist</option><option>Cosmetic dentist</option></select>
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Passing year</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group" id="counsultantadd">
											        	<label for="email">Address</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											         <div class="col-md-3 form-group" id="namec" >
											        	<label for="email">Clinic Name</label>
											        	<input type="email" class="form-control" id="clinic_name" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group" id="yearc" >
											        	<label for="email">Clinic Establishment Year</label>
											        	<input type="email" class="form-control" id="clinic_year" name="email" required="email" />
											        </div>
											         <div class="col-md-3 form-group" id="addc">
											        	<label for="email"> Counsultation Fees</label>
											        	<input type="text" class="form-control" id="clinic_add" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group" id="addc">
											        	<label for="email">Clinic Address</label>
											        	<input type="email" class="form-control" id="clinic_add" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Country</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">State</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">city</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Zip code</label>
											        	<input type="email" class="form-control" id="email" name="email" required="email" />
											        </div>
											        <div class="col-md-3 form-group">
											        	<label for="email">Your profile pic </label>
											        	<input type="file" class="form-control" id="email" name="email" required="email" />
											        </div>
										      </div>
											        <input type="submit" value="Get Subscription" class="button btn-success btn-lg" style=" height:40px; color: white; font-size: 15px; border-radius: 20px;"/>
								      					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">submit</button> -->
											 </form>
										</div>
		    						</div>
		  						</div>
    						</div>
 	 					</div>

<!--<script>-->
<!--    $(document).on('show.bs.modal', '.modal', function (event) {-->
<!--            var zIndex = 1040 + (10 * $('.modal:visible').length);-->
<!--            $(this).css('z-index', zIndex);-->
<!--            setTimeout(function() {-->
<!--                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');-->
<!--            }, 0);-->
<!--        });-->

        
<!--        $('#open').click(function() {-->
<!--            alert("hii");-->
<!--            $('#navbarNavDropdown').hide();-->
<!--        });-->
<!--</script>-->
