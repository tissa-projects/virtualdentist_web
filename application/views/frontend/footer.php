</div>	
	<footer class="footer footer-bottom">
	
			<div class="row">
				<div class="col-lg-12 text-center">
					<span>Copyright &copy; 2020 Virtual Dentist - All Rights Reserved.</span>
				</div>
			</div>
		
	</footer>

<!-- Footer END --><!-- scroll top button -->
	<button class="scroltop fa fa-arrow-up"></button>
	
	</div>
<!-- JAVASCRIPT FILES ========================================= -->
	<!--<script src="< ?php echo base_url();?>assets/js/jquery.min.js"></script>-->
	<!-- JQUERY.MIN JS -->
	<script src="<?php echo base_url();?>assets/plugins/wow/wow.js"></script><!-- WOW JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/popper.min.js"></script><!-- BOOTSTRAP.MIN JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script><!-- FORM JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
	<script src="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC POPUP JS -->
	<script src="<?php echo base_url();?>assets/plugins/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
	<script src="<?php echo base_url();?>assets/plugins/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
	<script src="<?php echo base_url();?>assets/plugins/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
	<script src="<?php echo base_url();?>assets/plugins/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
	<script src="<?php echo base_url();?>assets/plugins/masonry/masonry.filter.js"></script><!-- MASONRY -->
	<script src="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.js"></script><!-- OWL SLIDER -->
	<script src="<?php echo base_url();?>assets/plugins/rangeslider/rangeslider.js" ></script><!-- Rangeslider -->
	<script src="<?php echo base_url();?>assets/js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
	<script src="<?php echo base_url();?>assets/js/dz.carousel.js"></script><!-- SORTCODE FUCTIONS  -->
	<script src="<?php echo base_url();?>assets/js/dz.ajax.js"></script><!-- CONTACT JS  -->
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
	


</body>
</html>