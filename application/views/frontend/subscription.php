
<head>
<meta charset="UTF-8">
<title>Dental Clinic Management Software | Dental Office Software | Virtual Dentist</title>
<meta name="Virtual Dentist provides the best in class dental clinic management software and website exclusively for Dentists with Dental Office Software and free appointment software.">
<meta name="Keywords" content="dental practice management software, dental software programs, best dental websites, dental clinic website, dental office software, dental site, dental clinic management software, dental patient management software
dental patient management software">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css">
/*  .page-content {*/
/*    margin-top: -50px;*/
/*    padding-bottom: 50px;*/
/*}*/
select.bs-select-hidden, select.selectpicker {
    display: block!important;
}
.bootstrap-select{
   display: none!important;
}

#divLoading
{
    display : none;
}
#divLoading.show
{
    display : block;
    position : fixed;
    z-index: 100;
    background-image : url('http://loadinggif.com/images/image-selection/3.gif');
    background-color:#666;
    opacity : 0.4;
    background-repeat : no-repeat;
    background-position : center;
    left : 0;
    bottom : 0;
    right : 0;
    top : 0;
}
#loadinggif.show
{
    left : 50%;
    top : 50%;
    position : absolute;
    z-index : 101;
    width : 32px;
    height : 32px;
    margin-left : -16px;
    margin-top : -16px;
}
.forms {
   width : auto;
   height : auto;
}

</style>



<script type="text/javascript">
    $(function () {
          


        var e = document.getElementById("seeAnotherFieldGroup");
    var strUser = e.options[e.selectedIndex].value;

     $("#namec").show();
              $("#yearc").show();
              $("#addc").show();
              $("#fees").show();
              $("#counsultantadd").hide();
              $("#address").val("0");

        $("#seeAnotherFieldGroup").change(function () {
            
            var selectedValue = $(this).val();

            if(selectedValue == 'Counsultation'){

              $("#namec").hide();
              $("#yearc").hide();
              $("#addc").hide();
              $("#fees").hide();
              $("#doctor-specialization").show();
              $("#counsultantadd").show();
              $("#clinic_name").val("0");
              $("#clinic_year").val("0");
              $("#clinic_add").val("0");
              $("#fee").val("0");
              $("#address").val("");
            }

            if(selectedValue == 'doctor'){
                $("#namec").show();
              $("#yearc").show();
              $("#addc").show();
              $("#fees").show();
              $("#doctor-specialization").show();
              $("#counsultantadd").hide();
              $("#address").val("0");
              $("#clinic_name").val("");
              $("#clinic_year").val("");
              $("#clinic_add").val("");
              $("#fee").val("");
            }
            
        //});
    });


  });
</script>
<div class="page-content bg-white">
    <div class="content-block" >
    <div class="row"> 
      <div  class="col-md-2"></div>     
    <div  class="col-md-8" style="padding: 25px; color:blue; font-size:18px;"><br>
      <center><h6>Get Subscription</h6>
      <p>Virtual Dentist provides the best in class <b>dental clinic management software</b> and website exclusively for Dentists with <b>dental office software</b> and free appointment software that is easy and comfortable to use.</p>
      <p>Get free appointment software for your dental clinics with your website subscription only at Rs.2000 per year with zero maintenance charges. Also, get free unlimited blogs for your patients. Get dental patients through the Virtual Dentist portal. Any queries kindly call on below number. <br> Contact : +91 91334 92255</p>
            </center>
       <!--<div class="alert alert-success" id="result"></div>-->

      <div class="forms">
          <center><div id="message"><i> * Note: All fields are required! Please fill all the details</i> </div></center>

        <form action="" id="signup" class="form-horizontal" role ="form"  style="display: block;">
            <div class="row">
                  <div class="col-md-3 form-group">
                    <label for="Type">Select Type</label>
                    <select class="form-control" id="seeAnotherFieldGroup" name="type" >
                    <option value="doctor" selected="selected">Private Practitioner</option>
                    <option value="Counsultation">Consultant</option>
                 </select>
                  </div>
                  <div class="col-md-3 form-group">
                    <label for="registration">Registration Number</label>
                    <input type="text" class="form-control" id="reg_no" name="reg_no"  />
                </div>
                  <div class="col-md-3 form-group">
                    <label for="Name">Name</label>
                    <input type="text" class="form-control" id="name" name="name"  />
                  </div>
                  <div class="col-md-3 form-group">
                    <label for="email">Email Id</label>
                    <input type="email" class="form-control" id="email" name="email"  />
                </div>
                  <div class="col-md-3 form-group">
                    <label for="Type">Mobile Number</label>
                    <input type="text" class="form-control" id="phone" name="phone" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" minlength="10" pattern="\d{10}" />
                  </div>
                  <div class="col-md-3 form-group" id="doctor-specialization">
                    <label for="email">Specialization</label>
                    <select id="spacial" name="spacial">
                      <option value="">Select Specialization</option>
                      <option value="Oral Pathology">Oral Pathology</option>
                      <option value="Forensic Odontology">Forensic Odontology</option>
                      <option value="Oral Physiotherapy">Oral Physiotherapy</option>
                      <option value="Oral Nutrition And Detects">Oral Nutrition And Detects</option>
                      <option value="Oral Medicine">Oral Medicine</option>
                      <option value="Oral Radiology">Oral Radiology</option>
                      <option value="Cranio Oro Maxillofacial Surgery">Cranio Oro Maxillofacial Surgery</option>
                      <option value="Periodontics">Periodontics</option>
                      <option value="Operative Dentistry And Endodontics">Operative Dentistry And Endodontics</option>
                      <option value="Orthodontics">Orthodontics</option>
                      <option value="Prosthodontics">Prosthodontics</option>
                      <option value="Implantology">Implantology</option>
                      <option value="Aesthetic And Cosmetic Dentistry">Aesthetic And Cosmetic Dentistry</option>
                      <option value="Dental Lasers">Dental Lasers</option>
                      <option value="Minimal Intervention Dentistry">Minimal Intervention Dentistry</option>
                      <option value="Sedative Dentistry">Sedative Dentistry</option>
                      <option value="Dental Sleep Medicine">Dental Sleep Medicine</option>
                      <option value="Pedodontics">Pedodontics</option>
                      <option value="Public Health Dentistry">Public Health Dentistry</option>
                      <option value="General Dentistry">General Dentistry</option>
                    </select>
                  </div>
                  <!-- <div class="col-md-3 form-group" id="specialization">
                    <label for="email">Specialization</label>
                    <select id="spacial" name="spacial">
                      <option value="Orthodontist">Orthodontist</option>
                      <option value="Implantalogist">Implantalogist</option>
                      <option value="Cosmetic Dentist">Cosmetic Dentist</option>
                      <option value="Prosthodontist">Prosthodontist</option>
                      <option value="Endodontist">Endodontist</option>
                      <option value="Pedodontist">Pedodontist</option>
                      <option value="Periodontist">Periodontist</option>
                      <option value="Community Dentistry">Community Dentistry</option>
                      <option value="Oral Pathologist">Oral Pathologist</option>
                      <option value="Oral Medicine and Diagnostic Radiology">Oral Medicine and Diagnostic Radiology</option>
                      <option value="Oral Maxillofacial Surgeon">Oral Maxillofacial Surgeon</option>
                      <option value="Forensic Odontology">Forensic Odontology</option>
                    </select>
                  </div> -->
                  <div class="col-md-3 form-group">
                    <label for="passing_year">Passing year</label>
                    <input type="text" class="form-control" id="pass_y" name="pass_y" onkeypress="return isNum(event)" maxlength="4" minlength="4" pattern="\d{4}"/>
                  </div>
                  <div class="col-md-3 form-group" id="counsultantadd">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address" />
                  </div>
                   <div class="col-md-3 form-group" id="namec" >
                    <label for="email">Clinic Name</label>
                    <input type="text" class="form-control" id="clinic_name" name="clinic_name" />
                  </div>
                  <div class="col-md-3 form-group" id="yearc" >
                    <label for="email">Establishment Year</label>
                    <input type="text" class="form-control" id="clinic_year" name="clinic_year" onkeypress="return isNum(event)" maxlength="4" minlength="4" pattern="\d{4}"/>
                  </div>
                   <div class="col-md-3 form-group" id="fees">
                    <label for="email"> Counsultation Fees</label>
                    <input type="text" class="form-control" id="fee"  name="fee" onkeypress="return isNum(event)" />
                  </div>
                  <div class="col-md-3 form-group" id="addc">
                    <label for="email">Clinic Address</label>
                    <input type="text" class="form-control" id="clinic_add" name="clinic_add"  />
                  </div>
                  <div class="col-md-3 form-group">
                    <label for="email">Country</label>
                    
                  <select name="country" id="country" class="form-control">
                                    <option value="0">Select Country</option>
                                    <?php
                                    foreach($country as $row)
                                    {?>
                                      
                                     <option value="<?php echo $row->country_name?>" <?php if($row->country_name == 'India'){ echo 'selected'; }?>><?php echo $row->country_name?></option>
                                    
                                   <?php }
                                    ?>
                                   </select>
                  </div>
                  <div class="col-md-3 ">
                    <label for="email">State</label>
                         <select name="state" id="state" class="form-control">
                    <option value="0">Select State</option>
                    <?php
                                    foreach($state as $row)
                                    {?>
                                       <option value="<?php echo $row->state_name;?>"><?php echo $row->state_name;?></option>
                                       <?php }
                                    ?>
                  </select>
                  </div>
                  <div class="col-md-3 ">
                    <label for="email">City</label>
                    <select name="city" id="city" class="form-control"  >
                    <option value="0">Select City</option>
                    </select>
                  </div>
                  <div class="col-md-3 ">
                    <label for="zipcode">Pin code</label>
                    <input type="text" class="form-control" id="zipcode" name="zipcode" onkeypress="return isNum(event)"  />
                  </div>
                  <div class="col-md-3 ">
                    <label for="plan">Subscription Plan</label>
                    <select name="plan" id="plan" class="form-control">
                      <option value="">Select Plan</option>
                      <option value="0">Yearly (&#8377; 2000)</option>
                      <option value="1">Lifetime (&#8377; 5000)</option>
                    </select>
                  </div>
             
            </div>
                <input type="submit" id="submit"  value="Submit" class="button btn-success btn-lg" style=" height:40px; color: white; font-size: 15px; border-radius: 20px;cursor:pointer"/>
              
       </form>

    
        
      

       <form action="" id="fileupload" class="form-horizontal" role ="form" enctype="multipart/form-data" style="display: none">
          <div class="alert alert-info">
              <strong>One more step </strong>to get your subscription..!!
          </div>
        <div class="row">
              <div class="col-md-4"></div>
          <div class="col-md-5" id="message1" style="padding: 2px;  font-size: 15px;text-align: center;"> </div>
          <div class="col-md-3 "></div>
          <input type="hidden" name="doctor_id" id="doctor_id" value="">
           <div class="col-md-12 form-group" style=" margin:0px;">
                <label for="email" >Your Profile Pic 
                <input type="file"  id="profile" name="profile" style=" margin:4px;padding: 10px;background: #365BFA; display: table;color: #fff; font-size: 11px;border-radius: 3px" />
                </label>
              </div>
               
              <div class="col-md-12 form-group" style=" margin:0px;">
                <label for="email" >Your Degree 
                <input type="file"  id="degree" name="degree" style=" margin:4px;padding: 10px;background: #365BFA; display: table;color: #fff; font-size: 11px;border-radius: 3px" />
                </label>
              </div>
              <div class="col-md-12 form-group" style=" margin:0px;">
                <label for="email" >Your Registration Certificate 
                <input type="file"  id="cert" name="cert" style=" margin:4px;padding: 10px;background: #365BFA; display: table;color: #fff; font-size: 11px;border-radius: 3px" />
                </label>
              </div>
              
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="agree" id="agree"  value="1" style="font-size: 12px;display: block;width: auto;border: 1px solid #ddd;"><span style="padding-left: 3px; font-size: 13px;">I agree to Virtual Dentist's  <a href="<?php echo base_url()?>home/terms" target="_blank"> Terms and Privacy Policy </a> </span><br><br>

        </div>

        <input type="submit" id="upload"  value="Get Subscription" class="button btn-success btn-lg" style=" height:40px; color: white; font-size: 15px; border-radius: 20px;cursor:pointer"/>
        
       </form>
      </div>
      <div id="divLoading"> </div>
    </div>
    <div  class="col-md-2" style="padding: 25px; "></div>
  </div>  
  </div>
  </div>
<script type="text/javascript">
  function isNum(evt){

  evt =(evt)? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode<48 || charCode >57)){

    return false;
  }
return true;
}
</script>

<script>
$( document ).ready(function() {
    $( "#name" ).keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
  $('.btn-group.bootstrap-select').hide();
 $('#country').change(function(){
  var country_id = $('#country').val();
 
  if(country_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>home/sub_fetch_state",
    method:"POST",
    data:{country_id:country_id},
    success:function(data)
    {
      
     $('#state').html(data);
     $('#city').html('<option value="">Select City</option>');
    }
   });
  }
  else
  {
   $('#state').html('<option value="">Select State</option>');
   $('#city').html('<option value="">Select City</option>');
  }
 });

 $('#state').change(function(){
  var state_id = $('#state').val();
  if(state_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>home/sub_fetch_city",
    method:"POST",
    data:{state_id:state_id},
    success:function(data)
    {
      
     $('#city').html(data);
    }
   });
  }
  else
  {
   $('#city').html('<option value="">Select City</option>');
  }
 });
 
  var state_id = document.getElementById('state').value;

    $.ajax({
        url:"<?php echo base_url(); ?>home/sub_fetch_city",
        method:"POST",
        data:{state_id:state_id},
        success:function(data)
        {
          //alert(data);
         $('#city').html(data);
        }
      });

 
 
});
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
  $('#result').hide();
$(document).ready(function(){
    $(function() {
   $('#submit').click(function(e) {
        e.preventDefault();


        var type = document.getElementById('seeAnotherFieldGroup').value;
        var reg_no = document.getElementById('reg_no').value;
        var name = document.getElementById('name').value;
        var email = document.getElementById('email').value;
        var phone = document.getElementById('phone').value;
        var specialization = document.getElementById('spacial').value;
        var pass_y = document.getElementById('pass_y').value;
         var add = document.getElementById('address').value;
        var clinic_name = document.getElementById('clinic_name').value;
        var clinic_year = document.getElementById('clinic_year').value;
        var fee = document.getElementById('fee').value;
        var clinic_add = document.getElementById('clinic_add').value;
        var country1 = document.getElementById('country').value;
        var state1 = document.getElementById('state').value;
        var city1 = document.getElementById('city').value;
        var zipcode = document.getElementById('zipcode').value;
        var plan = document.getElementById('plan').value;

         if(reg_no == ''){

              $('#message').html("<i>Please enter registration number</i>").css('color', 'red');
              return false;
            }else if(name == ''){

              $('#message').html("<i>Please enter your name</i> ").css('color', 'red');
              return false;

            }else if(email == ''){

              $('#message').html("<i>Please enter your email address</i>").css('color', 'red');
              return false;
            }else if(IsEmail(email)==false){
                  $('#message').html("<i>Please enter correct email address</i>").css('color', 'red');
                  return false;
                }else if(phone == ''){

              $('#message').html("<i>Please enter your phone number</i>").css('color', 'red');
              return false;

            }
            else if(specialization == ''){

              $('#message').html("<i>Please select your specialization</i>").css('color', 'red');
              return false;
            }
            else if(pass_y == ''){

            $('#message').html("<i>Please enter passing year</i>").css('color', 'red');
            return false;
            }else if(add == ''){

            $('#message').html("<i>Please enter your address</i>").css('color', 'red');
            return false;
            }else if(clinic_name == ''){

            $('#message').html("<i>Please enter your clinic name</i>").css('color', 'red');
            return false;
            }else if(clinic_year == ''){

            $('#message').html("<i>Please enter your  established year</i>").css('color', 'red');
            return false;
            }else if(fee == ''){

            $('#message').html("<i>Please enter your counsultation fee</i>").css('color', 'red');
            return false;
            }else if(clinic_add == ''){

            $('#message').html("<i>Please enter your clinic address</i>").css('color', 'red');
            return false;
            }else if(country1 == '0'){

            $('#message').html("<i>Please enter your country</i>").css('color', 'red');
            return false;
            }else if(state1 == '0'){

            $('#message').html("<i>Please enter your state</i>").css('color', 'red');
            return false;
            }else if(city1 == '0'){

            $('#message').html("<i>Please enter your city</i>").css('color', 'red');
            return false;
            }else if(zipcode == ''){

            $('#message').html("<i>Please enter your pin code</i>").css('color', 'red');
            return false;
            }else if(plan == ''){

            $('#message').html("<i>Please select your subscription plan</i>").css('color', 'red');
            return false;
            }else{

              $('#message').html("").css('color', 'red');
              
    $.ajax({
        url: "<?php echo base_url();?>api/get_subscription",
        type:"POST",
        enctype: 'multipart/form-data',
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({type:type,reg_no:reg_no, name:name ,email:email,phone:phone,specialization:specialization,pass_y:pass_y, clinic_name:clinic_name,clinic_year:clinic_year,fee:fee,clinic_add:clinic_add,add:add,country1:country1,state1:state1,city1:city1,zipcode:zipcode,plan:plan}),
        success: function(response){

          if(response.status == true){

          var data = JSON.stringify(response.data);

          var doctor_id = data;

          $('#fileupload').css('display', 'block');
          $('#signup').css('display', 'none');
          $('#doctor_id').val(doctor_id);
        }

        if(response.status == false){

            var msg = JSON.stringify(response.msg);

            Swal.fire({
                        title: "<i style='color:orange'>Sorry...!!</i>", 
                        html: msg,  
                        confirmButtonText: "OK", 
                      });

        }

      }

                
      });


  }

    });


function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}

   
  });
});
</script>


<script type="text/javascript">
  
  $(document).ready(function(){
    
     $('#profile').change(function (event) {
    var val = $("#profile").val();
      if (!val.match(/(?:jpg|png)$/)) {
          $('#message1').text('Please upload only image ').css('color', 'red');
           $("#profile").val('');
          return false;
      }else{
        size = event.target.files[0].size;

        if(val.match(/(?:gif|jpg|png)$/)){

           if(size > 5000000){
            $('#message1').text('Uploaded image should not be greater than 50MB').css('color', 'red');
            $("#profile").val('');
            return false;
          }

          
        }

       
       
      }
       $('#message1').text('');
               
      });

  $('#degree').change(function (event) {
    var val = $("#degree").val();
      if (!val.match(/(?:jpg|png)$/)) {
          $('#message1').text('Please upload only image ').css('color', 'red');
           $("#degree").val('');
          return false;
      }else{
        size = event.target.files[0].size;
        if(val.match(/(?:gif|jpg|png|bmp)$/)){
          
          if(size > 5000000){
            $('#message1').text('Uploaded image should not be greater than 50MB').css('color', 'red');
            $("#degree").val('');
            return false;
          }
        }
       
      }
            $('#message1').text('');    
            });




$('#cert').change(function (event) {
    var val = $("#cert").val();
      if (!val.match(/(?:jpg|png)$/)) {
          $('#message1').text('Please upload only image').css('color', 'red');
           $("#cert").val('');
          return false;
      }else{
        size = event.target.files[0].size;
        if(val.match(/(?:gif|jpg|png|bmp)$/)){
          
          if(size > 5000000){
            $('#message1').text('Uploaded image should not be greater than 50MB').css('color', 'red');
            $("#cert").val('');
            return false;
          }
        }
       
      }
            $('#message1').text('');    
            });


  
      
      
      
      
   $('#fileupload').submit(function(e){
        e.preventDefault();
        var checkBox = document.getElementById("agree");
       var profile = $('#profile').prop('files')[0];
       var degree = $('#degree').prop('files')[0];
       var cert = $('#cert').prop('files')[0];
       var form_data = new FormData(this);
       form_data.append('profile', profile);
       form_data.append('degree', degree);
       form_data.append('cert', cert);

       if(document.getElementById("profile").value == ""){
       $('#message1').html("<i>Please upload your profile picture</i>").css('color', 'red');
        return false;
       }else if(document.getElementById("degree").value == ""){
       $('#message1').html("<i>Please upload your degree</i>").css('color', 'red');
        return false;
       }else if(document.getElementById("cert").value == ""){
       $('#message1').html("<i>Please upload your registration certificate</i>").css('color', 'red');
        return false;
       }else if(checkBox.checked == false){
       $('#message1').html("<i>Please agree to VirtualDentist's Terms & Privacy Policy</i>").css('color', 'red');
        return false;
       }else{
            $('#message1').html("");
             $("div#divLoading").addClass('show');
    $.ajax({
        url: "<?php echo base_url();?>api/update_document",
        dataType: 'json', // what to expect back from the server
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response){

          $('#fileupload').css('display', 'block');
          $('#signup').css('display', 'none');
                 $("div#divLoading").css('display', 'none');
           var data = JSON.stringify(response.msg);

        var obj = JSON.parse(data);

       
          
         Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: obj,  
                        confirmButtonText: "OK", 
                      });

          
           window.setTimeout(function() {
                       window.location.href = "<?php echo base_url();?>home/index";
                     }, 2000);

         
        }

                
      });
      
   }

  });


});
</script>