
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>
<div class="page-content bg-white"><!-- inner page banner -->

	<div class="dez-bnr-inr overlay-black-middle" style="background-image:url(images/doctor.jpg);">

		<div class="container">

			<div class="dez-bnr-inr-entry">

				<h1 class="text-white">Doctor Panel</h1>

<!-- Breadcrumb row -->



				<div class="breadcrumb-row">

					<ul class="list-inline">

						<li><a href="index.html">Home</a></li>

						<li>Doctor Panel</li>

					</ul>

				</div>

			</div>

		</div>

	</div>

<!-- inner page banner END -->



	<div class="content-block">

		<div class="section-full bg-white browse-job content-inner-2">

			<div class="container">

				<div class="row">

					<div class="col-md-12 col-xl-12 col-lg-12">

						<h5 class="widget-title font-weight-700 text-uppercase">Doctor or Specialist</h5>



						<p>1000+ online doctors, 40+ Specialities</p>



						<ul class="post-job-bx">

							<li>

								<div class="d-flex ">

									<div class="job-post-company">

										<a href="#"><span><img src="images/logo/sarika.jpg" /></span> </a>

									</div>



									<div class="job-post-info">

										<h4><a href="#">Dr. Sarika C Sonawane</a></h4>



											<ul>

												<li><a href="#">B.D.S., CRCDM</a></li>

											</ul>



										<div class="job-time">

											<p><a href="#">Register No.: A-16370</a></p>

										</div>

									</div>

								</div>

							</li>

							<li>

								<div class="d-flex ">

									<div class="job-post-company">

										<a href="#"><span><img src="images/logo/mahendra.jpg" /></span> </a>

									</div>



									<div class="job-post-info">

										<h4><a href="#">Dr. Mahendra Hiranand Rajpal</a></h4>



										<ul>

											<li><a href="#">B.D.S., from GDC &amp; H, 2007</a></li>

											<li><a href="#">12 yrs of experience with special attention to RCT, Implant, FMR N Cosmetics</a></li>

										</ul>



										<div class="job-time">

											<p><a href="#">Register No.: A-14354</a></p>

										</div>

									</div>

								</div>

							</li>

							<li>

								<div class="d-flex ">

									<div class="job-post-company">

										<a href="#"><span><img src="images/logo/ashvini.jpg" /></span> </a>

									</div>



									<div class="job-post-info">

										<h4><a href="#">Dr. Ashvini Phadnis</a></h4>



										<ul>

											<li><a href="#">BDS - 2006 &amp; MDS - 2017 </a></li>

											<li><a href="#">AV Children Dental Clinic, Degree- Pediatric and preventive dentist</a></li>

										</ul>

											<a href="#"><!--<div class="job-time">

												<p>Register No.: A-14354</p>

											</div>--> </a>

									</div>

								</div>

							</li>

							<li>

								<div class="d-flex ">

									<div class="job-post-company"><a href="#"><span><img src="images/logo/icon1.png" /></span> </a>

									</div>



									<div class="job-post-info">

										<h4><a href="#">Dr.</a></h4>



			



										<div class="job-time">

											<p><a href="#">Register No.: A-78905</a></p>

										</div>

									</div>

								</div>

							</li>

						</ul>



						<div class="pagination-bx m-t30">

							<ul class="pagination">

								<li class="previous"><a href="#">Prev</a></li>

								<li class="active"><a href="#">1</a></li>

								<li><a href="#">2</a></li>

								<li><a href="#">3</a></li>

								<li class="next"><a href="#">Next </a></li>

							</ul>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

