
<head>
<meta charset="UTF-8">
<title>Virtual Dentist</title>
</head>
<style type="text/css">
	.page-content {
    margin-top: -50px;
    padding-bottom: 50px;
}
</style>


<div class="page-content bg-white"><!-- inner page banner -->
  
    </div>
   	<div class="row" style="margin: 35px;">	
   		<div class="col-md-4" style="padding: 25px;"></div>			
		<div class="col-md-4" style="padding: 25px;">	
			<center><h6>Forgot Password ?</h6></center>
			
			<div class="forms">
				<form action="" id="forgot" class = "form-horizontal" role = "form">
				    <center><span id="message"></span></center>
			      	<div class="input-field">
			        	<div class="col-md-12 form-group">
				        	<label for="email">Email Id</label>
				        	<input type="email" class="form-control" id="femail" name="email_id" required="email" />
				    	</div>
				        
			        	<center>
			        		<input type="submit" value="Reset Password" id="forgotpass" class="button btn-success btn-lg" style=" height:30px; color: white; font-size: 15px; border-radius: 20px; ">
			        	</center><br>
			        		
				    </div>
				 </form>
			</div>
		</div>
		<div class="col-md-4" style="padding: 25px;"></div>	
	</div>
<br><br><br><br><br><br><br><br><br><br>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#forgotpass').click(function(e) {
        e.preventDefault();

		 var email = document.getElementById('femail').value;
		 
		 if(email == ''){

		        	$('#message').html("<i>Please enter your email address</i>").css('color', 'red');
		        	return false;
	        	}else if(IsEmail(email)==false){
                 	$('#message').html("<i>Please enter correct email address</i>").css('color', 'red');
                  return false;
                }else{
		 	$('#message').html("");
       
	        $.ajax({
	        url: "<?php echo base_url();?>api/getlink",
	        type:"POST",
	        processData:false,
	        contentType: 'application/json',
	        dataType: 'json',
	        async: false,
	        //data :form_data,
	        data: JSON.stringify({email:email}),
		    	success: function(response){
		    		var data = JSON.stringify(response.data);
		    		
		    		
		    		if(response.status == true){
		    		    
		    		    	Swal.fire({
			                  title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
			                  html: data,  
			                  confirmButtonText: "OK", 
			                });
			                
			                window.setTimeout(function() {
                      window.location.href = "<?php echo base_url();?>home/login";
                    }, 3000);
			                 
		    		    
		    		}
		    		
		    	 if(response.status == false){
		    	     
		    	     	Swal.fire({
			                  title: "<i style='color:orange'>Sorry...!!</i>", 
			                  html: data,  
			                  confirmButtonText: "OK", 
			                });
		    	     
		    	 }

		    	
		    	}

                
      		});
      		
		}

        });
        
        function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}
        
        
	});
</script>