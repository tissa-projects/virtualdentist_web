<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

  <style type="text/css">
  	.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0px;
    word-wrap: break-word;
    background-color: #FFFFFF;
    background-clip: border-box;
    border: 2px solid #FFFFFF;
    border-radius: 1.25rem;
}
.topcorner{
   position:absolute;
   top:0;
   right:11px;
  }
  .current {
  color: green;
}

.page-item.active .page-link {
    border-radius: 23px;
}
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
	  $(function(){
     
    });

	$(document).ready(function(){
				var data = sessionStorage.getItem('mydata');

				var obj = JSON.parse(data);
				var html = ''; var star = '';
				var i;var j;

				for(i=0; i< obj.length; i++){

					var note = obj[i].stars ;

				 var numberOfStars = Math.round(note);
      			var stars = '';
      			for(var index = 0; index < numberOfStars; index++)
        		stars += ' <span class="fa fa-star checked"></span>';
        	notrate = "Not rated yet";
        		 if(stars == ''){
        		 	notrate = "Not rated yet";
        		 	stars = '';
        		 }

			   html += '<div class="row align-items-center m-b50" style="border: 2px solid #1ebbf0;border-radius: 20px;" ><div class="col-md-2"><div class="card" style="border: none;"><div class="view overlay" ><img src="<?php echo base_url(); ?>uploads/profile_pics/'+obj[i].profile_pic+'" class="mx-auto img-fluid img-circle d-block" alt="avatar" width="163px;" style="height:150px;border-radius: 80px;"><a href=""><div class="mask rgba-white-slight"><b style="font-size:15px;">'+obj[i].name+'</b></div></a></div></div></div><div class="col-md-3"><div class="card" style=""><div class="card-body"><h6 class="card-title">'+obj[i].specialization+'</h6><p class="card-text"><span data-reactid="227">'+obj[i].passing_year+'</span><br><a href="/bangalore/dentist/malleswaram" data-reactid="230"><span data-qa-id="practice_locality" data-reactid="231">'+obj[i].address+'</span><span class="u-t-capitalize" data-qa-id="practice_city" >'+obj[i].city+'</span></a><br><span class="u-c-pointer u-t-hover-underline" data-qa-id="doctor_clinic_name" data-reactid="240">'+obj[i].clinic_name+'</span></p><b></b> '+stars+' </div></div></div><div class="col-md-3"><div class="card myDiv1" style="float: left; padding-bottom: 14px; "><div class="card-body "><i class="fa fa-calendar" aria-hidden="false" style="font-size:30px;color:blue"></i>&nbsp;&nbsp;&nbsp;<input type = "text" class="datepicker" id="datepicker_'+obj[i].doctor_id+'" onclick="showdate('+obj[i].doctor_id+');" ><input type="hidden" id="doctorid_'+obj[i].doctor_id+'" value="'+obj[i].doctor_id+'" ><br><br><button class="btn-sm btn btn-primary" onclick="show_time('+obj[i].doctor_id+')" "> Show Available Time</button><br><br><span id="showt_'+obj[i].doctor_id+'" style="display:none;">Select Appointment Date First</span></div></div></div><div class="col-md-4"><div class="card"><div class="card-body"><span data-qa-id="consultation_fee" class="" data-reactid="250">₹ &nbsp;&nbsp;'+obj[i].counsult_fee+'</span><span data-reactid="254">&nbsp;&nbsp;Consultation fee at clinic</span><br><br><a href="#" class="btn btn-primary" onclick="viewdoctor('+obj[i].doctor_id+')">View Profile</a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="book_appointment('+obj[i].doctor_id+')" style="color: white" class="sd1 btn btn-primary">Book Appointment</a></div></div></div><div class="cl_'+i+' col-md-12 " id="time_'+obj[i].doctor_id+'" style="display:none;cursor:pointer"><div class="card"><a href"#" class="topcorner" id="close_'+i+'" class="sd1 btn btn-primary">Close</a><div class="card-body"><div>Morning </div><div class = "col-md-12" id="mortime_'+obj[i].doctor_id+'"></div><hr><div>Evening</div><div class = "col-md-12" id="evetime_'+obj[i].doctor_id+'"></div></div></div></div></div>';		

    		}

    		 $('#app').html(html);

                for(j=0; j < obj.length; j++){

                	
                	// $("#datepicker_"+j).datepicker({ 
                 //    dateFormat: 'yy-mm-dd',
                 //    minDate: 0
                 //   // beforeShowDay: unavailable
                 //  });

                }


                 for(j=0; j < obj.length; j++){

                 	$("#close_"+j).click(function(){
                 		for(i=0; i < obj.length; i++){
                 			
	  			 		$(".cl_"+i).css("display", "none");
	  			 		}
					});

                 }


    });
  </script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script type="text/javascript">

    function showdate(a){
      alert("you have clicked on datepicker");

      var doctor_id = a;
      $.ajax({
            url: "<?php echo base_url();?>api/get_holiday",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            data: JSON.stringify({doctor_id:doctor_id}),
            success: function(response){
                var data = response.data;
              //var unavailableDates = Array [];
              var unavailableDates = new Array();
                var i;
              for(i=0; i< data.length; i++){
                var date = data[i].holiday_date;
                    unavailableDates.push(date);
                }
              function unavailable(date) {
                  dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
                  if ($.inArray(dmy, unavailableDates) < 0) {
                    return [true,"","Book Now"];
                  } else {
                    return [false,"","Booked Out"];
                  }
                }
               
                $("#datepicker_"+a).datepicker({ 
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                beforeShowDay: function(date){
                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                        return [ unavailableDates.indexOf(string) == -1 ]
                    }
              });
                                           
        }         
        });
    }



  	function show_time(value) {

  		var date = $("#datepicker_"+value).val();


  		if(date == ''){

  			 $("#showt_"+value).css("display", "block");

  		}else{

  		$("#showt_"+value).css("display", "none");

  		var doctor_id = $("#doctorid_"+value).val();
  		var date = $("#datepicker_"+value).val();

		$.ajax({
        url: "<?php echo base_url();?>api/gettimeslot",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id,date:date}),
	    	success: function(response){

	    		if(response.status == true){

	    		$("#time_"+value).css("display", "block");
	    		var morning = JSON.stringify(response.morning);
	    		var evening = JSON.stringify(response.evening);
	    		var schedule = JSON.stringify(response.schedule);

	    		var obj = JSON.parse(morning);
	    		var obj2 = JSON.parse(schedule);

				var html = '';
				var i;var j;
				for(i=0; i< obj.length; i++){

					if(obj2.length > 0){

					for(j=0; j< obj2.length ; j++){

						if(obj2[j].schedule_id == obj[i].mor_time){

							html +='<a href="" class="badge badge1 badge-dark badge-pill" style="color:blue;"> Booked slot</a>';

						}else{

							html +='<a href="" class="badge badge1 badge-dark badge-pill"><input type="radio" style="background-color:white;" name="time_morning" value="'+obj[i].mor_time+'">'+obj[i].mor_time+'</a>';
						}
					}
				}else{

					html +='<a href="" class="badge badge1 badge-dark badge-pill"><input type="radio" style="background-color:white;" name="time_morning" value="'+obj[i].mor_time+'">'+obj[i].mor_time+'</a>';

				}

				} 

			
				 $("#mortime_"+value).html(html);


				 var obj1 = JSON.parse(evening);
				var html1 = '';
				var k;var m;
				for(k=0; k< obj1.length; k++){

					if(obj2.length > 0 ){

					for(m=0; m< obj2.length ; m++){

						if(obj2[m].schedule_eve_id == obj1[k].eve_time){

							html1 +='<a href="" class="badge badge1 badge-dark badge-pill" style="color:blue;"> Booked slot</a>';

						}else{

							html1 +='<a href="" class="badge badge1 badge-dark badge-pill"><input type="radio" style="background-color:white;" name="time_morning" value="'+obj1[k].eve_time+'">'+obj1[k].eve_time+'</a>';
						}
					}

				}else{

					html1 +='<a href="" class="badge badge1 badge-dark badge-pill"><input type="radio" style="background-color:white;" name="time_morning" value="'+obj1[k].eve_time+'">'+obj1[k].eve_time+'</a>';


					}

				}

				 $("#evetime_"+value).html(html1);

				}


				if(response.status == false){

					var msg = JSON.stringify(response.msg);

    					Swal.fire({
			                  title: "<i>Sorry...!!</i>", 
			                  html: msg,  
			                  confirmButtonText: "OK", 
			                });
    					
			}          
		}           
    });
 }
}
  </script>


  <script type="text/javascript">

  	function book_appointment(value1){

  	
  		var doctor_id = $("#doctorid_"+value1).val();

  		var timeid =  $("input[name='time_morning']:checked").val();

  		var timeeveid =  $("input[name='time_morning']:checked").val();

  		var date = $("#datepicker_"+value1).val();

  		var time = ', ' + $('input[name="time_morning"]:checked').parent().text();

  		var timeeve = ', ' + $('input[name="time_morning"]:checked').parent().text();

  		if(date == '' ){

  			 Swal.fire({
                        title: "<i>Sorry...!!</i>", 
                        html: "Date and time Selection is Mandatory ",  
                        confirmButtonText: "OK", 
                      });

  			return false;

  		}else if(timeid == 'undefined'){

  			 Swal.fire({
                        title: "<i>Sorry...!!</i>", 
                        html: "Date and time Selection is Mandatory ",  
                        confirmButtonText: "OK", 
                      });

  			return false;

  		}else{

		$.ajax({
        url: "<?php echo base_url();?>api/get_doctor",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: JSON.stringify({doctor_id:doctor_id}),
	    	success: function(response){

	    		var data = JSON.stringify(response.data); 
	    	sessionStorage.setItem('timeid', timeid);
	    	sessionStorage.setItem('timeeveid', timeeveid);
	    	sessionStorage.setItem('time', time);
	    	sessionStorage.setItem('timeeve', timeeve);
	    	sessionStorage.setItem('date', date);
    		sessionStorage.setItem('mybooking', data);
			window.location.href = "<?php echo base_url();?>home/booking";
	    		
		}           
    });

	}

  	}

</script>

  <script type="text/javascript">
  	function viewdoctor(value){

  		var doctor_id = $("#doctorid_"+value).val();

  		$.ajax({
        url: "<?php echo base_url();?>api/get_doctor",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id}),
	    	success: function(response){

	    	var data = JSON.stringify(response.data); 

	    	var feedback = JSON.stringify(response.feedback); 
	    	
    		sessionStorage.setItem('myview', data);
    		sessionStorage.setItem('myfeed', feedback);
			window.location.href = "<?php echo base_url();?>home/view_doctor";
	    		
		}           
    });

  	}
  	
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#close").click(function(){
	  			 $(".cl").css("display", "none");
		});
	});
</script>


  
<style>

.checked {
  color: orange;
}
.badge1 {
    display: inline-block;
   /* padding: .25em .4em;*/
    font-size: 83%;
    /* font-weight: 700; */
    line-height: 1;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25rem;
    background-color: #ffff !important;
    border:1px solid gray;
    color:gray;
    padding:8px;
    /*padding-left: 20px;
    padding-bottom:  20px;*/
}

.badge-dark[href]:focus, .badge-dark[href]:hover {
    color: #ffff;
    text-decoration: none;
    background-color: blue !important;
}
</style>





<div class="page-content bg-white"><!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/images/about-banner.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">Doctor Panel</h1>
    <!-- Breadcrumb row -->

                <div class="breadcrumb-row">
                    <ul class="list-inline">
    	               <li><a href="index.html">Home</a></li>
    	               <li>Doctor Panel</li>
                    </ul>
                </div>
<!-- Breadcrumb row END -->
            </div>
        </div>
    </div>
<!-- inner page banner END -->
	
    <div class="content-block" >
        <div class="section-full content-inner overlay-white-middle">
            <div class="container" id="app">


        
             
            </div>

         
        </div>
    </div>
<!-- contact area END -->
</div>



<!--  <script type="text/javascript">-->

<!--   $(function (){-->
<!-- $(".datepicker").on("click",function () {-->
      //alert("you have clicked on datepicker");
<!--       var unavailableDates = new Array();-->
     
<!--      var a = $(this).attr("data-id");-->
<!--      var doctor_id = a;-->
<!--       $.ajax({-->
<!--            url: "<?php echo base_url();?>api/get_holiday",-->
<!--            type:"POST",-->
<!--            processData:false,-->
<!--            contentType: 'application/json',-->
<!--            dataType: 'json',-->
<!--            async: false,-->
<!--            data: JSON.stringify({doctor_id:doctor_id}),-->
<!--            success: function(response){-->
<!--                var data = response.data;-->
               //var unavailableDates = Array [];
              
<!--                var i;-->
<!--               for(i=0; i< data.length; i++){-->
<!--                var date = data[i].holiday_date;-->
<!--                    unavailableDates.push(date);-->
<!--                }-->
                                           
<!--        } -->

<!--        });-->


<!--        $(this).datepicker({ -->
<!--                dateFormat: 'yy-mm-dd',-->
<!--                minDate: 0,-->
<!--                beforeShowDay: function(date){-->
<!--                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);-->
<!--                        return [ unavailableDates.indexOf(string) == -1 ]-->
<!--                    }-->
<!--                }).datepicker( "show" );-->



<!--    });-->

<!--  });-->

<!--</script>-->



        <!-- footer bottom part -->


