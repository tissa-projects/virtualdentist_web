<head>
<meta charset="UTF-8">
<title>Dental Problems and Treatment | Best Dental Care | Virtual Dentist</title>
<meta name="Description" content="Virtual Dentist provides the best dental care for any dental problems and provide the best treatment for any teeth issues and problems like teeth filling, tooth denture etc.">
<meta name="Keywords" content="best dental care, best dental clinic, common dental problems, dental issues, dental problems and treatment, dental services, dental treatment, dentist near me, dentist specialist, denture dentist, kids teeth falling out, teeth issues, teeth problems, teeth treatment near me, tooth denture, tooth filling near me, treatment of teeth">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<style type="text/css">
	.find-job-bx form {
    padding: 30px 30px 5px 30px;
    background-color: rgba(213, 214, 218, 0.68);
    border-radius: 30px;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 1);
}

</style>
<div class="page-content"><!-- Section Banner -->


    <div class="dez-bnr-inr dez-bnr-inr-md" style="background-image:url(<?php echo base_url();?>assets/images/main-slider/slide.jpg);">
		<div class="container">
			<div class="dez-bnr-inr-entry align-m ">
				<div class="find-job-bx">
				<form class="dezPlaceAni" action="" method="post" id="search">
						<b><p style="font-size:20px; text-align: center; color: #474c51;">Highly Qualified, Experienced and Verified Dental Experts on-the-go </p><h6 style="font-size:15px; text-align: center; color: #474c51;">Search  Dentist And Book Your Appointment</h6></b>
						<div class="row">
							
							<div class="col-lg-2 col-md-2">
								<div class="form-group"><label></label>
									<div class="input-group"></div>
									<input list="browsersc" id="country" name="country" class="form-control" class="form-control" placeholder="" type="text" value="India">
									  <datalist id="browsersc" style="width: 100%">

									  	<?php
										    foreach($country as $row)
										    {
										     echo '<option value="'.$row->country_name.'" data-id="'.$row->country_id.'">';
										    }
										    ?>
									    
									    
									  </datalist>
								</div>
							</div>

							<div class="col-lg-2 col-md-2">
								<div class="form-group"><label>Enter State</label>
									<div class="input-group"></div>
									<input list="browsersc1" name="state" id="state" class="form-control" class="form-control" placeholder="" type="text" value="" >
									  <datalist id="browsersc1" style="width: 100%">
									    	 <?php
										    foreach($state as $row)
										    {
										     echo '<option value="'.$row->state_name.'" data-id="'.$row->state_id.'" >';
										    }
										    ?>
									    
									  </datalist>
								</div>
							</div>

							<div class="col-lg-2 col-md-2">
								<div class="form-group"><label>Enter City</label>
									<div class="input-group"></div>
									<input list="browsersc2" name="city" id="city" class="form-control" class="form-control" placeholder="" type="text" >
									  <datalist id="browsersc2" style="width: 100%">
									    <!-- <option value="Nagpur">
									    <option value="City">
									    <option value="Banglore"> -->
									    
									  </datalist>
								</div>
							</div>

							<div class="col-lg-2 col-md-2">
								<div class="form-group"><label>Enter Pincode</label>
									<!-- <div class="input-group"> -->
										<input class="form-control" name="pincode" id="pin" type="text" value="" >
										
									<!-- </div> -->
								</div>
							</div>

							<div class="col-lg-2 col-md-2">
								<div class="form-group"><label>Select Specialization</label>
									<div class="input-group"></div>
									<input list="browsersc3" name="specialization" id="specialization" class="form-control" class="form-control" placeholder="" type="text" value="" >
									<datalist id="browsersc3" style="width: 100%">
								    	<option value="">Select Specialization</option>
				                        <option value="Oral Pathology">Oral Pathology</option>
				                        <option value="Forensic Odontology">Forensic Odontology</option>
				                        <option value="Oral Physiotherapy">Oral Physiotherapy</option>
				                        <option value="Oral Nutrition And Detects">Oral Nutrition And Detects</option>
				                        <option value="Oral Medicine">Oral Medicine</option>
				                        <option value="Oral Radiology">Oral Radiology</option>
				                        <option value="Cranio Oro Maxillofacial Surgery">Cranio Oro Maxillofacial Surgery</option>
				                        <option value="Periodontics">Periodontics</option>
				                        <option value="Operative Dentistry And Endodontics">Operative Dentistry And Endodontics</option>
				                        <option value="Orthodontics">Orthodontics</option>
				                        <option value="Prosthodontics">Prosthodontics</option>
				                        <option value="Implantology">Implantology</option>
				                        <option value="Aesthetic And Cosmetic Dentistry">Aesthetic And Cosmetic Dentistry</option>
				                        <option value="Dental Lasers">Dental Lasers</option>
				                        <option value="Minimal Intervention Dentistry">Minimal Intervention Dentistry</option>
				                        <option value="Sedative Dentistry">Sedative Dentistry</option>
				                        <option value="Dental Sleep Medicine">Dental Sleep Medicine</option>
				                        <option value="Pedodontics">Pedodontics</option>
				                        <option value="Public Health Dentistry">Public Health Dentistry</option>
                      					<option value="General Dentistry">General Dentistry</option>
								    </datalist>
								</div>
							</div>


							<div class="col-lg-2 col-md-2">
								<button class="site-button btn-block" type="submit" id="getsearch">Get Your Search </button>
							</div>
							<div class="col-md-5"></div>
							<div class="col-md-4"><i>	<span id="message" style="font-size: 15px;"></span></i></div>
							<div class="col-md-3"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="dez-bnr-inr dez-bnr-inr-md" style="background-image:url(../images/main-slider/slide.jpg);"></div> -->
<!-- Section Banner END --><!-- About Us --><!-- About Us END -->
		<div class="section-full job-categories content-inner-2 bg-white" style="background-image:url(<?php echo base_url();?>assets/images/pattern/pic1.html);">
		<div class="container">
			<div class="section-head text-black text-center">
				<h2 class="m-b0">Our Services</h2>
			</div>

			<div class="row sp20">
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20">
								<a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon1.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Cosmetic Dentistry</a>
							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon1.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon2.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Dental Implant</a>
							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon2.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon3.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Laser Dentistry</a>
							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon3.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon4.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Full Mouth Rehabilitation</a>
							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon4.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon5.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Teeth Whitening</a>

							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon5.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon6.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Painless Root Canal Treatment</a>

							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon6.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon7.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">Orthodontics Emphasis</a>

							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon7.png" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="icon-bx-wraper">
						<div class="icon-content">
							<div class="icon-md text-primary m-b20"><a href="<?php echo base_url();?>home/services"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon8.png" /></a>
							</div>
							<a class="dez-tilte" href="<?php echo base_url();?>home/services">General Dentistry</a>

							<div class="rotate-icon">
								<img alt="" class="logo" src="<?php echo base_url();?>assets/images/icon8.png" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div style="padding: 0 65px 0 65px; text-align: justify">
	<h1><center>Online Dentist Consultation</center></h1>
<p>Tooth filling can be done by the dentist. Treatment of teeth and taking good care of your teeth. <b>Dental filling near me</b>, the dental clinic nearby and the dentist can provide you with all sorts of treatments. <b>Best doctor for dental treatment</b> the one who can surgically place the artificial tooth in your jaw bone to support another tooth or for better look etc.</p>

<p>Tooth filling near me, the dental clinic nearby and the dentist can provide you with all sorts of treatments. Dental issues like caries, fractured tooth, gum disease, and teeth whitening etc. Dental problems and treatment, Prevent dental problems and get treated by right and from the best dentist. <a href="https://www.virtualdentist.in/home/services"><b>Dental services</b></a>, the dental clinic that can provide you with all sort of problem and that will give you all sort of services. Dental treatments, like scaling, filling, cleaning, whitening teeth, etc. These are all comes under dental treatment. Teeth issues like caries tooth, swollen gums, bleeding gums, bad breath, can be treated by the dentist. Teeth problems can be treated by the best dentist. <b>Teeth treatment near</b>, taking better treatment for your dental problems. </p>
<br><br><br>
</div>
<!-- Testimonials -->

<div class="section-full p-tb70 overlay-black-dark text-white text-center bg-img-fix" style="background-image: url(<?php echo base_url();?>assets/images/background/bg4.jpg);">
	<div class="container">
		<div class="section-head text-center text-white">
			<h2 class="m-b5">Testimonials</h2>

			<h5 class="fw4">Few words from Patients</h5>
		</div>

		<div class="blog-carousel-center owl-carousel owl-none">
			<div class="item">
				<div class="testimonial-5">
					<div class="testimonial-text">
						<p>I was really nervous before the appointment but the doctor made me feel really safe and comfortable throughout the session. Even the staff at the clinic was very helpful and cheery. The doctor was very professional, and he was able to explain my dental problem.</p>
					</div>

					<div class="testimonial-detail clearfix">
						<div class="testimonial-pic radius shadow">
							<img alt="" height="100" src="<?php echo base_url();?>assets/images/testimonials/pic1.jpg" width="100" />
						</div>
						<strong class="testimonial-name">Rani Ojha</strong>
					</div>
				</div>
			</div>

			<div class="item">
				<div class="testimonial-5">
					<div class="testimonial-text">
						<p>It&rsquo;s not easy to find a good dentist who actually listens to your problem. I&rsquo;m glad I found one with the help of Virtual Dentist. I was afraid to consult a dentist, but I took a chance anyway. Along with offering the pain-free treatment, the doctor also gave me advice and suggestions on how I can take care of my teeth.</p>
					</div>

					<div class="testimonial-detail clearfix">
						<div class="testimonial-pic radius shadow"><img alt="" height="100" src="<?php echo base_url();?>assets/images/testimonials/pic1.jpg" width="100" />
					</div>
					<strong class="testimonial-name">Swapnil Chawan</strong></div>
				</div>
			</div>

			<div class="item">
				<div class="testimonial-5">
					<div class="testimonial-text">
						<p>I needed a root canal but couldn&rsquo;t find a good dentist anywhere. Virtual dentist helped me find an experienced dentist near my locality. I was greeted by the staff as soon as I entered the clinic, they really did put their efforts to make me feel relax and be comfortable</p>
					</div>

					<div class="testimonial-detail clearfix">
						<div class="testimonial-pic radius shadow">
							<img alt="" height="100" src="<?php echo base_url();?>assets/images/testimonials/pic1.jpg" width="100" />
						</div>
						<strong class="testimonial-name">Sarita Lokhande</strong>
					</div>
				</div>
			</div>

			<div class="item">
				<div class="testimonial-5">
					<div class="testimonial-text">
						<p>I wanted to get my teeth cleaned but couldn&rsquo;t find a reliable dentist. I know how expensive such procedures can be, and that&rsquo;s why I took the help of Virtual Dentist. And, I have to say that the dentist pays attention to every detail. All the dental procedures are reasonably priced.</p>
					</div>

					<div class="testimonial-detail clearfix">
					<div class="testimonial-pic radius shadow">
						<img alt="" height="100" src="<?php echo base_url();?>assets/images/testimonials/pic1.jpg" width="100" />
					</div>
					<strong class="testimonial-name">Yogesh Pawar</strong></div>
				</div>
			</div>

			<div class="item">
				<div class="testimonial-5">
					<div class="testimonial-text">
						<p>The dentist was on time and he explained the pros and cons of the dental treatment in detail. The staff was helpful and they work well together. My dentist was really thoughtful and even suggested me to switch to a better toothpaste and gave advice on how I can take better care of my oral hygiene with extreme patience.</p>
					</div>

					<div class="testimonial-detail clearfix">
						<div class="testimonial-pic radius shadow">
							<img alt="" height="100" src="<?php echo base_url();?>assets/images/testimonials/pic1.jpg" width="100" />
						</div>
						<strong class="testimonial-name">Akshta Jadhav</strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Call To Action END --><!-- Our Latest Blog -->

<div class="section-full content-inner-2 overlay-white-middle" style="background-image:url(<?php echo base_url();?>assets/images/lines.png); background-position:bottom; background-repeat:no-repeat; background-size: 100%;">
	<div class="container">
		<div class="section-head text-black text-center"><!--<h2 class="m-b0">Ask a Doctor or Specialist</h2>-->
			<p>1000+ online doctors, 40+ Specialities</p>
		</div>
<!-- Pricing table-1 Columns 3 with gap -->

		<div class="section-content box-sort-in button-example m-t80">
			<div class="pricingtable-row">
				<div class="row max-w1000 m-auto">
					<div class="col-sm-12 col-md-4 col-lg-4 p-lr0">
						<div class="pricingtable-wrapper style2 bg-white ">
							<div class="pricingtable-inner">
								<div class="pricingtable-price">
									<h4>Free Follow-up with online doctor consultation</h4>
								</div>

								<p>Now you do not have to waste your time visiting the doctor&rsquo;s clinic for a follow-up. You can get the proper diagnosis and treatment plan from the doctors online. You can instantly connect with the doctors through a live chat session, video call, or phone call. You will receive all your prescriptions and test reports online.</p>

								<div class="m-t20">
									<a class="site-button radius-xl" href="#"><span class="p-lr30">Read More</span></a>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 col-lg-4 p-lr0">
						<div class="pricingtable-wrapper style2 bg-primary text-white active">
							<div class="pricingtable-inner">
								<div class="pricingtable-price">
									<h4>Board Certified Specialist</h4>
								</div>

								<p>at Virtual Dentist, you will find the dental professionals who are licensed and board certified. All the doctors are highly experienced in handling and treating their patients with care and respect. You&rsquo;d be able to get the best medical advice from the medical professionals all over the world. You can find the best doctor near your locality at a cost-effective price. You can also get an online referral of the dentists who specialize in different areas of dentistry.</p>

								<div class="m-t20">
									<a class="site-button white radius-xl" href="#"><span class="p-lr30">Read More</span></a>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 col-lg-4 p-lr0">
						<div class="pricingtable-wrapper style2 bg-white">
							<div class="pricingtable-inner">
								<div class="pricingtable-price">
									<h4>Private and Anonymous Consultation</h4>
								</div>

								<p>Consult with the top medical experts online anonymously. You can get the opinion of the medical experts by just one call.</p>

								<div class="m-t20">
									<a class="site-button radius-xl" href="#"><span class="p-lr30">Read More</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Our Latest Blog -->

<div class="section-full content-inner-2 call-to-action overlay-black-dark text-white text-center bg-img-fix" style="background-image: url(<?php echo base_url();?>assets/images/background/bg4.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="m-b10">Find the highly-qualified and experienced dental experts on-the-go.</h2>
	
				<p class="m-b0">Book your appointment with the best-verified doctors online with just a click.</p>
				<a class="site-button m-t20 outline outline-2 radius-xl" href="<?php echo base_url();?>home/consultation">Online Consultation</a>
			</div>
		</div>
	</div>
</div>

<div class="section-full job-categories content-inner-2 bg-white" style="background-image:url(<?php echo base_url();?>assets/images/pattern/pic1.html);">
	<div class="container">
		<div class="d-flex head-counter clearfix">
			<div class="mr-auto">
				<h2 class="m-b5">Popular Categories</h2>

				<h6 class="fw3">20+ Categories work Waiting for you</h6>
			</div>

			<div class="head-counter-bx">
				<h2 class="m-b5 counter text-center">1800</h2>

				<h6 class="fw3">Professional Doctors</h6>
			</div>

			<div class="head-counter-bx">
				<h2 class="m-b5 counter text-center">45</h2>

				<h6 class="fw3">Doctor Awards</h6>
			</div>

			<div class="head-counter-bx">
				<h2 class="m-b5 counter text-center">2500</h2>

				<h6 class="fw3">People Like Us</h6>
			</div>

			<div class="head-counter-bx">
				<h2 class="m-b5 counter text-center">10,000+</h2>

				<h6 class="fw3">Patients Satisfied</h6>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
		$(document).ready(function(){
		 $('#country').change(function(){
		 	var value = $(this).val();
		    var country_id = $('#browsersc [value="' + value + '"]').data('id');
		  //alert(country_id);
		  if(country_id != '')
		  {
		   $.ajax({
		    url:"<?php echo base_url(); ?>home/fetch_state",
		    method:"POST",
		    data:{country_id:country_id},
		    success:function(data)
		    {
		    	//alert(data);
		     $('#browsersc1').html(data);
		    // $('#city').html('<option value="">Select City</option>');
		    }
		   });
		  }
		  // else
		  // {
		  //  $('#state').html('<option value="">Select State</option>');
		  //  $('#city').html('<option value="">Select City</option>');
		  // }
		 });

		$('#state').change(function(){
					var value = $(this).val();
			    var state_id = $('#browsersc1 [value="' + value + '"]').data('id');
			    //alert(state_id);
		if(state_id != '')
		{
		$.ajax({
			url:"<?php echo base_url(); ?>home/fetch_city",
			method:"POST",
			data:{state_id:state_id},
			success:function(data)
			{
				//alert(data);
			  $('#browsersc2').html(data);
			}
		});
	}
  // else
  // {
  //  $('#city').html('<option value="">Select City</option>');
  // }
 });
 
});
</script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
	
$(document).ready(function(){
    $(function() {
		   $('#getsearch').click(function(e) {
		        e.preventDefault();

		        var country = document.getElementById('country').value;
		        var state = document.getElementById('state').value;
		        var city = document.getElementById('city').value;
		        var pincode = document.getElementById('pin').value;
		        var specialization = document.getElementById('specialization').value;
		        if(state == ''){

		        	$('#message').html("Select state for city please").css('color', 'red');
		        	return false;
		        }else if(city == ''){

		        	$('#message').html("Select city for your search ").css('color', 'red');
		        	return false;

		        }else if(pincode == ''){

		        	$('#message').html("Give us pincode for exact search").css('color', 'red');
		        	return false;

		        }else if(specialization == ''){

		        	$('#message').html("Select specialization for your search").css('color', 'red');
		        	return false;

		        }else{
		        
		        $('#message').html("");
		      
		        $.ajax({
		        url: "<?php echo base_url();?>api/search",
		        type:"POST",
		        enctype: 'multipart/form-data',
		        processData:false,
		        contentType: 'application/json',
		        dataType: 'json',
		        async: false,
		        //data :form_data,
		        data: JSON.stringify({state:state, city:city ,pincode:pincode, specialization:specialization}),
			    	success: function(response){

			    		var data = JSON.stringify(response.data);

			    		//alert(data);

			    		var msg = JSON.stringify(response.msg);

			    		if(response.status == true){

			    		sessionStorage.setItem('mydata', data);
    					window.location.href = "<?php echo base_url();?>home/search";

    					}
    					if(response.status == false){



    					Swal.fire({
			                  title: "<i style='color:orange'>Sorry...!!</i>", 
			                  html: msg,  
			                  confirmButtonText: "OK", 
			                });
    					
						}           
    			}
		    });
		  }
    	});
	});
});
</script>
