
<head>
<meta charset="UTF-8">
<title>Contact Us | Virtual Dentist</title>
</head>

<style>
    .contact-style-1 .border-1 {
    border: none;
}
 
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="page-content bg-white"><!-- inner page banner -->
	<div class="dez-bnr-inr overlay-black-middle" style="background-image:url(<?php echo base_url();?>assets/images/contact.jpg);">
		<div class="container">
			<div class="dez-bnr-inr-entry">
			<h1 class="text-white">Contact Us</h1>
<!-- Breadcrumb row -->

				<div class="breadcrumb-row">
					<ul class="list-inline">
						<li><a href="<?php echo base_url();?>home/index">Home</a></li>
						<li>Contact Us</li>
					</ul>
				</div>
<!-- Breadcrumb row END -->
			</div>
		</div>
	</div>
<!-- inner page banner END --><!-- contact area -->

	<div class="section-full content-inner bg-white contact-style-1">
		<div class="container">
			<div class="row"><!-- right part start -->
				<div class="col-lg-4 col-md-6 d-lg-flex d-md-flex">
					<div class="p-a30 border m-b30 contact-area border-1 align-self-stretch radius-sm">
						<h4 class="m-b10">Quick Contact</h4>

						<p>If you have any questions simply use the following contact details.</p>

						<ul class="no-margin">
							<li class="icon-bx-wraper left m-b30">
								<div class="icon-bx-xs border-1"><i class="fa fa-user" style="font-size:35px;color:#1D46F5;" aria-hidden="true"></i></div>

								<div class="icon-content">
									<h6 class="text-uppercase m-tb0 dez-tilte">Contact Person:</h6>

									<p>Dr. Sarika C Sonawane</p>
								</div>
							</li>
							<li class="icon-bx-wraper left m-b30">
								<div class="icon-bx-xs border-1"><i class="fa fa-medkit" style="font-size:35px;color:#1D46F5;" aria-hidden="true"></i></div>

								<div class="icon-content">
									<h6 class="text-uppercase m-tb0 dez-tilte">Address:</h6>

									<p>Sector 20, Kharghar, Navi Mumbai, 410210</p>
								</div>
							</li>
							<li class="icon-bx-wraper left  m-b30">
								<div class="icon-bx-xs border-1"><i class="fa fa-envelope" style="font-size:35px;color:#1D46F5;" aria-hidden="true"></i></div>

								<div class="icon-content">
									<h6 class="text-uppercase m-tb0 dez-tilte">Email:</h6>

									<p>info@virtualdentist.in</p>
								</div>
							</li>
	
						</ul>

						<div class="m-t20">
							<ul class="dez-social-icon dez-social-icon-lg">
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
					</div>
				</div>
<!-- right part END --><!-- Left part start -->

				<div class="col-lg-4 col-md-6">
					<div class="p-a30 m-b30 radius-sm bg-gray clearfix">
						<h4>Message us</h4>

						<form action="enquiry.php" class="dzForm" method="post">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="input-group"><input class="form-control" id="name" name="name" placeholder="Your Name" required="" type="text" />
										</div>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="form-group">
										<div class="input-group"><input class="form-control" id="email" name="email" placeholder="Your Email Id" required="" type="email" />
										</div>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="form-group">
										<div class="input-group"><input class="form-control" id="contact" name="contact" placeholder="Your Contact No" required="" type="text" />
										</div>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="form-group">
										<div class="input-group"><textarea class="form-control" id="message" name="message" placeholder="Your Message..." required="" rows="4"></textarea>
										</div>
									</div>
								</div>

								<div class="col-lg-12"><button class="site-button " name="button" type="submit" value="Submit"><span>Submit</span></button>
								</div>
							</div>
						</form>
					</div>
				</div>
<!-- Left part END -->

					<div class="col-lg-4 col-md-12 d-lg-flex m-b30" style="width: 100%; overflow: hidden; height: 450px;">
				    <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3771.2761116110014!2d73.07205631490083!3d19.05159408710182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c21b62c2f00d%3A0x8e105aa55b631e94!2sBhumiraj+woods+CHS!5e0!3m2!1sen!2sin!4v1544764108821" width="100%" height="600" frameborder="0" style="border:0; margin-top: -150px;" allowfullscreen></iframe>
				     </div>
				
				<!--<div class="col-lg-4 col-md-12 d-lg-flex m-b30" style="width: 100%; overflow: hidden; height: 450px;">-->
				<!--    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3771.2761116110014!2d73.065270!3d19.049390!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c21b62c2f00d%3A0x8e105aa55b631e94!2sBhumiraj+woods+CHS!5e0!3m2!1sen!2sin!4v1544764108821" width="100%" height="600" frameborder="0" style="border:0; margin-top: -150px;" allowfullscreen></iframe>-->
				<!--     </div>-->
			</div>
		</div>
	</div>
<!-- contact area  END -->
</div>