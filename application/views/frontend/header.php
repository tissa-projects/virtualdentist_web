<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3pro.css">
	

	<meta name="google-site-verification" content="l1eW724-OqZYWyVY-DUdGjSq377ucgGVitiOC2NuqK0" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-N9EBYSW8NR"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'G-N9EBYSW8NR');
</script>

	<link href="<?php echo base_url();?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" /><!-- PAGE TITLE HERE --><!-- MOBILE SPECIFIC --><meta name="viewport" content="width=device-width, initial-scale=1"><!-- STYLESHEETS -->
	<link href="<?php echo base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/templete.css" rel="stylesheet" type="text/css" />
	<link class="skin" href="<?php echo base_url();?>assets/css/skin/skin-1.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" /><!-- Revolution Slider Css -->
	<link href="<?php echo base_url();?>assets/css/layers.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/settings.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/navigation.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link href="<?php echo base_url();?>assets/css/lightbox.css" rel="stylesheet" type="text/css" />
       <link href="<?php echo base_url();?>assets/css/lightbox.min.css" rel="stylesheet" type="text/css" />
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Revolution Navigation Style --><!-- Start of  Zendesk Widget script --><script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=4ca2b977-8d21-4ef5-a994-5ac1756555fa"> </script><!-- End of  Zendesk Widget script -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
</head>
<style>ul#menu li {
  display:inline;
}

.checked {
  color: orange;
}
.fullwidth .logo-header {
    margin-right: 25px;
}

.logo-header{
    margin-left: 34px;
} 
		.forms {
    background: #fff;
    box-shadow: 0 0 10px #28531f;
    margin: 10px auto;
    max-width: 1000px;
    overflow: hidden;
    position: relative;
    padding: 0;
    border-radius: 15px;
}

.forms form { padding: 15px; }
/*#signup { display: none; }*/
.forms .tab-group {
	list-style: none;
	padding: 0;
	margin: 0;
}
.forms .tab-group:after {
	content: "";
	display: table;
	clear: both;
}
.forms .tab-group li a {
	display: block;
	text-decoration: none;
	padding: 4px;
	background: #e5e6e7;
	color: #888;
	font-size: 15px;
	float: left;
	width: 100%;
	text-align: center;
	border-top: 3px solid transparent;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms .tab-group li a:hover {
	background: #dedfdf;
	color: #666;
}
.forms .tab-group .active a {
	background: #fff;
	color: #444;
	border-top: 3px solid #73cf41;
}
.forms input {
	font-size: 12px;
	display: block;
	width: 100%;
	padding: 4px 10px;
	border: 1px solid #ddd;
	color: #666;
	border-radius: 2px;
	margin-bottom: 10px;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms select {
	font-size: 12px;
	display: block;
	height: 30px !important;
	width: 100%;
	padding: 4px 10px;
	border: 1px solid #ddd;
	color: #666;
	border-radius: 2px;
	margin-bottom: 10px;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms input:focus {
	outline: 0;
	border-color: #2e5ed7;
}
.forms label {
	font-size: 12px;
	font-weight: normal;
	color: #666;
	margin-bottom: 5px;
	display: block;
}

.navbar-fixed-top {
    top: 0;
    border-width: 0 0 1px;
}

.top-bar-transparent { background-color: rgba(0, 0, 0, 0.1); padding-top: 10px; padding-bottom: 10px; color: #fff; }

@media (max-width:767px) {
    .top-message{display: none;}
    .top-link {padding-left: 270px;}
    
}

@media only screen and (max-width: 600px) {
 .logo-header {
    margin-left: -4px;
}
}
@media (min-width: 1025px) and (max-width: 1280px) {
  
 .logo-header {
    margin-left: -123px;
}
  
}

input[type="file"] {
    display: none;
}

a{
    text-decoration:none !important;
}
footer {
  flex-shrink: 0;
  /*line-height: 50px;*/
  /*text-align: center;*/
  /*color: #fff;*/
}

.page-wraper {
	display: flex;
	flex-direction: column;
	height: 100vh;
	background-color:#FFFFFF;
}

.content1 {
	flex: 1 0 auto;
	background-color:#FFFFFF;
}

@media screen and (max-width: 991px)
.mo-left .header-nav.show {
   
    width: 274px;
    height: 777px !important;
   
}
	</style>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


<body id="bg"><!--<div id="loading-area"></div>-->
	<div class="page-wraper"><!-- header -->
	  
		<header class="site-header mo-left header fullwidth" style="0px;">
			<div class="sticky-header main-bar-wraper navbar-expand-lg">
				<div class="main-bar clearfix ">
					<div class="container clearfix" style="max-width:1480px"><!-- website logo -->
						<div class="logo-header mostion" style="max-width: 250px !important;">
							<a href="<?php echo base_url();?>home"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" /></a>
						</div>
						

						<button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler collapsed navicon justify-content-end" data-target="#navbarNavDropdown" data-toggle="collapse" data-dismiss="modal" type="button" >
							<!--<span class="icon-bar"></span>-->
		    				<span class="icon-bar"></span>
		    				<span class="icon-bar"></span>
		    				<span class="icon-bar"></span>
		    			</button><!-- extra nav --><!-- Quik search -->

						<div class="dez-quik-search bg-primary">
							<form action="#">
								<input class="form-control" name="search" placeholder="Type to search" type="text" value="" />
							</form>
						</div>
<!-- main nav -->

						<div class="header-nav navbar-collapse collapse justify-content-start" id="navbarNavDropdown" style="padding-left:0px;">
							<ul class="nav navbar-nav">
								<li class="<?php if(isset($page) && $page=='home'){ echo 'active';} ?>"><a href="<?php echo base_url();?>home">Home </a></li>
								<!--<li class="<?php if(isset($page) && $page=='about'){ echo 'active';} ?>"><a href="<?php echo base_url();?>home/about">About Us</a></li>-->
								<li class="<?php if(isset($page) && $page=='services'){ echo 'active';} ?>"><a href="<?php echo base_url();?>home/services">Services</a></li>
								<!-- <li><a href="<?php echo base_url();?>home/doctor_panel">Doctor Panel</a></li> -->
								<li class="<?php if(isset($page) && $page=='consultation'){ echo 'active';} ?>"><a href="<?php echo base_url();?>home/consultation">Online Consultation</a></li>
								<!-- <li><a href="https://onlinecourses.virtualdentist.in">Online Courses</a></li> -->
								<li><a href="https://virtualdentist.in/onlinecourses">Online Courses</a></li>
								
								<li><a href="https://virtualdentist.in/blog">Blog</a></li>
								<li class="<?php if(isset($page) && $page=='contact'){ echo 'active';} ?>"><a href="<?php echo base_url();?>home/contact">Contact Us</a></li>
								<!-- <li><a href="<?php echo base_url();?>home/subscription">Get subscription</a></li> -->
								<li id="dpanel" class="active"></li>
								<li id="dcpanel" class="active"></li>
								<li class="<?php if(isset($page) && $page=='doctorlogin'){ echo 'active';} ?>" id="dlogin"><a href="<?php echo base_url();?>home/login">Doctor Login</a></li>
								<!-- <li id="proimg">  </li> -->
                                <!--<li id="log"> </li>-->
							</ul>

							 
								<ul class="footer-social-bookmark">
                                                                
                                <li class=""><a target="_blank" href="https://www.facebook.com/virtualdentistindia"><img src="/assets/images/facebook.png"></a></li>
                                <li class=""><a target="_blank" href="https://www.linkedin.com/in/virtual-dentist-448b31203/"><img src="/assets/images/linkedin.png"></a></li>
                                <li class=""><a target="_blank" href="https://www.instagram.com/dentalservices84/"><img src="/assets/images/instagram.png"></a></li>
                                <li class=""><a target="_blank" href="https://twitter.com/VirtualDentist2"><img src="/assets/images/twitter.png"></a></li>
                            </ul>
                       

						</div>    
					</div>
			</div>
		</div>
		<!-- main header END -->
	</header>
		<!-- header END --><!-- Content -->

        <div class="content1">                

<script type="text/javascript">
	function isNum(evt){

  evt =(evt)? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode<48 || charCode >57)){

    return false;
  }
return true;
}
</script>

<script>
$( document ).ready(function() {
    $( "#name" ).keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
});
</script>

<script type="text/javascript">
          
    function logout(){
        
         var doctor_id = document.getElementById('doctortime').value;
         
         
         $.ajax({
        url: "<?php echo base_url();?>api/lastlogout",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id}),
            success: function(response){

              if(response.status == true){
                var msg = response.data;

            Swal.fire({
                  title: "<i style='color:#ADD8E6'>Congratulation...!!</i>", 
                  html: '<b>' + msg + '</b>',
                  showConfirmButton: false
                });

                sessionStorage.clear();
               window.location.href = "<?php echo base_url();?>home";
                
                      
                    }
                  
          }
    });
    }
    
</script>

       

  <script type="text/javascript">
    $(document).ready(function(){

    	  $("#dpanel").hide();
    	  $("#dcpanel").hide();

        if(sessionStorage.getItem('myprofile')){
                    

                var data = sessionStorage.getItem('myprofile');

                 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                         $('#doctortime').val(json[0].doctor_id);
                       
                         $('#log').html('<i class="fa fa-sign-out" aria-hidden="true" style="font-size:30px;color:#ccc;margin-top: 6px; cursor:pointer;" id="logout" onclick=logout(this);></i>');

                            $("#dlogin").hide();
                }  


                if(json[0].doctor_type == 'doctor'){

                 $("#dpanel").show();
                 $("#dpanel").html('<a href="<?php echo base_url();?>doctor/index"> Doctor Panel</a>');
             }else{

             	 $("#dcpanel").show();
                 $("#dcpanel").html('<a href="<?php echo base_url();?>doctor/counsultant"> Doctor Panel</a>');

             }

    }               

               
});

</script> 