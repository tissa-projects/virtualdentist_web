    <div class="left-side-menu">
        <!-- LOGO -->
        <div class="logo-box">
            <a href="<?= base_url(); ?>onlinecourses" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/favicon.png"/>
                </span>
                <span class="logo-lg">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" width="90%"/>
                </span>
            </a>
        </div>

        <div class="h-100" data-simplebar>
            <?php $uri2 = $this->uri->segment(2);
            $uri5 = $this->uri->segment(5); ?>
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul id="side-menu">
                    <a href="<?= base_url(); ?>onlinecourses"><li class="menu-title"><h4 style="color: #3bafda!important;"><i class="mdi mdi-arrow-collapse-left"></i>  Back to store</h4></li></a>
                    <?php if(isset($_SESSION['VIR_SESSION']['user_role']) && $_SESSION['VIR_SESSION']['user_role'] == 'Admin') { ?>
                    <li class="<?php if($uri2== 'dashboard'){ echo 'menuitem-active'; } ?>">
                        <a href="<?php echo base_url();?>onlinecourses/dashboard">
                            <i class="ri-dashboard-line"></i>
                            <span> Dashboards </span>
                        </a>
                    </li>
                    <li class="<?php if($uri2== 'sales' || ($uri2== 'purchasecourselist' && $uri5 == '')){ echo 'menuitem-active'; } ?>">
                        <a href="#sidebarLayouts5" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-line"></i>
                            <span> Reports </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse <?php if($uri2== 'sales' || ($uri2== 'purchasecourselist' && $uri5 == '')){ echo 'show'; } ?>" id="sidebarLayouts5">
                            <ul class="nav-second-level">
                                <li class="<?php if($uri2== 'sales' || ($uri2== 'purchasecourselist' && $uri5 == '')){ echo 'menuitem-active'; } ?>">
                                    <a href="<?php echo base_url();?>onlinecourses/sales">Sales</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="<?php if($uri2== 'courses' || $uri2== 'addcourses' || $uri2== 'editcourse' || $uri2== 'assignments' || $uri2== 'assignment'){ echo 'menuitem-active'; } ?>">
                        <a href="#sidebarLayouts" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-line"></i>
                            <span> Content </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse <?php if($uri2== 'courses' || $uri2== 'addcourses' || $uri2== 'editcourse' || $uri2== 'assignments' || $uri2== 'assignment'){ echo 'show'; } ?>" id="sidebarLayouts">
                            <ul class="nav-second-level">
                                <li class="<?php if($uri2== 'courses' || $uri2== 'addcourses' || $uri2== 'editcourse'){ echo 'menuitem-active'; } ?>">
                                    <a href="<?php echo base_url();?>onlinecourses/courses">Courses</a>
                                </li>
                                <li class="<?php if($uri2== 'assignments' || $uri2== 'assignment'){ echo 'menuitem-active'; } ?>">
                                    <a href="<?php echo base_url();?>onlinecourses/assignments">Assignments</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="<?php if($uri2== 'learners' || $uri2== 'addlearner' || $uri2== 'editlearner' || $uri2== 'instructors' || $uri2== 'addinstructor' || $uri2== 'editinstructor' || ($uri2== 'purchasecourselist' && $uri5 == 'learners')){ echo 'menuitem-active'; } ?>">
                        <a href="#sidebarLayouts1" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts1">
                            <i class="ri-layout-line"></i>
                            <span> Users </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse <?php if($uri2== 'learners' || $uri2== 'addlearner' || $uri2== 'editlearner' || $uri2== 'instructors' || $uri2== 'addinstructor' || $uri2== 'editinstructor' || ($uri2== 'purchasecourselist' && $uri5 == 'learners')){ echo 'show'; } ?>" id="sidebarLayouts1">
                            <ul class="nav-second-level">
                                <li class="<?php if($uri2== 'learners' || $uri2== 'addlearner' || $uri2== 'editlearner' || ($uri2== 'purchasecourselist' && $uri5 == 'learners')){ echo 'menuitem-active'; } ?>">
                                    <a href="<?php echo base_url();?>onlinecourses/learners">Learners</a>
                                </li>
                                <!-- <li class="<?php if($uri2== 'instructors' || $uri2== 'addinstructor' || $uri2== 'editinstructor'){ echo 'menuitem-active'; } ?>">
                                    <a href="<?php echo base_url();?>onlinecourses/instructors">Instructor</a>
                                </li> -->
                            </ul>
                        </div>
                    </li>
                    <?php } else if(isset($_SESSION['VIR_SESSION']['user_role']) && ($_SESSION['VIR_SESSION']['user_role'] == 'Learner' || $_SESSION['VIR_SESSION']['user_role'] == 'Instructor')) {  ?>
                    <li class="<?php if($uri2== 'mycourses'){ echo 'menuitem-active'; } ?>">
                        <a href="<?php echo base_url();?>onlinecourses/mycourses">
                            <i class="ri-dashboard-line"></i>
                            <span> My Courses </span>
                        </a>
                    </li>
                    <li class="<?php if($uri2== 'purchasehistory' || ($uri2== 'purchasecourselist' && $uri5 == '')){ echo 'menuitem-active'; } ?>">
                        <a href="<?php echo base_url();?>onlinecourses/purchasehistory">
                            <i class="ri-dashboard-line"></i>
                            <span> Purchase History </span>
                        </a>
                    </li>
                    <?php } ?>

                    <!-- <li>
                        <a href="<?php echo base_url();?>onlinecourses/courses">
                            <i class="ri-layout-line"></i>
                            <span> Courses </span>
                        </a>
                    </li> -->
                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End