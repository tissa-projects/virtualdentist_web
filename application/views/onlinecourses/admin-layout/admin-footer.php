                <button type="button" class="btn btn-warning waves-effect waves-light btn-sm" id="toastr-success" style="display: none;">Click me</button>
                <button type="button" class="btn btn-warning waves-effect waves-light btn-sm" id="toastr-error" style="display: none;">Click me</button>
                <input type="hidden" name="site_url" id="site_url" value="<?= base_url(); ?>">
                <!-- Modal -->
                <div class="modal fade" id="deletemsgmodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <form method="post" id="deleteform">
                        <div class="modal-content">
                            <div class="modal-header" style="border-bottom: none!important;">
                                <h3 class="modal-title" id="deletehead"></h3>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="delete_id" id="delete_id">
                                <input type="hidden" name="status" id="status">
                                <p id="deletemsg" style="font-size: 18px;font-weight: bold;text-align: center;"></p>
                            </div>
                            <div class="modal-footer" style="border-top: none!important;text-align: center;flex: none;display: block;">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-primary">Yes</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <script>document.write(new Date().getFullYear())</script> &copy; Virtual Dentist
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        <!-- Vendor js -->
        <script src="<?php echo base_url();?>assets/js/vendor.min.js"></script>

        <!-- KNOB JS -->
        <script src="<?php echo base_url();?>assets/libs/jquery-knob/jquery.knob.min.js"></script>
        <!-- Apex js-->
        <!-- <script src="<?php echo base_url();?>assets/libs/apexcharts/apexcharts.min.js"></script> -->

        <!-- Plugins js-->
        <script src="<?php echo base_url();?>assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo base_url();?>assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js"></script>

        <!-- Dashboard init-->
        <!-- <script src="<?php echo base_url();?>assets/js/pages/dashboard-sales.init.js"></script> -->

        <!-- App js -->
        <script src="<?php echo base_url();?>assets/js/app.min.js"></script>

        <!-- third party js -->
        <script src="<?php echo base_url();?>assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url();?>assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url();?>assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
        <script src="<?php echo base_url();?>assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <!-- <script src="<?php echo base_url();?>assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/datatables.net-select/js/dataTables.select.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/pdfmake/build/pdfmake.min.js"></script> -->
        <!-- <script src="<?php echo base_url();?>assets/libs/pdfmake/build/vfs_fonts.js"></script> -->
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="<?php echo base_url();?>assets/js/pages/datatables.init.js"></script>
        <!-- Plugins js-->
        <script src="<?php echo base_url();?>assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

        <!-- Plugins js -->
        <!-- <script src="<?php echo base_url();?>assets/libs/quill/quill.min.js"></script> -->

        <!-- Select2 js-->
        <!-- <script src="<?php echo base_url();?>assets/libs/select2/js/select2.min.js"></script> -->
        <!-- Dropzone file uploads-->
        <script src="<?php echo base_url();?>assets/libs/dropzone/min/dropzone.min.js"></script>

        <!-- Init js-->
        <script src="<?php echo base_url();?>assets/js/pages/form-fileuploads.init.js"></script>

        <!-- Init js -->
        <!-- <script src="<?php echo base_url();?>assets/js/pages/add-product.init.js"></script> -->
        <script src="<?php echo base_url();?>assets/js/ckeditor/ckeditor.js"></script>
        <!-- Tost-->
        <script src="<?php echo base_url();?>assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url();?>assets/js/select2.full.js"></script>

        <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>

        <!-- Tippy js-->
        <script src="<?= base_url(); ?>assets/libs/tippy.js/tippy.all.min.js"></script>
        
        <!-- nestable2 js-->
        <script src="<?= base_url(); ?>assets/libs/nestable2/jquery.nestable.min.js"></script>

        <!-- Nestable init-->
        <script src="<?= base_url(); ?>assets/js/pages/nestable.init.js"></script>
        
        <!-- toastr init js-->
        <!-- <script src="<?php echo base_url();?>assets/js/pages/toastr.init.js"></script> -->
        <script type="text/javascript">
            function isNum(evt){
                evt =(evt)? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode<48 || charCode >57)){
                  return false;
                }
                return true;
            }
            
            !function($) {
                'use strict';
                var NotificationApp = function() {};
                NotificationApp.prototype.send = function(heading, body, position, loaderBgColor, icon, hideAfter, stack, showHideTransition) {
                    // default      
                    if (!hideAfter)
                        hideAfter = 3000;
                    if (!stack)
                        stack = 1;
                    var options = {
                        heading: heading,
                        text: body,
                        position: position,
                        loaderBg: loaderBgColor,
                        icon: icon,
                        hideAfter: hideAfter,
                        stack: stack
                    };
                    if(showHideTransition)
                        options.showHideTransition = showHideTransition;

                    console.log(options);
                    $.toast().reset('all');
                    $.toast(options);
                },
                $.NotificationApp = new NotificationApp, $.NotificationApp.Constructor = NotificationApp
            }(window.jQuery),
            function($) {
                "use strict";
                $("#toastr-success").on('click', function (e) {
                    $.NotificationApp.send("Well Done!", "<?php echo $this->session->flashdata('success'); ?>", 'top-center', '#5ba035', 'success');
                });
                $("#toastr-error").on('click', function (e) {
                    $.NotificationApp.send("Oh snap!", "<?php echo $this->session->flashdata('error'); ?>", 'top-center', '#bf441d', 'error');
                });
            }(window.jQuery);
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                var firstName = $('#firstName').text();
                // var lastName = $('#lastName').text();  + $('#lastName').text().charAt(0)
                var intials = $('#firstName').text().charAt(0);
                var profileImage = $('#profileImage').text(intials);
            });
            if('<?php echo $this->session->flashdata('success'); ?>' != "") {
                $("#toastr-success").click();
            } else if('<?php echo $this->session->flashdata('error'); ?>' != ""){
                $("#toastr-error").click();
            }
        </script>