<?php 
// if(!isset($_SESSION['VER_SESSION'])) {
//     redirect('Onlinecourses/logout');
// }
?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Dental Problems and Treatment | Best Dental Care | Virtual Dentist</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="Description" content="Virtual Dentist provides the best dental care for any dental problems and provide the best treatment for any teeth issues and problems like teeth filling, tooth denture etc.">
        <meta name="Keywords" content="best dental care, best dental clinic, common dental problems, dental issues, dental problems and treatment, dental services, dental treatment, dentist near me, dentist specialist, denture dentist, kids teeth falling out, teeth issues, teeth problems, teeth treatment near me, tooth denture, tooth filling near me, treatment of teeth">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3pro.css">
        <meta name="google-site-verification" content="l1eW724-OqZYWyVY-DUdGjSq377ucgGVitiOC2NuqK0" />
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-N9EBYSW8NR"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'G-N9EBYSW8NR');
        </script>

        <link href="<?php echo base_url();?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />

        <!-- plugin css -->
        <link href="<?php echo base_url();?>assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        
        <!-- Plugins css-->
        <!-- <link href="<?php echo base_url();?>assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="<?php echo base_url();?>assets/libs/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/libs/quill/quill.core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/libs/quill/quill.snow.css" rel="stylesheet" type="text/css" /> -->

        <!-- App css -->
        <link href="<?php echo base_url();?>assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="<?php echo base_url();?>assets/css/default/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <link href="<?php echo base_url();?>assets/css/default/bootstrap-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
        <link href="<?php echo base_url();?>assets/css/default/app-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

        <!-- icons -->
        <link href="<?php echo base_url();?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- third party css -->
        <link href="<?php echo base_url();?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/libs/datatables.net-select-bs4/css/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/onlinecourses-admin.css">
        <!-- third party css end -->
         <!-- Jquery Toast css -->
        <link href="<?php echo base_url();?>assets/libs/jquery-toast-plugin/jquery.toast.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/select2.min.css">
        <!-- nestable css -->
        <link href="<?php echo base_url();?>assets/libs/nestable2/jquery.nestable.min.css" rel="stylesheet" />
        <style type="text/css">
          .select2 {
            width: 100%!important;
          }
          .select2-search__field {
            width: 100%!important;
          }
          .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff;
          }
          .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #5897fb;
            border: 1px solid #5897fb;
            color: white;
          }
        </style>
    </head>
    <?php $page = $this->uri->segment(2); ?>
    <body class="loading">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-end mb-0">
                        <li class="dropdown d-none d-lg-inline-block">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                                <i class="fe-maximize noti-icon"></i>
                            </a>
                        </li>
                        <li class="dropdown notification-list topbar-dropdown">
                            <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <!-- <img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg" alt="user-image" class="rounded-circle"> -->
                                <?php if(isset($_SESSION['VIR_SESSION']['profile_picture']) && $_SESSION['VIR_SESSION']['profile_picture'] != '') { ?>
                                    <img src="<?= base_url('uploads/user_profile/'.$_SESSION['VIR_SESSION']['profile_picture']); ?>" class="rounded-circle avatar-xl img-thumbnail"
                                alt="profile-image">
                                <?php } else { ?>
                                    <span id="firstName" style="display: none;"><?php if(isset($_SESSION['VIR_SESSION']['name'])) { echo ucfirst($_SESSION['VIR_SESSION']['name']); } ?></span>
                                    <div id="profileImage"></div>
                                <?php } ?>
                                
                                <span class="pro-user-name ms-1">
                                    <?php if(isset($_SESSION['VIR_SESSION']['name'])) { echo ucfirst($_SESSION['VIR_SESSION']['name']); } ?> <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end profile-dropdown ">
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Welcome !</h6>
                                </div>
                                <a href="<?php echo base_url();?>onlinecourses/profile" class="dropdown-item notify-item <?php if($page=='profile') { echo 'active'; } ?>">
                                    <i class="ri-account-circle-line"></i>
                                    <span>My Profile</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?php echo base_url();?>onlinecourses/logout" class="dropdown-item notify-item">
                                    <i class="ri-logout-box-line"></i>
                                    <span>Logout</span>
                                </a>
                            </div>
                        </li>
                    </ul>
    
                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                        <li>
                            <button class="button-menu-mobile waves-effect waves-light">
                                <i class="fe-menu"></i>
                            </button>
                        </li>

                        <li>
                            <!-- Mobile menu toggle (Horizontal Layout)-->
                            <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>   
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->
