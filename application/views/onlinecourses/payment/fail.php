<!DOCTYPE html>
<html lang="en">
<head>
	<title>Dental Problems and Treatment | Best Dental Care | Virtual Dentist</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="Description" content="Virtual Dentist provides the best dental care for any dental problems and provide the best treatment for any teeth issues and problems like teeth filling, tooth denture etc.">
	<meta name="Keywords" content="best dental care, best dental clinic, common dental problems, dental issues, dental problems and treatment, dental services, dental treatment, dentist near me, dentist specialist, denture dentist, kids teeth falling out, teeth issues, teeth problems, teeth treatment near me, tooth denture, tooth filling near me, treatment of teeth">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3pro.css">
	<meta name="google-site-verification" content="l1eW724-OqZYWyVY-DUdGjSq377ucgGVitiOC2NuqK0" />
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-N9EBYSW8NR"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());

	 gtag('config', 'G-N9EBYSW8NR');
	</script>

	<link href="<?php echo base_url();?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
	<link href="<?php echo base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/templete.css" rel="stylesheet" type="text/css" />
	<link class="skin" href="<?php echo base_url();?>assets/css/skin/skin-1.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="<?php echo base_url();?>assets/css/lightbox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/lightbox.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/onlinecourse.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
	<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=4ca2b977-8d21-4ef5-a994-5ac1756555fa"> </script>
</head>
<body id="bg" style="overflow-x: none!important;padding-right: 0px!important;"><!--<div id="loading-area"></div>-->
	<div class="page-wraper"><!-- header -->
		<header class="site-header mo-left header fullwidth">
			<div class="sticky-header main-bar-wraper navbar-expand-lg" id="sticky-header-id" style="padding-right: 0px!important;">
				<div class="main-bar clearfix ">
					<div class="container clearfix" style="max-width:1480px"><!-- website logo -->
						<div class="logo-header mostion" style="max-width: 250px !important;">
							<a href="<?php echo base_url();?>home"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" /></a>
						</div>
						<button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler collapsed navicon justify-content-end" data-target="#navbarNavDropdown" data-toggle="collapse" data-dismiss="modal" type="button" >
							<!--<span class="icon-bar"></span>-->
		    				<span class="icon-bar"></span>
		    				<span class="icon-bar"></span>
		    				<span class="icon-bar"></span>
		    			</button><!-- extra nav --><!-- Quik search -->

					</div>
			</div>
			</div>
			<!-- main header END -->
		</header>
		<!-- header END --><!-- Content -->

	<div class="content1">  
		<div class="page-content" style="margin-top: 20px;">
		<div class="row">
		    <div class="col-md-2"></div>  
		     <div class="col-md-8 text-center">
		     	<div class="card">
		    		<h4 class="card-header">Transaction  <?php if($status == 'success') { ?><label for="success" class="btn btn-success"><?= ucfirst($status); ?></label><?php } else { ?><label for="failure" class="btn btn-danger"><?= ucfirst($status); ?></label><?php } ?></h4>
		    		<div class="card-body">
		    			<?php 
			                echo "<p>Your order status is ". ucfirst($status) ."</br>";
			                echo "Your transaction id for this transaction is ".$txnid.'<br>';
			                if($status != 'success') {
			                	echo "<br>Contact our customer support.</p><br>";
			                }
			                echo "<a href='".base_url('onlinecourses')."' class='btn btn-sm btn-info'> < - Go Back</a>";
			            ?>
		    		</div>
		    	</div>
		     </div> 
		    <div class="col-md-2"></div>
		</div>
	</div>
	</div>

	<footer class="footer footer-bottom">
			<div class="row">
				<div class="col-lg-12 text-center">
					<span>Copyright &copy; <script>document.write(new Date().getFullYear())</script> Virtual Dentist - All Rights Reserved.</span>
				</div>
			</div>
		
	</footer>

	<!-- Footer END --><!-- scroll top button -->
	<button class="scroltop fa fa-arrow-up"></button>
	
	</div>
	<!-- JAVASCRIPT FILES ========================================= -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
	<!-- JQUERY.MIN JS -->
	<script src="<?php echo base_url();?>assets/plugins/wow/wow.js"></script><!-- WOW JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/popper.min.js"></script><!-- BOOTSTRAP.MIN JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script><!-- FORM JS -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
	<script src="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC POPUP JS -->
	<!-- <script src="<?php echo base_url();?>assets/plugins/counter/waypoints-min.js"></script> -->
	<script src="<?php echo base_url();?>assets/plugins/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
	<script src="<?php echo base_url();?>assets/plugins/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
	<script src="<?php echo base_url();?>assets/plugins/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
	<script src="<?php echo base_url();?>assets/plugins/masonry/masonry.filter.js"></script><!-- MASONRY -->
	<script src="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.js"></script><!-- OWL SLIDER -->
	<script src="<?php echo base_url();?>assets/plugins/rangeslider/rangeslider.js" ></script><!-- Rangeslider -->
	<script src="<?php echo base_url();?>assets/js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
	<script src="<?php echo base_url();?>assets/js/dz.carousel.js"></script><!-- SORTCODE FUCTIONS  -->
	<script src="<?php echo base_url();?>assets/js/dz.ajax.js"></script><!-- CONTACT JS  -->
	<!-- App js -->
  <script src="<?php echo base_url();?>assets/js/app.min.js"></script>
</body>
</html>