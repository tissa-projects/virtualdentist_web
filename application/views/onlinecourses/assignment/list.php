<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Submitted Assignments</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">Submitted Assignments</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-10">
                                    <h4 class="header-title">Submitted Assignments</h4>
                                </div>
                            </div>

                            <table id="basic-datatable" class="table dt-responsive w-100">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Created On</th>
                                        <th>Modified On</th>
                                        <th>Title</th>
                                        <th>Course</th>
                                        <th>Available for Review</th>
                                        <th>Reviewed</th>
                                        <th>Rejected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($assignlist->success == 1) { 
                                        $i = 1;
                                        foreach($assignlist->data as $row) { ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= date("d/m/Y", strtotime($row->created_at)); ?><br/><?= date("h:i A", strtotime($row->created_at)); ?></td>
                                        <td><?= date("d/m/Y", strtotime($row->updated_at)); ?><br/><?= date("h:i A", strtotime($row->updated_at)); ?></td>
                                        <td><?= $row->title; ?></td>
                                        <td><?= $row->course; ?></td>
                                        <td style="font-size: 24px; cursor: pointer;font-weight: bolder;">
                                            <a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($row->course_id)?>/2" class='text text-warning'><?= $row->underreviewcount; ?></a></td>
                                        <td style="font-size: 24px; cursor: pointer;font-weight: bolder;"><a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($row->course_id)?>/1" class='text text-success'><?= $row->reviewedcount; ?></a></td>
                                        <td style="font-size: 24px; cursor: pointer;font-weight: bolder;"><a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($row->course_id)?>/3" class='text text-danger'><?= $row->rejectedcount; ?></a></td>
                                    </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            
        </div> <!-- container -->

    </div>
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
</body>
</html>