<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Assignment Submissions Manager</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/assignments">Assignments</a></li>
                                <li class="breadcrumb-item active">Assignment Submissions Manager</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-10">
                                    <h4 class="header-title">Assignment Submissions Manager ( <?= $course['data']->title; ?> )</h4>
                                </div>
                            </div>
                            <div class="row" style="border-bottom: 1px solid grey; margin: 0% 0% 3% 0%;">
                                <div class="col-md-6">
                                    <a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($course_id)?>/0"><label for="field-1" class="form-label <?php if($status == 0) { echo 'btn btn-info'; } else { echo 'btn btn-light'; } ?> btn-sm mystatus" id="all">All</label></a>
                                    <a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($course_id)?>/2"><label for="field-1" class="form-label <?php if($status == 2) { echo 'btn btn-warning'; } else { echo 'btn btn-light'; } ?> btn-sm mystatus" id="review">Available for Review</label></a>
                                    <a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($course_id)?>/1"><label for="field-1" class="form-label <?php if($status == 1) { echo 'btn btn-success'; } else { echo 'btn btn-light'; } ?> btn-sm mystatus" id="reviewed">Reviewed</label></a>
                                    <a href="<?= base_url(); ?>onlinecourses/assignment/<?= base64_encode($course_id)?>/3"><label for="field-1" class="form-label <?php if($status == 3) { echo 'btn btn-danger'; } else { echo 'btn btn-light'; } ?> btn-sm mystatus" id="rejected">Rejected</label></a>
                                </div>
                            </div>
                            <table id="basic-datatable" class="table dt-responsive w-100 table-assignment">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>File/Notes</th>
                                        <th>Submit Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($getdata['success'] == 1) { 
                                        $i = 1;
                                        foreach($getdata['data'] as $row) { 
                                            $status = 'NA';
                                            if($row->status == 1) {
                                                $status = '<span class="text text-success"><b>REVIEWED</b></span>';
                                            } else if($row->status == 2) {
                                                $status = '<span class="text text-warning"><b>AVAILABLE FOR REVIEW</b></span>';
                                            } else if($row->status == 3) {
                                                $status = '<span class="text text-danger"><b>REJECTED</b></span>';
                                            }
                                            $filenote = "";
                                            if($row->file) {
                                                $filenote .= '<a href="'.base_url().'uploads/assignment_upload/'.$row->file.'" target="_blank">'.$row->file.'</a>'; 
                                            } 
                                            if($row->notes) { 
                                                $filenote .= '<br/><button type="button" class="btn btn-info" id="viewbutton" data-notes="'.$row->notes.'">View Note</button>'; 
                                            }
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= ucfirst($row->users->name); ?></td>
                                        <td><?= $row->users->email; ?></td>
                                        <td><?= $status; ?></td>
                                        <td><?= $filenote; ?></td>
                                        <td><?= date("d/m/Y", strtotime($row->created_at))."<br/>".date("h:i A", strtotime($row->created_at)); ?></td>
                                        <td>
                                            <div class='button-list' id='tooltip-container'>
                                                <?php if($row->status != 3) { ?>
                                                <button type='button' class='btn btn-danger btn-xs waves-effect waves-light' data-bs-container='#tooltip-container' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Reject Assignment' data-user_id="<?= $row->user_id; ?>" data-course_id="<?= $row->course_id; ?>" data-enroll_id="<?= $row->enroll_id; ?>" data-status="3" data-name="<?= ucfirst($row->users->name); ?>" data-email="<?= $row->users->email; ?>" id="rejectassignment"><i class='mdi mdi-close'></i></button>
                                                <?php } ?>
                                                <?php if($row->status != 1) { ?>
                                                <button type='button' class='btn btn-success btn-xs waves-effect waves-light' data-bs-container='#tooltip-container' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Reviewed Assignment' data-user_id="<?= $row->user_id; ?>" data-course_id="<?= $row->course_id; ?>" data-enroll_id="<?= $row->enroll_id; ?>" data-status="1" data-name="<?= ucfirst($row->users->name); ?>" data-email="<?= $row->users->email; ?>" id="approvedassignment"><i class='mdi mdi-check'></i></button>
                                                <?php } ?>
                                                <button type='button' class='btn btn-info btn-xs waves-effect waves-light' data-bs-container='#tooltip-container' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Message' data-user_id="<?= $row->user_id; ?>" data-course_id="<?= $row->course_id; ?>" data-enroll_id="<?= $row->enroll_id; ?>" data-status="<?= $row->status; ?>" data-name="<?= ucfirst($row->users->name); ?>" data-email="<?= $row->users->email; ?>" id="messageassignment"><i class='mdi mdi-chat-plus'></i></button>
                                                <button type='button' class='btn btn-warning btn-xs waves-effect waves-light' data-bs-container='#tooltip-container' data-bs-toggle='tooltip' data-bs-placement='bottom' title='All History' onclick="return getassignmentlist(<?= $row->course_id;?>,<?= $row->user_id;?>,<?= $row->enroll_id;?>);"><i class='mdi mdi-history'></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php  $i++; } } ?>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            
        </div> <!-- container -->

    </div>
    <div id="viewassignmodal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Assignment Submissions Manager (<span id="head-title"></span>)</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row" id="table-assignment">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="viewnotemodal" tabindex="-1" role="dialog" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.07);">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">View Note</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                     <p id="viewmsg"></p>
                </div>
            </div>
        </div>
    </div>

    <div id="assignreplymodal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <form method="post" action="<?= base_url(); ?>onlinecourses/uploadassignmentreply/1" enctype="multipart/form-data" name="submit-assign" autocomplete="off">
                    <div class="modal-header">
                        <h4 class="modal-title" id="assign-head"></h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="border-bottom: 1px solid grey;margin-bottom: 2%;">
                            <input type="hidden" name="user_id" id="user_id">
                            <input type="hidden" name="course_id" id="course_id">
                            <input type="hidden" name="enroll_id" id="enroll_id">
                            <input type="hidden" name="status" id="status">
                            <div class="col-md-12">
                                <label for="field-1" class="form-label" id="user-data"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label for="field-3" class="form-label" id="first-label"></label>
                                    <textarea class="form-control" id="message" name="message" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="upload-file">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label for="field-3" class="form-label">Upload <span class="text text-muted">Max File Size is 15 MB</span></label>
                                    <input type="file" name="assignment_upload" id="assignment_upload" class="form-control" accept="application/pdf" onchange="FilevalidationUpload()">
                                    <label for="assignment_upload" id="assignment_upload_error" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="submit-assign" class="btn btn-info waves-effect waves-light">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
    $('.table-assignment').on('click', '#viewbutton', function() {
        var notes = $(this).data('notes');
        $("#viewmsg").html(notes);
        $("#viewnotemodal").modal('show');
    });

    function showNote(notes) {
        $("#viewmsg").html(notes);
        $("#viewnotemodal").modal('show');
    }

    $('.table-assignment').on('click', '#rejectassignment', function() {
        var user_id = $(this).data('user_id');
        var course_id = $(this).data('course_id');
        var enroll_id = $(this).data('enroll_id');
        var status = $(this).data('status');
        var name = $(this).data('name');
        var email = $(this).data('email');
        if(status == 3) {
            $("#assign-head").html('Rejected Assignment');
            $("#first-label").html('Feedback');
            $("#message").attr('placeholder','Enter Feedback');
        }
        $("#user_id").val(user_id);
        $("#course_id").val(course_id);
        $("#enroll_id").val(enroll_id);
        $("#status").val(status);
        $("#user-data").html(name+' ( '+email+' )');
        $("#assignreplymodal").modal('show');
    });

    $('.table-assignment').on('click', '#approvedassignment', function() {
        var user_id = $(this).data('user_id');
        var course_id = $(this).data('course_id');
        var enroll_id = $(this).data('enroll_id');
        var status = $(this).data('status');
        var name = $(this).data('name');
        var email = $(this).data('email');
        if(status == 1) {
            $("#assign-head").html('Approved Assignment');
            $("#first-label").html('Feedback');
            $("#message").attr('placeholder','Enter Feedback');
        }
        $("#user_id").val(user_id);
        $("#course_id").val(course_id);
        $("#enroll_id").val(enroll_id);
        $("#status").val(status);
        $("#user-data").html(name+' ( '+email+' )');
        $("#assignreplymodal").modal('show');
    });

    $('.table-assignment').on('click', '#messageassignment', function() {
        var user_id = $(this).data('user_id');
        var course_id = $(this).data('course_id');
        var enroll_id = $(this).data('enroll_id');
        var status = $(this).data('status');
        var name = $(this).data('name');
        var email = $(this).data('email');

        $("#assign-head").html('Send Message');
        $("#first-label").html('Message <span class="text-danger">*</span>');
            $("#message").attr('placeholder','Enter Message');
        $("#message").prop('required',true);
        $("#upload-file").css('display','none');

        $("#user_id").val(user_id);
        $("#course_id").val(course_id);
        $("#enroll_id").val(enroll_id);
        $("#status").val(status);
        $("#user-data").html(name+' ( '+email+' )');
        $("#assignreplymodal").modal('show');
    });

    function getassignmentlist(course_id, user_id, enroll_id) {
        var site_url = $('#site_url').val();
        $.ajax({
            url:site_url+"onlinecourses/getassignmenthistory",
            type:"POST",
            data: {course_id:course_id,user_id:user_id,enroll_id:enroll_id},
            success:function(data,status,xhr) {
                var obj = JSON.parse(data);
                if(obj.success == 1) {
                    $("#table-assignment").html(obj.message);
                    $("#head-title").html(obj.title);

                    $("#viewassignmodal").modal('show');
                } 
            },
            error: function (jqXhr, textStatus, errorMessage) {
                console.log(errorMessage);
            },
        });
    }

    FilevalidationUpload = () => {
        var fi = document.getElementById('assignment_upload');
        // Check if any file is selected.
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
  
                var fsize = fi.files.item(i).size;
                var file = Math.round((fsize / 1024));
                // The size of the file.
                if (file > 15360) {
                    $("#assignment_upload_error").html("File too Big, please select a file less than 15mb");
                    setTimeout(function() {
                        $("#assignment_upload_error").html("");
                    }, 3000);
                    return false;
                } 
            }
        }
    }
    $("form[name='submit-assign']").submit(function() {
        var fi = document.getElementById('assignment_upload');
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;
                var file = Math.round((fsize / 1024));
                if (file > 15360) {
                    $("#assignment_upload_error").html("File too Big, please select a file less than 15mb");
                    setTimeout(function() {
                        $("#assignment_upload_error").html("");
                    }, 3000);
                    return false;
                } 
            }
        } 
        // else {
        //     $("#submit-assign").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
        //     $("#submit-assign").attr('disabled',true);
        // }
        $("#submit-assign").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
        $("#submit-assign").attr('disabled',true);
    });
</script>
</body>
</html>