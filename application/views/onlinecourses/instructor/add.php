<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
            
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Add Instructor</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/instructors">Instructors</a></li>
                                <li class="breadcrumb-item active">Add Instructor</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->             
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" name="addinstructor" action="<?= base_url(); ?>onlinecourses/addinstructoraction" autocomplete="off">
                                <div>

                                    <div class="mb-2">
                                        <label class="form-label">Name <span class="text-danger">*</span></label>
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter Name">
                                    </div>

                                    <div class="mb-2">
                                        <label class="form-label">Email <span class="text-danger">*</span></label>
                                        <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email">
                                    </div>

                                    <div class="mb-2">
                                        <label class="form-label">Mobile <span class="text-danger">*</span></label>
                                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter Mobile">
                                    </div>

                                    <div class="mb-2">
                                        <label class="form-label">Password <span class="text-danger">*</span></label>
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password">
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-2">
                                                <label class="form-label">Role <span class="text-danger">*</span></label>
                                                <input type="text" id="user_role" name="user_role" value="Instructor" class="form-control" placeholder="Enter Role" readonly="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-2 mt-3">
                                                <label class="form-label" style="float: left;">Send Email to User &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <div class="form-check form-switch" style="float: left;">
                                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="is_send_email">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                    <li class="next list-inline-item">
                                        <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                        <button type="submit" class="btn btn-success">Add Instructor <i class="mdi mdi-arrow-right ms-1"></i></button>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
$(function() {
    var site_url = $("#site_url").val();
  $("form[name='addinstructor']").validate({
    ignore: [],
    debug: false,
    rules: {
        name: "required",
        email: {
            required: true,
            email: true,
            remote: {
                url: "<?= base_url(); ?>onlinecourses/check_email",
                type: "post",
                data: {
                    email: function() {
                        return $("#email").val();
                    },
                    user_role: function(){
                        return $('#user_role').val();
                    }
                }
            }
        },
        mobile: {
            required: true,
            number: true,
            remote: {
                url: "<?= base_url(); ?>onlinecourses/check_mobile",
                type: "post",
                data: {
                    mobile: function() {
                        return $("#mobile").val();
                    },
                    user_role: function(){
                        return $('#user_role').val();
                    }
                }
            }
        },
        password: {
          required: true,
          minlength: 8
        }
    },
    messages: {
        name: "Please enter name",
        email: {
            required: "Please enter email",
            email: "Please enter valid email",
            remote: "This email already exist"
        },
        mobile: {
            required: "Please enter mobile",
            number: "Please enter a valid mobile",
            remote: "This mobile already exist"
        },
        password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long"
        }
    },  
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>
</body>
</html>