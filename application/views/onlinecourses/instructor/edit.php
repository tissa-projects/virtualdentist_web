<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
            
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Edit Instructor</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/instructor">Instructors</a></li>
                                <li class="breadcrumb-item active">Edit Instructor</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->             
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" name="editinstructor" action="<?= base_url(); ?>onlinecourses/editinstructoraction" autocomplete="off">
                                <div>
                                    <div class="mb-2">
                                        <label class="form-label">Name <span class="text-danger">*</span></label>
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter Name" value="<?= $getusers['data']->name; ?>">
                                        <input type="hidden" id="user_id" name="user_id" class="form-control" placeholder="Enter User Id" value="<?= $getusers['data']->user_id; ?>">
                                    </div>

                                    <div class="mb-2">
                                        <label class="form-label">Email <span class="text-danger">*</span></label>
                                        <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email" value="<?= $getusers['data']->email; ?>" readonly>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-2">
                                                <label class="form-label">Mobile <span class="text-danger">*</span></label>
                                                <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter Mobile" value="<?= $getusers['data']->mobile_number; ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-2">
                                                <label class="form-label">Role <span class="text-danger">*</span></label>
                                                <input type="text" id="user_role" name="user_role" value="Instructor" class="form-control" placeholder="Enter Role" readonly="true">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-2">
                                                <label class="form-label">States <span class="text-danger">*</span></label>
                                                <select class="form-select form-select-md" id="state_id" name="state_id">
                                                    <option value="">Select States</option>
                                                    <?php if($states['data']) { 
                                                        foreach($states['data'] as $state) { ?>
                                                    <option value="<?= $state->state_id; ?>" <?php if($getusers['data']->state_id == $state->state_id) { echo "selected"; } ?>><?= $state->name; ?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-2">
                                                <label class="form-label">Language <span class="text-danger">*</span></label>
                                                <select class="form-select form-select-md" id="language_id" name="language_id">
                                                    <option value="">Select Language</option>
                                                    <?php if($languages['data']) { 
                                                        foreach($languages['data'] as $lang) { ?>
                                                    <option value="<?= $lang->language_id; ?>" <?php if($getusers['data']->language_id == $lang->language_id) { echo "selected"; } ?>><?= $lang->name; ?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                    <li class="next list-inline-item">
                                        <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                        <button type="submit" class="btn btn-success">Edit Instructor <i class="mdi mdi-arrow-right ms-1"></i></button>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h3>Change Password</h3>
                            <form method="post" name="password_details" action="<?= base_url(); ?>onlinecourses/changepassword/instructors" autocomplete="off">
                                <input type="hidden" name="user_id" id="user_id_pass" value="<?= $getusers['data']->user_id; ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-2">
                                            <label class="form-label">Current Password <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="current_password" name="current_password" placeholder="Enter current password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-2">
                                            <label class="form-label">New Password <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="new_password" name="new_password" placeholder="Enter new password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-2">
                                            <label class="form-label">Retype new Password <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="conf_password" name="conf_password" placeholder="Retype new Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-end">
                                    <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
$(function() {
    var site_url = $("#site_url").val();
    $("form[name='editinstructor']").validate({
        ignore: [],
        debug: false,
        rules: {
            name: "required",
            email: {
                required: true,
                email: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/check_email",
                    type: "post",
                    data: {
                        email: function() {
                            return $("#email").val();
                        },
                        user_role: function(){
                            return $('#user_role').val();
                        },
                        user_id: function(){
                            return $('#user_id').val();
                        }
                    }
                }
            },
            mobile: {
                required: true,
                number: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/check_mobile",
                    type: "post",
                    data: {
                        mobile: function() {
                            return $("#mobile").val();
                        },
                        user_role: function(){
                            return $('#user_role').val();
                        },
                        user_id: function(){
                            return $('#user_id').val();
                        }
                    }
                }
            },
            state_id: "required",
            language_id: "required"
        },
        messages: {
            name: "Please enter name",
            email: {
                required: "Please enter email",
                email: "Please enter valid email",
                remote: "This email already exist"
            },
            mobile: {
                required: "Please enter mobile",
                number: "Please enter a valid mobile",
                remote: "This mobile already exist"
            },
            state_id: "Please select state",
            language_id: "Please select language"
        },  
        submitHandler: function(form) {
          form.submit();
        }
    });
    $("form[name='password_details']").validate({
        ignore: [],
        debug: false,
        rules: {
            current_password: {
                required: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/checkcurrentpassword",
                    type: "post",
                    data: {
                        current_password: function() {
                            return $("#current_password").val();
                        },
                        user_id: function(){
                            return $('#user_id_pass').val();
                        }
                    }
                }
            },
            new_password: {
                required: true,
                minlength: 8
            },
            conf_password: {
                required: true,
                minlength: 8,
                equalTo : "#new_password"
            }
        },
        messages: {
            current_password: {
                required: "Please enter current password",
                remote: "This password does not match"
            },
            new_password: {
                required: "Please enter new password",
                minlength: "Please enter at least 8 characters"
            },
            conf_password: {
                required: "Please retype new password",
                minlength: "Please enter at least 8 characters",
                equalTo: "New password and retype new password should be same"
            }
        },  
        submitHandler: function(form) {
          form.submit();
        }
    });
});
</script>
</body>
</html>