<link href="<?php echo base_url();?>assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
<div class="page-content pg-cont">
    <div class="account-pages">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-6">
                    <div class="card">

                        <div class="card-body p-4">
                            
                            <div class="text-center w-75 m-auto">
                                <h1>Sign Up</h1>
                            </div>

                            <form action="#" id="registerform">
                                <div id="finalmsg"></div>
                                <div class="mb-2">
                                    <label for="fullname" class="form-label">Full Name <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" id="fullname" name="fullname" placeholder="Enter your name" required>
                                </div>
                                <div class="mb-2">
                                    <label for="email" class="form-label">Email address <span class="text-danger">*</span></label>
                                    <input class="form-control" type="email" name="email" id="email" required placeholder="Enter your email">
                                    <span class="text-danger" id="emailerror"></span>
                                </div>
                                <div class="mb-2">
                                    <label for="password" class="form-label">Password <span class="text-danger">*</span></label>
                                    <div class="input-group input-group-merge">
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter your password" minlength="8">

                                        <div class="input-group-text" data-password="false">
                                            <span class="password-eye"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <label for="mobile" class="form-label">Mobile <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" id="mobile" name="mobile" placeholder="Enter your mobile" onkeypress="return isNum(event)" required>
                                    <span class="text-danger" id="mobileerror"></span>
                                </div>
                                <div class="mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkbox-signup" required>
                                        <label class="form-check-label" for="checkbox-signup">
                                            I agree to the <a href="<?= base_url(); ?>termsofuse" target="_blank">Terms of Use</a> and <a href="<?= base_url(); ?>privacypolicy" target="_blank">Privacy Policy</a>.
                                        </label>
                                    </div>
                                </div>
                                <div class="d-grid text-center">
                                    <button class="btn btn-primary" type="submit" id="signup"> Sign Up </button>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-12 text-center">
                                        <p class="text-muted">Already have account? <a href="<?= base_url(); ?>onlinecourses/login" class="text-primary fw--medium ms-1">Sign In</a></p>
                                    </div> <!-- end col -->
                                </div>
                            </form>

                        </div> <!-- end card-body -->
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
</div>
<?php $this->load->view('onlinecourses/footer'); ?>
<script src="<?php echo base_url();?>assets/js/custom/register.js"></script>
</body>
</html>