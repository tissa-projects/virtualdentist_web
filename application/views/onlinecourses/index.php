<?php
function star_rating($rating)
{
    $rating_round = round($rating * 2) / 2;
    if ($rating_round <= 0.5 && $rating_round > 0) {
        return '<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 1 && $rating_round > 0.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 1.5 && $rating_round > 1) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 2 && $rating_round > 1.5) {
        return '<i class="fa fa-star  text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 2.5 && $rating_round > 2) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;&nbsp;<i class="fa fa-star  text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 3 && $rating_round > 2.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 3.5 && $rating_round > 3) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 4 && $rating_round > 3.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o" style="display:none"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 4.5 && $rating_round > 4) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 5 && $rating_round > 4.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i> ('.$rating.' rating)';
    }
}
?>
<div class="page-content" style="margin-top: 20px;">
	<main class="grid" style="margin: 0% 2%;">
		<?php if($courselist) {
            if($courselist->success == 1) {
            foreach($courselist->data as $row) { ?>
	    <article>
	    	<a href="<?php echo base_url();?>onlinecourses/coursedetail/<?= base64_encode($row->course_id);?>">
		    	<img src="<?= base_url(); ?>uploads/course_cover/<?= $row->course_cover;?>" alt="course cover" class="img-fluid cover-img">
		    </a>
	    	<div class="text">
	    		<a href="<?php echo base_url();?>onlinecourses/coursedetail/<?= base64_encode($row->course_id);?>">
		        	<h3><?= $row->title;?></h3>
		    	</a>
		        <p><?= $row->instructor_display_name;?></p>
                <?php
                  echo star_rating($row->rating);
                ?>
		        <p class="text-primary" style="font-size: 18px;">
		        	<span style="text-decoration: line-through;"><?php if($row->plans) { if($row->plans->final_payable_price != $row->plans->list_price) { 
	                        		echo "₹".$row->plans->list_price; } } ?> </span>&nbsp;
	                        		<?php if($row->plans->final_payable_price != 0) { echo '₹'.$row->plans->final_payable_price; } else { echo 'FREE'; } ?></p>
	    	</div>
	    </article>
		<?php } } } ?>
	</main>
</div>