<link href="<?php echo base_url();?>assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
<style type="text/css">
    .error {
        color: red;
    }
</style>
<div class="page-content pg-cont">
    <div class="account-pages">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">
                        <div class="card-body p-4">
                            <div class="text-center w-75 m-auto">
                                <h1>Change Password</h1>
                            </div>
                            <form action="javascript:void(0)" name="changepassform">
                                <div id="finalmsg"></div>
                                <div class="mb-2">
                                    <label for="password" class="form-label">Password <span class="text-danger">*</span></label>
                                    <input class="form-control" type="password" id="password" name="password" placeholder="Enter your password" >
                                </div>
                                <div class="mb-2">
                                    <label for="password" class="form-label">Confirm Password <span class="text-danger">*</span></label>
                                    <input class="form-control" type="password" id="conf_password" name="conf_password" placeholder="Enter your confirm password" >
                                </div>

                                <div class="d-grid mb-0 text-center">
                                    <input type="hidden" name="email" id="email" value="<?= $email; ?>">
                                    <button class="btn btn-primary" type="submit"> Change Password </button>
                                </div>
                            </form>
                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
</div>

<?php $this->load->view('onlinecourses/footer'); ?>

<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom/login.js"></script>
</body>
</html>