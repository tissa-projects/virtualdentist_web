<link href="<?php echo base_url();?>assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />

<div class="page-content pg-cont">
    <div class="account-pages">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">

                        <div class="card-body p-4">
                            
                            <div class="text-center w-75 m-auto">
                                <h1>Forgot Password</h1>
                            </div>

                            <form action="javascript:void(0)" id="forgotform">
                                <div id="finalmsg"></div>
                                <div class="mb-2">
                                    <label for="email" class="form-label">Email address <span class="text-danger">*</span></label>
                                    <input class="form-control" type="email" id="email" name="email" required="" placeholder="Enter your email" required>
                                    <span class="text-danger" id="emailerror"></span>
                                </div>

                                <div class="d-grid mb-0 text-center">
                                    <button class="btn btn-primary" type="submit"> Forgot </button>
                                </div>

                            </form>
                            <div class="row mt-3">
                                <div class="col-12 text-center">
                                    <p class="text-muted">Don't have an account? <a href="<?= base_url(); ?>onlinecourses/register" class="text-primary fw-medium ms-1">Sign Up</a></p>
                                </div> <!-- end col -->
                            </div>
                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
</div>

<?php $this->load->view('onlinecourses/footer'); ?>
<script src="<?php echo base_url();?>assets/js/custom/login.js"></script>
</body>
</html>