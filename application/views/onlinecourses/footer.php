</div>
<!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#cartModal">View Cart</button>   -->
<?php 
  $cart_list = "";
  if(isset($_SESSION['Local_SESS'])) {
    $cart_list = json_decode(json_encode($_SESSION['Local_SESS']));
  } else {
    $api = $this->session->userdata('url');
    $method = 'POST';
      $url = $api.'getcartlist';
      $header = array(
         'Content-Type: application/json'
      );
      $cart_id = 0;
      if(isset($_SESSION['VIR_SESSION'])) {
        $cart_id = $_SESSION['VIR_SESSION']['cart_id'];
      }
      $apidata = array("cart_id" => $cart_id);
      $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
      $getdata = json_decode($details);
    if($getdata) {
    if($getdata->success == 1) {
      $cart_list = $getdata->data;
      $_SESSION['VIR_SESSION']['cart_sum'] = $getdata->cart_sum;
    }
    }
  }
?>

  <footer class="footer footer-bottom">
    <input type="hidden" name="site_url" id="site_url" value="<?= base_url(); ?>">
    <input type="hidden" name="user" id="user" value="<?php if(isset($_SESSION['VIR_SESSION']) && $_SESSION['VIR_SESSION']['user_id'] != 0) { echo $_SESSION['VIR_SESSION']['user_id']; } ?>">
      <div class="row">
        <div class="col-lg-12 text-center">
          <span>Copyright &copy; <script>document.write(new Date().getFullYear())</script> Virtual Dentist - All Rights Reserved.</span>
        </div>
      </div>
    
  </footer>

  <!-- Footer END --><!-- scroll top button -->
  <button class="scroltop fa fa-arrow-up"></button>
  
  </div>
  <div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-right: 0px!important;margin-bottom:15px;">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="margin-top: 6rem;">
        <div class="modal-header border-bottom-0">
          <h5 class="modal-title" id="exampleModalLabel">
            Your Shopping Cart
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="overflow-x: auto;">
          <div id="cart_list">
              <?php 
                $sum = 0;
                if($cart_list) { ?>
                  <table class="table table-image">
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Product</th>
                        <th scope="col">Price</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        foreach($cart_list as $row) { ?>
                          <tr>
                            <td class="w-25">
                              <img src="<?= base_url(); ?>uploads/course_cover/<?= $row->course_cover;?>" alt="course cover" style="height: 84px;width: 10rem;max-width: none!important;">
                            </td>
                            <td>
                              <?= $row->title;?>
                              <?php if($row->course_access) { ?>
                              <div style="color: #0006;">Course Access | Till <?= $row->course_access;?> </div>
                              <?php } ?>
                            </td>
                            <td><?php if($row->price == 0) { echo "FREE"; } else { echo "₹".$row->price; } ?></td>
                            <td>
                              <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="return deletecartitem(<?= $row->ci;?>);">
                                <i class="fa fa-times"></i>
                              </a>
                            </td>
                          </tr>
                      <?php $sum += $row->price; } ?>
                    </tbody>
                  </table> 
                  <div class="d-flex justify-content-end">
                    <h5>Total Amount: <span class="price text-success">₹<?= $sum; ?></span></h5>
                  </div>
                <div class="modal-footer border-top-0 d-flex justify-content-between">
                  <button class="btn btn-secondary" data-dismiss="modal">Add More</button>
                  <button type="button" class="btn btn-success" onclick="return checkoutfunction();">Checkout</button>
                </div>
                <?php } else { ?>
                  <div class="modal-body">
                    <p style="text-align: center;"><b>There are no items in your cart</b><br/><br/>

                    <button class="btn btn-secondary" data-dismiss="modal" style="text-align: center;">Add More</button></p>
                  </div>
                  <div class="modal-footer border-top-0 d-flex justify-content-between">
                  </div>
               <?php } ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <!-- JAVASCRIPT FILES ========================================= -->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
  <!-- JQUERY.MIN JS -->
  <script src="<?php echo base_url();?>assets/plugins/wow/wow.js"></script><!-- WOW JS -->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/popper.min.js"></script><!-- BOOTSTRAP.MIN JS -->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script><!-- FORM JS -->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
  <script src="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC POPUP JS -->
  <!-- <script src="<?php echo base_url();?>assets/plugins/counter/waypoints-min.js"></script> -->
  <script src="<?php echo base_url();?>assets/plugins/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
  <script src="<?php echo base_url();?>assets/plugins/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
  <script src="<?php echo base_url();?>assets/plugins/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
  <script src="<?php echo base_url();?>assets/plugins/masonry/masonry.filter.js"></script><!-- MASONRY -->
  <script src="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.js"></script><!-- OWL SLIDER -->
  <script src="<?php echo base_url();?>assets/plugins/rangeslider/rangeslider.js" ></script><!-- Rangeslider -->
  <script src="<?php echo base_url();?>assets/js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
  <script src="<?php echo base_url();?>assets/js/dz.carousel.js"></script><!-- SORTCODE FUCTIONS  -->
  <script src="<?php echo base_url();?>assets/js/dz.ajax.js"></script><!-- CONTACT JS  -->
  <!-- App js -->
  <script src="<?php echo base_url();?>assets/js/app.min.js"></script>
  <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
  <script type="text/javascript">
    function isNum(evt){
      evt =(evt)? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode<48 || charCode >57)){
        return false;
      }
      return true;
    }
    $(document).ready(function(){
      var firstName = $('#firstName').text();
      // var lastName = $('#lastName').text();  + $('#lastName').text().charAt(0)
      var intials = $('#firstName').text().charAt(0);
      var profileImage = $('#profileImage').text(intials);
    });
    function deletecartitem(cartitem,checkout='') {
      var site_url = $("#site_url").val();
      var user = $("#user").val();
      if(!user) {
        $.ajax({
          url:site_url+"onlinecourses/deletecartitem",
          type:"POST",
          catch:false,
          data: {cartitem:cartitem},
          success:function(data,status,xhr) {
            // alert(data);
            // console.log(data);return false;
            var obj = JSON.parse(data);
            if(obj.success == 1) {
              $( "#addtocartbutton" ).load(window.location.href + " #addtocartbutton" );
              $("#cart_list").html(obj.cart_list_html);
              $(".payableAmount").html('₹'+obj.sum);
              $("#cart_count").html('<i data-count="'+obj.cart_count+'" class="fa fa-shopping-cart fa-x badge"></i>');
              $("#cartModal").modal('show');
            }
          },
          error: function (jqXhr, textStatus, errorMessage) {
            console.log(errorMessage)
          },
        });
      } else {
        $.ajax({
          url:site_url+"onlinecourses/deletemaincartitem",
          type:"POST",
          catch:false,
          data: {cartitem:cartitem},
          success:function(data,status,xhr) {
            // alert(data);
            // console.log(data);return false;
            var obj = JSON.parse(data);
            if(obj.success == 1) {
              $( "#addtocartbutton" ).load(window.location.href + " #addtocartbutton" );
              $("#cart_list").html(obj.cart_list_html);
              $("#checkout_cart_list").html(obj.cart_list_checkout_html);
              $("#cart_count").html('<i data-count="'+obj.cart_count+'" class="fa fa-shopping-cart fa-x badge"></i>');
              $(".payableAmount").html('₹'+obj.sum);
              if(!checkout) 
                $("#cartModal").modal('show');
            }
          },
          error: function (jqXhr, textStatus, errorMessage) {
            console.log(errorMessage)
          },
        });
      }
    }

    function checkoutfunction() {
      var site_url = $("#site_url").val();
      var user = $("#user").val();
      if(user) {
        location.href = site_url+'onlinecourses/checkout';
      } else {
       location.href = site_url+'onlinecourses/login';
      }
    }

    $(document).ready(function(){
        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;
        $(".next").click(function(){
          current_fs = $(this).parent();
          next_fs = $(this).parent().next();

          //Add Class Active
          $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

          //show the next fieldset
          next_fs.show();
          //hide the current fieldset with style
          current_fs.animate({opacity: 0}, {
          step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
          'display': 'none',
          'position': 'relative'
          });
          next_fs.css({'opacity': opacity});
          },
          duration: 600
          });
        });

        $(".previous").click(function(){
          current_fs = $(this).parent();
          previous_fs = $(this).parent().prev();
          //Remove class active
          $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
          //show the previous fieldset
          previous_fs.show();
          //hide the current fieldset with style
          current_fs.animate({opacity: 0}, {
          step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;
          current_fs.css({
          'display': 'none',
          'position': 'relative'
          });
          previous_fs.css({'opacity': opacity});
          },
          duration: 600
          });
        });

        $('.radio-group .radio').click(function(){
          $(this).parent().find('.radio').removeClass('selected');
          $(this).addClass('selected');
        });

        $(".submit").click(function(){
          return false;
        })
      });

    $("#bill_button").click(function() {
      var name = $("#name").val();
      var mobile = $("#mobile").val();
      var user_id = $("#user").val();
      var site_url = $("#site_url").val();
      if(name == "") {
        $("#name_error").html("Please enter name");
        setTimeout(function() {
          $("#name_error").html("");
        }, 3000);
        $("#name").focus();
        return false;
      }
      if(mobile == "") {
        $("#mobile_error").html("Please enter mobile");
        setTimeout(function() {
          $("#mobile_error").html("");
        }, 3000);
        $("#mobile").focus();
        return false;
      }
      // $( "#pay-button" ).css('display', 'none');
      $.ajax({
        url:site_url+"onlinecourses/addbillingdetails",
        type:"POST",
        catch:false,
        data: {name:name,mobile:mobile,user_id:user_id},
        success:function(data,status,xhr) {
          $( "#pay-button" ).load(window.location.href + " #pay-button" );
          if(data == 1) {
            $( "#bill_div" ).load(window.location.href + " #bill_div" );
            // $( "#pay-button" ).load(window.location.href + " #pay-button" );
          }
          $("#bill_next").click();
          // $( "#mybutton" ).show();
        },
        error: function (jqXhr, textStatus, errorMessage) {
          console.log(errorMessage)
          $( "#pay-button" ).load(window.location.href + " #pay-button" );
          // $( "#mybutton" ).show();
        },
      });
    });

    // var tanId = '<?php if(isset($txid)) { echo $txid; } else { echo ''; } ?>';
    // var status = '<?php if(isset($status)) { echo $status; } else { echo ''; } ?>';
    // if(tanId != '' && status != '') {
    //   $("#bill_next").click();
    //   // $("#bill_next").click();
    //   // $("#nextpaymentbutton").click();
    // }
  </script>
</body>
</html>