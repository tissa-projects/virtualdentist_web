<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
<?php $page = $this->uri->segment(4);?>
<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">     
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Edit Course</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/courses">Courses</a></li>
                                <li class="breadcrumb-item active">Edit Course</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <div class="card text-center">
                        <div class="card-body" style="padding: 0px!important;">
                            <div class="p-3">
                                <img src="<?= base_url('uploads/course_cover/'.$getcourse['data']->course_cover); ?>" alt="course_cover" class="img-fluid profile-pic" style="height: 144px;width: 100%;">
                                <!-- <span class="close-jq-toast-single">×</span> -->
                            </div>
                            <form method="post" action="<?= base_url(); ?>onlinecourses/upload_course_cover" name="upload_cover" enctype="multipart/form-data">
                                <div class="fileupload btn btn-success waves-effect waves-light mb-3">
                                    <span><i class="mdi mdi-cloud-upload me-1 upload-button"></i> Change Course Cover</span>
                                    <input type="file" class="upload" name="course_cover" id="course_cover" accept="image/*">
                                    <input type="hidden" name="course_cover_old" id="course_cover_old" value="<?= $getcourse['data']->course_cover; ?>">
                                    <input type="hidden" name="course_id" id="course_id_add" value="<?= $getcourse['data']->course_id; ?>">
                                </div>
                                <button type="submit" id="upload" style="display: none;">Submit</button>
                            </form>
                            <h4><?= $getcourse['data']->title; ?></h4>
                            <p class="text-muted"><?= $getcourse['data']->instructor_display_name; ?></p>
                            <?php if($getcourse['data']->is_publish == 0 && $coursecontentlist['success'] == 1) { ?>
                            <h5 class="mt-3 mb-2" style="text-align: left;margin-left: 10px;">Publish this course :</h5>
                            <p style="text-align: left;margin-left: 10px;"><button type="button" class="btn btn-info waves-effect waves-light" onclick="return publishcourse(<?= $getcourse['data']->course_id; ?>);"><i class="mdi mdi-arrow-collapse-up me-1"></i> Publish Course</button></p>
                            <?php } else if($getcourse['data']->is_publish == 1 && $coursecontentlist['success'] == 1) { ?>
                                <h5 class="mt-3 mb-2" style="text-align: left;margin-left: 10px;">Unpublish this course :</h5>
                                <p style="text-align: left;margin-left: 10px;"><button type="button" class="btn btn-danger waves-effect waves-light" onclick="return unpublishcourse(<?= $getcourse['data']->course_id; ?>);"><i class="mdi mdi-arrow-collapse-up me-1"></i> Unpublish Course</button></p>
                            <?php } ?>
                            <!-- <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light">Follow</button>
                            <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light">Message</button> -->

                            <div class="text-start mt-3">
                                <div class="table-responsive">
                                    <table class="table table-borderless table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Created Date:</th>
                                                <td class="text-muted"><?= date("d-m-Y",strtotime($getcourse['data']->created_at)); ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Updated Date:</th>
                                                <td class="text-muted"><?= date("d-m-Y",strtotime($getcourse['data']->updated_at)); ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Publish Status:</th>
                                                <td class="text-muted"><?php if($getcourse['data']->is_publish == 1) { ?><span class="badge bg-success">Published</span><?php } else { ?><span class="badge bg-danger">Not Published</span><?php } ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col-->

                <div class="col-lg-9 col-xl-9">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-bordered nav-justified">
                                <li class="nav-item">
                                    <a href="#home-b2" data-bs-toggle="tab" aria-expanded="false" class="nav-link <?php if($page != 'pricing') { echo 'active'; } ?>">
                                        <span class="d-inline-block d-sm-none">Details<!-- <i class="mdi mdi-home-variant"></i> --></span>
                                        <span class="d-none d-sm-inline-block">Details</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#profile-b2" data-bs-toggle="tab" aria-expanded="true" class="nav-link <?php if($page == 'pricing') { echo 'active'; } ?>">
                                        <span class="d-inline-block d-sm-none">Pricing<!-- <i class="mdi mdi-account"></i> --></span>
                                        <span class="d-none d-sm-inline-block">Pricing</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#rating-b2" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                                        <span class="d-inline-block d-sm-none">Rating<!-- <i class="mdi mdi-account"></i> --></span>
                                        <span class="d-none d-sm-inline-block">Rating</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane <?php if($page != 'pricing') { echo 'active'; } ?>" id="home-b2">
                                    <form method="post" name="editcourse" action="<?= base_url(); ?>onlinecourses/editcourseaction" enctype="multiple/form-data" autocomplete="off">
                                        <input type="hidden" name="course_id" id="course_id" value="<?= $getcourse['data']->course_id; ?>">
                                        <div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label class="form-label">Title <span class="text-danger">*</span></label>
                                                        <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title" value="<?= $getcourse['data']->title; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label class="form-label">Instructor Display Name <span class="text-danger">*</span></label>
                                                        <input type="text" id="instructor_display_name" name="instructor_display_name" class="form-control" placeholder="Enter Instructor Display Name" value="<?= $getcourse['data']->instructor_display_name; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label class="form-label">Description <span class="text-danger">*</span> <div id="error_description"></div></label>
                                                <textarea type="text" rows="2" class="form-control" name="description"  id="description"><?= $getcourse['data']->description; ?></textarea>
                                            </div>

                                            <div class="mb-3">
                                                <label class="form-label">Course Tagline </label>
                                                <textarea type="text" rows="2" class="form-control" name="course_tagline"  id="course_tagline"><?= $getcourse['data']->course_tagline; ?></textarea>
                                            </div>

                                            <div class="mb-3">
                                                <label class="form-label">How to Use <span class="text-danger">*</span> <div id="error_how_to_use"></div></label>
                                                <textarea type="text" rows="2" class="form-control" name="how_to_use"  id="how_to_use"><?= $getcourse['data']->how_to_use; ?></textarea>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label class="form-label">Language <span class="text-danger">*</span></label>
                                                        <select class="form-select form-select-md" id="language_id" name="language_id">
                                                            <option value="">Select Language</option>
                                                            <?php if($languages['data']) { 
                                                                foreach($languages['data'] as $lang) { ?>
                                                            <option value="<?= $lang->language_id; ?>" <?php if($getcourse['data']->language_id == $lang->language_id) { echo 'selected'; } ?>><?= $lang->name; ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                    <label class="form-label">Sequence <span class="text-danger">*</span> <span class="text-info"><?php if($course_sequence != 0) { echo 'Note: Max course sequence is '.$course_sequence; } ?></span> </label>
                                                    <input type="number" class="form-control" id="sequence" name="sequence" placeholder="Enter Sequence" value="<?= $getcourse['data']->sequence; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                            <li class="next list-inline-item">
                                                <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                                <button type="submit" class="btn btn-success" id="edit-course-button">Edit Course <i class="mdi mdi-arrow-right ms-1"></i></button>
                                            </li>
                                        </ul>
                                    </form> 
                                </div>
                                <div class="tab-pane <?php if($page == 'pricing') { echo 'active'; } ?>" id="profile-b2">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <?php if(count($course_plan['data']) < 1) { ?>
                                                    <div class="row mb-2">
                                                        <div class="col-sm-4">
                                                            <a href="javascript:void(0);" class="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#add-modal"><i class="mdi mdi-plus-circle me-1"></i> Add Plan</a>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <div class = "table-responsive">
                                                    <table id="basic-datatable" class="table dt-responsive nowrap w-100 table-plan">
                                                        <thead>
                                                            <tr>
                                                                <th>Sr. No.</th>
                                                                <th>Plan name</th>
                                                                <th>Plan type</th>
                                                                <th>Price</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if($course_plan['success'] == 1) { 
                                                                $i =1; 
                                                                foreach($course_plan['data'] as $row) {  ?>                                         
                                                            <tr>
                                                                <td><?= $i; ?></td>
                                                                <td><?= $row->plan_name; ?></td>
                                                                <td><?php if($row->is_free == 0) { echo 'Free'; } else { echo 'One Time Payment'; } ?></td>
                                                                <td>₹<?= $row->final_payable_price; ?></td>
                                                                <td>
                                                                    <div class="button-list" id="tooltip-container">
                                                                    <button type="button" class="btn btn-info btn-xs waves-effect waves-light" id="editplanbutton" data-plan_id="<?= $row->plan_id; ?>", data-plan_name="<?= $row->plan_name; ?>", data-is_free="<?= $row->is_free; ?>", data-list_price="<?= $row->list_price; ?>", data-final_payable_price="<?= $row->final_payable_price; ?>",  data-access_days="<?= $row->access_days; ?>", data-tax_rate="<?= $row->tax_rate; ?>", data-is_include_tax_in_price="<?= $row->is_include_tax_in_price; ?>" data-bs-container="#tooltip-container" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"><i class="mdi mdi-square-edit-outline"></i></button>
                                                                    <button type="button" class="btn btn-danger btn-xs waves-effect waves-light" id="deleteplanbutton"data-plan_id="<?= $row->plan_id; ?>" data-bs-container="#tooltip-container" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"><i class="ri-delete-bin-fill align-middle"></i></button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php $i++; } } ?>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="rating-b2">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <form method="post" name="ratecourse" action="<?= base_url(); ?>onlinecourses/ratingaction" enctype="multiple/form-data" autocomplete="off">
                                                        <input type="hidden" name="course_id" id="course_id_rating" value="<?= $getcourse['data']->course_id; ?>">
                                                        <input type="hidden" name="rating_id" id="rating_id" value="<?php if(isset($getcourse['data']->rating_id)) { echo $getcourse['data']->rating_id; } ?>">
                                                        <input type="hidden" name="user_id" id="user_id" value="<?= $_SESSION['VIR_SESSION']['user_id']; ?>">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="mb-3">
                                                                        <label class="form-label">Rating <span class="text-danger">*</span></label>
                                                                        <input type="number" id="rating" name="rating" class="form-control" placeholder="Enter Rating" min="0" max="5" value="<?php if(isset($getcourse['data']->rating)) { echo $getcourse['data']->rating; } ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                                            <li class="next list-inline-item">
                                                                <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                                                <button type="submit" class="btn btn-success" id="add-rate-button">Add Rating <i class="mdi mdi-arrow-right ms-1"></i></button>
                                                            </li>
                                                        </ul>
                                                    </form> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>    
        </div> <!-- container -->
    </div> <!-- content -->

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" name="addplan" action="<?= base_url(); ?>onlinecourses/addcourseplan" autocomplete="off">
                    <input type="hidden" name="course_id" id="course_id_edit" value="<?= $getcourse['data']->course_id; ?>">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Pricing Plan</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Plan Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="plan_name" name="plan_name" placeholder="Enter Plan Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Plan Type <span class="text-danger">*</span></label><br/>
                                    <input class="form-check-input" type="radio" name="is_free" id="flexRadioDefault1" value="0">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        &nbsp;Free&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input" type="radio" name="is_free" id="flexRadioDefault2" value="1">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        &nbsp;One Time Payment
                                    </label>
                                    <br/>
                                    <label for="is_free" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: none;" id="pricing">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">List price <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-text">₹</div>
                                        <input type="number" class="form-control" id="list_price" name="list_price" placeholder="List price">
                                    </div>
                                    <label for="list_price" class="error" generated="true"></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Final payable price <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-text">₹</div>
                                        <input type="number" class="form-control" id="final_payable_price" name="final_payable_price" placeholder="Final payable price">
                                    </div>
                                    <label for="final_payable_price" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" name="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Limit course access duration
                                </label>
                            </div>
                        </div>
                        <div class="row course_access_none" style="padding: 15px 0 0 15px;display: none;">
                            <div class="col-md-7">
                                <input class="form-check-input access_date_days" type="checkbox" value="date" id="flexCheckDefault1">
                                <label class="form-check-label" for="flexCheckDefault1">
                                    Until fixed date
                                </label>
                            </div>
                            <div class="col-md-5 access_date" style="display: none;">
                                <input class="form-control" type="date" name="access_date" id="access_date">
                            </div>
                        </div>
                        <div class="row course_access" style="padding: 15px 0 0 15px;display: none;">
                            <div class="col-md-7">
                                <input class="form-check-input access_date_days" type="checkbox" value="days" id="flexCheckDefault2">
                                <label class="form-check-label" for="flexCheckDefault2">
                                    Until specific number of days
                                </label>
                            </div>
                            <div class="col-md-5 access_days" style="display: none;">
                                <input class="form-control" type="number" name="access_days" id="access_days" placeholder="Days">
                            </div>
                        </div>
                        <div class="row course_access_none" style="padding: 15px 0 0 15px;display: none;">
                            <div class="col-md-7">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault3">
                                <label class="form-check-label" for="flexCheckDefault3">
                                    By maximum video watch time
                                </label>
                            </div>
                            <div class="col-md-5 access_hours" style="display: none;">
                                <input class="form-control" type="number" name="access_hours" id="access_hours" placeholder="Hours">
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <h4>Tax Settings</h4>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Tax Rate (in %)</label>
                                    <input type="number" class="form-control" id="tax_rate" name="tax_rate" placeholder="Enter Tax Rate">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Show Pricing including taxes</label><br/>
                                    <input class="form-check-input" type="radio" name="is_include_tax_in_price" id="flexRadioDefault8" value="1" checked="">
                                    <label class="form-check-label" for="flexRadioDefault8">
                                        &nbsp;Yes&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input" type="radio" name="is_include_tax_in_price" id="flexRadioDefault9" value="0">
                                    <label class="form-check-label" for="flexRadioDefault9">
                                        &nbsp;No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light" id="add-plan-buttton">Save Plan</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->

    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" name="editplan" action="<?= base_url(); ?>onlinecourses/editcourseplan" autocomplete="off">
                    <input type="hidden" name="plan_id" id="plan_id_edit" value="">
                    <input type="hidden" name="course_id" id="course_id_edit1" value="<?= $getcourse['data']->course_id; ?>">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Pricing Plan</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Plan Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="plan_name_edit" name="plan_name" placeholder="Enter Plan Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Plan Type <span class="text-danger">*</span></label><br/>
                                    <input class="form-check-input is_free_edit" type="radio" name="is_free_edit" id="flexRadioDefault11" value="0">
                                    <label class="form-check-label" for="flexRadioDefault11">
                                        &nbsp;Free&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input is_free_edit" type="radio" name="is_free_edit" id="flexRadioDefault12" value="1">
                                    <label class="form-check-label" for="flexRadioDefault22">
                                        &nbsp;One Time Payment
                                    </label>
                                    <br/>
                                    <label for="is_free_edit" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: none;" id="pricing_edit">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">List price <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-text">₹</div>
                                        <input type="number" class="form-control" id="list_price_edit" name="list_price" placeholder="List price">
                                    </div>
                                    <label for="list_price_edit" class="error" generated="true"></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Final payable price <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-text">₹</div>
                                        <input type="number" class="form-control" id="final_payable_price_edit" name="final_payable_price" placeholder="Final payable price">
                                    </div>
                                    <label for="final_payable_price" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault0" name="flexCheckDefault0">
                                <label class="form-check-label" for="flexCheckDefault0">
                                    Limit course access duration
                                </label>
                            </div>
                        </div>
                        <div class="row course_access_edit_none" style="padding: 15px 0 0 15px;display: none;">
                            <div class="col-md-7">
                                <input class="form-check-input access_date_days_edit" type="checkbox" value="date" id="flexCheckDefault10">
                                <label class="form-check-label" for="flexCheckDefault10">
                                    Until fixed date
                                </label>
                            </div>
                            <div class="col-md-5 access_date_edit" style="display: none;">
                                <input class="form-control" type="date" name="access_date" id="access_date_edit">
                            </div>
                        </div>
                        <div class="row course_access_edit" style="padding: 15px 0 0 15px;display: none;">
                            <div class="col-md-7">
                                <input class="form-check-input access_date_days_edit" type="checkbox" value="days" id="flexCheckDefault20">
                                <label class="form-check-label" for="flexCheckDefault20">
                                    Until specific number of days
                                </label>
                            </div>
                            <div class="col-md-5 access_days_edit" style="display: none;">
                                <input class="form-control" type="number" name="access_days" id="access_days_edit" placeholder="Days">
                            </div>
                        </div>
                        <div class="row course_access_edit_none" style="padding: 15px 0 0 15px;display: none;">
                            <div class="col-md-7">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault30">
                                <label class="form-check-label" for="flexCheckDefault30">
                                    By maximum video watch time
                                </label>
                            </div>
                            <div class="col-md-5 access_hours_edit" style="display: none;">
                                <input class="form-control" type="number" name="access_hours" id="access_hours_edit" placeholder="Hours">
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <h4>Tax Settings</h4>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Tax Rate (in %)</label>
                                    <input type="number" class="form-control" id="tax_rate_edit" name="tax_rate" placeholder="Enter Tax Rate">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Show Pricing including taxes</label><br/>
                                    <input class="form-check-input" type="radio" name="is_include_tax_in_price_edit" id="flexRadioDefault18" value="1" checked="">
                                    <label class="form-check-label" for="flexRadioDefault18">
                                        &nbsp;Yes&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input" type="radio" name="is_include_tax_in_price_edit" id="flexRadioDefault19" value="0">
                                    <label class="form-check-label" for="flexRadioDefault19">
                                        &nbsp;No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light" id="edit-plan-button">Save Plan</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
$("#instructor").select2({
  placeholder: "Select Instructor",
  allowClear: true
});
/* CK Editors */
CKEDITOR.replace('description');
CKEDITOR.replace('course_tagline');
CKEDITOR.replace('how_to_use');
CKEDITOR.instances.description.on('change', function() {    
    if(CKEDITOR.instances.description.getData().length >  0) {
      $('label[for="description"]').hide();
    }
});
CKEDITOR.instances.how_to_use.on('change', function() {    
    if(CKEDITOR.instances.how_to_use.getData().length >  0) {
      $('label[for="how_to_use"]').hide();
    }
});
$(function() {
    var site_url = $("#site_url").val();
    $("form[name='editcourse']").validate({
        ignore: [],
        debug: false,
        rules: {
            title: "required",
            instructor_display_name: "required",
            language_id: "required",
            sequence: {
                required: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/check_course_sequence",
                    type: "post",
                    data: {
                        sequence: function() {
                            return $("#sequence").val();
                        },
                        course_id: function(){
                            return $('#course_id').val();
                        }
                    }
                }
            },
            description: {
                required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement();
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                    return editorcontent.length === 0;
                }
            },
            how_to_use: {
                required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement();
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                    return editorcontent.length === 0;
                }
            }
        },
        messages: {
            title: "Please enter title",
            instructor_display_name: "Please enter instructor display name",
            language_id: "Please select language",
            sequence: {
                required: "Please enter sequence",
                remote: "This sequence already exist"
            },
            description: {
                required: "Please enter description"
            },
            how_to_use: {
                required: "Please enter how to use"
            }
        },  
        submitHandler: function(form) {
            $("#edit-course-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#edit-course-button").attr('disabled',true);
            form.submit();
        }
    });
    $("form[name='addplan']").validate({
        ignore: [],
        debug: false,
        rules: {
            plan_name: "required",
            is_free: "required",
        },
        messages: {
            title: "Please enter title",
            plan_name: "Please enter plan name",
            is_free: "Please select plan type",
            list_price: "Please enter list price",
            final_payable_price: "Please enter final payable price",
            access_days: "Please enter day count"          
        },  
        submitHandler: function(form) {
            $("#add-plan-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#add-plan-button").attr('disabled',true);
          form.submit();
        }
    });
    $("form[name='editplan']").validate({
        ignore: [],
        debug: false,
        rules: {
            plan_name: "required",
            is_free_edit: "required",
        },
        messages: {
            title: "Please enter title",
            plan_name: "Please enter plan name",
            is_free: "Please select plan type",
            list_price: "Please enter list price",
            final_payable_price: "Please enter final payable price",
            access_days: "Please enter day count"           
        },  
        submitHandler: function(form) {
            $("#edit-plan-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#edit-plan-button").attr('disabled',true);
          form.submit();
        }
    });
    $("form[name='ratecourse']").validate({
        ignore: [],
        debug: false,
        rules: {
            rating: {
                required: true,
                maxlength: 5
            }
        },
        messages: {
            rating: {
                required: "Please enter rating",
                maxlength: "Max rating is 5"
            }
        },  
        submitHandler: function(form) {
            $("#add-rate-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#add-rate-button").attr('disabled',true);
          form.submit();
        }
    });
});

$("input[name=is_free]:radio").change(function () {
    if(this.value != 0) {
        $("#pricing").show();
        $("#list_price").attr('required', true);
        $("#final_payable_price").attr('required', true);
    } else {
        $("#pricing").hide();
        $("#list_price").attr('required', false);
        $("#final_payable_price").attr('required', false);
        $("#list_price").val('');
        $("#final_payable_price").val('');
    }
});
$('#flexCheckDefault').change(function() {
    if(this.checked) {
        $(".course_access").show();
    } else {
        $(".course_access").hide();
    }
});
$(document).ready(function(){
    $('.access_date_days').click(function() {
        if(this.value == 'date') {
            $(".access_date").show();
            $(".access_days").hide();
            $("#access_days").val('');
            // $("#access_date").attr('required', true);
            $("#access_days").attr('required', false);
        } else {
            $(".access_days").show();
            $(".access_date").hide();
            $("#access_date").val('');
            $("#access_days").attr('required', true);
            // $("#access_date").attr('required', false);
        }
        $('.access_date_days').not(this).prop('checked', false);
    });
});
// $('#flexCheckDefault1').change(function() {
//     if(this.checked) {
//         $(".access_date").show();
//         $("#access_date").attr('required', true);
//     } else {
//         $(".access_date").hide();
//         $("#access_date").val('');
//         $("#access_date").attr('required', false);
//     }
// });
$('#flexCheckDefault2').change(function() {
    if(this.checked) {
        $(".access_days").show();
        $("#access_days").attr('required', true);
    } else {
        $(".access_days").hide();
        $("#access_days").val('');
        $("#access_days").attr('required', false);
    }
});
// $('#flexCheckDefault3').change(function() {
//     if(this.checked) {8
//         $(".access_hours").show();
//         $("#access_hours").attr('required', true);
//     } else {
//         $(".access_hours").hide();
//         $("#access_hours").val('');
//         $("#access_hours").attr('required', false);
//     }
// });

$('.table-plan').on('click', '#editplanbutton', function() {
    event.preventDefault();
    var plan_id = $(this).data('plan_id');
    var plan_name = $(this).data('plan_name');
    var is_free = $(this).data('is_free');
    var list_price = $(this).data('list_price');
    var final_payable_price = $(this).data('final_payable_price');
    // var access_date = $(this).data('access_date');
    var access_days = $(this).data('access_days');
    // var access_hours = $(this).data('access_hours');
    var tax_rate = $(this).data('tax_rate');
    var is_include_tax_in_price = $(this).data('is_include_tax_in_price');

    $('#plan_id_edit').val(plan_id);
    $('#plan_name_edit').val(plan_name);
    $("input[name=is_free_edit][value=" + is_free + "]").prop('checked', true);
    setpaymenttypechecked(is_free);
    $('#list_price_edit').val(list_price);
    $('#final_payable_price_edit').val(final_payable_price);
    if(access_days != '') {
        $("input[name=flexCheckDefault0]").prop('checked', true);
        if($("input[name=flexCheckDefault0]").prop('checked', true)) {
            $(".course_access_edit").show();
        } else {
            $(".course_access_edit").hide();
        }
    }

    // if(access_date != '' && access_date != '0000-00-00') {
    //     $("#flexCheckDefault10").prop('checked', true);
    //     if($("#flexCheckDefault10").prop('checked', true)) {
    //         $(".access_date_edit").show();
    //         $("#access_date_edit").attr('required', true);
    //     } else {
    //         $(".access_date_edit").hide();
    //         $("#access_date_edit").val('');
    //         $("#access_date_edit").attr('required', false);
    //     }
    //     $("#access_date_edit").val(access_date);
    // }
    if(access_days != ''){
        $('#flexCheckDefault20').prop('checked', true);
        if($('#flexCheckDefault20').prop('checked', true)) {
            $(".access_days_edit").show();
            $("#access_days_edit").attr('required', true);
        } else {
            $(".access_days_edit").hide();
            $("#access_days_edit").val('');
            $("#access_days_edit").attr('required', false);
        }
        $("#access_days_edit").val(access_days);
    }
    // if(access_hours != ''){
    //     $('#flexCheckDefault30').prop('checked', true);
    //     if($('#flexCheckDefault30').prop('checked', true)) {
    //         $(".access_hours_edit").show();
    //         $("#access_hours_edit").attr('required', true);
    //     } else {
    //         $(".access_hours_edit").hide();
    //         $("#access_hours_edit").val('');
    //         $("#access_hours_edit").attr('required', false);
    //     }
    //     $("#access_hours_edit").val(access_hours);
    // }
    $('#tax_rate_edit').val(tax_rate);
    $("input[name=is_include_tax_in_price_edit][value=" + is_include_tax_in_price + "]").prop('checked', true);
    $('#edit-modal').modal('show');
});

function setpaymenttypechecked(type) {
    if(type != 0) {
        $("#pricing_edit").show();
        $("#list_price_edit").attr('required', true);
        $("#final_payable_price_edit").attr('required', true);
    } else {
        $("#pricing_edit").hide();
        $("#list_price_edit").attr('required', false);
        $("#final_payable_price_edit").attr('required', false);
        $("#list_price_edit").val('');
        $("#final_payable_price_edit").val('');
    }
}

$("input[name=is_free_edit]:radio").change(function () {
    if(this.value != 0) {
        $("#pricing_edit").show();
        $("#list_price_edit").attr('required', true);
        $("#final_payable_price_edit").attr('required', true);
    } else {
        $("#pricing_edit").hide();
        $("#list_price_edit").attr('required', false);
        $("#final_payable_price_edit").attr('required', false);
        $("#list_price_edit").val('');
        $("#final_payable_price_edit").val('');
    }
});

$(document).ready(function(){
    $('.access_date_days_edit').click(function() {
        if(this.value == 'date') {
            $(".access_date_edit").show();
            $(".access_days_edit").hide();
            $("#access_days_edit").val('');
            $("#access_date_edit").attr('required', true);
            $("#access_days_edit").attr('required', false);
        } else {
            $(".access_days_edit").show();
            $(".access_date_edit").hide();
            $("#access_date_edit").val('');
            $("#access_days_edit").attr('required', true);
            $("#access_date_edit").attr('required', false);
        }
        $('.access_date_days_edit').not(this).prop('checked', false);
    });
});
$('#flexCheckDefault0').change(function() {
    if(this.checked) {
        $(".course_access_edit").show();
    } else {
        $(".course_access_edit").hide();
    }
});
// $('#flexCheckDefault10').change(function() {
//     if(this.checked) {
//         $(".access_date_edit").show();
//         $("#access_date_edit").attr('required', true);
//     } else {
//         $(".access_date_edit").hide();
//         $("#access_date_edit").val('');
//         $("#access_date_edit").attr('required', false);
//     }
// });
$('#flexCheckDefault20').change(function() {
    if(this.checked) {
        $(".access_days_edit").show();
        $("#access_days_edit").attr('required', true);
    } else {
        $(".access_days_edit").hide();
        $("#access_days_edit").val('');
        $("#access_days_edit").attr('required', false);
    }
});
// $('#flexCheckDefault30').change(function() {
//     if(this.checked) {
//         $(".access_hours_edit").show();
//         $("#access_hours_edit").attr('required', true);
//     } else {
//         $(".access_hours_edit").hide();
//         $("#access_hours_edit").val('');
//         $("#access_hours_edit").attr('required', false);
//     }
// });
$('.table-plan').on('click', '#deleteplanbutton', function() {
    event.preventDefault();
    var plan_id = $(this).data('plan_id');
    var site_url = $('#site_url').val();
    var course_id = $('#course_id').val();
    $("#deleteform").attr('action', site_url+'onlinecourses/deletecourseplan/'+course_id);
    $("#deletehead").html('Delete Plan');
    $("#deletemsg").html('Do you want to delete this plan');
    $("#delete_id").val(plan_id);
    $("#deletemsgmodal").modal('show');
});
function publishcourse(course_id) {
    var site_url = $('#site_url').val();
    $("#deleteform").attr('action', site_url+'onlinecourses/publishcourse');
    $("#deletehead").html('Publish Course');
    $("#deletemsg").html('Do you want to publish this course');
    $("#delete_id").val(course_id);
    $("#status").val(1);
    $("#deletemsgmodal").modal('show');
}
function unpublishcourse(course_id) {
    var site_url = $('#site_url').val();
    $("#deleteform").attr('action', site_url+'onlinecourses/publishcourse');
    $("#deletehead").html('Unpublish Course');
    $("#deletemsg").html('Do you want to unpublish this course');
    $("#status").val(0);
    $("#delete_id").val(course_id);
    $("#deletemsgmodal").modal('show');
}
$(document).ready(function() {
    $(".upload").on('change', function(){
        $("#upload").click();
    });
});
</script>
</body>
</html>