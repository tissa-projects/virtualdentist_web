<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<?php
function star_rating($rating)
{
    $rating_round = round($rating * 2) / 2;
    if ($rating_round <= 0.5 && $rating_round > 0) {
        return '<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 1 && $rating_round > 0.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 1.5 && $rating_round > 1) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 2 && $rating_round > 1.5) {
        return '<i class="fa fa-star  text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 2.5 && $rating_round > 2) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;&nbsp;<i class="fa fa-star  text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 3 && $rating_round > 2.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 3.5 && $rating_round > 3) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 4 && $rating_round > 3.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 4.5 && $rating_round > 4) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 5 && $rating_round > 4.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i> ('.$rating.' rating)';
    }
    
}
?>
<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Courses</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">Courses</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title --> 
            <div class="row mb-2">
                <div class="col-sm-4">
                    <a href="<?= base_url(); ?>onlinecourses/addcourses" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle me-1"></i> Add Course</a>
                </div>
                <!-- <div class="col-sm-8">
                    <div class="float-sm-end">
                        <form class="d-flex align-items-center flex-wrap">
                            <div class="me-2">
                                <label for="productssearch-input" class="visually-hidden">Search</label>
                                <input type="search" class="form-control border-light" id="productssearch-input" placeholder="Search...">
                            </div>
                            <button type="button" class="btn btn-success mb-2 mb-sm-0"><i class="mdi mdi-cog"></i></button>
                        </form>
                    </div>
                </div> -->
            </div>
            <!-- end row -->

            
            <div class="row">
                <?php if($courselist['success'] == 1) {
                	foreach($courselist['data'] as $row) { ?>
		                <div class="col-md-6 col-xl-3" style="display:grid;">
		                    <div class="card product-box">
		                        <div class="product-img">
		                            <div class="p-3">
		                                <img src="<?= base_url(); ?>uploads/course_cover/<?= $row->course_cover;?>" alt="course_cover" class="img-fluid" style="height: 144px;width: 100%;"/>
		                            </div>
		                            <div class="product-action">
		                                <div class="d-flex">
		                                    <!-- <a href="javascript: void(0);" class="btn btn-white d-block w-100 action-btn" style="margin:0.25rem!important;"><i class="ri-edit-2-fill align-middle"></i> Details</a> -->
		                                    <a href="<?= base_url(); ?>onlinecourses/editcourse/<?= base64_encode($row->course_id);?>" class="btn btn-info btn-sm d-block action-btn" style="margin:0.25rem!important;" title="Edit Course/Plan"><i class="ri-edit-2-fill align-middle"></i></a>
		                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm d-block action-btn" style="margin:0.25rem!important;" onclick="return deletecourse(<?= $row->course_id; ?>);" title="Delete Course"><i class="ri-delete-bin-fill align-middle"></i></a>
                                            <a href="<?= base_url(); ?>onlinecourses/coursecontent/<?= base64_encode($row->course_id);?>" class="btn btn-success btn-sm d-block action-btn" style="margin:0.25rem!important;" title="Upload Course PDF"><!-- <i class="ri-layout-line"></i> -->Upload PDF</a>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="product-info border-top p-3">
		                            <div>
		                                <h5 class="font-16 mt-0 mb-1"><a href="javascript:void(0);" class="text-dark"><?= $row->title;?></a></h5>
		                                <span><?= $row->instructor_display_name;?></span><br/>
		                                <span><?php if($row->is_publish == 1) { ?><span class="badge bg-success">Published</span><?php } else { ?><span class="badge bg-danger">Not Published</span><?php } ?></span>
		                                <p class="text-muted">
                                                <?php
                                                  echo star_rating($row->rating);
                                                ?>
		                                </p>
		                                <h4 class="m-0"> <span class="text-muted">Price :  <?php if($row->plans) { echo '₹'.$row->plans->final_payable_price; } else { echo '₹0'; } ?></span></h4>
		                            </div>
		                        </div> <!-- end product info-->
		                    </div>
		                </div>
		        <?php } } else { ?>
                    <div class="col-md-12 col-xl-12">
                        <div class="card product-box" style="text-align: center;">
                            No Data Available
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!-- end row -->

            <!-- <div class="row">
                <div class="col-12">
                    <ul class="pagination pagination-rounded justify-content-end mb-3">
                        <li class="page-item">
                            <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                <span aria-hidden="true">«</span>
                            </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                        <li class="page-item">
                            <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                <span aria-hidden="true">»</span>
                            </a>
                        </li>
                    </ul>
                </div> 
            </div> -->
            <!-- end row-->
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
	function deletecourse(course_id) {
	    var site_url = $('#site_url').val();
	    $("#deleteform").attr('action', site_url+'onlinecourses/deletecourse/');
	    $("#deletehead").html('Delete Course');
	    $("#deletemsg").html('Do you want to delete this course');
	    $("#delete_id").val(course_id);
	    $("#deletemsgmodal").modal('show');
	}
</script>
</body>
</html>