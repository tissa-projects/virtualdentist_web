<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
            
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Add Course</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/courses">Courses</a></li>
                                <li class="breadcrumb-item active">Add Course</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->             
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" name="addcourse" action="<?= base_url(); ?>onlinecourses/addcourseaction" enctype="multipart/form-data" autocomplete="off">
                                <div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label">Title <span class="text-danger">*</span></label>
                                                <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label">Instructor Display Name <span class="text-danger">*</span></label>
                                                <input type="text" id="instructor_display_name" name="instructor_display_name" class="form-control" placeholder="Enter Instructor Display Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6" style="display: none;">
                                            <div class="mb-3">
                                                <label class="form-label">Tags comma separated for multiple tags </label>
                                                <input type="text" id="tags" name="tags" class="form-control" placeholder="Enter Tags">
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="display: none;">
                                            <div class="mb-3">
                                                <label class="form-label">Instructor </label>
                                                <select class="form-select form-select-md select2" id="instructor" name="instructor[]" multiple="true">
                                                    <option value="">Select Instructor</option>
                                                    <?php if($instructors['data']) { 
                                                        foreach($instructors['data'] as $lang) { ?>
                                                    <option value="<?= $lang->user_id; ?>"><?= $lang->name; ?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label">Language <span class="text-danger">*</span></label>
                                                <select class="form-select form-select-md" id="language_id" name="language_id">
                                                    <option value="">Select Language</option>
                                                    <?php if($languages['data']) { 
                                                        foreach($languages['data'] as $lang) { ?>
                                                    <option value="<?= $lang->language_id; ?>"><?= $lang->name; ?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 
                                    <div class="row">
                                    </div> -->

                                    <div class="mb-3">
                                        <label class="form-label">Description <span class="text-danger">*</span> <div id="error_description"></div></label>
                                        <textarea type="text" rows="2" class="form-control" name="description"  id="description"></textarea>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Course Tagline </label>
                                        <textarea type="text" rows="2" class="form-control" name="course_tagline"  id="course_tagline"></textarea>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">How to Use <span class="text-danger">*</span> <div id="error_how_to_use"></div></label>
                                        <textarea type="text" rows="2" class="form-control" name="how_to_use"  id="how_to_use"></textarea>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label">Course Cover Image <span class="text-danger">*</span></label>
                                                <input type="file" id="course_cover" name="course_cover" class="form-control" placeholder="Select course cover"  accept="image/png, image/gif, image/jpeg">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label">Sequence <span class="text-danger">*</span> <span class="text-info"><?php if($course_sequence != 0) { echo 'Note: Max course sequence is '.$course_sequence; } ?></span> </label>
                                            <input type="number" class="form-control" id="sequence" name="sequence" placeholder="Enter Sequence">
                                        </div>
                                        <div class="col-lg-6" style="display: none;">
                                            <div class="mb-3">
                                                <label class="mb-2">Make it freely available to learners on sign up <span class="text-danger">*</span></label>
                                                <br/>
                                                <div class="radio form-check-inline">
                                                    <input type="radio" id="inlineRadio1" value="0" name="is_freely_available" checked="">
                                                    <label for="inlineRadio1"> No </label>
                                                </div>
                                                <div class="radio form-check-inline">
                                                    <input type="radio" id="inlineRadio2" value="1" name="is_freely_available">
                                                    <label for="inlineRadio2"> Yes </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                    <li class="next list-inline-item">
                                        <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                        <button type="submit" class="btn btn-success" id="add-course-button">Add Course <i class="mdi mdi-arrow-right ms-1"></i></button>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
$("#instructor").select2({
  placeholder: "Select Instructor",
  allowClear: true
});
/* CK Editors */
CKEDITOR.replace('description');
CKEDITOR.replace('course_tagline');
CKEDITOR.replace('how_to_use');
CKEDITOR.instances.description.on('change', function() {   
    if(CKEDITOR.instances.description.getData().length >  0) {
      $('label[for="description"]').hide();
    }
});
CKEDITOR.instances.how_to_use.on('change', function() {    
    if(CKEDITOR.instances.how_to_use.getData().length >  0) {
      $('label[for="how_to_use"]').hide();
    }
});
$(function() {
    var site_url = $("#site_url").val();
  $("form[name='addcourse']").validate({
    ignore: [],
    debug: false,
    rules: {
        title: "required",
        instructor_display_name: "required",
        language_id: "required",
        sequence: {
            required: true,
            remote: {
                url: "<?= base_url(); ?>onlinecourses/check_course_sequence",
                type: "post",
                data: {
                    sequence: function() {
                        return $("#sequence").val();
                    }
                }
            }
        },
        description: {
            required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement();
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                return editorcontent.length === 0;
            }
        },
        how_to_use: {
            required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement();
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                return editorcontent.length === 0;
            }
        },
        course_cover: "required"
    },
    messages: {
        title: "Please enter title",
        instructor_display_name: "Please enter instructor display name",
        language_id: "Please select language",
        sequence: {
            required: "Please enter sequence",
            remote: "This sequence already exist"
        },
        description: {
            required: "Please enter description"
        },
        how_to_use: {
            required: "Please enter how to use"
        },
        course_cover: "Please select course cover image"
    },  
    submitHandler: function(form) {
        $("#add-course-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
        $("#add-course-button").attr('disabled',true);
        form.submit();
    }
  });
});
</script>
</body>
</html>