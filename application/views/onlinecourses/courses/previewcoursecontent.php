<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<style type="text/css">
    .top-bar {
      background: #333;
      color: #fff;
      padding: 1rem;
    }

    .btnnew {
      background: coral;
      color: #fff;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 0.7rem 2rem;
    }

    .btnnew:hover {
      opacity: 0.9;
    }

    .page-info {
      margin-left: 1rem;
    }

    .error {
      background: orangered;
      color: #fff;
      padding: 1rem;
    }
    .active-menu {
        color: #37cde6!important;
    }
</style>
    <div class="left-side-menu">
        <!-- LOGO -->
        <div class="logo-box">
            <a href="<?= base_url(); ?>onlinecourses" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/favicon.png"/>
                </span>
                <span class="logo-lg">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" width="90%"/>
                </span>
            </a>
        </div>

        <div class="h-100" data-simplebar>
            <?php $uri2 = $this->uri->segment(2); ?>
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul id="side-menu">
                    <a href="<?= base_url(); ?>onlinecourses/courses"><li class="menu-title"><h4 style="color: #3bafda!important;"><i class="mdi mdi-arrow-collapse-left"></i>  Back to courses</h4></li></a>
                
                    <li class="menuitem-active">
                        <a href="#sidebarLayouts" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-line"></i>
                            <span> Course Contents </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse show" id="sidebarLayouts">
                            <ul class="nav-second-level" style="padding: 0px;">
                                <?php if($coursecontentlist) { 
                                    $i = 1;
                                    foreach($coursecontentlist['data'] as $contlist) { 
                                        $active = '';
                                        if($i == 1) {
                                            $active = 'active-menu';
                                            $collapsed = '';
                                        }
                                ?>
                                <li class="dd-item dd-handle <?= $active; ?>" style="height: auto!important;cursor: pointer;"  onclick="return getChapterContent(<?= $i; ?>,<?= $contlist->chapter_id; ?>,'');">
                                    <i class="mdi mdi-arrow-left-circle"></i> <?= $contlist->title; ?> -- <?php if($contlist->chapter_type == 1) { echo 'Course PDF'; } else { echo "Assignment"; } ?>
                                </li>
                            <?php  $i++; } } ?>
                            </ul>
                        </div>
                    </li>
                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Preview Course  --  <?= $getcourse['data']->title; ?></h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses/dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses/courses">Courses</a></li>
                                <li class="breadcrumb-item active">Preview Course</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="row">
                <div class="col-xl-12 col-md-12 pdf-div" style="display: none;">
                    <div class="card">
                        <div class="card-body">
                            <div class="top-bar">
                              <button class="btnnew" id="prev-page">
                                <i class="fas fa-arrow-circle-left"></i> Prev Page
                              </button>
                              <button class="btnnew" id="next-page">
                                Next Page <i class="fas fa-arrow-circle-right"></i>
                              </button><br/>
                              <span class="page-info">
                                Page <span id="page-num"></span> of <span id="page-count"></span>
                              </span>
                            </div>

                            <canvas id="pdf-render" style="width: 100%!important;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12 assign-div" style="display: none;">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label class="form-label">Assignment Instructions </label><br/>
                                       <label style="border:1px solid grey;width:100%;padding:10px;"  id="assign_instructions"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label class="form-label">Assignment Upload <span class="text-muted">You can upload maximum 4 MB of file size.</span></label>
                                        <input type="file" id="assignment_upload" name="assignment_upload" class="form-control" placeholder="Upload Assignment" disabled="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label class="form-label">Notes </label>
                                        <textarea type="text" id="note" name="note" class="form-control" placeholder="Enter Notes" rows="10" cols="40" disabled="true"></textarea>
                                    </div>
                                </div>
                            </div>
                            <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                <li class="next list-inline-item">
                                    <button type="button" class="btn btn-success" disabled="true">Submit Assignment <i class="mdi mdi-arrow-right ms-1"></i></button>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var index = $('.active-menu').index('.dd-handle')
    var $li = $('.active-menu')
    $li.click();
});

$(".button-next").click(function() {
  var index = $('.active-menu').index('.dd-handle')
  var $li = $('.dd-handle')
  $li.removeClass('active-menu') 
  $li.eq( (index+1) % $li.length ).addClass('active-menu')
  $li.eq( (index+1) % $li.length ).click();
});
$(".button-prev").click(function() {
  var index = $('.active-menu').index('.dd-handle')
  var $li = $('.dd-handle')
  $li.removeClass('active-menu')
  $li.eq( (index-1) % $li.length ).addClass('active-menu')
  $li.eq( (index-1) % $li.length ).click();
});

function getChapterContent(indexclass, chapter_id, sub_chapter_id='') {
    var site_url = $("#site_url").val();
    var index = $('.active-menu').index('.dd-handle')
    var $li = $('.dd-handle')
    $li.removeClass('active-menu') 
    $li.eq( (indexclass-1) % $li.length ).addClass('active-menu')
    // $("#loader").show();
    $.ajax({
        url:site_url+"onlinecourses/getchaptercontents",
        type:"POST",
        data: {chapter_id:chapter_id,sub_chapter_id:sub_chapter_id},
        success:function(data,status,xhr) {
            // $("#loader").hide();
            var obj = JSON.parse(data);
            if(obj.success == 1) {
                if(obj.data.chapter_type == 2) {
                    $("#assign_instructions").html(obj.data.instructions+"<br/><a href='<?= base_url(); ?>uploads/upload_template/"+obj.data.upload_template+"'>"+obj.data.upload_template+"</a>");
                    $(".pdf-div").hide();
                    $(".assign-div").show();
                } else {
                    if(obj.data.pdf_file == 1) {
                        var pdf_file = obj.data.pdf_file;
                    } else {
                        var pdf_file = '<?= base_url(); ?>uploads/course_files/'+obj.data.pdf_file;
                    }
                    $(".assign-div").hide();
                    $(".pdf-div").show();
                    getpdf(pdf_file);
                }
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            // $("#loader").hide();
            console.log(errorMessage);
        },
    });
}


function getpdf(pdfurl) {
    const url = pdfurl;
    let pdfDoc = null,
      pageNum = 1,
      pageIsRendering = false,
      pageNumIsPending = null;

    const scale = 1.5,
      canvas = document.querySelector('#pdf-render'),
      ctx = canvas.getContext('2d');

    // Render the page
    const renderPage = num => {
      pageIsRendering = true;

      // Get page
      pdfDoc.getPage(num).then(page => {
        // Set scale
        const viewport = page.getViewport({ scale });
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        const renderCtx = {
          canvasContext: ctx,
          viewport
        };

        page.render(renderCtx).promise.then(() => {
          pageIsRendering = false;

          if (pageNumIsPending !== null) {
            renderPage(pageNumIsPending);
            pageNumIsPending = null;
          }
        });

        // Output current page
        document.querySelector('#page-num').textContent = num;
      });
    };

    // Check for pages rendering
    const queueRenderPage = num => {
      if (pageIsRendering) {
        pageNumIsPending = num;
      } else {
        renderPage(num);
      }
    };

    // Show Prev Page
    const showPrevPage = () => {
      if (pageNum <= 1) {
        return;
      }
      pageNum--;
      queueRenderPage(pageNum);
    };

    // Show Next Page
    const showNextPage = () => {
      if (pageNum >= pdfDoc.numPages) {
        return;
      }
      pageNum++;
      queueRenderPage(pageNum);
    };

    // Get Document
    pdfjsLib
      .getDocument(url)
      .promise.then(pdfDoc_ => {
        pdfDoc = pdfDoc_;

        document.querySelector('#page-count').textContent = pdfDoc.numPages;

        renderPage(pageNum);
      })
      .catch(err => {
        // Display error
        const div = document.createElement('div');
        div.className = 'error';
        div.appendChild(document.createTextNode(err.message));
        // var parentDiv = document.getElementsByClassName("card-body");
        // parentDiv.insertBefore(div, canvas.nextSibling);
        document.querySelector('body').insertBefore(div, canvas);
        // Remove top bar
        document.querySelector('.top-bar').style.display = 'none';
      });

    // Button Events
    document.querySelector('#prev-page').addEventListener('click', showPrevPage);
    document.querySelector('#next-page').addEventListener('click', showNextPage);

}
</script>
</body>
</html>