<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<style type="text/css">
    .top-bar {
      background: #333;
      color: #fff;
      padding: 1rem;
    }

    .btnnew {
      background: coral;
      color: #fff;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 0.7rem 2rem;
    }

    .btnnew:hover {
      opacity: 0.9;
    }

    .page-info {
      margin-left: 1rem;
    }

    .pdferror {
      background: orangered;
      color: #fff;
      padding: 1rem;
    }
</style>
    <div class="left-side-menu">
        <!-- LOGO -->
        <div class="logo-box">
            <a href="<?= base_url(); ?>onlinecourses" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/favicon.png"/>
                </span>
                <span class="logo-lg">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" width="90%"/>
                </span>
            </a>
        </div>

        <div class="h-100" data-simplebar>
            <?php $uri2 = $this->uri->segment(2); ?>
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul id="side-menu">
                    <a href="<?= base_url(); ?>onlinecourses/courses"><li class="menu-title"><h4 style="color: #3bafda!important;"><i class="mdi mdi-arrow-collapse-left"></i>  Back to courses</h4></li></a>
                    <a href="<?= base_url(); ?>onlinecourses/previewcoursecontent/<?= base64_encode($course_id); ?>"><button class="btn btn-info btn-sm" style="margin: 10px;"><i class="mdi mdi-eye"></i> Preview Courses</button></a>
                    <li class="menuitem-active">
                        <a href="#sidebarLayouts" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-line"></i>
                            <span> Course Contents </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse show" id="sidebarLayouts">
                            <ul class="nav-second-level" style="padding: 0px;">
                                <?php if($coursecontentlist) { 
                                    foreach($coursecontentlist['data'] as $contlist) { 
                                ?>
                                <a href="<?= base_url(); ?>onlinecourses/coursecontent/<?= base64_encode($contlist->course_id); ?>/<?= base64_encode($contlist->chapter_id); ?>">
                                <li  class="dd-item dd-handle" style="height: auto!important;cursor: pointer;">
                                    <i class="mdi mdi-arrow-left-circle"></i> <?= $contlist->title; ?> -- <?php if($contlist->chapter_type == 1) { echo 'Course PDF'; } else { echo "Assignment"; } ?>
                                </li>
                                </a>
                            <?php } } ?>
                            <?php if(count($coursecontentlist['data']) != 1 && count($coursecontentlist['data']) != 2) { ?>
                                <li style="bottom: 0px;position: fixed;">
                                    <button type="button" class="btn btn-info btn-lg waves-effect waves-light" style="width: 240px;" data-bs-toggle="modal" data-bs-target="#add-chapter-modal"><i class="mdi mdi-plus-circle me-1"></i> Add Course PDF</button>
                                </li>
                            <?php } else if(count($coursecontentlist['data']) != 2) { ?>
                                <li style="bottom: 0px;position: fixed;">
                                    <button type="button" class="btn btn-info btn-lg waves-effect waves-light" style="width: 240px;" data-bs-toggle="modal" data-bs-target="#add-assignment-modal"><i class="mdi mdi-plus-circle me-1"></i> Add Assignment</button>
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                    </li>
                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Upload Course PDF  --  <?= $getcourse['data']->title; ?></h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses/dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses/courses">Courses</a></li>
                                <li class="breadcrumb-item active">Upload Course PDF</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <?php if($getcoursecontent['success'] == 1 && $getcoursecontent['data']->chapter_type == 1) {
                                            if($coursecontentid == 0 && $subcontentid != '') {
                                                $sub_chapter_id = $subcontentid;
                                            } else {
                                                $sub_chapter_id = 0;
                                            }
                                         ?>
                                            <form method="post" name="updatecoursecontent" action="<?= base_url(); ?>onlinecourses/updatecoursecontent" enctype="multipart/form-data" autocomplete="off">
                                                <input type="hidden" name="course_id" id="course_id_add" value="<?= $getcourse['data']->course_id; ?>">
                                                <input type="hidden" name="chapter_id" id="chapter_id_add" value="<?= $getcoursecontent['data']->chapter_id; ?>">
                                                <input type="hidden" name="sub_chapter_id" id="sub_chapter_id_add" value="<?= $sub_chapter_id; ?>">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="mb-3">
                                                                <label class="form-label">Title <span class="text-danger">*</span></label>
                                                                <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title" value="<?= $getcoursecontent['data']->title; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="mb-3">
                                                                <label class="form-label">Link <span class="text-danger">*</span></label>
                                                                <input type="text" id="link" name="link" class="form-control" placeholder="Enter Link" value="<?php if($getcoursecontent['data']->pdf_type == 1) { echo $getcoursecontent['data']->pdf_file; } else { echo base_url()."uploads/course_files/".$getcoursecontent['data']->pdf_file; } ?>" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                                    <li class="next list-inline-item">
                                                        <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                                        <button type="submit" class="btn btn-success" id="edit-course-button">Save <i class="mdi mdi-arrow-right ms-1"></i></button>
                                                    </li>
                                                </ul>
                                            </form>
                                            <br/><br/>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <input type="hidden" name="pdfurl" id="pdfurl" value="<?php if($getcoursecontent['data']->pdf_type == 1) { echo $getcoursecontent['data']->pdf_file; } else { echo base_url()."uploads/course_files/".$getcoursecontent['data']->pdf_file; } ?>">
                                                        <div class="top-bar">
                                                          <button class="btnnew" id="prev-page">
                                                            <i class="fas fa-arrow-circle-left"></i> Prev Page
                                                          </button>
                                                          <button class="btnnew" id="next-page">
                                                            Next Page <i class="fas fa-arrow-circle-right"></i>
                                                          </button><br/>
                                                          <span class="page-info">
                                                            Page <span id="page-num"></span> of <span id="page-count"></span>
                                                          </span>
                                                        </div>

                                                        <canvas id="pdf-render" style="width: 100%!important;"></canvas>
                                                        <!-- <iframe src="<?= base_url(); ?>uploads/course_files/<?= $getcoursecontent['data']->pdf_file; ?>"
                                                            width="800px"
                                                            height="600px" /> -->
                                                        <!-- <embed src="<?= base_url(); ?>uploads/course_files/<?= $getcoursecontent['data']->pdf_file; ?>#toolbar=0&navpanes=0&scrollbar=0" type="application/pdf" width="100%" height="600px" /> -->
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else if($getcoursecontent['success'] == 1 && $getcoursecontent['data']->chapter_type == 2) {  
                                            if($coursecontentid == 0 && $subcontentid != '') {
                                                $sub_chapter_id = $subcontentid;
                                            } else {
                                                $sub_chapter_id = 0;
                                            }
                                            ?>
                                            <form method="post" name="updatecoursecontentassign" action="<?= base_url(); ?>onlinecourses/updatecoursecontentassign" enctype="multipart/form-data" autocomplete="off">
                                            <input type="hidden" name="course_id" id="course_id_my" value="<?= $getcourse['data']->course_id; ?>">
                                            <input type="hidden" name="chapter_id" id="chapter_id_my" value="<?= $getcoursecontent['data']->chapter_id; ?>">
                                            <input type="hidden" name="sub_chapter_id" id="sub_chapter_id_my" value="<?= $sub_chapter_id; ?>">
                                            <div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Title <span class="text-danger">*</span></label>
                                                            <input type="text" name="title" class="form-control" placeholder="Enter Title" value="<?= $getcoursecontent['data']->title; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Instructions <span class="text-danger">*</span> <div id="error_instructions"></div></label>
                                                            <textarea type="text" rows="2" class="form-control" name="instructions" id="instructions">Please download the assignment and answer all questions accordingly.</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label class="form-label">Upload Template <span class="text-muted">A template for your learners they can download and write answers</span> </label>
                                                        <input type="file" id="upload_template" name="upload_template" class="form-control" placeholder="Upload Template" accept="application/pdf" onchange="FilevalidationUpload()">
                                                        <input type="hidden" id="upload_template_edit" name="upload_template_edit" class="form-control" placeholder="Upload Template" value="<?= $getcoursecontent['data']->upload_template; ?>">
                                                        <label for="upload_template" id="upload_template_error" class="error" generated="true"></label>
                                                        <?php if($getcoursecontent['data']->upload_template != '') { ?>
                                                        <br/>
                                                        <a href="<?= base_url(); ?>uploads/upload_template/<?= $getcoursecontent['data']->upload_template; ?>" download target="_blank"><button type="button" class="btn btn-info">Download</button></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Confirmation Message <span class="text-danger">*</span><div id="error_confirmation_message"></div></label>
                                                            <textarea type="text" rows="2" class="form-control" name="confirmation_message" id="confirmation_message">Thank you for submitting the assignment.</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                                <li class="next list-inline-item">
                                                    <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                                    <button type="submit" class="btn btn-success" id="edit-assign-button">Save <i class="mdi mdi-arrow-right ms-1"></i></button>
                                                </li>
                                            </ul>
                                        </form>
                                        <?php } else { ?>
                                            <ul id="side-menu">
                                                <a href="<?= base_url(); ?>onlinecourses/courses"><li class="menu-title"><h4 style="color: #3bafda!important;"><i class="mdi mdi-arrow-collapse-left"></i>  Back to courses</h4></li></a>
                                                <a href="<?= base_url(); ?>onlinecourses/previewcoursecontent/<?= base64_encode($course_id); ?>"><button class="btn btn-info btn-sm" style="margin: 10px;"><i class="mdi mdi-eye"></i> Preview Courses</button></a>
                                                <li class="menuitem-active">
                                                    <a href="#sidebarLayouts" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts">
                                                        <i class="ri-layout-line"></i>
                                                        <span> Course Contents </span>
                                                        <span class="menu-arrow"></span>
                                                    </a>
                                                    <div class="collapse show" id="sidebarLayouts">
                                                        <ul class="nav-second-level" style="padding: 0px;">
                                                            <?php if($coursecontentlist) { 
                                                                foreach($coursecontentlist['data'] as $contlist) { 
                                                            ?>
                                                            <a href="<?= base_url(); ?>onlinecourses/coursecontent/<?= base64_encode($contlist->course_id); ?>/<?= base64_encode($contlist->chapter_id); ?>">
                                                            <li  class="dd-item dd-handle" style="height: auto!important;cursor: pointer;">
                                                                <i class="mdi mdi-arrow-left-circle"></i> <?= $contlist->title; ?> -- <?php if($contlist->chapter_type == 1) { echo 'Course PDF'; } else { echo "Assignment"; } ?>
                                                            </li>
                                                            </a>
                                                        <?php } } ?>
                                                        <?php if(count($coursecontentlist['data']) != 1 && count($coursecontentlist['data']) != 2) { ?>
                                                            <li style="bottom: 0px;position: fixed;">
                                                                <button type="button" class="btn btn-info btn-lg waves-effect waves-light" style="width: 240px;" data-bs-toggle="modal" data-bs-target="#add-chapter-modal"><i class="mdi mdi-plus-circle me-1"></i> Add Course PDF</button>
                                                            </li>
                                                        <?php } else if(count($coursecontentlist['data']) != 2) { ?>
                                                            <li style="bottom: 0px;position: fixed;">
                                                                <button type="button" class="btn btn-info btn-lg waves-effect waves-light" style="width: 240px;" data-bs-toggle="modal" data-bs-target="#add-assignment-modal"><i class="mdi mdi-plus-circle me-1"></i> Add Assignment</button>
                                                            </li>
                                                        <?php } ?>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
            
        </div> <!-- container -->

    </div> <!-- content -->


<div id="add-chapter-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="post" name="addchapterfile" action="<?= base_url(); ?>onlinecourses/addchapterfile" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="course_id" id="course_id_edit" value="<?= $getcourse['data']->course_id; ?>">
                    <div class="modal-header">
                        <h4 class="modal-title">Upload Course PDF</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row" style="display: none;">
                            <div class="col-md-12">
                                <div>
                                    <label class="form-label">Select Chapter Type <span class="text-danger">*</span></label><br/>
                                    <input class="form-check-input" type="radio" name="chapter_type" id="flexRadioDefault1" value="1" checked>
                                    <label class="form-check-label" for="flexRadioDefault1" style="font-weight: 300!important;">
                                        &nbsp;<b>PDF:</b> Add a PDF file in the course.&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input" type="radio" name="chapter_type" id="flexRadioDefault2" value="2">
                                    <label class="form-check-label" for="flexRadioDefault2" style="font-weight: 300!important;">
                                        &nbsp;<b>Assignment:</b> Use it to take or give assignments to your learners.
                                    </label>
                                </div><br/>
                                <label for="chapter_type" class="error" generated="true"></label>
                            </div>
                        </div>
                        <div class="row assign-div pdf-div-file">
                            <input class="form-check-input" type="hidden" name="pdf_type" id="flexRadioDefault3" value="2">
                            <div class="col-md-12">
                                <div class="mb-3 assign-div" style="display: none;">
                                    <label class="form-label">New Assignment <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
                                </div>
                                <div class="mb-3 pdf-file-div">
                                    <label class="form-label">Upload File <span class="text-danger">*</span> <span style="font-weight: 300!important;">Max Pdf Size :: 15MB</span></label>
                                    <input type="file" class="form-control" id="pdf_file" name="pdf_file" placeholder="Select Pdf File" accept="application/pdf" onchange="Filevalidation()" required="true">
                                     <label for="pdf_file" id="pdf_file_error" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light" style="display: none;">Upload</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light" id="submit-chapter">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="add-assignment-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="post" name="addassign" action="<?= base_url(); ?>onlinecourses/addchapterfile" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="course_id" id="course_id_edit" value="<?= $getcourse['data']->course_id; ?>">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Assignment</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row" style="display: none;">
                            <div class="col-md-12">
                                <div>
                                    <label class="form-label">Select Chapter Type <span class="text-danger">*</span></label><br/>
                                    <input class="form-check-input" type="radio" name="chapter_type" id="flexRadioDefault1" value="1">
                                    <label class="form-check-label" for="flexRadioDefault1" style="font-weight: 300!important;">
                                        &nbsp;<b>PDF:</b> Add a PDF file in the course.&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input" type="radio" name="chapter_type" id="flexRadioDefault2" value="2" checked>
                                    <label class="form-check-label" for="flexRadioDefault2" style="font-weight: 300!important;">
                                        &nbsp;<b>Assignment:</b> Use it to take or give assignments to your learners.
                                    </label>
                                </div><br/>
                                <label for="chapter_type" class="error" generated="true"></label>
                            </div>
                        </div>
                        <div class="row assign-div pdf-div-file">
                            <div class="col-md-12">
                                <div class="mb-3 assign-div">
                                    <label class="form-label">Assignment Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light" id="add-chapter-button">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>
<script type="text/javascript">
    $(function() {
        $("form[name='addchapterfile']").submit(function() {
            var fi = document.getElementById('pdf_file');
            if (fi.files.length > 0) {
                for (var i = 0; i <= fi.files.length - 1; i++) {
                    var fsize = fi.files.item(i).size;
                    var file = Math.round((fsize / 1024));
                    if (file > 15360) {
                        $("#pdf_file_error").html("File too Big, please select a file less than 15mb");
                        setTimeout(function() {
                            $("#pdf_file_error").html("");
                        }, 3000);
                        return false;
                    } 
                }
            } 
            $("#submit-chapter").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#submit-chapter").attr('disabled',true);
        });
        // $("form[name='addchapterfile']").validate({
        //     debug: true,
        //     rules: {
        //         pdf_file: "required"
        //     },
        //     messages: {
        //         pdf_file: "Please select pdf file"
        //     },  
        //     submitHandler: function(form) {
        //         $("#submit-chapter").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
        //         $("#submit-chapter").attr('disabled',true);
        //         form.submit();
        //     }
        // });
        $("form[name='addassign']").validate({
            ignore: [],
            debug: false,
            rules: {
                title: "required"
            },
            messages: {
                title: "Please enter assignment title"
            },  
            submitHandler: function(form) {
                $("#add-chapter-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
                $("#add-chapter-button").attr('disabled',true);
              form.submit();
            }
        });
        $("form[name='updatecoursecontent']").validate({
            ignore: [],
            debug: false,
            rules: {
                title: "required"
            },
            messages: {
                title: "Please enter title"
            },  
            submitHandler: function(form) {
                $("#edit-course-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
                $("#edit-course-button").attr('disabled',true);
                form.submit();
            }
        });
        $("form[name='updatecoursecontentassign']").validate({
            ignore: [],
            debug: false,
            rules: {
                title: "required",
                instructions: {
                    required: function() 
                    {
                     CKEDITOR.instances.instructions.updateElement();
                    }
                },
                confirmation_message: {
                    required: function() 
                    {
                     CKEDITOR.instances.confirmation_message.updateElement();
                    }
                }
            },
            messages: {
                title: "Please enter title",
                instructions: "Please enter instructions",
                confirmation_message: "Please enter confirmation message",
            }
        });
        $("form[name='updatecoursecontentassign']").submit(function() {
            var fi = document.getElementById('upload_template');
            if (fi.files.length > 0) {
                for (var i = 0; i <= fi.files.length - 1; i++) {
                    var fsize = fi.files.item(i).size;
                    var file = Math.round((fsize / 1024));
                    if (file > 15360) {
                        $("#upload_template_error").fadeIn().html("File too Big, please select a file less than 15mb");
                        setTimeout(function() {
                            $("#upload_template_error").html("");
                        }, 3000);
                        return false;
                    } 
                }
            } 
            $("#edit-assign-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#edit-assign-button").attr('disabled',true);
        });
    });
    $("input[name=chapter_type]:radio").change(function () {
        if(this.value == 1) {
            $(".pdf-div").show();
            $("input[name=pdf_type]").attr('required', true);
            $("#title").val('');
            $(".assign-div").hide();
            $(".pdf-div-file").hide();
            $(".pdf-file-div").hide();
            $(".pdf-div-url").hide();
            $("#title").attr('required', false);
            $("input[name=pdf_type]").prop('checked', false);
        } else {
            $(".pdf-div").hide();
            $("input[name=pdf_type]").attr('required', false);
            $(".assign-div").show();
            $("#title").attr('required', true);
            $(".pdf-div-file").show();
            $(".pdf-file-div").hide();
            $(".pdf-div-url").hide();
            $("input[name=pdf_type]").prop('checked', false);
        }
    });
    $("input[name=pdf_type]:radio").change(function () {
        if(this.value == 2) {
            $(".pdf-div-file").show();
            $(".pdf-div-url").hide();
            $("#pdf_file").attr('required', true);
            $(".pdf-file-div").show();
            $("#pdf_file_url").attr('required', false);
            $("#pdf_file_url").val('');
        } else {
            $(".pdf-div-file").show();
            $(".pdf-file-div").hide();
            $("#pdf_file").attr('required', false);
            $(".pdf-div-url").show();
            $("#pdf_file_url").attr('required', true);
            $("#pdf_file").val('');
        }
    });

    function subchaptermodal(chapter_id) {
        $("#chapter_id_edit").val(chapter_id);
        $("#add-sub-chapter-modal").modal("show");
    }

    Filevalidation = () => {
        var fi = document.getElementById('pdf_file');
        // Check if any file is selected.
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
  
                var fsize = fi.files.item(i).size;
                var file = Math.round((fsize / 1024));
                // The size of the file.
                if (file > 15360) {
                    // alert("File too Big, please select a file less than 4mb");
                    $("#pdf_file_error").html("File too Big, please select a file less than 15mb");
                    setTimeout(function() {
                        $("#pdf_file_error").html("");
                    }, 3000);
                    return false;
                } 
                // else if (file < 2048) {
                //     $("#pdf_file_error").html("File too small, please select a file greater than 2mb");
                //     setTimeout(function() {
                //      $("#pdf_file_error").html("");
                //     }, 3000);
                // } 
                // else {
                //     document.getElementById('size').innerHTML = '<b>'
                //     + file + '</b> KB';
                // }
            }
        }
    }

    var instructions = $("#instructions").val();
    var confirmation_message = $("#confirmation_message").val();
    if(instructions || confirmation_message) {
        /* CK Editors */
        CKEDITOR.replace('instructions');
        CKEDITOR.replace('confirmation_message');
        CKEDITOR.instances.instructions.on('change', function() {    
            if(CKEDITOR.instances.instructions.getData().length >  0) {
              $('label[for="instructions"]').hide();
            }
        });
        CKEDITOR.instances.confirmation_message.on('change', function() {    
            if(CKEDITOR.instances.confirmation_message.getData().length >  0) {
              $('label[for="confirmation_message"]').hide();
            }
        });
    }

    FilevalidationUpload = () => {
        var fi = document.getElementById('upload_template');
        // Check if any file is selected.
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
  
                var fsize = fi.files.item(i).size;
                var file = Math.round((fsize / 1024));
                // The size of the file.
                if (file > 15360) {
                    $("#upload_template_error").html("File too Big, please select a file less than 15mb");
                    setTimeout(function() {
                        $("#upload_template_error").html("");
                    }, 3000);
                    return false;
                } 
            }
        }
    }

    $(document).ready(function() {
        // var availability_setting =  $("input[name='availability_setting']:checked").val();
        // if(availability_setting == 2) {
        //     $(".availability_setting_div").show();
        //     $("input[name=available_from]").attr('required', true);
        //     $("input[name=available_till]").attr('required', true);
        // } else {
        //  $(".availability_setting_div").hide();
        //     $("input[name=available_from]").attr('required', false);
        //     $("input[name=available_till]").attr('required', false);
        //     $("input[name=available_from]").val('');
        //     $("input[name=available_till]").val('');
        // }

        var pdfurl = $("#pdfurl").val();
        if(pdfurl != "") {
            getpdf(pdfurl);
        }
    });

    function getpdf(pdfurl) {
        const url = pdfurl;
        let pdfDoc = null,
          pageNum = 1,
          pageIsRendering = false,
          pageNumIsPending = null;

        const scale = 1.5,
          canvas = document.querySelector('#pdf-render'),
          ctx = canvas.getContext('2d');

        // Render the page
        const renderPage = num => {
          pageIsRendering = true;

          // Get page
          pdfDoc.getPage(num).then(page => {
            // Set scale
            const viewport = page.getViewport({ scale });
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            const renderCtx = {
              canvasContext: ctx,
              viewport
            };

            page.render(renderCtx).promise.then(() => {
              pageIsRendering = false;

              if (pageNumIsPending !== null) {
                renderPage(pageNumIsPending);
                pageNumIsPending = null;
              }
            });

            // Output current page
            document.querySelector('#page-num').textContent = num;
          });
        };

        // Check for pages rendering
        const queueRenderPage = num => {
          if (pageIsRendering) {
            pageNumIsPending = num;
          } else {
            renderPage(num);
          }
        };

        // Show Prev Page
        const showPrevPage = () => {
          if (pageNum <= 1) {
            return;
          }
          pageNum--;
          queueRenderPage(pageNum);
        };

        // Show Next Page
        const showNextPage = () => {
          if (pageNum >= pdfDoc.numPages) {
            return;
          }
          pageNum++;
          queueRenderPage(pageNum);
        };

        // Get Document
        pdfjsLib
          .getDocument(url)
          .promise.then(pdfDoc_ => {
            pdfDoc = pdfDoc_;

            document.querySelector('#page-count').textContent = pdfDoc.numPages;

            renderPage(pageNum);
          })
          .catch(err => {
            // Display error
            const div = document.createElement('div');
            div.className = 'pdferror';
            div.appendChild(document.createTextNode(err.message));

            document.querySelector('body').insertBefore(div, canvas);
            // Remove top bar
            document.querySelector('.top-bar').style.display = 'none';
          });

        // Button Events
        document.querySelector('#prev-page').addEventListener('click', showPrevPage);
        document.querySelector('#next-page').addEventListener('click', showNextPage);

    }
</script>
</body>
</html>