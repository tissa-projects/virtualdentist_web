<style type="text/css">
	p{
		color: #50596c!important;text-align: justify;
	}
</style>
<?php 
  $cart_list = "";
  if(isset($_SESSION['Local_SESS'])) {
    $cart_list = json_decode(json_encode($_SESSION['Local_SESS']));
  } else {
    $api = $this->session->userdata('url');
    $method = 'POST';
      $url = $api.'getcartlist';
      $header = array(
         'Content-Type: application/json'
      );
      $cart_id = 0;
      if(isset($_SESSION['VIR_SESSION'])) {
        $cart_id = $_SESSION['VIR_SESSION']['cart_id'];
      }
      $apidata = array("cart_id" => $cart_id);
      $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));
      $getdata = json_decode($details);
    if($getdata) {
	    if($getdata->success == 1) {
	      $cart_list = $getdata->data;
	      $_SESSION['VIR_SESSION']['cart_sum'] = $getdata->cart_sum;
	    }
    }
  }
?>
<?php
function star_rating($rating)
{
    $rating_round = round($rating * 2) / 2;
    if ($rating_round <= 0.5 && $rating_round > 0) {
        return '<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 1 && $rating_round > 0.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 1.5 && $rating_round > 1) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 2 && $rating_round > 1.5) {
        return '<i class="fa fa-star  text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 2.5 && $rating_round > 2) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;&nbsp;<i class="fa fa-star  text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 3 && $rating_round > 2.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 3.5 && $rating_round > 3) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 4 && $rating_round > 3.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-o"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 4.5 && $rating_round > 4) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star-half text-warning"></i> ('.$rating.' rating)';
    }
    if ($rating_round <= 5 && $rating_round > 4.5) {
        return '<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i>&nbsp;<i class="fa fa-star text-warning"></i> ('.$rating.' rating)';
    }
}
?>
<div class="page-content" style="margin-top: 20px;"><!-- Section Banner -->
	<div class="super_container">
	    <div class="single_product">
	        <div class="container-fluid" style=" background-color: #fff; padding: 11px;">

	            <div class="row">
	                <div class="col-lg-5 order-lg-2 order-1">
	                    <div class="image_selected">
	                    	<img src="<?= base_url('uploads/course_cover/'.$getcourse->data->course_cover); ?>" alt="course cover" style="max-height: 20rem;height: auto;">
	                    	<input type="hidden" name="course_cover" id="course_cover" value="<?= $getcourse->data->course_cover; ?>">
	                    </div>
	                </div>
	                <div class="col-lg-7 order-3">
	                    <div class="product_description">
	                        <nav>
	                            <ol class="breadcrumb">
	                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
	                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses">Online Courses</a></li>
	                                <li class="breadcrumb-item active">Details</li>
	                            </ol>
	                        </nav>
	                        <div class="product_name"><h3><b><?= $getcourse->data->title; ?></b></h3>
	                        	<input type="hidden" name="title" id="title" value="<?= $getcourse->data->title; ?>">
	                        	<input type="hidden" name="course_id" id="course_id" value="<?= $getcourse->data->course_id; ?>">
	                        </div>
	                        <p><?= $getcourse->data->course_tagline; ?></p>
	                        <!-- <p><?= $getcourse->data->tags; ?></p> -->
	                        <div class="product-rating">
	                        	<?php echo star_rating($getcourse->data->rating); ?>
	                        </div>
	                        <div> 
	                        	<span class="product_info">Instructor: <?= $getcourse->data->instructor_display_name; ?><span><br> 
	                        	<span class="product_info">Language: <?= $getcourse->data->language; ?><span><br> 
	                        	<span class="product_info">Validity Period: <?php if($getcourse->data->plans) { if($getcourse->data->plans->access_days) { echo $getcourse->data->plans->access_days.' days'; } } ?><span>
	                        		<input type="hidden" name="course_access" id="course_access" value="<?php if($getcourse->data->plans) { if($getcourse->data->plans->access_days) { echo $getcourse->data->plans->access_days.' days'; } } ?>">
	                        </div>
	                        <div> 
	                        	<span class="formatRating" style="text-decoration: line-through;"><?php if($getcourse->data->plans) { if($getcourse->data->plans->final_payable_price != $getcourse->data->plans->list_price) { 
	                        		$subprice = $getcourse->data->plans->list_price - $getcourse->data->plans->final_payable_price;
	                        		$decimalprice = $subprice / $getcourse->data->plans->list_price;
	                        		echo "₹".$getcourse->data->plans->list_price. '</span> '.number_format($decimalprice * 100, 2).' % OFF'; } } ?><br/>
	                        	<span class="product_price"><h2 class="text-primary curr">
	                        		<?php if($getcourse->data->plans) { if($getcourse->data->plans->is_free == 0) { echo "FREE"; } else { echo "₹".$getcourse->data->plans->final_payable_price;} } else { echo "FREE";} ?>  
	                        		<small class="text-dark"><?php if($getcourse->data->plans) { if($getcourse->data->plans->is_include_tax_in_price == 1 && $getcourse->data->plans->tax_rate != 0) { echo "including ".$getcourse->data->plans->tax_rate."% GST"; } elseif($getcourse->data->plans->is_include_tax_in_price == 0 && $getcourse->data->plans->tax_rate != 0 && $getcourse->data->plans->is_free != 0) { echo "not including ".$getcourse->data->plans->tax_rate."% GST"; } } ?>
	                        		</small></h2></span>
	                        		<input type="hidden" name="price" id="price" value="<?php if($getcourse->data->plans) { if($getcourse->data->plans->is_free == 0) { echo "0"; } else { echo $getcourse->data->plans->final_payable_price;} } else{  echo "0";  } ?>">
	                        	</div>
	                        <div class="col-xs-12" id="addtocartbutton">
	                        	<?php
	                        	$cart_items_arr = array();
	                        	if($cart_list) {
                        			foreach($cart_list as $row) { 
                        				array_push($cart_items_arr, $row->ci);
                        			} 
                        		} ?>
                        		<?php if(in_array($getcourse->data->course_id, $cart_items_arr)) { ?>
	                         		<button type="button" class="btn btn-primary shop-button" onclick="return shopnow();">Already in Cart</button>
	                         	<?php } else { ?>
	                         		<button type="button" class="btn btn-primary shop-button" onclick="return shopnow();">Add to Cart</button>
	                         	<?php } ?>
                            </div>
	                        <hr class="singleline">
	                    </div>
	                </div>
	            </div>
                    <div class="row">
	                    <div class="col-md-12">
	                        <nav>
	                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
	                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>
	                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Courses Content</a>
	                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">How to Use</a>
	                            </div>
	                        </nav>
	                        <div class="tab-content" id="nav-tabContent">
	                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	                            	<p class="mt-4"><?= $getcourse->data->description; ?></p>
	                            </div>
	                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
									<p class="mt-4"><?= $getcourse->data->course_tagline; ?></p>										
	                            </div>
	                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
									<p class="mt-4"><?= $getcourse->data->how_to_use; ?></p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	        </div>
	    </div>
	</div>		
</div>
<?php $this->load->view('onlinecourses/footer'); ?>
<script type="text/javascript">
	function shopnow() {
		var course_id = $("#course_id").val();
		var course_cover = $("#course_cover").val();
		var title = $("#title").val();
		var price = $("#price").val();
		var course_access = $("#course_access").val();
		var site_url = $("#site_url").val();
		var user = $("#user").val();
		if(!user) {
			$.ajax({
		        url:site_url+"onlinecourses/setcartlist",
		        type:"POST",
		        catch:false,
		        data: {ci:course_id,course_cover:course_cover,title:title,price:price,course_access:course_access},
		        success:function(data,status,xhr) {
		        	// alert(data);
		        	// console.log(data);return false;
		        	var obj = JSON.parse(data);
		        	if(obj.success == 1) {
		        		$( "#addtocartbutton" ).load(window.location.href + " #addtocartbutton" );
		        		$("#cart_list").html(obj.cart_list_html);
		        		$("#cart_count").html('<i data-count="'+obj.cart_count+'" class="fa fa-shopping-cart fa-x badge"></i>');
						$("#cartModal").modal('show');
		        	}
		        },
		        error: function (jqXhr, textStatus, errorMessage) {
		        	console.log(errorMessage)
		        },
		    });
		} else {
			$.ajax({
		        url:site_url+"onlinecourses/addcartlist",
		        type:"POST",
		        catch:false,
		        data: {ci:course_id,course_cover:course_cover,title:title,price:price,course_access:course_access},
		        success:function(data,status,xhr) {
		        	// alert(data);
		        	// console.log(data);return false;
		        	var obj = JSON.parse(data);
		        	if(obj.success == 1) {
		        		$( "#addtocartbutton" ).load(window.location.href + " #addtocartbutton" );
		        		$("#cart_list").html(obj.cart_list_html);
		        		$("#cart_count").html('<i data-count="'+obj.cart_count+'" class="fa fa-shopping-cart fa-x badge"></i>');
						$("#cartModal").modal('show');
		        	}
		        },
		        error: function (jqXhr, textStatus, errorMessage) {
		        	console.log(errorMessage)
		        },
		    });
		}
	}
	// $(".shop-button").click(function() {
	// 	var course_id = $("#course_id").val();
	// 	var course_cover = $("#course_cover").val();
	// 	var title = $("#title").val();
	// 	var price = $("#price").val();
	// 	var course_access = $("#course_access").val();
	// 	var site_url = $("#site_url").val();
	// 	var user = $("#user").val();
	// 	if(!user) {
	// 		$.ajax({
	// 	        url:site_url+"onlinecourses/setcartlist",
	// 	        type:"POST",
	// 	        catch:false,
	// 	        data: {ci:course_id,course_cover:course_cover,title:title,price:price,course_access:course_access},
	// 	        success:function(data,status,xhr) {
	// 	        	// alert(data);
	// 	        	// console.log(data);return false;
	// 	        	var obj = JSON.parse(data);
	// 	        	if(obj.success == 1) {
	// 	        		$( "#addtocartbutton" ).load(window.location.href + " #addtocartbutton" );
	// 	        		$("#cart_list").html(obj.cart_list_html);
	// 	        		$("#cart_count").html('<i data-count="'+obj.cart_count+'" class="fa fa-shopping-cart fa-x badge"></i>');
	// 					$("#cartModal").modal('show');
	// 	        	}
	// 	        },
	// 	        error: function (jqXhr, textStatus, errorMessage) {
	// 	        	console.log(errorMessage)
	// 	        },
	// 	    });
	// 	} else {
	// 		$.ajax({
	// 	        url:site_url+"onlinecourses/addcartlist",
	// 	        type:"POST",
	// 	        catch:false,
	// 	        data: {ci:course_id,course_cover:course_cover,title:title,price:price,course_access:course_access},
	// 	        success:function(data,status,xhr) {
	// 	        	// alert(data);
	// 	        	// console.log(data);return false;
	// 	        	var obj = JSON.parse(data);
	// 	        	if(obj.success == 1) {
	// 	        		$( "#addtocartbutton" ).load(window.location.href + " #addtocartbutton" );
	// 	        		$("#cart_list").html(obj.cart_list_html);
	// 	        		$("#cart_count").html('<i data-count="'+obj.cart_count+'" class="fa fa-shopping-cart fa-x badge"></i>');
	// 					$("#cartModal").modal('show');
	// 	        	}
	// 	        },
	// 	        error: function (jqXhr, textStatus, errorMessage) {
	// 	        	console.log(errorMessage)
	// 	        },
	// 	    });
	// 	}
	// });
</script>