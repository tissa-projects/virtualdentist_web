<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
            
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Welcome !</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboards</a></li>
                                <li class="breadcrumb-item active">Overview</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title --> 

            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#1abc9c"
                                        data-bgColor="#d1f2eb" value="<?= $totalsale; ?>"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-end">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $totalsale; ?></span> </h3>
                                    <p class="text-muted mb-0"><a href="<?= base_url(); ?>onlinecourses/sales">Total Sales</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#3bafda"
                                        data-bgColor="#d8eff8" value="<?= $newsignups; ?>"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-end">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $newsignups; ?></span> </h3>
                                    <p class="text-muted mb-0"><a href="<?= base_url(); ?>onlinecourses/learners">This Month New Signups Learners</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->

                <!-- <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#f672a7"
                                        data-bgColor="#fde3ed" value="0"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-end">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup">0</span> </h3>
                                    <p class="text-muted mb-0">Enrollments</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
            <!-- end row -->
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
</body>
</html>