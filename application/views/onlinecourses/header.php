<!DOCTYPE html>

<html lang="en">

<head>

	<title>Online Courses | Virtual Dentist</title>

	<meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="Description" content="Virtual Dentist provides the best dental care for any dental problems and provide the best treatment for any teeth issues and problems like teeth filling, tooth denture etc.">

	<meta name="Keywords" content="best dental care, best dental clinic, common dental problems, dental issues, dental problems and treatment, dental services, dental treatment, dentist near me, dentist specialist, denture dentist, kids teeth falling out, teeth issues, teeth problems, teeth treatment near me, tooth denture, tooth filling near me, treatment of teeth">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3pro.css">

	<meta name="google-site-verification" content="l1eW724-OqZYWyVY-DUdGjSq377ucgGVitiOC2NuqK0" />

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-N9EBYSW8NR"></script>

	<script>

	 window.dataLayer = window.dataLayer || [];

	 function gtag(){dataLayer.push(arguments);}

	 gtag('js', new Date());



	 gtag('config', 'G-N9EBYSW8NR');

	</script>



	<link href="<?php echo base_url();?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />

	<link href="<?php echo base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo base_url();?>assets/css/templete.css" rel="stylesheet" type="text/css" />

	<link class="skin" href="<?php echo base_url();?>assets/css/skin/skin-1.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link href="<?php echo base_url();?>assets/css/lightbox.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/lightbox.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/onlinecourse.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />

	<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=4ca2b977-8d21-4ef5-a994-5ac1756555fa"> </script>

</head>

<?php 

$page = $this->uri->segment(2);

$page1 = $this->uri->segment(1);

$cart_list = "";

if(isset($_SESSION['Local_SESS'])) {

	$cart_list = json_decode(json_encode($_SESSION['Local_SESS']));

} else {

	$api = $this->session->userdata('url');

	$method = 'POST';

    $url = $api.'getcartlist';

    $header = array(

       'Content-Type: application/json'

    );

    $cart_id = 0;

    if(isset($_SESSION['VIR_SESSION'])) {

    	$cart_id = $_SESSION['VIR_SESSION']['cart_id'];

    }

    $apidata = array("cart_id" => $cart_id);

    $details = $this->onlinecourses_model->CallAPI($method, $url, $header, json_encode($apidata));

    $getdata = json_decode($details);

	if($getdata)  {

	if($getdata->success == 1)  {

	  $cart_list = $getdata->data;

	  $_SESSION['VIR_SESSION']['cart_sum'] = $getdata->cart_sum;

	}

	}

}

?>

<body id="bg" style="overflow-x: hidden!important;padding-right: 0px!important;"><!--<div id="loading-area"></div>-->

	<div class="page-wraper"><!-- header -->

		<header class="site-header mo-left header fullwidth">

			<div class="sticky-header main-bar-wraper navbar-expand-lg" id="sticky-header-id" style="padding-right: 0px!important;">

				<div class="main-bar clearfix ">

					<div class="container clearfix" style="max-width:1480px"><!-- website logo -->

						<div class="logo-header mostion" style="max-width: 250px !important;">

							<a href="<?php echo base_url();?>home"><img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" /></a>

						</div>

						



						<button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler collapsed navicon justify-content-end" data-target="#navbarNavDropdown" data-toggle="collapse" data-dismiss="modal" type="button" >

							<!--<span class="icon-bar"></span>-->

		    				<span class="icon-bar"></span>

		    				<span class="icon-bar"></span>

		    				<span class="icon-bar"></span>

		    			</button><!-- extra nav --><!-- Quik search -->



						<div class="dez-quik-search bg-primary">

							<form action="#">

								<input class="form-control" name="search" placeholder="Type to search" type="text" value="" />

							</form>

						</div>

						<!-- main nav -->

 

						<ul class="navbar-nav" style="float: right;padding-top: 20px;display: -webkit-inline-box;cursor: pointer;">

							<?php if($page != 'login' && $page != 'register' && $page != 'forgotpassword' && $page != 'forgot' && $page1 != 'termsofuse' && $page1 != 'privacypolicy'){ ?>

								<li style="font-size: 40px;top: -8px;color: #2e55fa;margin-top: -7px !important;" data-toggle="modal" data-target="#cartModal" id="cart_count">

									<i data-count="<?php if($cart_list) { echo count($cart_list); } else { echo 0; } ?>" class="fa fa-shopping-cart fa-x badge"></i>

								</li>

							<?php } ?>

							<?php /*if(isset($_SESSION['VIR_SESSION']) && $_SESSION['VIR_SESSION']['user_id'] != 0) { ?>

					        <li class="nav-item dropdown">

						        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0px!important;">

						        	<span id="firstName" style="display: none;"><?php if(isset($_SESSION['VIR_SESSION']['name'])) { echo ucfirst($_SESSION['VIR_SESSION']['name']); } ?></span>

									<div id="profileImage"></div>

						          <!-- <img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg" width="40" height="40" class="rounded-circle">-->

						          <?php if(isset($_SESSION['VIR_SESSION']['name'])) { echo ucfirst($_SESSION['VIR_SESSION']['name']); } ?> 

						        </a>

						        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="transform: translate3d(-82px, 46px, 0px)!important;">

						          <div class="dropdown-item" href="<?php echo base_url();?>onlinecourses/profile">Profile</div>

						          <a href="<?php echo base_url();?>onlinecourses/logout"><div class="dropdown-item">Log Out</div></a>

						        </div>

						    </li>  

						    <?php }*/ ?> 

					    </ul>

						<div class="header-nav navbar-collapse collapse justify-content-start" id="navbarNavDropdown" style="padding-left:0px; float: right;">

							<ul class="nav navbar-nav">

								<li class="<?php if(isset($page) && $page=='home'){ echo 'active';} ?>"><a href="<?php echo base_url();?>home">Home </a></li>

								<li><a href="<?php echo base_url();?>blog/">Blog</a></li>

								<li class="<?php if(($page=='' || $page=='coursedetail') && ($page1 != 'termsofuse' && $page1 != 'privacypolicy')){ echo 'active';} ?>"><a href="<?php echo base_url();?>onlinecourses">Courses</a></li>

								<?php if(isset($_SESSION['VIR_SESSION']) && $_SESSION['VIR_SESSION']['user_id'] != 0 && $_SESSION['VIR_SESSION']['user_role'] == 'Admin') { ?>

								<li><a href="<?php echo base_url();?>onlinecourses/dashboard/">Dashboard</a></li>

							    <?php } else if(isset($_SESSION['VIR_SESSION']) && $_SESSION['VIR_SESSION']['user_id'] != 0 && ($_SESSION['VIR_SESSION']['user_role'] == 'Learner' || $_SESSION['VIR_SESSION']['user_role'] == 'Instructor')) { ?> 

									<li><a href="<?php echo base_url();?>onlinecourses/mycourses/">My Courses</a></li>

								<?php } ?>

								<!-- <li>

									<i data-count="3" class="fa fa-shopping-cart fa-5x badge"></i>

								</li> -->

								<?php if(empty($_SESSION['VIR_SESSION'])) { ?>

								<li class="<?php if(isset($page) && ($page=='login' || $page=='register')){ echo 'active';} ?>"><a href="<?= base_url(); ?>onlinecourses/login">Login</a></li>

								<?php } ?> 

							</ul>

						</div> 

					</div>

			</div>

			</div>

			<!-- main header END -->

		</header>

		<!-- header END --><!-- Content -->



	<div class="content1">  