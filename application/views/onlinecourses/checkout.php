<div class="page-content" style="margin-top: 5px;padding-bottom: 0px;"><!-- Section Banner -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-10 col-md-10 col-lg-12 col-xl-10 text-center p-0 mb-2">
            <div class="card px-0 pt-4 pb-0 mb-3">
                <!-- <h2 id="heading">Sign Up Your User Account</h2>
                <p>Fill all form field to go to next step</p> -->
                <form id="msform">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="account"><strong>Order Details</strong></li>
                        <li id="personal"><strong>Billing Details</strong></li>
                        <li id="payment"><strong>Make Payment</strong></li>
                    </ul>
                    <!-- <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> --> <br> <!-- fieldsets -->
                    <fieldset>
                        <div class="form-card">
                            <div class="row" id="checkout_cart_list">
                                <table class="table table-image">
                                  <thead>
                                    <tr>
                                      <th scope="col"></th>
                                      <th scope="col">Product</th>
                                      <th scope="col">Price</th>
                                      <th scope="col">Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($cartitemlist->success == 1) {
                                        foreach ($cartitemlist->data as $row) { ?>
                                    <tr>
                                      <td class="w-25">
                                        <img src="<?= base_url(); ?>uploads/course_cover/<?= $row->course_cover;?>" alt="course cover" style="height: 84px;width: 100%">
                                      </td>
                                      <td>
                                        <?= $row->title;?>
                                        <?php if($row->course_access) { ?>
                                        <div style="color: #0006;">Course Access | Till <?= $row->course_access;?> </div>
                                        <?php } ?>
                                      </td>
                                      <td><?php if($row->price == 0) { echo "FREE"; } else { echo "₹".$row->price; } ?></td>
                                      <td>
                                        <a href="#" class="btn btn-danger btn-sm" onclick="return deletecartitem(<?= $row->ci;?>,'checkout');">
                                          <i class="fa fa-times"></i>
                                        </a>
                                      </td>
                                    </tr>
                                    <?php } } ?>
                                  </tbody>
                                </table> 
                            </div> 
                        </div> 
                        <input type="button" name="next" class="next action-button" value="Next" id="first_button"  />
                    </fieldset>
                    <fieldset>
                            <div class="form-card" id="bill_div">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="fs-title">Billing Details:</h2>
                                        <p>All <span class="text-danger">*</span> marked fields are required.</p>
                                    </div>
                                    <br/>
                                </div> 
                                <label class="fieldlabels">Name: <span class="text-danger">*</span></label> 
                                <input type="text" name="name" placeholder="Name" id="name" style="margin-bottom: 0px;" autocomplete="off" value="<?php if(isset($billdata->data->name)) { echo $billdata->data->name; } ?>" /> 
                                <label for="name" id="name_error" class="text-danger" generated="true"></label> <br/>
                                <label class="fieldlabels">Mobile: <span class="text-danger">*</span></label> 
                                <input type="text" name="mobile" placeholder="Mobile" id="mobile" style="margin-bottom: 0px;" autocomplete="off"  onkeypress="return isNum(event)"  value="<?php if(isset($billdata->data->mobile)) { echo $billdata->data->mobile; } ?>"/>
                                <label for="mobile" id="mobile_error" class="text-danger" generated="true"></label>
                            </div> 
                            <input type="button" name="next" class="action-button" value="Next" id="bill_button" /> 
                            <input type="hidden" name="next" id="bill_next" class="action-button next" value="Next" /> 
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-12">
                                    <p>Order Details will be sent to your email address <b><?php if(isset($_SESSION['VIR_SESSION']['email'])) { echo $_SESSION['VIR_SESSION']['email']; } ?></b></p>
                                </div>
                            </div> 
                            <br/>
                            <table class="table">
                                <tbody>
                                    <!-- <tr class="offerAmount" style="display: none;">
                                        <td>Total Amount</td>
                                        <td class="text-right totalAmount"></td>
                                    </tr>
                                    <tr class="offerAmount" style="display: none;">
                                        <td>Offer Discount</td>
                                        <td class="text-right offerDiscount"></td>
                                    </tr> -->
                                    <tr class="bg-secondary">
                                        <td>Payable Amount</td>
                                        <td class="text-right payableAmount">₹<?php if(isset($_SESSION['VIR_SESSION']['cart_sum'])) { echo number_format($_SESSION['VIR_SESSION']['cart_sum'],2); } ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="text-center m-2 p-2" id="pay-button">
                                <?php if(isset($_SESSION['VIR_SESSION']['cart_sum']) && $_SESSION['VIR_SESSION']['cart_sum'] != 0) {  ?>
                                    <a class="btn btn-lg btn-success goToPaymentPageBtn" style="max-width: 300px; width: 90%;" href="<?= base_url(); ?>onlinecourses/pay_now/<?= base64_encode($_SESSION['VIR_SESSION']['cart_sum'])?>/<?= base64_encode($_SESSION['VIR_SESSION']['email'])?>/<?php if(isset($billdata->data->name)) { echo base64_encode($billdata->data->name); } ?>/<?php if(isset($billdata->data->mobile)) { echo base64_encode($billdata->data->mobile); } ?>/<?= base64_encode($_SESSION['VIR_SESSION']['cart_id'])?>">Proceed to Payment</a>
                                <?php } else { ?>
                                    <a class="btn btn-lg btn-success" style="max-width: 300px; width: 90%;" href="<?= base_url(); ?>onlinecourses/payment/<?= base64_encode($_SESSION['VIR_SESSION']['cart_sum'])?>/<?= base64_encode($_SESSION['VIR_SESSION']['email'])?>/<?php if(isset($billdata->data->name)) { echo base64_encode($billdata->data->name); } ?>/<?php if(isset($billdata->data->mobile)) { echo base64_encode($billdata->data->mobile); } ?>/<?= base64_encode($_SESSION['VIR_SESSION']['cart_id'])?>">Proceed</a>
                                <?php } ?>
                            </div>
                            <br/>
                            <div class="text-center m-2">
                                On successful purchase, the course(s) will be available instantly in My Courses. If it doesn't, please write an email to
                                <b><?php if(isset($_SESSION['VIR_SESSION']['adminemail'])) { echo $_SESSION['VIR_SESSION']['adminemail']; } ?></b>
                            </div>
                        </div> 
                        <input type="hidden" name="next" class="next action-button" value="Next"  id="nextpaymentbutton" /> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>		
</div>