<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Purchase History</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Purchase History</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-10">
                                    <h4 class="header-title">Purchase History</h4>
                                </div>
                            </div>

                            <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Date</th>
                                        <th>Order Id / Transaction Id</th>
                                        <th>No. Items</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($purchaselist->success == 1) { 
                                        $i = 1;
                                        foreach($purchaselist->data as $row) { ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?php if(isset($row->payments)) { echo date("d/m/Y h:i A", strtotime($row->payments->created_at)); } else { echo "NA"; } ?></td>
                                        <td><?php if(isset($row->payments)) { 
                                            if(isset($row->order_id)) { echo "#VIR-ORD-".str_pad($row->order_id, 9, 0, STR_PAD_LEFT);
                                            } 
                                            if(isset($row->payments->transaction_id)) { 
                                                echo ' / '.$row->payments->transaction_id; 
                                            } 
                                        } else { echo "NA"; } ?></td>
                                        <td><a href="<?= base_url(); ?>onlinecourses/purchasecourselist/<?= base64_encode($row->order_id)?>/<?php if(isset($row->payments->transaction_id)) { echo base64_encode($row->payments->transaction_id); } ?>"><span class="btn btn-info btn-xs"><?= $row->totalorders; ?></span></a></td>
                                        <td><?php if(isset($row->payments)) { echo "₹".$row->payments->amount; } else { echo "NA"; } ?></td>
                                        <td><?php if(isset($row->payments)) { echo $row->payments->status; } else { echo "NA"; } ?></td>
                                    </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
</body>
</html>