<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Sales</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">Sales</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-10">
                                    <h4 class="header-title">Sales</h4>
                                </div>
                            </div>

                            <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                        <th>Order Id / Transaction Id</th>
                                        <th>Users</th>
                                        <th>Items</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($totalsale['success'] == 1) { 
                                        $i = 1;
                                        foreach($totalsale['data'] as $row) { ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= date("d/m/Y h:i A", strtotime($row->created_at)); ?></td>
                                        <td><?= $row->status; ?></td>
                                        <td>₹<?= $row->amount; ?></td>
                                        <td><?php if(isset($row->order_id)) { echo "#VIR-ORD-".str_pad($row->order_id, 9, 0, STR_PAD_LEFT); } ?> / <?php if(isset($row->transaction_id)) { echo $row->transaction_id; } ?></td>
                                        <td><?php if(isset($row->users->name)) { echo $row->users->name; } ?><br>( <?php if(isset($row->users->email)) { echo $row->users->email; } ?> )</td>
                                        <td><a href="<?= base_url(); ?>onlinecourses/purchasecourselist/<?= base64_encode($row->order_id)?>/<?php if(isset($row->payments->transaction_id)) { echo base64_encode($row->payments->transaction_id); } ?>"><span class="btn btn-info btn-xs"><?= $row->totalorders; ?></span></a></td>
                                    </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
</body>
</html>