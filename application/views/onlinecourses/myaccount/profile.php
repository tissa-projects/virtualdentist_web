<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<style type="text/css">
#profileImageProfile {
    height: 6rem;
    width: 6rem;
    border-radius: 50%!important;
    padding: .25rem;
    background-color: #2e55fa;
    border: 1px solid #dee2e6;
    max-width: 100%;
    vertical-align: middle;
    margin: 0 auto;
    font-size: 45px;
    color: #fff;
    text-align: center;
}
</style>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
<?php $page = $this->uri->segment(4);?>
<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">     
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">My Profile</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">My Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-4 col-xl-4">
                    <div class="card text-center">
                        <div class="card-body">
                            <?php if($userdata->data->profile_picture != '') { ?>
                                <img src="<?= base_url('uploads/user_profile/'.$userdata->data->profile_picture); ?>" class="rounded-circle avatar-xl img-thumbnail"
                            alt="profile-image">
                            <?php } else { ?>
                                <span id="firstNameProfile" style="display: none;"><?= ucfirst($userdata->data->name); ?></span>
                                <div id="profileImageProfile"></div>
                            <?php } ?>
                            <br/>
                            <form method="post" action="<?= base_url(); ?>onlinecourses/upload_profile" enctype="multipart/form-data">
                                <div class="fileupload btn btn-success waves-effect waves-light mb-3">
                                    <span><i class="mdi mdi-cloud-upload me-1 upload-button"></i> Change Profile Picture</span>
                                    <input type="file" class="upload" name="profile_picture" id="profile_picture" accept="image/*">
                                    <input type="hidden" name="profile_picture_old" id="profile_picture_old" value="<?= $userdata->data->profile_picture; ?>">
                                    <input type="hidden" name="user_id" id="user_id_add" value="<?= $userdata->data->user_id; ?>">
                                </div>
                                <button type="submit" id="upload" style="display: none;">Submit</button>
                            </form>

                            <h4><?= $userdata->data->name; ?></h4>
                            <!-- <p class="text-muted">@webdesigner</p> -->

                            <!-- <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light">Follow</button>
                            <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light">Message</button> -->

                            <div class="text-start mt-3">
                                <!-- <h4 class="font-13 text-uppercase">About Me :</h4>
                                <p class="text-muted font-13 mb-3">
                                    Hi I'm Johnathn Deo,has been the industry's standard dummy text ever since the
                                    1500s, when an unknown printer took a galley of type.
                                </p> -->

                                <div class="table-responsive">
                                    <table class="table table-borderless table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Full Name :</th>
                                                <td class="text-muted"><?= $userdata->data->name; ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Mobile :</th>
                                                <td class="text-muted"><?= $userdata->data->mobile_number; ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Email :</th>
                                                <td class="text-muted"><?= $userdata->data->email; ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">State :</th>
                                                <td class="text-muted"><?php if(isset($userdata->data->state)) { echo $userdata->data->state; } else { echo "NA"; } ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Language :</th>
                                                <td class="text-muted"><?php if(isset($userdata->data->lang)) { echo $userdata->data->lang; } else { echo "NA"; } ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> <!-- end col-->

                <div class="col-lg-8 col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings">
                                    <form method="post" name="profile_details" action="<?= base_url(); ?>onlinecourses/profileaction" autocomplete="off">
                                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-account-circle me-1"></i> Update Profile Details</h5>
                                        
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <input type="hidden" name="user_id" id="user_id" value="<?= $userdata->data->user_id; ?>">
                                                    <label class="form-label">Full Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter full name" value="<?= $userdata->data->name; ?>">
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Email Address <span class="text-danger">*</span></label>
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" readonly="true" value="<?= $userdata->data->email; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Mobile No. <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile no." value="<?= $userdata->data->mobile_number; ?>" onkeypress="return isNum(event)">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">State </label>
                                                    <select class="form-select form-select-md" id="state_id" name="state_id">
                                                        <option value="">Select State</option>
                                                        <?php if($statelist->data) { 
                                                            foreach($statelist->data as $states) { ?>
                                                        <option value="<?= $states->state_id; ?>" <?php if($states->state_id == $userdata->data->state_id) { echo "selected"; } ?>><?= $states->name; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Language <span class="text-danger">*</span></label>
                                                    <select class="form-select form-select-md" id="language_id" name="language_id">
                                                        <option value="">Select Language</option>
                                                        <?php if($languagelist->data) { 
                                                            foreach($languagelist->data as $lang) { ?>
                                                        <option value="<?= $lang->language_id; ?>" <?php if($lang->language_id == $userdata->data->language_id) { echo "selected"; } ?>><?= $lang->name; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="text-end">
                                            <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- end settings content-->

                            </div> <!-- end tab-content -->
                        </div>
                    </div> <!-- end card-->
                    <?php /*<div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings">
                                    <form method="post" name="billing_details" action="<?= base_url(); ?>onlinecourses/updatebillingdetails">
                                        <input type="hidden" name="user_id" id="user_id_bill" value="<?= $userdata->data->user_id; ?>">
                                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building me-1"></i> Billing Details</h5>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Billing Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="billing_name" name="billing_name" placeholder="Enter billing name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Billing Address <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="billing_address" name="billing_address" placeholder="Enter billing address">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-2">
                                                    <label class="form-label">Country </label>
                                                    <select class="form-select form-select-md" id="country_id" name="country_id">
                                                        <option value="">Select Country</option>
                                                        <?php if($countrylist->data) { 
                                                            foreach($countrylist->data as $countrys) { ?>
                                                        <option value="<?= $countrys->country_id; ?>"><?= $countrys->country_name; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-2">
                                                    <label class="form-label">State </label>
                                                    <select class="form-select form-select-md" id="state_id_bill" name="state_id_bill">
                                                        <option value="">Select State</option>
                                                        <?php if($statelist->data) { 
                                                            foreach($statelist->data as $states) { ?>
                                                        <option value="<?= $states->state_id; ?>"><?= $states->name; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-2">
                                                    <label class="form-label">City </label>
                                                    <input type="text" class="form-control" id="city" name="city" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-2">
                                                    <label class="form-label">GST Number </label>
                                                    <input type="text" class="form-control" id="gst_no" name="gst_no" placeholder="Enter gst no.">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="text-end">
                                            <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- end settings content-->

                            </div> <!-- end tab-content -->
                        </div>
                    </div>*/ ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings">
                                    <form method="post" name="password_details" action="<?= base_url(); ?>onlinecourses/changepassword" autocomplete="off">
                                        <input type="hidden" name="user_id" id="user_id_pass" value="<?= $userdata->data->user_id; ?>">
                                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building me-1"></i> Change Password</h5>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Current Password <span class="text-danger">*</span></label>
                                                    <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Enter current password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">New Password <span class="text-danger">*</span></label>
                                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter new password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Retype new Password <span class="text-danger">*</span></label>
                                                    <input type="password" class="form-control" id="conf_password" name="conf_password" placeholder="Retype new Password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-end">
                                            <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- end settings content-->

                            </div> <!-- end tab-content -->
                        </div>
                    </div> <!-- end card-->

                </div> <!-- end col -->
            </div>    
        </div> <!-- container -->
    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        var firstName = $('#firstNameProfile').text();
        // var lastName = $('#lastName').text();  + $('#lastName').text().charAt(0)
        var intials = $('#firstNameProfile').text().charAt(0);
        var profileImage = $('#profileImageProfile').text(intials);
        $(".upload").on('change', function(){
            $("#upload").click();
        });
    });

    $(function() {
        $("form[name='profile_details']").validate({
            ignore: [],
            debug: false,
            rules: {
                name: "required",
                email: {
                  required: true,
                  email: true
                },
                mobile: "required",
                state_id: "required",
                language_id: "required"
            },
            messages: {
                name: "Please enter name",
                email: {
                    required: "Please enter email",
                    remote: "Please enter valid email"
                },
                mobile: "Please enter mobile",
                state_id: "Please select state",
                language_id: "Please select language"
            },  
            submitHandler: function(form) {
              form.submit();
            }
        });
        // $("form[name='billing_details']").validate({
        //     ignore: [],
        //     debug: false,
        //     rules: {
        //         billing_name: "required",
        //         billing_address: "required",
        //         country_id: "required",
        //         state_id_bill: "required",
        //         city: "required"
        //     },
        //     messages: {
        //         billing_name: "Please enter billing name",
        //         billing_address: "Please enter billing address",
        //         country_id: "Please select country",
        //         state_id_bill: "Please select state",
        //         city: "Please enter city"
        //     },  
        //     submitHandler: function(form) {
        //       form.submit();
        //     }
        // });
        $("form[name='password_details']").validate({
            ignore: [],
            debug: false,
            rules: {
                current_password: {
                    required: true,
                    remote: {
                        url: "<?= base_url(); ?>onlinecourses/checkcurrentpassword",
                        type: "post",
                        data: {
                            current_password: function() {
                                return $("#current_password").val();
                            },
                            user_id: function(){
                                return $('#user_id_pass').val();
                            }
                        }
                    }
                },
                new_password: {
                    required: true,
                    minlength: 8
                },
                conf_password: {
                    required: true,
                    minlength: 8,
                    equalTo : "#new_password"
                }
            },
            messages: {
                current_password: {
                    required: "Please enter current password",
                    remote: "This password does not match"
                },
                new_password: {
                    required: "Please enter new password",
                    minlength: "Please enter at least 8 characters"
                },
                conf_password: {
                    required: "Please retype new password",
                    minlength: "Please enter at least 8 characters",
                    equalTo: "New password and retype new password should be same"
                }
            },  
            submitHandler: function(form) {
              form.submit();
            }
        });
    });
</script>
</body>
</html>