<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Purchase Course Items</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Purchase Course Items</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-10">
                                    <h4 class="header-title"><b>Purchase Course Items ( Order Id / Transaction Id --> <?= "#VIR-ORD-".str_pad($order_id, 9, 0, STR_PAD_LEFT); ?> / <?= $transaction_id; ?>)</b></h4>
                                </div>
                            </div>

                            <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Course Cover</th>
                                        <th>Course Title</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($orderlist->success == 1) { 
                                        $i = 1;
                                        foreach($orderlist->data as $row) { ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><img src="<?= base_url(); ?>uploads/course_cover/<?= $row->courses->course_cover; ?>" width="30%" height="80px"></td>
                                        <td><?= $row->courses->title; ?></td>
                                        <td><?= "₹".$row->amount; ?></td>
                                    </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
</body>
</html>