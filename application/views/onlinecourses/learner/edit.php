<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
            
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Edit Learner</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/learners">Learners</a></li>
                                <li class="breadcrumb-item active">Edit Learner</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->             
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-bordered nav-justified">
                                <li class="nav-item">
                                    <a href="#home-b2" data-bs-toggle="tab" aria-expanded="false" class="nav-link  <?php if($page == '') { echo 'active'; } ?>">
                                        <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                        <span class="d-none d-sm-inline-block">Information</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#profile-b2" data-bs-toggle="tab" aria-expanded="true" class="nav-link <?php if($page == 'enroll') { echo 'active'; } ?>">
                                        <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                        <span class="d-none d-sm-inline-block">Enrolled Courses</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#purchase-b2" data-bs-toggle="tab" aria-expanded="true" class="nav-link ">
                                        <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                        <span class="d-none d-sm-inline-block">Purchase History</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane  <?php if($page == '') { echo 'active'; } ?>" id="home-b2">
                                    <form method="post" name="editlearner" action="<?= base_url(); ?>onlinecourses/editlearneraction" autocomplete="off">
                                        <div>
                                            <div class="mb-2">
                                                <label class="form-label">Name <span class="text-danger">*</span></label>
                                                <input type="text" id="name" name="name" class="form-control" placeholder="Enter Name" value="<?= $getusers['data']->name; ?>">
                                                <input type="hidden" id="user_id" name="user_id" class="form-control" placeholder="Enter User Id" value="<?= $getusers['data']->user_id; ?>">
                                            </div>

                                            <div class="mb-2">
                                                <label class="form-label">Email <span class="text-danger">*</span></label>
                                                <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email" value="<?= $getusers['data']->email; ?>" readonly>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-2">
                                                        <label class="form-label">Mobile <span class="text-danger">*</span></label>
                                                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter Mobile" value="<?= $getusers['data']->mobile_number; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-2">
                                                        <label class="form-label">Role <span class="text-danger">*</span></label>
                                                        <input type="text" id="user_role" name="user_role" value="Learner" class="form-control" placeholder="Enter Role" readonly="true">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-2">
                                                        <label class="form-label">States <span class="text-danger">*</span></label>
                                                        <select class="form-select form-select-md" id="state_id" name="state_id">
                                                            <option value="">Select States</option>
                                                            <?php if($states['data']) { 
                                                                foreach($states['data'] as $state) { ?>
                                                            <option value="<?= $state->state_id; ?>" <?php if($getusers['data']->state_id == $state->state_id) { echo "selected"; } ?>><?= $state->name; ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-2">
                                                        <label class="form-label">Language <span class="text-danger">*</span></label>
                                                        <select class="form-select form-select-md" id="language_id" name="language_id">
                                                            <option value="">Select Language</option>
                                                            <?php if($languages['data']) { 
                                                                foreach($languages['data'] as $lang) { ?>
                                                            <option value="<?= $lang->language_id; ?>" <?php if($getusers['data']->language_id == $lang->language_id) { echo "selected"; } ?>><?= $lang->name; ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                            <li class="next list-inline-item">
                                                <button type="button" class="btn btn-secondary" onclick="window.history.back();">Cancel </button>
                                                <button type="submit" class="btn btn-success" id="edit-learner-button">Edit Learner <i class="mdi mdi-arrow-right ms-1"></i></button>
                                            </li>
                                        </ul>
                                    </form>

                                    <h3>Change Password</h3>
                                    <form method="post" name="password_details" action="<?= base_url(); ?>onlinecourses/changepassword/learners" autocomplete="off">
                                        <input type="hidden" name="user_id" id="user_id_pass" value="<?= $getusers['data']->user_id; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Current Password <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="current_password" name="current_password" placeholder="Enter current password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">New Password <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="new_password" name="new_password" placeholder="Enter new password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-2">
                                                    <label class="form-label">Retype new Password <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="conf_password" name="conf_password" placeholder="Retype new Password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-end">
                                            <button type="submit" class="btn btn-success waves-effect waves-light mt-2" id="edit-pass-button"><i class="mdi mdi-content-save"></i> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane <?php if($page == 'enroll') { echo 'active'; } ?>" id="profile-b2">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-4">
                                                            <a href="javascript:void(0);" class="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#add-modal"><i class="mdi mdi-plus-circle me-1"></i> Enroll</a>
                                                        </div>
                                                    </div>
                                                    <table id="basic-datatable-new" class="table w-100 nowrap table-enroll-course" style="width: 100%!important;">
                                                        <thead>
                                                            <tr>
                                                                <th>Sr. No.</th>
                                                                <th>Course Title</th>
                                                                <th>Assigned Through</th>
                                                                <th>Status</th>
                                                                <th>Expiry</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if($enrollcourseslist['success'] == 1) { 
                                                                $i =1; 
                                                                foreach($enrollcourseslist['data'] as $row) {  ?>                                         
                                                            <tr>
                                                                <td><?= $i; ?></td>
                                                                <td>
                                                                    <img src="<?= base_url(); ?>uploads/course_cover/<?= $row->courses->course_cover; ?>" width="20%" height="20%">
                                                                    <?= $row->courses->title; ?>
                                                                </td>
                                                                <td><?php if($row->assign_by == 1) { echo 'Admin'; } else { echo 'Self'; } ?></td>
                                                                <td>Progress: <?= $row->progress; ?>%</td>
                                                                <td><?php if($row->expiry != '0000-00-00') {echo date("d/m/Y",strtotime($row->expiry)); }else {echo "-"; } ?></td>
                                                                <td>
                                                                    <div class="button-list" id="tooltip-container">
                                                                        <button type="button" class="btn btn-danger btn-xs waves-effect waves-light" id="deletecoursebutton" data-enroll_id="<?= $row->enroll_id; ?>" data-bs-container="#tooltip-container" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Remove From Course"><i class="ri-delete-bin-fill align-middle"></i></button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php $i++; } } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="purchase-b2">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                                        <thead>
                                                            <tr>
                                                                <th>Sr. No.</th>
                                                                <th>Date</th>
                                                                <th>Order Id/Transaction Id</th>
                                                                <th>No. of Items</th>
                                                                <th>Amount</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if($purchaselist->success == 1) { 
                                                                $i = 1;
                                                                foreach($purchaselist->data as $row) { ?>
                                                            <tr>
                                                                <td><?= $i; ?></td>
                                                                <td><?php if(isset($row->payments)) { echo date("d/m/Y h:i A", strtotime($row->payments->created_at)); } else { echo "NA"; } ?></td>
                                                                <td><?php if(isset($row->payments)) { 
                                                                    if(isset($row->order_id)) { echo "#VIR-ORD-".str_pad($row->order_id, 9, 0, STR_PAD_LEFT);
                                                                    } 
                                                                    if(isset($row->payments->transaction_id)) { 
                                                                        echo ' / '.$row->payments->transaction_id; 
                                                                    } 
                                                                } else { echo "NA"; } ?></td>
                                                                <td><a href="<?= base_url(); ?>onlinecourses/purchasecourselist/<?= base64_encode($row->order_id)?>/<?php if(isset($row->payments->transaction_id)) { echo base64_encode($row->payments->transaction_id); } ?>/learners"><span class="btn btn-info btn-xs"><?= $row->totalorders; ?></span></a></td>
                                                                <td><?php if(isset($row->payments)) { echo "₹".$row->payments->amount; } else { echo "NA"; } ?></td>
                                                                <td><?php if(isset($row->payments)) { echo $row->payments->status; } else { echo "NA"; } ?></td>
                                                            </tr>
                                                            <?php $i++; } } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div> <!-- container -->
    </div> <!-- content -->
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" name="enrollcourse" action="<?= base_url(); ?>onlinecourses/enrollcourseaction">
                    <div class="modal-header">
                        <h4 class="modal-title">Enroll Course</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <input type="hidden" name="user_id" id="user_id_new" value="<?= $getusers['data']->user_id; ?>">
                                    <label class="form-label">Select Course <span class="text-danger">*</span></label>
                                    <select class="form-select form-select-md select2" id="course_id" name="course_id" onchange="return getcourseplans(this.value);">
                                        <option value="">Select Course</option>
                                        <?php if($courselist['data']) { 
                                            foreach($courselist['data'] as $courseli) { ?>
                                                <option value="<?= $courseli->course_id; ?>"><?= $courseli->title; ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label class="form-label">Choose Validity <span class="text-danger">*</span></label><br/>
                                    <input class="form-check-input validity" type="radio" name="validity" id="flexRadioDefault11" value="1">
                                    <label class="form-check-label" for="flexRadioDefault11">
                                        &nbsp;Custom&nbsp;&nbsp;
                                    </label>
                                    <input class="form-check-input validity" type="radio" name="validity" id="flexRadioDefault12" value="2">
                                    <label class="form-check-label" for="flexRadioDefault12">
                                        &nbsp;Plan
                                    </label>
                                    <br/>
                                    <label for="validity" class="error" generated="true"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row validity_type_div">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label class="form-label">Number of Days <span class="text-danger">*</span></label>
                                    <input class="form-control" type="number" name="no_of_days" id="no_of_days" placeholder="Enter days">
                                </div>
                            </div>
                        </div>
                        <div class="row plan-div" style="display: none;">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label class="form-label">Select Plan <span class="text-danger">*</span></label>
                                    <select class="form-select form-select-md select2" id="plan_id" name="plan_id">
                                        <option value="">Select Plan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light" id="enroll-course-button">Confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
$(function() {
    var site_url = $("#site_url").val();
    $("form[name='editlearner']").validate({
        ignore: [],
        debug: false,
        rules: {
            name: "required",
            email: {
                required: true,
                email: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/check_email",
                    type: "post",
                    data: {
                        email: function() {
                            return $("#email").val();
                        },
                        user_role: function(){
                            return $('#user_role').val();
                        },
                        user_id: function(){
                            return $('#user_id').val();
                        }
                    }
                }
            },
            mobile: {
                required: true,
                number: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/check_mobile",
                    type: "post",
                    data: {
                        mobile: function() {
                            return $("#mobile").val();
                        },
                        user_role: function(){
                            return $('#user_role').val();
                        },
                        user_id: function(){
                            return $('#user_id').val();
                        }
                    }
                }
            },
            state_id: "required",
            language_id: "required"
        },
        messages: {
            name: "Please enter name",
            email: {
                required: "Please enter email",
                email: "Please enter valid email",
                remote: "This email already exist"
            },
            mobile: {
                required: "Please enter mobile",
                number: "Please enter a valid mobile",
                remote: "This mobile already exist"
            },
            state_id: "Please select state",
            language_id: "Please select language"
        },  
        submitHandler: function(form) {
            $("#edit-learner-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#edit-learner-button").attr('disabled',true);
            form.submit();
        }
    });
    $("form[name='password_details']").validate({
        ignore: [],
        debug: false,
        rules: {
            current_password: {
                required: true,
                remote: {
                    url: "<?= base_url(); ?>onlinecourses/checkcurrentpassword",
                    type: "post",
                    data: {
                        current_password: function() {
                            return $("#current_password").val();
                        },
                        user_id: function(){
                            return $('#user_id_pass').val();
                        }
                    }
                }
            },
            new_password: {
                required: true,
                minlength: 8
            },
            conf_password: {
                required: true,
                minlength: 8,
                equalTo : "#new_password"
            }
        },
        messages: {
            current_password: {
                required: "Please enter current password",
                remote: "This password does not match"
            },
            new_password: {
                required: "Please enter new password",
                minlength: "Please enter at least 8 characters"
            },
            conf_password: {
                required: "Please retype new password",
                minlength: "Please enter at least 8 characters",
                equalTo: "New password and retype new password should be same"
            }
        },  
        submitHandler: function(form) {
            $("#edit-pass-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#edit-pass-button").attr('disabled',true);
            form.submit();
        }
    });
    $("form[name='enrollcourse']").validate({
        ignore: [],
        debug: false,
        rules: {
            course_id: "required",
            validity: "required"
        },
        messages: {
            course_id: "Please select course",
            validity: "Please select validity",
            plan_id: "Please select plan",
            no_of_days: "Please enter no. of days"
        },  
        submitHandler: function(form) {
            $("#enroll-course-button").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
            $("#enroll-course-button").attr('disabled',true);
            form.submit();
        }
    });
});

function getcourseplans(course_id) {
    var site_url = $("#site_url").val();
    $.ajax({
        url:site_url+"onlinecourses_Api/getcourseplans",
        type:"POST",
        // enctype: 'multipart/form-data',
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: JSON.stringify({course_id:course_id}),
        success:function(data,status,xhr) {
            var json_text = JSON.stringify(data, null, 2);
            var obj = JSON.parse(json_text);
            if(obj.success == 1) {
                var i;
                var html = "<option value=''>Select Plan</option>";
                for (i = 0; i < obj.data.length; ++i) {
                    var till = "";
                    if(obj.data[i].access_date && obj.data[i].access_date != '0000-00-00') {
                        till += "( upto "+obj.data[i].access_date+" )";
                    } else if(obj.data[i].access_days) {
                        till += "( "+obj.data[i].access_days+" days )";
                    } else if(obj.data[i].access_hours) {
                        till += "( "+obj.data[i].access_hours+" hours )";
                    } else if(obj.data[i].access_date && obj.data[i].access_date != '0000-00-00' && obj.data[i].access_hours) {
                        till += "( upto "+obj.data[i].access_date+" "+obj.data[i].access_hours+" hours )";
                    } else if(obj.data[i].access_days && obj.data[i].access_hours) {
                        till += "( "+obj.data[i].access_days+" days "+obj.data[i].access_hours+" hours )";
                    }
                    html += "<option value='"+obj.data[i].plan_id+"'>"+obj.data[i].plan_name+" "+till+"</option>";
                }
                $("#plan_id").html(html);
            } 
        },
        error: function (jqXhr, textStatus, errorMessage) {
            console.log(errorMessage);
        },
    });
}

$("input[name=validity]:radio").change(function () {
    if(this.value != 1) {
        $(".validity_type_div").hide();
        $("#fixed_date").attr('required', false);
        $("#no_of_days").attr('required', false);
        $("#max_hours").attr('required', false);
        $(".plan-div").show();
        $("#plan_id").attr('required', true);
        $("#no_of_days").val('');
        $("#no_of_days").attr('required', false);
    } else {
        $(".plan-div").hide();
        $("#plan_id").attr('required', false);
        $(".validity_type_div").show();
        $("#no_of_days").val('');
        $("#no_of_days").attr('required', true);
    }
});
$('.table-enroll-course').on('click', '#deletecoursebutton', function() {
    event.preventDefault();
    var enroll_id = $(this).data('enroll_id');
    var site_url = $('#site_url').val();
    var user_id = $('#user_id').val();
    $("#deleteform").attr('action', site_url+'onlinecourses/deleteenrollcourse/'+user_id);
    $("#deletehead").html('Remove Enroll Course');
    $("#deletemsg").html('Do you want to remove this course');
    $("#delete_id").val(enroll_id);
    $("#deletemsgmodal").modal('show');
});
</script>
</body>
</html>