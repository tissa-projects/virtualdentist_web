<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Learners</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url();?>onlinecourses/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">Learners</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-10">
                                    <h4 class="header-title">Learners</h4>
                                </div>
                                <div class="col-sm-2">
                                    <a href="<?= base_url(); ?>onlinecourses/addlearner" class="btn btn-danger mb-2" style="float: right;"><i class="mdi mdi-plus-circle me-1"></i> Add Learner</a>
                                </div>
                            </div>

                            <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Created Date</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Last Login</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($learnerlist['success'] == 1) { 
                                        $i = 1;
                                        foreach($learnerlist['data'] as $row) { ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= date("d/m/Y h:i A", strtotime($row->created_at)); ?></td>
                                        <td><?= $row->name; ?></td>
                                        <td><?= $row->email; ?></td>
                                        <td><?php if($row->last_login != '0000-00-00 00:00:00') { echo date("d/m/Y h:i A", strtotime($row->last_login)); } else { echo ""; } ?></td>
                                        <td>
                                            <div class="button-list" id="tooltip-container">
                                                <a href="<?= base_url(); ?>onlinecourses/editlearner/<?= base64_encode($row->user_id); ?>"><button type="button" class="btn btn-info btn-xs waves-effect waves-light"><i class="mdi mdi-square-edit-outline"></i></button></a>
                                                <?php if($row->status == 'Active') { ?>
                                                    <button type="button" class="btn btn-success btn-xs waves-effect waves-light" data-bs-container="#tooltip-container" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Active" onclick="return changestatus(<?= $row->user_id; ?>,'Active');"><i class="mdi mdi-check-circle"></i></button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-danger btn-xs waves-effect waves-light" data-bs-container="#tooltip-container" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Deactive" onclick="return changestatus(<?= $row->user_id; ?>,'Deactive');"><i class="mdi mdi-close-circle"></i></button>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
    <!-- Modal -->
    <div class="modal fade" id="changestatusmodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <form method="post" id="changestatusform">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none!important;">
                    <h3 class="modal-title" id="changestatushead"></h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" id="user_id">
                    <input type="hidden" name="status" id="status">
                    <p id="changestatusmsg" style="font-size: 18px;font-weight: bold;text-align: center;"></p>
                </div>
                <div class="modal-footer" style="border-top: none!important;text-align: center;flex: none;display: block;">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><span id="submitname"></span></button>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script type="text/javascript">
    function changestatus(user_id, status) {
        var site_url = $('#site_url').val();
        mystatus = 'Active';
        if(status == 'Active') {
            mystatus = 'Deactive';
        }
        $("#changestatusform").attr('action', site_url+'onlinecourses/changestatus/learners');
        $("#changestatushead").html('Change Status');
        $("#changestatusmsg").html('Do you want to make it '+mystatus);
        $("#status").val(status);
        $("#user_id").val(user_id);
        $("#submitname").html(mystatus);
        $("#changestatusmodal").modal('show');
    }
</script>
</body>
</html>