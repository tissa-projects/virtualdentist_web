<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<?php $this->load->view('onlinecourses/admin-layout/admin-leftpanel'); ?>
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">My Courses</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">My Courses</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="row">
                <?php if($enrollcourseslist->success == 1) {
                	foreach($enrollcourseslist->data as $row) { 
                        $anchor = base_url().'/onlinecourses/mycourseview/'.base64_encode($row->enroll_id).'/'.base64_encode($row->course_id);
                        if($row->expiry < date('Y-m-d') && $row->expiry != '0000-00-00') { $anchor = '';  }?>
		                <div class="col-md-6 col-xl-3">
                            <a href="<?= $anchor; ?>" target="_blank">
		                    <div class="card product-box">
		                        <div class="product-img">
		                            <div class="p-3">
		                                <img src="<?= base_url(); ?>uploads/course_cover/<?= $row->courses->course_cover;?>" alt="course_cover" class="img-fluid" style="height: 144px;width: 100%;"/>
		                            </div>
		                        </div>
		                        <div class="product-info border-top p-3">
		                            <div>
		                                <h5 class="font-16 mt-0 mb-1"><a href="<?= $anchor; ?>" class="text-dark" target="_blank"><?= $row->courses->title;?></a></h5>
		                                <span><?= $row->courses->instructor_display_name;?></span>
                                        <br/>
                                        <?php if($row->expiry < date('Y-m-d') && $row->expiry != '0000-00-00') { ?>
                                            <span class="text text-danger">Expired</span>
                                        <?php } else { ?>
                                            <?php if($row->progress == 0) { ?>
                                                <span class="text text-info">Start Now</span>
                                            <?php } else if($row->progress > 0 && $row->status == 2) { ?>
                                                <span class="text text-warning">In Progress</span>
                                            <?php } else if( $row->status == 1) { ?>
                                                <span class="text text-success">Completed</span>
                                            <?php } ?>
                                        <?php }  ?>
                                        <div class="mt-2 mb-3" id="myprogress-div">
                                            <div class="progress mb-0">
                                                <div class="progress-bar" role="progressbar" style="width: <?= $row->progress; ?>%;" aria-valuenow="<?= $row->progress; ?>" aria-valuemin="0" aria-valuemax="100" id="progress"><?= $row->progress; ?>%</div>
                                            </div>
                                            <label class="text text-info"><?= $row->progress; ?>% Completed</label>
                                        </div>
		                                <h4 class="m-0"> <span class="text-muted">Valid Till :  <?php if($row->validity_type == 1) { echo '<span class="text text-success">Lifetime</span>'; } else if($row->validity_type == 2) { echo date("d/m/Y", strtotime($row->expiry)); } else if($row->validity_type == 3) { echo date("d/m/Y", strtotime($row->expiry)); } else if($row->validity_type == 4) { echo $row->max_hours.' hours'; } else { echo '-'; } ?></span></h4>
		                            </div>
		                        </div> <!-- end product info-->

		                    </div>
                            </a>
		                </div>
		        <?php } } else { ?>
                    <div class="col-md-12 col-xl-12">
                        <p class="text-center">No Data Available</p>
                    </div>
                <?php } ?>
            </div>
            <!-- <div class="row">
                <div class="col-12">
                    <ul class="pagination pagination-rounded justify-content-end mb-3">
                        <li class="page-item">
                            <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                <span aria-hidden="true">«</span>
                            </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                        <li class="page-item">
                            <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                <span aria-hidden="true">»</span>
                            </a>
                        </li>
                    </ul>
                </div> 
            </div> -->
            <!-- end row-->
            
        </div> <!-- container -->

    </div> <!-- content -->
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
</body>
</html>