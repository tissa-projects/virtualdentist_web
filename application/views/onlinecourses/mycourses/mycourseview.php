<?php $this->load->view('onlinecourses/admin-layout/admin-header'); ?>
<style type="text/css">
    .top-bar {
      background: #333;
      color: #fff;
      padding: 1rem;
    }

    .btnnew {
      background: coral;
      color: #fff;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 0.7rem 2rem;
    }

    .btnnew:hover {
      opacity: 0.9;
    }

    .page-info {
      margin-left: 1rem;
    }

    .error {
      background: orangered;
      color: #fff;
      padding: 1rem;
    }

    .error-new {
      color: red;
    }
    .active-menu {
        color: #37cde6!important;
    }
</style>
    <div class="left-side-menu">
        <!-- LOGO -->
        <div class="logo-box">
            <a href="<?= base_url(); ?>onlinecourses" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/favicon.png"/>
                </span>
                <span class="logo-lg">
                    <img alt="" class="logo" src="<?php echo base_url();?>assets/images/logo.png" width="90%"/>
                </span>
            </a>
        </div>

        <div class="h-100" data-simplebar>
            <?php $uri2 = $this->uri->segment(2); ?>
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul id="side-menu">
                    <a href="<?= base_url(); ?>onlinecourses/mycourses"><li class="menu-title"><h4 style="color: #3bafda!important;"><i class="mdi mdi-arrow-collapse-left"></i>  Back to my courses</h4></li></a>
                    <li>
                        <div class="mt-2 mb-3" style="padding: 0% 2%;" id="myprogress-div">
                            <div class="progress mb-0">
                                <div class="progress-bar" role="progressbar" style="width: <?= $getenrolldatabyid->data->progress; ?>%;" aria-valuenow="<?= $getenrolldatabyid->data->progress; ?>" aria-valuemin="0" aria-valuemax="100" id="progressbar"><?= $getenrolldatabyid->data->progress; ?>%</div>
                            </div>
                            <label class="text text-info"><?= $getenrolldatabyid->data->progress; ?>% Completed</label>

                            <input type="hidden" name="totalchapter" id="totalchapter" value="<?= count($coursecontentlist->data); ?>">
                            <input type="hidden" name="progress" id="progress" value="<?= $getenrolldatabyid->data->progress; ?>">
                            <input type="hidden" name="enroll_id" id="enroll_id" value="<?= $getenrolldatabyid->data->enroll_id; ?>">
                        </div>
                            <input type="hidden" name="chapter_index" id="chapter_index" value="<?= $getenrolldatabyid->data->chapter_index; ?>">
                            <input type="hidden" name="pgcount" id="pgcount">
                            <input type="hidden" name="rendpagecount" id="rendpagecount">
                            <input type="hidden" name="chapter" class="chapter">
                            <input type="hidden" name="page" id="page" value="<?= $page; ?>">
                    </li>
                    <li class="menuitem-active">
                        <a href="#sidebarLayouts" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-line"></i>
                            <span> Course Contents </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse show" id="sidebarLayouts">
                            <ul class="nav-second-level" style="padding: 0px;">
                                <?php if($coursecontentlist) { 
                                $i = 1;
                                foreach($coursecontentlist->data as $contlist) { 
                                    $active = '';
                                    if($i == 1) {
                                        $active = 'active-menu';
                                        $collapsed = '';
                                    }
                                ?>
                                <li class="dd-handle <?= $active; ?>" style="height: auto!important;cursor: pointer;" onclick="return getChapterContent(<?= $i; ?>,<?= $contlist->chapter_id; ?>,<?= $contlist->chapter_type; ?>);">
                                    <i class="mdi mdi-arrow-left-circle"></i> <?= $contlist->title; ?> -- <?php if($contlist->chapter_type == 1) { echo 'Course PDF'; } else { echo "Assignment"; } ?>
                                </li>
                            <?php  $i++; } } ?>
                            </ul>
                        </div>
                    </li>
                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>

<div class="content-page">
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Preview Course  --  <?= $getcourse->data->title; ?></h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses/dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>onlinecourses/mycourses">My Courses</a></li>
                                <li class="breadcrumb-item active">Preview Course</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="row">
                <div class="col-xl-12 col-md-12 pdf-div" style="display: none;">
                    <div class="card">
                        <div class="card-body">
                            <div class="top-bar">
                              <button class="btnnew" id="prev-page">
                                <i class="fas fa-arrow-circle-left"></i> Prev Page
                              </button>
                              <button class="btnnew button-next" id="next-page">
                                Next Page <i class="fas fa-arrow-circle-right"></i>
                              </button><br/>
                              <span class="page-info">
                                Page <span id="page-num"></span> of <span id="page-count"></span>
                              </span>
                            </div>

                            <canvas id="pdf-render" style="width: 100%!important;"></canvas>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12 assign-div" style="display: none;">
                    <div class="card">
                        <?php if($assignlist->success != 1 || $assignlist->data[0]->status == 3) { ?>
                            <div class="card-body">
                                <form method="post" name="submit-assign" action="<?= base_url(); ?>onlinecourses/uploadassignmentreply" enctype="multipart/form-data" autocomplete="off">
                                    <input type="hidden" name="course_id" id="course_id" value="<?= $course_id; ?>">
                                    <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($_SESSION['VIR_SESSION']) && $_SESSION['VIR_SESSION']['user_id'] != 0) { echo $_SESSION['VIR_SESSION']['user_id']; } ?>">
                                    <input type="hidden" name="enroll_id" id="enroll_id_assign" value="<?= $getenrolldatabyid->data->enroll_id; ?>">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="mb-3">
                                                <label class="form-label">Assignment Instructions </label><br/>
                                               <label style="border:1px solid grey;width:100%;padding:10px;"  id="assign_instructions"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="mb-3">
                                                <label class="form-label">Assignment Upload <span class="text-muted">You can upload maximum 15 MB of file size.</span></label>
                                                <input type="file" id="assignment_upload" name="assignment_upload" class="form-control" placeholder="Upload Assignment" accept="application/pdf" onchange="FilevalidationUpload()">
                                                <label for="assignment_upload" id="assignment_upload_error" class="error-new" generated="true"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="mb-3">
                                                <label class="form-label">Notes <span class="text-danger">*</span> </label>
                                                <textarea type="text" id="note" name="note" class="form-control" placeholder="Enter Notes" rows="10" cols="40" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="pager wizard mb-0 list-inline text-end mt-3">
                                        <li class="next list-inline-item">
                                            <input type="hidden" name="message" id="message" value="Submitted Successfully">
                                            <button type="submit" class="btn btn-success" id="submit-assign">Submit Assignment <i class="mdi mdi-arrow-right ms-1"></i></button>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        <?php } else { ?>
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label class="form-label"><h4>Assignment Instructions</h4> </label><br/>
                                           <label style="border:1px solid grey;width:100%;padding:10px;"  id="assign_instructions"></label>
                                        </div>
                                    </div>
                                </div>
                                <h4>Assignment History</h4>
                                <table id="basic-datatable" class="table dt-responsive nowrap w-100 table-history">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Date</th>
                                            <th>Message</th>
                                            <th>Status</th>
                                            <th>File/Notes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($assignlist->success == 1) { 
                                            $i = 1;
                                            foreach($assignlist->data as $row) { ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= date("d/m/Y", strtotime($row->created_at)); ?><br/><?= date("h:i A", strtotime($row->created_at)); ?></td>
                                            <td><?= $row->message; ?></td>
                                            <td><?php if($row->status == 1) { echo "<label class='text text-success'>".ucwords('Reviewed')."</label>"; } else if($row->status == 2) { echo "<label class='text text-warning'>".ucwords('Under Review')."</label>"; } else if($row->status == 3) { echo "<label class='text text-danger'>".ucwords('Rejected')."</label>"; } ?></td>
                                            <td>
                                                <?php if($row->file) { echo '<a href="'.base_url().'uploads/assignment_upload/'.$row->file.'" target="_blank">'.$row->file.'</a>'; } ?>
                                                <?php if($row->notes) { echo '<br/><button type="button" class="btn btn-info" id="viewbutton" data-notes="'.$row->notes.'"">View Note</button>'; } ?>
                                                </td>
                                        </tr>
                                        <?php $i++; } } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            
        </div> <!-- container -->

    </div> <!-- content -->
<div class="modal fade" id="warningmodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none!important;">
                <h3 class="modal-title">Alert Message</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p style="font-size: 18px;font-weight: bold;text-align: center;">Please first complete the course content PDF</p>
            </div>
            <div class="modal-footer" style="border-top: none!important;text-align: center;flex: none;display: block;">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewnotemodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">View Note</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                 <p id="viewmsg"></p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('onlinecourses/admin-layout/admin-footer'); ?>
<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var page = $("#page").val();
            if(page != "") {
                var index = $('.active-menu').index('.dd-handle')
                var $li = $('.dd-handle')
                $li.removeClass('active-menu') 
                $li.eq( $li.length - 1 ).addClass('active-menu')
                $li.eq( $li.length  - 1).click();
            } else {
                var index = $('.active-menu').index('.dd-handle')
                var $li = $('.active-menu')
                $li.click();
            }
            $("form[name='submit-assign']").submit(function() {
                var fi = document.getElementById('assignment_upload');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var file = Math.round((fsize / 1024));
                        if (file > 15360) {
                            $("#assignment_upload_error").fadeIn().html("File too Big, please select a file less than 15mb");
                            setTimeout(function() {
                                $("#assignment_upload_error").html("");
                            }, 3000);
                            return false;
                        } 
                    }
                } 
                $("#submit-assign").html('<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>Loading...');
                $("#submit-assign").attr('disabled',true);
            });
        });

        $(".button-next").click(function() {
            if(this.id != 'next-page') {
                var index = $('.active-menu').index('.dd-handle')
                var $li = $('.dd-handle')
                $li.removeClass('active-menu') 
                $li.eq( (index+1) % $li.length ).addClass('active-menu')
                $li.eq( (index+1) % $li.length ).click();
            }

            var site_url = $("#site_url").val();
            var pgcount = parseInt($("#pgcount").val())  || 0;
            var rendpagecount = parseInt($("#rendpagecount").val())  || 0;
            var chapter = $(".chapter").val();
            var totalchapter = parseInt($("#totalchapter").val())  || 0;
            var progress = parseFloat($("#progress").val())  || 0;
            var enroll_id = $("#enroll_id").val();
            var chapter_index = $("#chapter_index").val();
            // var myval = 100 / totalchapter;
            var myval = 100;
            var progresspercent = parseFloat((rendpagecount * myval) / pgcount);
            if(chapter == 2 && (progress < progresspercent)) {
                var mypercent = parseFloat(progress + progresspercent);
            } else if(chapter == 1) {
                if(this.id != 'next-page' && progress > progresspercent) {
                    var mypercent = parseFloat(progress + progresspercent);
                } else {
                    if(rendpagecount < chapter_index) {
                        var mypercent = progress;
                    } else {
                        // if(progress >= 50) {
                        //  var progresspercent = parseFloat((1 * myval) / pgcount);
                            // var mypercent = parseFloat(progress + progresspercent);
                        // } else {
                        //  var mypercent = parseFloat(progresspercent);
                        // }
                        var progresspercent = parseFloat((1 * myval) / pgcount);
                        var mypercent = parseFloat(progress + progresspercent);
                    }
                }
            }

            $.ajax({
                url:site_url+"Onlinecourses_Api/updateprogress",
                type:"POST",
                // enctype: 'multipart/form-data',
                processData:false,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                data: JSON.stringify({progress:mypercent,enroll_id:enroll_id,chapter_index:chapter_index}),
                success:function(data,status,xhr) {
                    var json_text = JSON.stringify(data, null, 2);
                    var obj = JSON.parse(json_text);
                    
                    $( "#myprogress-div" ).load(window.location.href + " #myprogress-div" );
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    // $("#loader").hide();
                    console.log(errorMessage);
                },
            });
        });
        $(".button-prev").click(function() {
          var index = $('.active-menu').index('.dd-handle')
          var $li = $('.dd-handle')
          $li.removeClass('active-menu')
          $li.eq( (index-1) % $li.length ).addClass('active-menu')
          $li.eq( (index-1) % $li.length ).click();
        });

        $('.table-history').on('click', '#viewbutton', function() {
            event.preventDefault();
            var notes = $(this).data('notes');
            $("#viewmsg").html(notes);
            $("#viewnotemodal").modal('show');
        });

        FilevalidationUpload = () => {
            var fi = document.getElementById('assignment_upload');
            // Check if any file is selected.
            if (fi.files.length > 0) {
                for (var i = 0; i <= fi.files.length - 1; i++) {
      
                    var fsize = fi.files.item(i).size;
                    var file = Math.round((fsize / 1024));
                    // The size of the file.
                    if (file > 15360) {
                        $("#assignment_upload_error").html("File too Big, please select a file less than 15mb");
                        setTimeout(function() {
                            $("#assignment_upload_error").html("");
                        }, 3000);
                        return false;
                    } 
                }
            }
        }

        function getChapterContent(indexclass, chapter_id, sub_chapter_id='') {
            var site_url = $("#site_url").val();
            var progress = $("#progress").val();
            if(sub_chapter_id == 2 && progress < 100) {
                $("#warningmodal").modal('show');
            } else {
                var index = $('.active-menu').index('.dd-handle')
                var $li = $('.dd-handle')
                $li.removeClass('active-menu') 
                $li.eq( (indexclass-1) % $li.length ).addClass('active-menu')
                // $("#loader").show();
                $.ajax({
                    url:site_url+"onlinecourses/getchaptercontents",
                    type:"POST",
                    data: {chapter_id:chapter_id,sub_chapter_id:''},
                    success:function(data,status,xhr) {
                        // $("#loader").hide();
                        var obj = JSON.parse(data);
                        if(obj.success == 1) {
                            if(obj.data.chapter_type == 2) {
                                $("#assign_instructions").html(obj.data.instructions+"<br/><a href='<?= base_url(); ?>uploads/upload_template/"+obj.data.upload_template+"' target='_blank'>"+obj.data.upload_template+"</a>");
                                $(".pdf-div").hide();
                                $(".assign-div").show();
                                $(".chapter").val(obj.data.chapter_type);
                                $("#pgcount").val(1);
                                $("#rendpagecount").val(1);
                            } else if(obj.data.chapter_type == 1){
                                if(obj.data.pdf_file == 1) {
                                    var pdf_file = obj.data.pdf_file;
                                } else {
                                    var pdf_file = '<?= base_url(); ?>uploads/course_files/'+obj.data.pdf_file;
                                }
                                $(".chapter").val(obj.data.chapter_type);
                                $(".assign-div").hide();
                                $(".pdf-div").show();
                                getpdf(pdf_file);
                            }
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        // $("#loader").hide();
                        console.log(errorMessage);
                    },
                });
            }
        }


        function getpdf(pdfurl) {
            const url = pdfurl;
            let pdfDoc = null,
              pageNum = 1,
              pageIsRendering = false,
              pageNumIsPending = null;

            const scale = 1.5,
              canvas = document.querySelector('#pdf-render'),
              ctx = canvas.getContext('2d');

            // Render the page
            const renderPage = num => {
              pageIsRendering = true;

              // Get page
              pdfDoc.getPage(num).then(page => {
                // Set scale
                const viewport = page.getViewport({ scale });
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                const renderCtx = {
                  canvasContext: ctx,
                  viewport
                };

                page.render(renderCtx).promise.then(() => {
                  pageIsRendering = false;

                  if (pageNumIsPending !== null) {
                    renderPage(pageNumIsPending);
                    pageNumIsPending = null;
                  }
                });

                // Output current page
                document.querySelector('#page-num').textContent = num;
                $("#rendpagecount").val(num);
                if($("#chapter_index").val() <= num) {
                    $("#chapter_index").val(num);
                }

              });
            };

            // Check for pages rendering
            const queueRenderPage = num => {
              if (pageIsRendering) {
                pageNumIsPending = num;
              } else {
                renderPage(num);
              }
            };

            // Show Prev Page
            const showPrevPage = () => {
              if (pageNum <= 1) {
                return;
              }
              pageNum--;
              queueRenderPage(pageNum);
            };

            // Show Next Page
            const showNextPage = () => {
              if (pageNum >= pdfDoc.numPages) {
                return;
              }
              pageNum++;
              queueRenderPage(pageNum);
            };

            // Get Document
            pdfjsLib
              .getDocument(url)
              .promise.then(pdfDoc_ => {
                pdfDoc = pdfDoc_;

                document.querySelector('#page-count').textContent = pdfDoc.numPages;
                $("#pgcount").val(pdfDoc.numPages);

                renderPage(pageNum);
              })
              .catch(err => {
                // Display error
                const div = document.createElement('div');
                div.className = 'error';
                div.appendChild(document.createTextNode(err.message));
                // var parentDiv = document.getElementsByClassName("card-body");
                // parentDiv.insertBefore(div, canvas.nextSibling);
                document.querySelector('body').insertBefore(div, canvas);
                // Remove top bar
                document.querySelector('.top-bar').style.display = 'none';
              });

            // Button Events
            document.querySelector('#prev-page').addEventListener('click', showPrevPage);
            document.querySelector('#next-page').addEventListener('click', showNextPage);

        }
    </script>
    <script type="text/javascript">
    </script>
</body>
</html>