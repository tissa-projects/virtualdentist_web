

 <?php 
    $this->load->view('frontend/leftsidebar2');
?> 
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/css/font_style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/font-awesome-4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" /> 
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/responsive.dataTables.min.css" type="text/css" />
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<style type="text/css">
.forms {
    background: #fff;
    box-shadow: 0 0 10px #28531f;
    margin: 10px auto;
    max-width: 1000px;
    overflow: hidden;
    position: relative;
    padding: 0;
    border-radius: 15px;
}

.forms form { padding: 30px; }

.forms input {
	font-size: 12px;
	display: block;
	width: 100%;
	padding: 4px 10px;
	border: 1px solid #ddd;
	color: #666;
	border-radius: 2px;
	margin-bottom: 10px;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms select {
	font-size: 12px;
	display: block;
	width: 100%;
	padding: 4px 10px;
	border: 1px solid #ddd;
	color: #666;
	border-radius: 2px;
	margin-bottom: 10px;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-webkit-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}
.forms input:focus {
	outline: 0;
	border-color: #2e5ed7;
}
.forms label {
	font-size: 12px;
	font-weight: normal;
	color: #666;
	margin-bottom: 5px;
	display: block;
}
.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 28px;
    font-size: 13px;
    color: #495057;
}

.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}

.checked {
  color: black;
}

.badge-dark[href]:focus, .badge-dark[href]:hover {
    color: #ffff;
    text-decoration: none;
    background-color: blue !important;
}
 .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

#outer
{
    width:100%;
    text-align: center;
}
.inner
{
    display: inline-block;
}
.img{
    height:80px !important;
    width:100px !important;
}
.white {
    color: #FFFFFF !important;
}
a{
    text-decoration:none !important;
}
</style>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

 <script type="text/javascript">

 $(document).ready(function() {

      $.ajax({    //create an ajax request to display.php
        type: "GET",
        url: "<?php echo base_url();?>api/get_all_subscribed",             
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false, //expect html to be returned                
        success: function(response){                    
           var data = JSON.stringify(response.data);
 
            var obj = JSON.parse(data);
            var html = '';
                var i;var j;
                for(i=0; i< obj.length; i++){

                    if(obj[i].acc_status == 1){

                        var st = "display: none";
                    }else{

                         var st = "display: block";

                    }

                     if(obj[i].acc_status == 0){

                        var st1 = "display: none";
                    }else{

                         var st1 = "display: block";

                    }
                    
                    if(obj[i].doctor_type == 'doctor'){
                         var text ="Private Practitioner";
                        var st11 = "display: none";
                    }

                     if(obj[i].doctor_type == 'Counsultation'){
                        var text ="Counsultation";
                        var st12 = "display: none";
                    }else{

                         var st12 = "display: block";

                    }



                    j = i+1;
                    html += '<tr class="clrtext"><td>'+j+'</td><td align="center">'+obj[i].reg_number+'</td><td align="center">'+obj[i].name+'</td><td align="center">'+obj[i].email_id+'</td><td align="center">'+text+'</td><td align="center">'+obj[i].mobile_number+'</td><td align="center"><div id="outer"><div class="inner"><a  class="btn-primary btn-sm" data-toggle="modal" data-target="#myModal" href="#" onclick="view('+obj[i].doctor_id+')" style="'+st12+'">View</a><a  class="btn-primary btn-sm" data-toggle="modal" data-target="#myModal1" href="#" onclick="viewc('+obj[i].doctor_id+')" style="'+st11+'">View </a></div>&nbsp;<div class="inner"><a href="#" title="Active"  id="activeBtn'+obj[i].doctor_id+'" onclick="active(\''+obj[i].doctor_id+'\');" class=" btn-success btn-sm" style="'+st+'"> Active</a></div>&nbsp;<div class="inner"><a href="#" title="Active"  id="activeBtn'+obj[i].doctor_id+'" onclick="active(\''+obj[i].doctor_id+'\');" class="btn-danger btn-sm" style="'+st1+'"> Deactive</i></a></div></div> </td></tr>';


                }

                 $('#tbody').html(html);
        }

    });
});



 function active(doctor_id) {

    var doctor_id = doctor_id;
   
    //alert(doctor_id);
    $.ajax({
        url: "<?php echo base_url();?>api/active",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id}),
            success: function(response){

                var data = JSON.stringify(response.data);
                 var obj = JSON.parse(data);

                 //alert(data);

                 location.reload();
                  
          }
    });
    
}


</script>

<div class="col-md-10 dshbrd">
     <input type="hidden" name="doctortime" id="doctortime">
    <div class="row">
        <div class="col-md-12"><center><h2><b style="color:#FFFFFF;">Change Password</b><h2></center></div>
            <div class="col-md-3"></div>
        <div class="col-md-5" >
           
        <div class="forms">
           
            <form action="" id="forgot" class = "form-horizontal" role = "form">
			      	<div class="input-field">

			      		<center><span id="message"></span></center>
			        	<div class="col-md-12 form-group">
					        	<label for="email">New Password</label>
					        	<input type="password" class="form-control" id="pass" name="pass"  />
					    </div>

					    <div class="col-md-12 form-group">
					        	<label for="email">Confirm Password</label>
					        	<input type="password" class="form-control" id="cpass" name="pass"  />
					    </div>
				        
			        	 <div class="col-md-12 form-group">
			        		<input type="submit" value="Reset password" id="changepass" class="btn btn-success" style=" height:30px!important; color: white; font-size: 15px; border-radius: 20px; ">
			        	</div><br>
			        		 
				    </div>
				 </form>
        </div>
            
        </div>
        <div class="col-md-3" >
           
             
        </div>
        <!--<div class="col-md-1"></div>-->

    </div>
    <br><br><br><br><br><br>
</div></div></div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#changepass').click(function(e) {
        e.preventDefault();

		 var pass = document.getElementById('pass').value;
		  var cpass = document.getElementById('cpass').value;

		  if(pass == ''){

		  	$('#message').html("<i>Please Enter Password</i>").css('color', 'red');
		        	return false;
		  }else if(pass != cpass){

		  	$('#message').html("<i>Entered Password Doesn't Match With Confirm Password</i>").css('color', 'red');
		        	return false;

		  }else{
       $('#message').html("");
	        $.ajax({
	        url: "<?php echo base_url();?>api/admin_change_pass",
	        type:"POST",
	        processData:false,
	        contentType: 'application/json',
	        dataType: 'json',
	        async: false,
	        data: JSON.stringify({pass:pass}),
		    	success: function(response){
		    		var data = JSON.stringify(response.data);
		    		 Swal.fire({
                        title: "<i style='color:#ADD8E6'>Congratulations...!!</i>", 
                        html: data,  
                        confirmButtonText: "OK", 
                      });

                     $("#pass").val('');
                     $("#cpass").val('');
		    		
		    	}

                
      		});
	    }
        });
	});
</script>

