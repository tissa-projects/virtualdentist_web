

 <?php 
    $this->load->view('frontend/leftsidebar2');
?> 
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/css/font_style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/font-awesome-4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" /> 
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/responsive.dataTables.min.css" type="text/css" />
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 28px;
    font-size: 13px;
    color: #495057;
}

.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}

.checked {
  color: black;
}

.badge-dark[href]:focus, .badge-dark[href]:hover {
    color: #ffff;
    text-decoration: none;
    background-color: blue !important;
}
 .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

#outer
{
    width:100%;
    text-align: center;
}
.inner
{
    display: inline-block;
}
.img{
    height:100px !important;
    width:100px !important;
}
.white {
    color: #FFFFFF !important;
}
a{
    text-decoration:none !important;
}
</style>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

 <script type="text/javascript">

 $(document).ready(function() {

      $.ajax({    //create an ajax request to display.php
        type: "GET",
        url: "<?php echo base_url();?>api/get_all_subscribed",             
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false, //expect html to be returned                
        success: function(response){                    
           var data = JSON.stringify(response.data);
 
            var obj = JSON.parse(data);
            var html = '';
                var i;var j;
                for(i=0; i< obj.length; i++){

                    if(obj[i].acc_status == 1){

                        var st = "display: none";
                    }else{

                         var st = "display: block";

                    }

                     if(obj[i].acc_status == 0){

                        var st1 = "display: none";
                    }else{

                         var st1 = "display: block";

                    }
                    
                    if(obj[i].doctor_type == 'doctor'){
                         var text ="Private Practitioner";
                        var st11 = "display: none";
                        var st12 = "display: block";
                    }

                     if(obj[i].doctor_type == 'Counsultation'){
                        var text ="Consultant";
                        var st12 = "display: none";
                         var st11 = "display: block";
                    }else{

                         var st12 = "display: block";

                    }

                    var plan = "Yearly";
                    if(obj[i].plan_id == 1) {
                        var plan = "Lifetime";
                    }

                    j = i+1;
                    html += '<tr class="clrtext"><td>'+j+'</td><td>'+obj[i].reg_number+'</td><td>'+obj[i].name+'</td><td>'+obj[i].email_id+'</td><td>'+text+'</td><td>'+obj[i].mobile_number+'</td><td>'+plan+'</td><td>'+obj[i].ends_on+'</td><td><div id="outer"><div class="inner"><a  class="btn-primary btn-sm" data-toggle="modal" data-target="#myModal" href="#" onclick="view('+obj[i].doctor_id+')" style="'+st12+'">View</a><a  class="btn-primary btn-sm" data-toggle="modal" data-target="#myModal1" href="#" onclick="viewc('+obj[i].doctor_id+')" style="'+st11+'">View </a></div></div></td><td align="center"><a href="#"  id="activeBtn'+obj[i].doctor_id+'" onclick="active(\''+obj[i].doctor_id+'\');" class=" btn-success btn-sm" style="'+st+'"> Active</a><a href="#" title="Active"  id="activeBtn'+obj[i].doctor_id+'" onclick="active(\''+obj[i].doctor_id+'\');" class="btn-danger btn-sm" style="'+st1+'"> Deactive</a> </td></tr>';


                }

                 $('#tbody').html(html);
        }

    });
});



 function active(doctor_id) {

    var doctor_id = doctor_id;
   
    //alert(doctor_id);
    $.ajax({
        url: "<?php echo base_url();?>api/active",
        type:"POST",
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        //data :form_data,
        data: JSON.stringify({doctor_id:doctor_id}),
            success: function(response){

                var data = JSON.stringify(response.data);
                 var obj = JSON.parse(data);

                 //alert(data);

                 location.reload();
                  
          }
    });
    
}


</script>

 
<div class="col-md-10 dshbrd">
    <section class="panel dshbrd">
        <div class="panel-body dshbrd">
            <div class="row">
                
                <div class="col-md-12">
                     <center><h3 style="color:#FFFFFF">Subscribed list</h3></center>
            <div class="table-responsive"> 
                <table class="table table-bordered display" id="example1" cellspacing="0" width="100%" style="font-family: Lustria;">
                    <thead style="border: 1px solid rgb(12,11,11); ">
                    <th>Sr.No</th>
                    <th>Reg. number</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                     <th>Mobile number</th>
                     <th>Sub. Plan</th>
                     <th>Expiry Date</th>
                      <th class="text-center">View</th>
                    <th class="text-center">Action</th>
                    </thead>
                    </thead>
                    <tbody id="tbody">
                        
                   

                               
                                
                                                 
                    </tbody>
                </table>
                </div>
                    
                </div>
               
                <div class="dataTables_paginate paging_bootstrap pagination"><ul></ul></div>
            </div>
        </div>
    </section>
</div></div></div>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
       
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body">
                <!-- <div class="row">
                   
                    <div class="col-lg-6" id="name_"></div>
                    <div class="col-lg-6" id="email_"></div>
                </div> -->

                <div class="row">
                                    <div class="col-xs-12 col-sm-4 center">
                                         <span class="profile-picture" id="avtar">
                                           
                                        </span><br><span>Profile Picture</span><hr>

                                        <span class="profile-picture" id="avtar2">
                                        
                                           
                                        </span><span>Degree</span><hr>
                                        <span class="profile-picture" id="avtar3">
                                        
                                           
                                        </span><span>Certificate</span><hr>

                                        <div class="space space-4"></div>




                                       
                                    </div><!-- /.col -->

                                    <div class="col-xs-12 col-sm-8">
                                        <h4 class="blue">
                                            <span class="middle" id="name_">John Doe</span> 
                                            <!--(<span id="doctor_typep"></span>)-->
                                        </h4>

                                        <div class="profile-user-info">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b> Registration number </b> </div>

                                                <div class="profile-info-value">
                                                    <span id="reg"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b>Passing year</b></div>

                                                <div class="profile-info-value">
                                                    
                                                    <span id="passing_year"></span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b>Clinic name</b></div>

                                                <div class="profile-info-value">
                                                   
                                                    <span id="clinic_name"></span>
                                                    
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b>Clinic Establishing year </b></div>

                                                <div class="profile-info-value">
                                                    <span id="establish_year"></span><br>
                                                     <span id="year" ></span>&nbsp;<span>Years Experience Overall.</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> <b>Clinic address</b> </div>

                                                <div class="profile-info-value">
                                                     <i class="fa fa-map-marker light-orange bigger-110"></i>
                                                    <span id="clinic_address"></span><br>
                                                    <span id="city"></span>
                                                    <span id="pincode"></span>
                                                    
                                                </div>
                                            </div>
                                             <div class="profile-info-row">
                                                <div class="profile-info-name"> <b>Consultation fees</b> </div>

                                                <div class="profile-info-value">
                                                   
                                                    &#8377;&nbsp;<span id="fees"></span>
                                                    
                                                    
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b> Mobile number </b></div>

                                                <div class="profile-info-value">
                                                    <span id="mobile_">789456123</span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b> Email Id</b> </div>

                                                <div class="profile-info-value">
                                                    <span id="email_"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hr hr-8 dotted"></div>

                                       
                                    </div><!-- /.col -->
                                </div>
               
                
            </div>
            <div class="modal-footer">
               
            </div>
        </div>
            
    </div>
</div>

<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">
       
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body">
                

                <div class="row">
                                    <div class="col-xs-12 col-sm-4 center">
                                      
                                        
                                        <span class="profile-picture" id="avtarc">
                                           
                                        </span><span>Profile Picture</span><hr>

                                       <span class="profile-picture" id="avtar2c">
                                        
                                           
                                        </span><span>Degree</span><hr>
                                        <span class="profile-picture" id="avtar3c">
                                        
                                           
                                        </span><span>Certificate</span><hr>

                                        <div class="space space-4"></div>




                                       
                                    </div><!-- /.col -->

                                    <div class="col-xs-12 col-sm-8">
                                        <h4 class="blue">
                                            <span class="middle" id="namec_"></span> 
                                            <!--(<span id="doctor_type"></span>)-->
                                        </h4>

                                        <div class="profile-user-info">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b> Registration number </b></div>

                                                <div class="profile-info-value">
                                                    <span id="regc"></span>
                                                </div>
                                            </div>
                                             <div class="profile-info-row">
                                                <div class="profile-info-name"><b>Specialization</b></div>

                                                <div class="profile-info-value">
                                                    
                                                    <span id="specializationc"></span>
                                                   
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b>Passing year</b></div>

                                                <div class="profile-info-value">
                                                    
                                                    <span id="passing_yearc"></span>
                                                </div>
                                            </div>

                                           <div class="profile-info-row">
                                                <div class="profile-info-name"> <b> address</b> </div>

                                                <div class="profile-info-value">
                                                     <i class="fa fa-map-marker light-orange bigger-110"></i>
                                                    <span id="clinic_addressc"></span><br>
                                                    <span id="cityc"></span>
                                                    <span id="pincodec"></span>
                                                    
                                                </div>
                                            </div>


                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b> Mobile number</b> </div>

                                                <div class="profile-info-value">
                                                    <span id="mobilec_"></span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"><b> Email Id </b></div>

                                                <div class="profile-info-value">
                                                    <span id="emailc_"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hr hr-8 dotted"></div>

                                       
                                    </div><!-- /.col -->
                                </div>
               
                
            </div>
            <div class="modal-footer">
               
            </div>
        </div>
            
    </div>
</div>

<script>  
    

    function view(id)
    {
        //alert(id);
            var doctor_id = id;

            $.ajax({
            url: "<?php echo base_url();?>api/get_doctor",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){

                var data = JSON.stringify(response.data); 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                   $('#reg').html(json[0].reg_number);
                    $('#name_').html(json[0].name);
                    $('#email_').html(json[0].email_id);
                    $('#mobile_').html(json[0].mobile_number);
                    $('#clinic_name').html(json[0].clinic_name);
                    $('#doctor_typep').html(json[0].doctor_type);
                    $('#clinic_address').html(json[0].address);
                    $('#fees').html(json[0].counsult_fee);
                    $('#establish_year').html(json[0].establish_year);
                    
                     var curyear = new Date().getFullYear();

                         var exp =  curyear - json[0].passing_year;

                         $('#year').html(exp);
                         
                     $('#country').html(json[0].country);  
                     $('#state').html(json[0].state);  
                     $('#city').html(json[0].city); 
                     $('#pincode').html(json[0].pincode); 
                     $('#passing_year').html(json[0].passing_year);
                     $('#doctor_type').html(json[0].doctor_type); 
                      $('#specialization').html(json[0].specialization); 
                     $('#avtar').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'" data-lightbox="example-1"><img class="example-image img img-responsive" alt=" Avatar" id="avatar2" src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"></a>') ;
                      $('#avtar2').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/degree/'+json[0].degree_cert+'" data-lightbox="example-2"><img class="example-image img img-responsive" alt=" Avatar"  src="<?php echo base_url(); ?>uploads/degree/'+json[0].degree_cert+'"></a>') ;
                      $('#avtar3').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/degree/'+json[0].reg_cert+'" data-lightbox="example-3"><img class="example-image img img-responsive" alt=" Avatar"  src="<?php echo base_url(); ?>uploads/degree/'+json[0].reg_cert+'"></a>') ;  
                    $('#myModal').modal("show");
                }

                
            }           
        });
    }


    function viewc(id)
    {
       
            var doctor_id = id;

            $.ajax({
            url: "<?php echo base_url();?>api/getcounsultant",
            type:"POST",
            processData:false,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            //data :form_data,
            data: JSON.stringify({doctor_id:doctor_id}),
                success: function(response){

                var data = JSON.stringify(response.data); 

                var json = $.parseJSON(data)                
                if(json.length>0)
                {
                   $('#regc').html(json[0].reg_number);
                    $('#namec_').html(json[0].name);
                    $('#emailc_').html(json[0].email_id);
                    $('#mobilec_').html(json[0].mobile_number);
                         $('#clinic_addressc').html(json[0].c_address); 
                          $('#doctor_type').html(json[0].doctor_type);
                     $('#country').html(json[0].c_country);  
                     $('#state').html(json[0].c_state);  
                     $('#cityc').html(json[0].c_city); 
                     $('#pincodec').html(json[0].c_pincode); 
                     $('#passing_yearc').html(json[0].passing_year);
                     $('#doctor_type').html(json[0].doctor_type); 
                      $('#specializationc').html(json[0].specialization); 
                      $('#avtarc').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'" data-lightbox="example-1"><img class="example-image img img-responsive" alt=" Avatar" id="avatar2" src="<?php echo base_url(); ?>uploads/profile_pics/'+json[0].profile_pic+'"></a>') ;
                      $('#avtar2c').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/degree/'+json[0].degree_cert+'" data-lightbox="example-2"><img class="example-image img img-responsive" alt=" Avatar"  src="<?php echo base_url(); ?>uploads/degree/'+json[0].degree_cert+'"></a>') ;
                      $('#avtar3c').html('<a class="example-image-link" href="<?php echo base_url(); ?>uploads/degree/'+json[0].reg_cert+'" data-lightbox="example-3"><img class="example-image img img-responsive" alt=" Avatar"  src="<?php echo base_url(); ?>uploads/degree/'+json[0].reg_cert+'"></a>') ;    
                    $('#myModal1').modal("show");
                }

                
            }           
        });
    }
</script>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#example1').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
<script src="<?php echo base_url();?>assets/js/lightbox.min.js"></script> 