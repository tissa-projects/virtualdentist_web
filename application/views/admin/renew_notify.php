

 <?php 
    $this->load->view('frontend/leftsidebar2');
?> 
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/css/font_style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/font-awesome-4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" /> 
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/ext_css/responsive.dataTables.min.css" type="text/css" />
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<style type="text/css">.label1 {
    font-family: serif;
    font-size: 14px;
    font-weight: normal;
    text-align: justify;
}
input{height: 25px !important; }

.bootstrap-select .dropdown-toggle {
    border: 1px solid #e7ecf1 !important;
    background-color: #fff !important;
    height: 28px;
    font-size: 13px;
    color: #495057;
}

.scrollable {
        height: 150px;
        overflow-y: scroll;
      }
      .form-horizontal .form-group {
    margin-right: -2px !important;
    margin-left: -15px !important;
}

.checked {
  color: black;
}

.badge-dark[href]:focus, .badge-dark[href]:hover {
    color: #ffff;
    text-decoration: none;
    background-color: blue !important;
}
 .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    border-bottom: 2px solid blue;
    border-top:none;
    border-left:none;
    border-right: none;
    color: #555;
    cursor: default;
    background-color: #fff;
    /* border: 1px solid #ddd; */
    /* border-bottom-color: #4424e300; */
}

.nav-tabs {
    border-bottom: 1px solid blue;
}

#outer
{
    width:100%;
    text-align: center;
}
.inner
{
    display: inline-block;
}
.img{
    height:80px;
    width:100px;
}
.white {
    color: #FFFFFF !important;
}
a{
    text-decoration:none;
}
</style>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

 <script type="text/javascript">

 $(document).ready(function() {

      $.ajax({    //create an ajax request to display.php
        type: "GET",
        url: "<?php echo base_url();?>api/get_all_subscribed",             
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false, //expect html to be returned                
        success: function(response){                    
           var data = JSON.stringify(response.data);
 
            var obj = JSON.parse(data);
            var html = '';
                var i;var j;
                for(i=0; i< obj.length; i++){

                    if(obj[i].is_verified == 1){

                        var st = "display: none";
                    }else{

                         var st = "display: block";

                    }

                     if(obj[i].is_verified == 0){

                        var st1 = "display: none";
                    }else{

                         var st1 = "display: block";

                    }
                    
                    if(obj[i].doctor_type == 'doctor'){
                         var text ="Private Practitioner";
                        
                    }

                     if(obj[i].doctor_type == 'Counsultation'){
                        var text ="Consultant";
                        
                    }

                    var today = new Date();
                    var dd = today.getDate();

                    var mm = today.getMonth()+1; 
                    var yyyy = today.getFullYear();
                    if(dd<10) 
                    {
                        dd='0'+dd;
                    } 

                    if(mm<10) 
                    {
                        mm='0'+mm;
                    } 
                    today = yyyy+'-'+mm+'-'+dd;
                    //alert(today);

                    if(today != obj[i].ends_on){

                        var b1 = "disabled";

                    }else{
                        var b1 = "";
                    }

                    // var newdate    = new Date(obj[i].ends_on),
                    // yr      = newdate.getFullYear(),
                    // month   = newdate.getMonth() < 10 ? '0' + newdate.getMonth() : newdate.getMonth(),
                    // day     = newdate.getDate()  < 10 ? '0' + newdate.getDate()  : newdate.getDate(),
                    // enddate = day + '-' + month + '-' + yr ;
                    j = i+1;
                    html += '<tr class="clrtext"><td>'+j+'</td><td>'+obj[i].reg_number+'</td><td>'+obj[i].name+'</td><td>'+obj[i].email_id+'<input type="hidden" value="'+obj[i].email_id+'" id="demail"></td><td>'+text+'</td><td>'+obj[i].mobile_number+'</td><td>'+obj[i].ends_on+'</td><td>&nbsp;<button class="btn btn-info" '+b1+' onclick="sendemail('+obj[i].doctor_id+');">Send notification</button>&nbsp;<input type="hidden" value="'+today+'" id="cur_date"><input type="hidden" value="'+obj[i].ends_on+'" id="end_date"><input type="hidden" value="'+obj[i].doctor_id+'" id="doctorid"> </td></tr>';


                }

                 $('#tbody').html(html);
        }

    });


});


 function sendemail(doctor_id){

var curdate = $("#cur_date").val();
 var enddate = $("#end_date").val();
 var doctor_id = doctor_id;
 var email = $("#demail").val();
 
 //alert(doctor_id);
 //alert(email);

 //if(curdate == enddate){

  $.ajax({    
        type: "POST",
        url: "<?php echo base_url();?>api/send_notification",             
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false, 
        data: JSON.stringify({doctor_id:doctor_id}),               
        success: function(response){ 
             var msg = JSON.stringify(response.data);
        Swal.fire({
                  title: "<i style='color:#ADD8E6'>Congratulation...!!</i>", 
                  html: msg,  
                  confirmButtonText: "OK", 
                });
                
                
            location.reload();

        }
    });


 //}

}



//  function active(doctor_id) {

//     var doctor_id = doctor_id;
   
//     alert(doctor_id);
//     $.ajax({
//         url: "<?php echo base_url();?>api/approved",
//         type:"POST",
//         processData:false,
//         contentType: 'application/json',
//         dataType: 'json',
//         async: false,
//         //data :form_data,
//         data: JSON.stringify({doctor_id:doctor_id}),
//             success: function(response){

//                 var data = JSON.stringify(response.is_verified);
//                  var obj = JSON.parse(data);

//                  location.reload();
                  
//           }
//     });
    
// }


</script>

 
<div class="col-md-10 dshbrd">
    <section class="panel dshbrd">
        <div class="panel-body dshbrd">
            <div class="row">
                
                <div class="col-md-12">
                      <center><h3 style="color:#FFFFFF">Renewal Notification</h3></center>
                    <div class="table-responsive"> 
                        <table class="table table-bordered display" id="example3" cellspacing="0" width="100%" style="font-family: Lustria;">
                            <thead style="border: 1px solid rgb(12,11,11); ">
                            <th>Sr.No</th>
                            <th>Reg. number</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Type</th>
                             <th>Mobile number</th>
                             <th>Expiry Date</th>
                            <th class="text-center">Action</th>
                            </thead>
                            
                          
                            <tbody id="tbody">           
                                                     
                            </tbody>
                        </table>
                    </div>
                
            </div>
        </div>

       
  <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" /> -->

    </section>
</div>
</div></div>
</div>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#example3').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>






