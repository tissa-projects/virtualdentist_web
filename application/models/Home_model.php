<?php
class Home_model extends CI_Model
{
 function fetch_country()
 {
  $this->db->order_by("country_name", "ASC");
  $query = $this->db->get("vd_country");
  return $query->result();
 }

 function get_state(){

  $this->db->where('country_id', 4);
  $this->db->order_by('state_name', 'ASC');
  $query = $this->db->get('vd_state');
  return $query->result();
 }
 
 function get_city(){

  //$this->db->where('state_id', 10);
  $this->db->order_by('city_name', 'ASC');
  $query = $this->db->get('vd_city');
  return $query->result();
 }

 function fetch_state($country_id)
 {
  $this->db->where('country_id', $country_id);
  $this->db->order_by('state_name', 'ASC');
  $query = $this->db->get('vd_state');
  $output = '';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->state_name.'" data-id="'.$row->state_id.'" >';
  }
  return $output;
 }

 function fetch_city($state_id)
 {
  $this->db->where('state_id', $state_id);
  $this->db->order_by('city_name', 'ASC');
  $query = $this->db->get('vd_city');
  $output = '';
  foreach($query->result() as $row)
  {
   $output .= '<option data-id ="'.$row->city_id.'" value ="'.$row->city_name.'" >';
  }
  return $output;
 }
 
 function sub_fetch_state($country_id)
 {
  $c = $this->db->query("SELECT `country_id` FROM `vd_country` WHERE `country_name` = '$country_id'")->row();
  $this->db->where('country_id', $c->country_id);
  $this->db->order_by('state_name', 'ASC');
  $query = $this->db->get('vd_state');
  $output = '';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->state_name.'"> '.$row->state_name.'</option>';
  }
  return $output;
 }

 function sub_fetch_city($state_id)
 {
  $c = $this->db->query("SELECT `state_id` FROM `vd_state` WHERE `state_name` = '$state_id'")->row();
  $this->db->where('state_id', $c->state_id);
  $this->db->order_by('city_name', 'ASC');
  $query = $this->db->get('vd_city');
  $output = '';
  foreach($query->result() as $row)
  {
   $output .= '<option value ="'.$row->city_name.'" >'.$row->city_name.'</option>';
  }
  return $output;
 }
 
}

?>