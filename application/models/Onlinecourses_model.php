<?php
class Onlinecourses_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('email');
        $this->load->model('PassHash_Model');
    }

    function CallAPI($method, $url, $header, $data = false)
    {
        // Method: POST, PUT, GET etc
        // Data: array("param" => "value") ==> index.php?param=value
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                 if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);                              
                 break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 

        $res = curl_exec($curl);
        $result = curl_getinfo($curl,CURLINFO_HTTP_CODE);
        $err = curl_error($curl);
        // print_r($err);echo "<pre>";
        // print_r($result);echo "<pre>";
        // print_r($res);echo "<pre>";
        curl_close($curl);
        if ($result == 401 || $result == 500 || $result == 503 || $result == 400 || $result == 404 || $result == 403 ) {
            return $result;
        }else {
            return $res;
        }
    }

    //SELECT SINGLE RECORD DATA BY CONDITION WITH DYNAMIC TABLE NAME
    public function GetSingleData($tablename,$condition,$field='',$order='',$group='',$limit='')
    {
        if($field != '')
            $this->db->select($field);
        if($condition != '')
            $this->db->where($condition);
        if($order != '')
            $this->db->order_by($order);
        if($limit != '')
            $this->db->limit($limit);
        if($group != '')
            $this->db->group_by($group);
        return $this->db->get($tablename)->row();
    }

    //SELECT MULTIPLE RECORD DATA BY CONDITION WITH DYNAMIC TABLE NAME
    public function GetAllData($tablename,$field='',$condition='',$group='',$order='',$limit='')
    {
        if($field != '')
            $this->db->select($field);
        if($condition != '')
            $this->db->where($condition);
        if($order != '')
            $this->db->order_by($order);
        if($limit != '')
            $this->db->limit($limit);
        if($group != '')
            $this->db->group_by($group);
        return $this->db->get($tablename)->result();
    }

    //UPDATE OR INSERT QUERY STATEMENT WITH DYNAMIC TABLE NAME
    public function SaveData($tablename,$data,$condition='')
    {
        $DataArray = array();
        $table_fields = $this->db->list_fields($tablename);
        foreach($data as $field=>$value)
        {
            if(in_array($field,$table_fields))
            {
                $DataArray[$field]= $value;
            }
        }
        if($condition != '')
        {
            $this->db->where($condition);
            if($this->db->update($tablename, $DataArray)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }else{
            if($this->db->insert($tablename, $DataArray)) {
                return $this->db->insert_id();
            }else{
                return FALSE;
            }
        }
    }

    // DELETE STATEMENT WITH DYNAMIC TABLE
    public function DeleteData($tablename, $condition)
    {
        $this->db->where($condition);
        $this->db->delete($tablename);
    }   

    public function randomPassword()
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 4; $i++)
        {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

/* Web Panel Functions */
    // Register
    public function register($obj)
    {
        $fullname = $obj['fullname'];
        $email = $obj['email'];
        $password = $obj['password'];
        $mobile = $obj['mobile'];
        $user_role = $obj['user_role'];

        $checkemail = $this->GetSingleData('vd_users','email="'.$email.'" and user_role="'.$user_role.'"','user_id');

        if(!empty($checkemail)) {
            $response = array('success' => 0,'message'=> 'We already have this email ID registered with us, welcome back! Please use the Forgot password option to reset your password');
        } else {
            $checkmobile = $this->GetSingleData('vd_users','mobile_number="'.$mobile.'" and user_role="'.$user_role.'"','user_id');

            if(!empty($checkmobile)) {
                $response = array('success' => 2,'message'=> 'This mobile already exists');
            } else {
                $pass = $this->PassHash_Model->hash($password);
                $data = array("name"=>$fullname,"email"=>$email,"mobile_number"=>$mobile,"password"=>$pass,"user_role"=>$user_role,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
                $insert_id = $this->SaveData('vd_users',$data);
                // $insert_id=$this->db->insert_id();
                if($insert_id) {
                    $response = array('success' => 1,'message'=> 'Registration has been successful. A verification link has been sent to your email account.');
                    $sub = 'Verify Email';
                    $msg = "Hello ".ucfirst($fullname).",<br><br/>
                          
                        You have successfully created your account. <br/><br/>
                        Please click on the link below to verify your email address and complete the registration on VirtualDentist Online Course Panel.<br><br/>
                        <a href=".base_url()."onlinecourses/verify/".base64_encode($email)." target='_blank'><button class='btn btn-info' style='background-color: #0275d8;border-color: #0275d8;color: #ffffff;border: 1px solid rgba(0, 0, 0, 0);border-radius: 4px;display: inline-block;font-weight: 400;line-height: 1.25;padding: 8px 16px;text-align: center;transition: all 0.2s ease-in-out 0s;vertical-align: middle;white-space: nowrap;'>Verify Your Email</button></a><br><br/>
                        <b>Thank You!!</b><br>
                        <b>Team VirtualDentist!</b>";
                    $this->sendemail($email,$sub,$msg);
                } else {
                    $response = array('success' => 2,'message'=> 'Failed to register');
                }
            }
        }
        return  $response;
    }

    // Login
    public function login($obj)
    {
        $email = $obj['email'];
        $password = $obj['password'];

        $checkemail = $this->GetSingleData('vd_users','email="'.$email.'"','user_id, password, is_verify, status');

        if(empty($checkemail)) {
            $response = array('success' => 3,'message'=> "The email you entered does not belong to any account. If you are a new user, please click on Create New Account");
        } else {
            if($checkemail->is_verify == 1) {
                if($checkemail->status == 'Active') {
                    $checkpass = $this->PassHash_Model->check_password($checkemail->password, $password);
                    if($checkpass == 0) {
                        $response = array('success' => 2,'message'=> "The password you entered is incorrect");
                    } else {
                        $checklogin = $this->GetSingleData('vd_users','email="'.$email.'" and password="'.$checkemail->password.'"','user_id,name,email,mobile_number,profile_picture,user_role,state_id,language_id,created_at');
                        if($checklogin) {

                            $checkusercart = $this->GetSingleData('vd_carts','user_id="'.$checklogin->user_id.'"','cart_id');
                            if($checkusercart) {
                                $cart_id = $checkusercart->cart_id;
                            } else {
                                $instdata = array("user_id"=>$checklogin->user_id,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
                                $cart_id = $this->SaveData('vd_carts',$instdata);
                            }

                            $checkadmin = $this->GetSingleData('vd_users','user_role="Admin"','email');

                            $logindata = array("last_login"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
                            $this->SaveData('vd_users',$logindata,"user_id='".$checklogin->user_id."'");
                            $data['VIR_SESSION'] = array("user_id"=>$checklogin->user_id,"name"=>$checklogin->name,"email"=>$checklogin->email,"mobile_number"=>$checklogin->mobile_number,"profile_picture"=>$checklogin->profile_picture,"user_role"=>$checklogin->user_role,"state_id"=>$checklogin->state_id,"language_id"=>$checklogin->language_id,"created_at"=>$checklogin->created_at,'cart_id'=>$cart_id,'adminemail'=>$checkadmin->email);
                            $this->session->set_userdata($data);

                            $this->updateCourseExpiredStatus($checklogin->user_id);

                            $response = array('success' => 1,'message'=> "Login has been successful", "data"=>$data['VIR_SESSION']);
                        } else {
                            $response = array('success' => 0,'message'=> "This email and password does not match");
                        }
                    }
                } else {
                    $response = array('success' => 0,'message'=> "Your account is Deactived. Please contact our admin.");
                }
            } else {
                $response = array('success' => 0,'message'=> "You are not verified user. Please verify your email account.");
            }
        }
        return  $response;
    }

    public function updateCourseExpiredStatus($user_id) {
        $getlist = $this->GetAllData('vd_enroll_courses','*','user_id="'.$user_id.'" and is_deleted=0','','enroll_id ASC','');
        if(empty($getlist)) {
            $response = false;
        } else {
            $i =0;
            foreach ($getlist as $row) {
                if($row->expiry < date("Y-m-d") && $row->expiry != '0000-00-00') {
                    $data = array("status"=>4,"updated_at"=>date("Y-m-d H:i:s"));
                    $insert_id = $this->SaveData('vd_enroll_courses',$data,"enroll_id='".$row->enroll_id."'");
                }
                $i++;
            }
            $response = true;
        }
        return  $response;
    }

    // Save Cart Item
    public function addbeforelogincartitem($obj) {
        $cart_id = $obj['cart_id'];
        foreach ($obj['cartdata'] as $row) {
            $checkitem = $this->GetSingleData('vd_cart_items','course_id="'.$row['ci'].'" and cart_id="'.$cart_id.'" and status="Active"','cart_item_id');
            if(!$checkitem) {
                $checkseq = $this->GetSingleData('vd_cart_items','','max(cart_item_id) as cart_item_id');
                $cart_item_id = 1;
                if($checkseq->cart_item_id) {
                    $cart_item_id = $checkseq->cart_item_id+1;
                }
                $data = array("cart_id"=>$cart_id,"course_id"=>$row['ci'],"sequence"=>$cart_item_id,"status"=>'Active',"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
                $insert_id = $this->SaveData('vd_cart_items',$data);
            }
        }
        $data = array("cart_id"=>$cart_id);
        $getdata = $this->getcartlist($data);
        $cartlist = array();
        $cart_sum = 0;
        if($getdata['success'] == 1) {
            $cartlist = $getdata['data'];
            $cart_sum = $getdata['cart_sum'];
        }
        $response = array('success' => 1,'message'=> "Add data successfully", 'data'=>$cartlist, 'cart_sum'=>$cart_sum);
        return  $response;
    }

    // Get Cart List
    public function getcartlist($obj)
    {
        $cart_id = $obj['cart_id'];
        $getdata = $this->GetAllData('vd_cart_items','*','cart_id="'.$cart_id.'" and status="Active"','','cart_item_id ASC','');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $cartlist = array();
            $sum = 0;
            foreach ($getdata as $row) {
                $cond = 'course_id="'.$row->course_id.'"';
                $getcourselist = $this->GetSingleData('vd_courses',$cond,'*','sequence ASC');
                if($getcourselist) {
                    $getplan = $this->GetSingleData('vd_plans','course_id="'.$row->course_id.'" and is_deleted=0','*','plan_id DESC');
                    $getcourselist->plans = $getplan;
                    $course_access = 0;
                    if($getcourselist->plans) {
                        if($getcourselist->plans->access_days) { 
                            $course_access = $getcourselist->plans->access_days.' days'; 
                        } 
                    }
                    $price = 0; 
                    if($getcourselist->plans) { 
                        if($getcourselist->plans->is_free == 0) { 
                            $price = 0; 
                        } else { 
                            $price = $getcourselist->plans->final_payable_price;
                        } 
                    }
                    $list = array("cart_item_id"=>$row->cart_item_id,"ci"=>$getcourselist->course_id,"course_cover"=>$getcourselist->course_cover,"title"=>$getcourselist->title,"course_access"=>$course_access,"price"=>$price);
                    array_push($cartlist, $list);
                    $sum += $price;
                }
            }
            $getcartlist = $cartlist;
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getcartlist, 'cart_sum'=>$sum);
        }
        return  $response;
    }

    public function addtocart($obj) {
        $cart_id = $obj['cart_id'];
        $course_id = $obj['ci'];
        $checkitem = $this->GetSingleData('vd_cart_items','course_id="'.$course_id.'" and cart_id="'.$cart_id.'" and status="Active"','cart_item_id');
        if(!$checkitem) {
            $checkseq = $this->GetSingleData('vd_cart_items','','max(cart_item_id) as cart_item_id');
            $cart_item_id = 1;
            if($checkseq->cart_item_id) {
                $cart_item_id = $checkseq->cart_item_id+1;
            }
            $data = array("cart_id"=>$cart_id,"course_id"=>$course_id,"sequence"=>$cart_item_id,"status"=>'Active',"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
            $insert_id = $this->SaveData('vd_cart_items',$data);
        } 
        $data = array("cart_id"=>$cart_id);
        $getdata = $this->getcartlist($data);
        $cartlist = array();
        $cart_sum = 0;
        if($getdata['success'] == 1) {
            $cartlist = $getdata['data'];
            $cart_sum = $getdata['cart_sum'];
        }
        $response = array('success' => 1,'message'=> 'Course added in cart successfully', 'data'=>$cartlist, 'cart_sum'=>$cart_sum);
        return  $response;
    }

    public function deletemaincartitem($obj) {
        $data = array("status"=>"Inactive");
        $cart_id = $obj['cart_id'];
        $course_id = $obj['course_id'];
        $editdata = $this->SaveData('vd_cart_items',$data,"course_id='".$course_id."' and cart_id = '".$cart_id."' and status='Active'");
        $data = array("cart_id"=>$cart_id);
        $getdata = $this->getcartlist($data);
        $cartlist = array();
        $cart_sum = 0;
        if($getdata['success'] == 1) {
            $cartlist = $getdata['data'];
            $cart_sum = $getdata['cart_sum'];
        }
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Cart item deleted successfully', 'data'=>$cartlist, 'cart_sum'=>$cart_sum);
        } else {
            $response = array('success' => 0,'message'=> 'Failed to delete cart item', 'data'=>$cartlist, 'cart_sum'=>$cart_sum);
        }
        return  $response;
    }


    // Course List
    public function courselist($obj='')
    {
        $getlist = $this->GetAllData('vd_courses','*','is_deleted=0 and is_publish=1','','sequence ASC, course_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $i =0;
            foreach ($getlist as $row) {
                $getplan = $this->GetSingleData('vd_plans','course_id="'.$row->course_id.'" and is_deleted=0','*','plan_id DESC');
                $getlist[$i]->plans = $getplan;
                $getrating = $this->GetSingleData('vd_course_rating','course_id="'.$row->course_id.'"','rating_id,rating');
                $getlist[$i]->rating_id = 0;
                $getlist[$i]->rating = 0;
                if($getrating) {
                    $getlist[$i]->rating_id = $getrating->rating_id;
                    $getlist[$i]->rating = $getrating->rating;
                }
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    public function addbillingdetails($obj) {
        $name = $obj['name'];
        $mobile = $obj['mobile'];
        $user_id = $obj['user_id'];
        $getdata = $this->GetSingleData('vd_user_bill_details','user_id="'.$user_id.'"','*');
        $con = "";
        $data = array("name"=>$name,"mobile"=>$mobile,"user_id"=>$user_id,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
        if($getdata) {
            $con = "user_id = '".$user_id."'";
            $data = array("name"=>$name,"mobile"=>$mobile,"user_id"=>$user_id,"updated_at"=>date("Y-m-d H:i:s"));
        } 
        $insert_id = $this->SaveData('vd_user_bill_details',$data,$con);
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'Billing data added successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to add billing data');
        }
        return  $response;
    }

    // Get billing data
    public function getbillingdata($obj)
    {
        $getdata = $this->GetSingleData('vd_user_bill_details','user_id="'.$obj['user_id'].'"','*');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getdata);
        }
        return  $response;
    }

    public function addorders($obj) {
        $cart_id = $obj['cart_id'];
        $amount = $obj['amount'];
        $txnid = $obj['txnid'];
        $status = $obj['status'];
        $PG_TYPE = $obj['PG_TYPE'];
        $apidata = array("cart_id" => $cart_id);
        $getcartlist = $this->getcartlist($apidata);
        $getuserId = $this->GetSingleData('vd_carts','cart_id="'.$cart_id.'"','user_id');
        $user_id = 0;
        $user_name = "";
        $user_email = "";
        if(!empty($getuserId)) {
            $user_id = $getuserId->user_id;
            $getusername = $this->GetSingleData('vd_users','user_id="'.$user_id.'"','name,email');
            if($getusername) {
                $user_name = $getusername->name;
                $user_email = $getusername->email;
            }
        }

        $order_id = '';
        $data = array("user_id"=>$user_id,"total"=>$amount,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
        $order_id = $this->SaveData('vd_order',$data);
        if($order_id) {
            $order_id = $order_id;
        }
        $getcourse = "";
        if($getcartlist['success'] == 1) {
            $sub = 'Order Confirmation';
            $msg = "Hello ".ucfirst($user_name).",<br><br/>
                <b>Order successfully placed.</b><br/><br/>
                We are pleased to confirm your order no. #VIR-ORD-".str_pad($order_id, 9, 0, STR_PAD_LEFT)."<br/><br/>
                
                <table width='100%' style='border: 1px solid black;' cellpadding='10px' cellspacing='0px'>
                    <thead>
                        <tr>
                            <th style='border: 1px solid black;'>Sr. No.</th>
                            <th style='border: 1px solid black;'>Course Title</th>
                            <th style='border: 1px solid black;'>Amount</th>
                        </tr>
                    </thead>
                    <tbody>";
            $p = 1;

            foreach ($getcartlist['data'] as $row) {
                $data = array("status"=>'Inactive',"updated_at"=>date("Y-m-d H:i:s"));
                $deleteid = $this->SaveData('vd_cart_items',$data,'cart_item_id="'.$row['cart_item_id'].'"');
                if($deleteid) {
                    $checkseq = $this->GetSingleData('vd_order_item','','max(order_item_id) as order_item_id');
                    $order_item_id = 1;
                    if($checkseq->order_item_id) {
                        $order_item_id = $checkseq->order_item_id+1;
                    }
                    $data = array("order_id"=>$order_id,"course_id"=>$row['ci'],"total"=>$row['price'],"sequence"=>$order_item_id,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
                    $insert_id = $this->SaveData('vd_order_item',$data);

                    $cdata = array('course_id'=>$row['ci']);
                    $getcourse = $this->getcourse($cdata);
                    if($getcourse['success'] == 1) {
                        $validity_type = 1;
                        $no_of_days = 0;
                        if($getcourse['data']) {
                            if(isset($getcourse['data']->plans)) {
                                if($getcourse['data']->plans->access_days != '' && $getcourse['data']->plans->access_days != '0') {
                                    $validity_type = 3;
                                    $no_of_days = $getcourse['data']->plans->access_days;
                                }
                            }
                            /** Mail Code */                            
                            $msg .= "<tr>
                                    <td style='border: 1px solid black;'>".$p."</td>
                                    <td style='border: 1px solid black;'>".$getcourse['data']->title."</td>
                                    <td style='border: 1px solid black;'>₹".$row['price']."</td>
                                </tr>";
                            /** /. Mail Code */
                        }
                        $expiry = '';
                        if($no_of_days) {
                            $expiry = date('Y-m-d',strtotime('+'.$no_of_days.' day'));
                            $validity_type = 3;
                        }
                        $enrolldata = array("user_id"=>$user_id,"course_id"=>$row['ci'],"assign_by"=>2,"validity"=>2,"validity_type"=>$validity_type,"no_of_days"=>$no_of_days,"expiry"=>$expiry,"status"=>3,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
                        $insert_id = $this->SaveData('vd_enroll_courses',$enrolldata);
                    }
                }
                $p++;
            }
            $data = array("order_id"=>$order_id,"transaction_id"=>$txnid,"payment_method"=>$PG_TYPE,"status"=>$status,"amount"=>$amount,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
            $insert_id = $this->SaveData('vd_order_payment',$data);

            $msg .="</tbody></table><br/><br/>
            <b>Thank You!!</b><br>
            <b>Team VirtualDentist!</b>";
            $this->sendemail($user_email,$sub,$msg);
        }
        $response = array('success' => 1,'message'=> "Add order data successfully");
        return  $response;
    }

    public function forgotpassword($obj) {
        $email = $obj['email'];

        $checkemail = $this->GetSingleData('vd_users','email="'.$email.'"','user_id, name');
        if(empty($checkemail)) {
            $response = array('success' => 0,'message'=> "The email you entered does not belong to any account. If you are a new user, please click on Create New Account");
        } else {
                $response = array('success' => 1,'message'=> "We sent reset password link to your email address. Please check your email");
                $sub = 'Reset Password';
                $msg = "Hello ".ucfirst($checkemail->name).",<br><br/>
                      
                    A request was submitted to change your account credentials. If this was you, click on the link below to reset them.<br><br>
                    <a href=".base_url()."onlinecourses/forgot/".base64_encode($email)." target='_blank'>click here</a><br><br>
                    This link will expire within 5 minutes.<br><br>
                    If you don't want to reset your credentials, just ignore this email and nothing will be changed..<br><br>
                    <b>Thank You!!</b><br>
                    <b>Team VirtualDentist!</b>";
                $this->sendemail($email,$sub,$msg); 
        }
        return $response;
    }

    public function changeforgotpassword($obj) {
        $email = $obj['email'];
        $password = $obj['password'];

        $checkemail = $this->GetSingleData('vd_users','email="'.$email.'"','user_id, name');
        if(empty($checkemail)) {
            $response = array('success' => 0,'message'=> "The email you entered does not belong to any account. If you are a new user, please click on Create New Account");
        } else {
            $newpass = $this->PassHash_Model->hash($password);
            $logindata = array("password"=>$newpass,"updated_at"=>date("Y-m-d H:i:s"));
            $edit_id = $this->SaveData('vd_users',$logindata,"email='".$email."'");
            if(empty($edit_id)) {
                $response = array('success' => 0,'message'=> "Failed to changed password");
            } else {
                $response = array('success' => 1,'message'=> "Password changed successfully");
            }
        }
        return $response;
    }

    public function sendemail($emails,$sub,$msg)
    {
        $message = '';
            $message = '<html><head><title>Virtual Dentist</title></head><body><div style="overflow: hidden;" class="a3s" id=":16z"><div class="adM"></div><div><div class="adM"></div><div style="border-left: 1px solid #ddd;min-height: 52px;border-right: 1px solid #ddd;border-top: 1px solid #ddd;color: #666;margin: 0 auto;padding: 11px 0 10px 12px;"><div class="adM"> </div><a target="_blank" href="'.base_url().'"><img style="border:0 none;color:#666; vertical-align:middle;" src="'.base_url().'assets/images/logo.png" alt="virtualdentist" class="CToWUd"></a>  </div><div style="border:1px solid #ddd;color:#666;margin:0 auto;padding:0 20px 20px"><div style="color:#666"><h4 style="font-family:arial!important"> </h4><div style="font-size:12px;line-height:20px;font-family:arial!important">'.$msg.' </div></div></div><div><div style="background-color:rgb(51,51,51)!important;margin:0px auto;padding:8px 46px;min-height:50px;clear:both"><div style="width:60%;float:left;color:#fff;padding-top:10px;font-size:12px"> <span>Copyright &copy; 2020 Virtual Dentist - All Rights Reserved.</span></div><div style="float:right"><div style="overflow:hidden;padding-top:5px"><a target="_blank" href="https://www.facebook.com/virtualdentistindia/" style="color:#666;display:block;float:left;margin-left:15px"><img src="https://img.icons8.com/fluent/48/000000/facebook-new.png"/><a target="_blank" href="" style="color:#666;display:block;float:left;margin-left:8px"><img src="https://img.icons8.com/fluent/48/000000/twitter.png"/></a><a target="_blank" style="color:#666;display:block;float:left;margin-left:12px" href=""><img src="https://img.icons8.com/color/48/000000/pinterest--v1.png"/></a></div></div></div></div></div><div class="yj6qo"></div><div class="adL"></div></div></body></html>';
        $this->email->set_mailtype("html");
        $this->email->from('info@virtualdentist.in', 'Virtual Dentist');
        $this->email->to($emails);
        $this->email->subject($sub);
        $this->email->message($message);
        $this->email->send();
        return 1;
    }
/* Web Panel Functions */

/* Admin Functions */
    // Language List
    public function languagelist($obj='')
    {
        $getlist = $this->GetAllData('vd_languages','*','','','language_id ASC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // new signup learners List
    public function newsignups($obj='')
    {
        $getlist = $this->GetSingleData('vd_users','MONTH(created_at)=MONTH(now()) and YEAR(created_at)=YEAR(now()) and user_role="Learner"','count(user_id) as totalusers','user_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // getallsale List
    public function getallsale($obj='')
    {
        $getlist = $this->GetAllData('vd_order_payment','*','status="success"','','payment_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $i =0;
            foreach ($getlist as $row) {
                $getorders = $this->GetSingleData('vd_order_item','order_id="'.$row->order_id.'"','count(order_item_id) as totalorders');
                $getlist[$i]->totalorders = $getorders->totalorders;

                $getordersuser = $this->GetSingleData('vd_order','order_id="'.$row->order_id.'"','user_id');
                $getlist[$i]->user_id = $getordersuser->user_id;

                $getuser = $this->GetSingleData('vd_users','user_id="'.$getlist[$i]->user_id.'"','name,email');
                $getlist[$i]->users = $getuser;
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Instructor List
    public function instructorlist($obj='')
    {
        $getlist = $this->GetAllData('vd_users','*','user_role="Instructor"','','user_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Get Course
    public function admingetcourse($course_id)
    {
        $getdata = $this->GetSingleData('vd_courses','course_id="'.$course_id.'"','*');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $getplan = $this->GetSingleData('vd_plans','course_id="'.$course_id.'" and is_deleted=0','*','plan_id DESC');
            $getdata->plans = $getplan;
            $getlang = $this->GetSingleData('vd_languages','language_id="'.$getdata->language_id.'"','*');
            $getdata->language = strstr($getlang->name, ' (', true);
            $getrating = $this->GetSingleData('vd_course_rating','course_id="'.$course_id.'"','rating_id,rating');
            if($getrating) {
            $getdata->rating_id = $getrating->rating_id;
            $getdata->rating = $getrating->rating;
            }
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    // Admin Course List
    public function admincourselist($obj='')
    {
        $getlist = $this->GetAllData('vd_courses','*','is_deleted=0','','sequence ASC, course_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $i =0;
            foreach ($getlist as $row) {
                $getplan = $this->GetSingleData('vd_plans','course_id="'.$row->course_id.'" and is_deleted=0','*','plan_id DESC');
                $getlist[$i]->plans = $getplan;

                $getrating = $this->GetSingleData('vd_course_rating','course_id="'.$row->course_id.'"','rating_id,rating');
                $getlist[$i]->rating_id = 0;
                $getlist[$i]->rating = 0;
                if($getrating) {
                    $getlist[$i]->rating_id = $getrating->rating_id;
                    $getlist[$i]->rating = $getrating->rating;
                }
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Add Course
    public function addcourse($obj)
    {
        $title = $obj['title'];
        $instructor_display_name = $obj['instructor_display_name'];
        $tags = $obj['tags'];
        $description = $obj['description'];
        $course_tagline = $obj['course_tagline'];
        $how_to_use = $obj['how_to_use'];
        $language_id = $obj['language_id'];
        $is_freely_available = $obj['is_freely_available'];
        $sequence = $obj['sequence'];
        $instructor = $obj['instructor'];
        
        $data = 'data:image/png;base64,'.$obj['course_cover'];
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $img = 'course_image_'.$this->randomPassword().'.png';
        file_put_contents('./uploads/course_cover/'.$img, $data);

        $data = array("title"=>$title,"instructor_display_name"=>$instructor_display_name,"tags"=>$tags,"description"=>$description,"course_tagline"=>$course_tagline,"how_to_use"=>$how_to_use,"language_id"=>$language_id,"is_freely_available"=>$is_freely_available,"course_cover"=>$img,"sequence"=>$sequence,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
        $insert_id = $this->SaveData('vd_courses',$data);
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'Course added successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to add course');
        }
        return  $response;
    }

    // Edit Course
    public function editcourse($obj)
    {
        $course_id = $obj['course_id'];
        $title = $obj['title'];
        $instructor_display_name = $obj['instructor_display_name'];
        $tags = $obj['tags'];
        $description = $obj['description'];
        $course_tagline = $obj['course_tagline'];
        $how_to_use = $obj['how_to_use'];
        $language_id = $obj['language_id'];
        $is_freely_available = $obj['is_freely_available'];
        $sequence = $obj['sequence'];
        $instructor = $obj['instructor'];

        $getcourse = $this->getcourse($course_id);
        if($getcourse['success'] == 1) {
            $data = array("title"=>$title,"instructor_display_name"=>$instructor_display_name,"tags"=>$tags,"description"=>$description,"course_tagline"=>$course_tagline,"how_to_use"=>$how_to_use,"language_id"=>$language_id,"is_freely_available"=>$is_freely_available,"sequence"=>$sequence,"updated_at"=>date("Y-m-d H:i:s"));
            $editdata = $this->SaveData('vd_courses',$data,'course_id="'.$course_id.'"');
            if($editdata) {
                $response = array('success' => 1,'message'=> 'Course updated successfully');
            } else {
                $response = array('success' => 0,'message'=> 'Failed to update course');
            }
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update course');
        }
        return  $response;
    }

    // Check Course Sequence
    public function check_course_sequence($obj)
    {
        $sequence = $obj['sequence'];
        $course_id = $obj['course_id'];
        $cond = 'sequence="'.$sequence.'"';
        if($course_id) {
            $cond = 'sequence="'.$sequence.'" and course_id != "'.$course_id.'"';
        }
        $checkseq = $this->GetSingleData('vd_courses',$cond);
        if(empty($checkseq)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully");
        }
        return  $response;
    }

    // Get Course Sequence
    public function get_course_sequence($obj='')
    {
        $getseq = $this->GetSingleData('vd_courses','','max(sequence) as seq');
        if(empty($getseq)) {
            $response = array('success' => 0,'message'=> "No record found", 'seq'=>$getseq->seq);
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'seq'=>$getseq->seq);
        }
        return  $response;
    }

    // Get Course
    public function getcourse($obj)
    {
        $course_id = $obj['course_id'];
        $getdata = $this->GetSingleData('vd_courses','course_id="'.$course_id.'"','*');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $getplan = $this->GetSingleData('vd_plans','course_id="'.$course_id.'" and is_deleted=0','*','plan_id DESC');
            $getdata->plans = $getplan;
            $getlang = $this->GetSingleData('vd_languages','language_id="'.$getdata->language_id.'"','*');
            $getdata->language = strstr($getlang->name, ' (', true);
            $getrating = $this->GetSingleData('vd_course_rating','course_id="'.$course_id.'"','rating_id,rating');
            $getdata->rating_id = 0;
            $getdata->rating = 0;
            if($getrating) {
                $getdata->rating_id = $getrating->rating_id;
                $getdata->rating = $getrating->rating;
            }
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    public function deletecourse($course_id) {
        $data = array("is_deleted"=>1);
        $editdata = $this->SaveData('vd_courses',$data,"course_id='".$course_id."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Course deleted successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to delete course');
        }
        return  $response;
    }

    public function publishcourse($course_id,$status) {
        $data = array("is_publish"=>$status);
        $editdata = $this->SaveData('vd_courses',$data,"course_id='".$course_id."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Course publish status changed successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to changed publish status');
        }
        return  $response;
    }

    // Add Course Plan
    public function addcourseplan($obj)
    {
        $course_id = $obj['course_id'];
        $plan_name = $obj['plan_name'];
        $is_free = $obj['is_free'];
        $list_price = $obj['list_price'];
        $final_payable_price = $obj['final_payable_price'];
        $access_days = $obj['access_days'];
        $tax_rate = $obj['tax_rate'];
        $is_include_tax_in_price = $obj['is_include_tax_in_price'];

        $data = array("course_id"=>$course_id,"plan_name"=>$plan_name,"is_free"=>$is_free,"list_price"=>$list_price,"final_payable_price"=>$final_payable_price,"access_days"=>$access_days,"tax_rate"=>$tax_rate,"is_include_tax_in_price"=>$is_include_tax_in_price,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
        $insert_id = $this->SaveData('vd_plans',$data);
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'Course plan added successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to add course plan');
        }
        return  $response;
    }

    // Edit Course Plan
    public function editcourseplan($obj)
    {
        $plan_id = $obj['plan_id'];
        $plan_name = $obj['plan_name'];
        $is_free = $obj['is_free_edit'];
        $list_price = $obj['list_price'];
        $final_payable_price = $obj['final_payable_price'];
        $access_days = $obj['access_days'];
        $tax_rate = $obj['tax_rate'];
        $is_include_tax_in_price = $obj['is_include_tax_in_price_edit'];

        $data = array("plan_name"=>$plan_name,"is_free"=>$is_free,"list_price"=>$list_price,"final_payable_price"=>$final_payable_price,"access_days"=>$access_days,"tax_rate"=>$tax_rate,"is_include_tax_in_price"=>$is_include_tax_in_price,"updated_at"=>date("Y-m-d H:i:s"));
        $editdata = $this->SaveData('vd_plans',$data,"plan_id='".$plan_id."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Course plan updated successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update course plan');
        }
        return  $response;
    }

    public function deletecourseplan($plan_id) {
        $data = array("is_deleted"=>1);
        $editdata = $this->SaveData('vd_plans',$data,"plan_id='".$plan_id."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Course plan deleted successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to delete course plan');
        }
        return  $response;
    }

    // Get Course Plan
    public function getcourseplans($course_id)
    {
        $getdata = $this->GetAllData('vd_plans','*','course_id="'.$course_id.'" and is_deleted=0','','plan_id ASC','');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    // Get user data
    public function getuserdata($obj)
    {
        $getdata = $this->GetSingleData('vd_users','user_id="'.$obj['user_id'].'"','*','user_id ASC');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $getstate = $this->GetSingleData('vd_states','state_id="'.$getdata->state_id.'"','name','state_id ASC');
            $getlang = $this->GetSingleData('vd_languages','language_id="'.$getdata->language_id.'"','name','language_id ASC');
            if($getstate)
                $getdata->state = $getstate->name;
            if($getlang)
                $getdata->lang = $getlang->name;

            unset($getdata->password);
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    // Get state
    public function statelist($obj='')
    {
        $getdata =  $this->GetAllData('vd_states','*','','','state_id ASC','');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    // Get country
    public function countrylist($obj='')
    {
        $getdata =  $this->GetAllData('vd_country','*','','','country_id ASC','');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    // update user
    public function updateuserdata($obj)
    {
        $user_id = $obj['user_id'];
        $name = $obj['name'];
        $email = $obj['email'];
        $mobile = $obj['mobile'];
        $state_id = $obj['state_id'];
        $language_id = $obj['language_id'];

        $data = array("name"=>$name,"email"=>$email,"mobile_number"=>$mobile,"state_id"=>$state_id,"language_id"=>$language_id,"updated_at"=>date("Y-m-d H:i:s"));
        $editdata = $this->SaveData('vd_users',$data,"user_id='".$user_id."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Profile updated successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update profile');
        }
        return  $response;
    }

    // rating action
    public function ratingaction($obj)
    {
        $rating_id = $obj['rating_id'];
        $course_id = $obj['course_id'];
        $user_id = $obj['user_id'];
        $rating = $obj['rating'];

        if($rating_id) {
            $data = array("course_id"=>$course_id,"user_id"=>$user_id,"rating"=>$rating,"updated_at"=>date("Y-m-d H:i:s"));
            $editdata = $this->SaveData('vd_course_rating',$data,"rating_id='".$rating_id."'");
        } else {
            $data = array("course_id"=>$course_id,"user_id"=>$user_id,"rating"=>$rating,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
            $editdata = $this->SaveData('vd_course_rating',$data);
        }
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Rating added successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to add profile');
        }
        return  $response;
    }

    // check current password
    public function checkcurrentpassword($obj)
    {
        $user_id = $obj['user_id'];
        $current_password = $obj['current_password'];

        $checkuser = $this->GetSingleData('vd_users','user_id="'.$user_id.'"','user_id, password');
        if(empty($checkuser)) {
            $response = array('success' => 0,'message'=> "This user does not exists");
        } else {
            $checkpass = $this->PassHash_Model->check_password($checkuser->password, $current_password);
            if($checkpass == 0) {
                $response = array('success' => 0,'message'=> 'Failed to matched password');
            } else {
                $response = array('success' => 1,'message'=> 'Password matched');
            }
        }
        return  $response;
    }

    // change password
    public function changepassword($obj)
    {
        $user_id = $obj['user_id'];
        $current_password = $obj['current_password'];
        $new_password = $obj['new_password'];
        $conf_password = $obj['conf_password'];

        $checkuser = $this->GetSingleData('vd_users','user_id="'.$user_id.'"','user_id, password');
        if(empty($checkuser)) {
            $response = array('success' => 0,'message'=> "This user does not exists");
        } else {
            $checkpass = $this->PassHash_Model->check_password($checkuser->password, $current_password);
            if($checkpass == 0) {
                $response = array('success' => 0,'message'=> 'Incorrect current password');
            } else {
                $pass = $this->PassHash_Model->hash($new_password);
                $data = array("password"=>$pass,"updated_at"=>date("Y-m-d H:i:s"));
                $editdata = $this->SaveData('vd_users',$data,"user_id='".$user_id."'");
                if($editdata) {
                    $response = array('success' => 1,'message'=> 'Password changed successfully');
                } else {
                    $response = array('success' => 0,'message'=> 'Failed to change password');
                }
            }
        }
        return  $response;
    }

    public function addchapterfile($obj,$file) {
        $pdf_file = '';
        $pdf_file_title = "";
        if($file['pdf_file']['name']!='')
        { 
            if($file['pdf_file']['error'] == 0)
            {       
                $src = $file['pdf_file']['tmp_name'];
                $filEnc = time();
                $files=$file['pdf_file']['name'];
                $files= $filEnc."_".$file['pdf_file']['name'];
                $files = str_replace(array( '(', ')',':',' '), '', $files);
                $dest ="./uploads/course_files/".$files;
                if(move_uploaded_file($src,$dest))
                {
                    $pdf_file=$files;
                    $pdf_file_title=$file['pdf_file']['name'];
                }
            }else {
                $pdf_file_title=$file['pdf_file']['name'];
                $response = array('success' => 0,'message'=> 'Failed to add chapter','data'=>0);
            }
        }
        if(isset($obj['pdf_type']) && $obj['pdf_type'] == 1) {
            $pdf_type = $obj['pdf_type'];
            $pdf_file = $obj['pdf_file_url'];
            $pdf_file_title = preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($pdf_file));
        } else if(isset($obj['pdf_type']) && $obj['pdf_type'] == 2) {
            $pdf_type = $obj['pdf_type'];
            $pdf_file = $pdf_file;
            $pdf_file_title = preg_replace('/\\.[^.\\s]{3,4}$/', '', $pdf_file_title);;
        } else {
            $pdf_type = 0;
            $pdf_file_title = $obj['title']; 
        }

        $getlast = $this->GetSingleData('vd_course_chapter','course_id="'.$obj['course_id'].'"','chapter_id');
        $sequence = 1;
        if($getlast) {
            $sequence = $getlast->chapter_id + 1;
        }
        $data = array("course_id"=>$obj['course_id'],"title"=>$pdf_file_title,"chapter_type"=>$obj['chapter_type'],"pdf_type"=>$pdf_type,"pdf_file"=>$pdf_file,"sequence"=>$sequence,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
        $insert_id = $this->SaveData('vd_course_chapter',$data);
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'PDF has been added successful','data'=>$insert_id);
        } else {
            $response = array('success' => 0,'message'=> 'Failed to add pdf','data'=>0);
        }
        return  $response;
    }

    // public function addsubchapterfile($obj,$file) {
    //     $pdf_file = '';
    //     $pdf_file_title = "";
    //     if($file['pdf_file']['name']!='')
    //     { 
    //         if($file['pdf_file']['error'] ==0)
    //         {       
    //             $src = $file['pdf_file']['tmp_name'];
    //             $filEnc = time();
    //             $files=$file['pdf_file']['name'];
    //             $files= $filEnc."_".$file['pdf_file']['name'];
    //             $files = str_replace(array( '(', ')',':',' '), '', $files);
    //             $dest ="./uploads/course_files/".$files;
    //             if(move_uploaded_file($src,$dest))
    //             {
    //                 $pdf_file=$files;
    //                 $pdf_file_title=$file['pdf_file']['name'];
    //             }
    //         } 
    //     }
    //     if(isset($obj['pdf_type']) && $obj['pdf_type'] == 1) {
    //         $pdf_type = $obj['pdf_type'];
    //         $pdf_file = $pdf_file;
    //         $pdf_file_title = preg_replace('/\\.[^.\\s]{3,4}$/', '', $pdf_file_title);;
    //     } else if(isset($obj['pdf_type']) && $obj['pdf_type'] == 2) {
    //         $pdf_type = $obj['pdf_type'];
    //         $pdf_file = $obj['pdf_file_url'];
    //         $pdf_file_title = preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($pdf_file));
    //     } else {
    //         $pdf_type = 0;
    //         $pdf_file_title = $obj['title']; 
    //     }

    //     $getlast = $this->GetSingleData('vd_course_sub_chapter','chapter_id="'.$obj['chapter_id'].'"','sub_chapter_id');
    //     $sequence = 1;
    //     if($getlast) {
    //         $sequence = $getlast->sub_chapter_id + 1;
    //     }
    //     $data = array("chapter_id"=>$obj['chapter_id'],"title"=>$pdf_file_title,"chapter_type"=>$obj['chapter_type'],"pdf_type"=>$pdf_type,"pdf_file"=>$pdf_file,"sequence"=>$sequence,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
    //     $insert_id = $this->SaveData('vd_course_sub_chapter',$data);
    //     if($insert_id) {
    //         $response = array('success' => 1,'message'=> 'Pfd Item has been added successful','data'=>$insert_id);
    //     } else {
    //         $response = array('success' => 0,'message'=> 'Failed to add pdf item','data'=>0);
    //     }
    //     return  $response;
    // }

    // Get Course content
    public function getcoursecontent($coursecontentid)
    {
        $getdata = $this->GetSingleData('vd_course_chapter','chapter_id="'.$coursecontentid.'"','*');
        if(empty($getdata)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
        }
        return  $response;
    }

    // Get Course sub content
    // public function getcoursesubcontent($subcontentid)
    // {
    //     $getdata = $this->GetSingleData('vd_course_sub_chapter','sub_chapter_id="'.$subcontentid.'"','*');
    //     if(empty($getdata)) {
    //         $response = array('success' => 0,'message'=> "No record found", 'data'=>$getdata);
    //     } else {
    //         $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getdata);
    //     }
    //     return  $response;
    // }

    // Edit Course content
    public function updatecoursecontent($obj)
    {
        $data = array("title"=>$obj['title'],"updated_at"=>date("Y-m-d H:i:s"));
        $editdata = $this->SaveData('vd_course_chapter',$data, "chapter_id='".$obj['chapter_id']."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'PDF has been updated successful');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update pdf');
        }
        return  $response;
    }

    // Get all course content
    public function coursecontentlist($course_id)
    {
        $getlist = $this->GetAllData('vd_course_chapter','*','course_id="'.$course_id.'"','','chapter_id ASC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>$getlist);
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getlist);
        }
        return  $response;
    }

    // Get all sub course content
    // public function subcoursecontentlist($chapter_id)
    // {
    //     $getlist = $this->GetAllData('vd_course_sub_chapter','*','chapter_id="'.$chapter_id.'"','','sub_chapter_id ASC','');
    //     if(empty($getlist)) {
    //         $response = array('success' => 0,'message'=> "No record found", 'data'=>$getlist);
    //     } else {
    //         $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getlist);
    //     }
    //     return  $response;
    // }

    // Edit Course content Assignment
    public function updatecoursecontentassign($obj,$file)
    {
        $upload_template= "";
        if($file['upload_template']['name']!='')
        { 
            if($file['upload_template']['error'] ==0)
            {       
                $src = $file['upload_template']['tmp_name'];
                $filEnc = time();
                $files=$file['upload_template']['name'];
                $files= $filEnc."_".$file['upload_template']['name'];
                $files = str_replace(array( '(', ')',':',' '), '', $files);
                $dest ="./uploads/upload_template/".$files;
                if(move_uploaded_file($src,$dest))
                {
                    $upload_template=$files;
                    if($obj['upload_template_edit'] != '') {
                        unlink("./uploads/upload_template/".$obj['upload_template_edit']);
                    }
                }
            } 
        } else {
            $upload_template = $obj['upload_template_edit'];
        }
        $data = array("title"=>$obj['title'],"instructions"=>$obj['instructions'],"confirmation_message"=>$obj['confirmation_message'],"upload_template"=>$upload_template,"updated_at"=>date("Y-m-d H:i:s"));
        if($obj['sub_chapter_id'] == 0) {
            $editdata = $this->SaveData('vd_course_chapter',$data, "chapter_id='".$obj['chapter_id']."'");
        } 
        // else {
        //     $editdata = $this->SaveData('vd_course_sub_chapter',$data, "sub_chapter_id='".$obj['sub_chapter_id']."'");
        // }
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Assignment has been updated successful');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update assignment');
        }
        return  $response;
    }

    // Edit Course Cover
    public function updatecoursecover($obj,$file)
    {
        $course_cover= "";
        if($file['course_cover']['name']!='')
        { 
            if($file['course_cover']['error'] ==0)
            {       
                $src = $file['course_cover']['tmp_name'];
                $filEnc = time();
                $files=$file['course_cover']['name'];
                $files= $filEnc."_".$file['course_cover']['name'];
                $files = str_replace(array( '(', ')',':',' '), '', $files);
                $dest ="./uploads/course_cover/".$files;
                if(move_uploaded_file($src,$dest))
                {
                    $course_cover=$files;
                    if($obj['course_cover_old'] != '') {
                        unlink("./uploads/course_cover/".$obj['course_cover_old']);
                    }
                }
            } 
        } else {
            $course_cover = $obj['course_cover_old'];
        }
        $data = array("course_cover"=>$course_cover,"updated_at"=>date("Y-m-d H:i:s"));
        $editdata = $this->SaveData('vd_courses',$data, "course_id='".$obj['course_id']."'");
        
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Course cover has been updated successful');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update course cover');
        }
        return  $response;
    }

    // Edit Profile
    public function upload_profile($obj,$file)
    {
        $profile_picture= "";
        if($file['profile_picture']['name']!='')
        { 
            if($file['profile_picture']['error'] ==0)
            {       
                $src = $file['profile_picture']['tmp_name'];
                $filEnc = time();
                $files=$file['profile_picture']['name'];
                $files= $filEnc."_".$file['profile_picture']['name'];
                $files = str_replace(array( '(', ')',':',' '), '', $files);
                $dest ="./uploads/user_profile/".$files;
                if(move_uploaded_file($src,$dest))
                {
                    $profile_picture=$files;
                    if($obj['profile_picture_old'] != '') {
                        unlink("./uploads/user_profile/".$obj['profile_picture_old']);
                    }
                }
            } 
        } else {
            $profile_picture = $obj['profile_picture_old'];
        }
        $data = array("profile_picture"=>$profile_picture,"updated_at"=>date("Y-m-d H:i:s"));
        $editdata = $this->SaveData('vd_users',$data, "user_id='".$obj['user_id']."'");
        
        if($editdata) {
            $_SESSION['VIR_SESSION']['profile_picture'] = $profile_picture;
            $response = array('success' => 1,'message'=> 'Profile picture has been updated successful');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to update profile picture');
        }
        return  $response;
    }

    // Learner List
    public function learnerlist($obj='')
    {
        $getlist = $this->GetAllData('vd_users','*','user_role="Learner"','','user_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Check Email
    public function check_email($obj)
    {
        $email = $obj['email'];
        $user_role = $obj['user_role'];
        $cond = 'email="'.$email.'" and user_role="'.$user_role.'"';
        if(isset($obj['user_id'])) {
            $user_id = $obj['user_id'];
            $cond = 'email="'.$email.'" and user_role="'.$user_role.'" and user_id != "'.$user_id.'"';
        }
        $checkdata = $this->GetSingleData('vd_users',$cond);
        if(empty($checkdata)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully");
        }
        return  $response;
    }

    // Check Mobile
    public function check_mobile($obj)
    {
        $mobile = $obj['mobile'];
        $user_role = $obj['user_role'];
        $user_id = $obj['user_id'];
        $cond = 'mobile_number="'.$mobile.'" and user_role="'.$user_role.'"';
        if($user_id) {
            $cond = 'mobile_number="'.$mobile.'" and user_role="'.$user_role.'" and user_id != "'.$user_id.'"';
        }
        $checkdata = $this->GetSingleData('vd_users',$cond);
        if(empty($checkdata)) {
            $response = array('success' => 0,'message'=> "No record found");
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully");
        }
        return  $response;
    }

    public function adduseraction($obj) {
        $name = $obj['name'];
        $email = $obj['email'];
        $mobile_number = $obj['mobile'];
        $password = $obj['password'];
        $user_role = $obj['user_role'];
        $is_send_email = $obj['is_send_email'];
        $is_send_email = 0;
        if($is_send_email == 'on') {
            $is_send_email = 1;
        }
        $pass = $this->PassHash_Model->hash($password);
        $data = array("name"=>$name,"email"=>$email,"mobile_number"=>$mobile_number,"password"=>$pass,"user_role"=>$user_role,"is_send_email"=>$is_send_email,'is_verify'=>1,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
       
        $insert_id = $this->SaveData('vd_users',$data);
        if($insert_id) {
            $response = array('success' => 1,'message'=> $user_role.' added successfully');
            if($is_send_email == 1) {
                $sub = $user_role.' Login Details';
                $msg = "Hello ".ucfirst($name).",<br><br>
                      
                    Your account has been created on VirtualDentist Online Courses Panel.<br>
                    Below are your system generated credentials.<br/>
                    <b>Please change the password immediately after login.</b><br/><br/>
                    <b>Email : </b>".$email."<br/>
                    <b>Password : </b>".$password."<br/><br/>
                    <b>Thank You!!</b><br>
                    <b>Team VirtualDentist!</b>";
                $this->sendemail($email,$sub,$msg);
            }
        } else {
            $response = array('success' => 1,'message'=> 'Failed to add '.$user_role);
        }
        return  $response;
    }

    public function edituseraction($obj) {
        $name = $obj['name'];
        $email = $obj['email'];
        $mobile_number = $obj['mobile'];
        $user_role = $obj['user_role'];
        $state_id = $obj['state_id'];
        $language_id = $obj['language_id'];
        $user_id = $obj['user_id'];
        
        $data = array("name"=>$name,"email"=>$email,"mobile_number"=>$mobile_number,"user_role"=>$user_role,"state_id"=>$state_id,"language_id"=>$language_id,"updated_at"=>date("Y-m-d H:i:s"));
       
        $insert_id = $this->SaveData('vd_users',$data,"user_id='".$user_id."'");
        if($insert_id) {
            $response = array('success' => 1,'message'=> $user_role.' edited successfully');
        } else {
            $response = array('success' => 1,'message'=> 'Failed to edit '.$user_role);
        }
        return  $response;
    }

    public function edituser_isverify($obj) {
        $email = $obj['email'];
        
        $data = array("is_verify"=>1,"updated_at"=>date("Y-m-d H:i:s"));
       
        $insert_id = $this->SaveData('vd_users',$data,"email='".$email."'");
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'User edited successfully');
        } else {
            $response = array('success' => 1,'message'=> 'Failed to edit user');
        }
        return  $response;
    }

    public function changestatus($obj) {
        $user_id = $obj['user_id'];
        $status = $obj['status'];
        $mystatus = 'Active';
        if($status == 'Active') {
            $mystatus = 'Deactive';
        }
        
        $data = array("status"=>$mystatus,"updated_at"=>date("Y-m-d H:i:s"));
       
        $insert_id = $this->SaveData('vd_users',$data,"user_id='".$user_id."'");
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'Status changed successfully');
        } else {
            $response = array('success' => 1,'message'=> 'Failed to changed status');
        }
        return  $response;
    }

    public function enrollcourseaction($obj) {
        $user_id = $obj['user_id'];
        $course_id = $obj['course_id'];
        $validity = $obj['validity'];
        $no_of_days = $obj['no_of_days'];
        $plan_id = $obj['plan_id'];
        $validity_type = 1;
        if($validity != 'Custom') {
            $getPlanData = $this->GetSingleData('vd_plans','plan_id="'.$plan_id.'"','access_days');
            if($getPlanData) {
                $no_of_days = $getPlanData->access_days;
                if($getPlanData->access_days) {
                    $validity_type = 3;
                    $no_of_days = $getPlanData->access_days;
                }
            }
        }

        $expiry = '';
        if($no_of_days != '') {
            $expiry = date('Y-m-d',strtotime('+'.$no_of_days.' day'));
            $validity_type = 3;
        }
        
        $data = array("user_id"=>$user_id,"course_id"=>$course_id,"assign_by"=>1,"validity"=>$validity,"validity_type"=>$validity_type,"plan_id"=>$plan_id,"no_of_days"=>$no_of_days,"expiry"=>$expiry,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
       
        $insert_id = $this->SaveData('vd_enroll_courses',$data);
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'Enroll Course successfully');
        } else {
            $response = array('success' => 1,'message'=> 'Failed to enroll course');
        }
        return  $response;
    }

    // Enroll Course List
    public function getenrollcourseslist($obj)
    {
        $user_id = $obj['user_id'];
        $getlist = $this->GetAllData('vd_enroll_courses','*','user_id="'.$user_id.'" and is_deleted=0','','enroll_id ASC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found",'data'=>array());
        } else {
            $i =0;
            foreach ($getlist as $row) {
                $getcourse = $this->GetSingleData('vd_courses','course_id="'.$row->course_id.'"','*');
                $getlist[$i]->courses = $getcourse;

                $getplan = $this->GetSingleData('vd_plans','plan_id="'.$row->plan_id.'"','*');
                $getlist[$i]->courses->plans = $getplan;
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Enroll Course List By Id
    public function getenrollcoursesbyid($obj)
    {
        $enroll_id = $obj['enroll_id'];
        $getlist = $this->GetSingleData('vd_enroll_courses','enroll_id="'.$enroll_id.'" and is_deleted=0','*');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found",'data'=>array());
        } else {
            $i =0;
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    public function deleteenrollcourse($enroll_id) {
        $data = array("is_deleted"=>1);
        $editdata = $this->SaveData('vd_enroll_courses',$data,"enroll_id='".$enroll_id."'");
        if($editdata) {
            $response = array('success' => 1,'message'=> 'Course has been removed successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to remove course');
        }
        return  $response;
    }

    // Purchase history List
    public function purchasehistorylist($obj)
    {
        $user_id = $obj['user_id'];
        $getlist = $this->GetAllData('vd_order','*','user_id="'.$user_id.'"','','order_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found",'data'=>array());
        } else {
            $i =0;
            foreach ($getlist as $row) {
                $getlist[$i]->order_id = $row->order_id;

                $getorders = $this->GetSingleData('vd_order_item','order_id="'.$row->order_id.'"','count(order_item_id) as totalorders');
                $getlist[$i]->totalorders = $getorders->totalorders;

                $getpayments = $this->GetSingleData('vd_order_payment','order_id="'.$row->order_id.'"','*');
                $getlist[$i]->payments = $getpayments;
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Order history List
    public function purchasecourselist($obj)
    {
        $order_id = $obj['order_id'];
        $getlist = $this->GetAllData('vd_order_item','*','order_id="'.$order_id.'"','','order_item_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found",'data'=>array());
        } else {
            $i =0;
            foreach ($getlist as $row) {
                $getlist[$i]->amount = $row->total;
                $getcourse = $this->GetSingleData('vd_courses','course_id="'.$row->course_id.'"','*');
                $getlist[$i]->courses = $getcourse;

                // $getplan = $this->GetSingleData('vd_plans','plan_id="'.$row->plan_id.'"','*');
                // $getlist[$i]->courses->plans = $getplan;
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully",'data'=>$getlist);
        }
        return  $response;
    }

    // Get assignment by course id
    public function getassignmentlist($course_id, $status)
    {
        // $getlist = $this->GetAllData('vd_course_assignment_history','*','course_id="'.$course_id.'"','enroll_id','assign_log_id DESC','');
        // $getlist = $this->db->query('SELECT m1.* FROM vd_course_assignment_history m1 LEFT JOIN vd_course_assignment_history m2 ON (m1.user_id = m2.user_id AND m1.assign_log_id < m2.assign_log_id) WHERE m2.assign_log_id IS NULL');
        $getlist = $this->db->query('SELECT * FROM vd_course_assignment_history WHERE assign_log_id IN (SELECT MAX(assign_log_id) AS assign_log_id FROM vd_course_assignment_history WHERE course_id='.$course_id.' GROUP BY enroll_id) ORDER BY assign_log_id DESC');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>array());
        } else {
            $i = 0;
            $getlist = $getlist->result();
            foreach ($getlist as $row) {
                $getusers = $this->GetSingleData('vd_users','user_id="'.$row->user_id.'"','name, email');
                $getlist[$i]->users = $getusers;

                $getcourse = $this->GetSingleData('vd_courses','course_id="'.$row->course_id.'"','title');
                $getlist[$i]->course = $getcourse->title;
                if($row->status != $status && $status != 0) {
                    unset($getlist[$i]);
                } 
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getlist);
        }
        return  $response;
    }
/* /. Admin Functions */

    public function updateprogress($obj) {
        $enroll_id = $obj['enroll_id'];
        $progress = $obj['progress'];
        $chapter_index = $obj['chapter_index'];
        $status = 3;
        if($progress > 0 && $progress < 100) {
            $status = 2;
        } else if($progress >= 100) {
            $status = 1;
            $progress = 100;
        }

        if($progress) {
            $data = array("progress"=>$progress,"chapter_index"=>$chapter_index,"status"=>$status,"updated_at"=>date("Y-m-d H:i:s"));
        } else {
            $data = array("status"=>$status,"chapter_index"=>$chapter_index,"updated_at"=>date("Y-m-d H:i:s"));
        }
        $insert_id = $this->SaveData('vd_enroll_courses',$data,"enroll_id='".$enroll_id."'");
        if($insert_id) {
            $getdata = $this->GetSingleData('vd_enroll_courses','enroll_id="'.$enroll_id.'"','progress');
            $response = array('success' => 1,'message'=> 'Progress added successfully', "data" => $getdata);
        } else {
            $response = array('success' => 1,'message'=> 'Failed to add progress', "data" => array());
        }
        return  $response;
    }

    // Upload Assignment
    public function uploadassignmentreply($obj)
    {
        $note = '';
        if(isset($obj['note'])) {
            $note = $obj['note'];
        }
        $status = '2';
        if(isset($obj['status'])) {
            $status = $obj['status'];
        }
        $course_id = $obj['course_id'];
        $user_id = $obj['user_id'];
        $enroll_id = $obj['enroll_id'];

        $message = '';
        if(isset($obj['message']) && $obj['message'] != "") {
            $message = $obj['message'];
        }
        $img = '';
        if(isset($obj['assignment_upload']) && $obj['assignment_upload'] != "") {
            $data = 'data:application/pdf;base64,'.$obj['assignment_upload'];
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $img = 'course_assignment_'.$this->randomPassword().'.pdf';
            file_put_contents('./uploads/assignment_upload/'.$img, $data);
        }

        $data = array("user_id"=>$user_id,"course_id"=>$course_id,"enroll_id"=>$enroll_id,"notes"=>$note,"message"=>$message,"file"=>$img,"status"=>$status,"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"));
        $insert_id = $this->SaveData('vd_course_assignment_history',$data);
        if($insert_id) {
            $response = array('success' => 1,'message'=> 'Assignment submitted successfully');
        } else {
            $response = array('success' => 0,'message'=> 'Failed to submit assignment');
        }
        return  $response;
    }

    // Get assignment history by userid and course id
    public function getassignmenthistory($obj)
    {
        $getlist = $this->GetAllData('vd_course_assignment_history','*','course_id="'.$obj['course_id'].'" and user_id="'.$obj['user_id'].'" and enroll_id="'.$obj['enroll_id'].'"','','assign_log_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>array());
        } else {
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getlist);
        }
        return  $response;
    }

    // Get assignment history by type
    public function assignmenthistorybytype($obj)
    {
        $getlist = $this->GetAllData('vd_course_chapter','*','chapter_type=2','','chapter_id DESC','');
        if(empty($getlist)) {
            $response = array('success' => 0,'message'=> "No record found", 'data'=>array());
        } else {
            $i = 0;
            foreach ($getlist as $row) {
                // $getdata = $this->GetAllData('vd_course_assignment_history','assign_log_id, status','course_id="'.$row->course_id.'"','enroll_id','assign_log_id DESC');
                $getdata = $this->db->query('SELECT * FROM vd_course_assignment_history WHERE assign_log_id IN (SELECT MAX(assign_log_id) AS assign_log_id FROM vd_course_assignment_history WHERE course_id='.$row->course_id.' GROUP BY enroll_id) ORDER BY assign_log_id DESC');
                $getlist[$i]->reviewedcount = 0;
                $getlist[$i]->underreviewcount = 0;
                $getlist[$i]->rejectedcount = 0;
                if($getdata->result()) {
                    foreach ($getdata->result() as $hist) {
                        if($hist->status == 1) {
                            $getlist[$i]->reviewedcount += 1;
                        } else if($hist->status == 2) {
                            $getlist[$i]->underreviewcount += 1;
                        } else if($hist->status == 3) {
                            $getlist[$i]->rejectedcount += 1;
                        }
                    }
                }
                $getcourse = $this->GetSingleData('vd_courses','course_id="'.$row->course_id.'"','title');
                $getlist[$i]->course = $getcourse->title;
                $i++;
            }
            $response = array('success' => 1,'message'=> "Get data successfully", 'data'=>$getlist);
        }
        return  $response;
    }
}
?>