<?php

class api_model extends CI_Model {

     public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('email');
       

    }
    public function NewGuid() {
        $s = strtoupper(md5(uniqid(rand(), true)));
        $guidText = substr($s, 0, 8) . '-' .
                substr($s, 8, 4) . '-' .
                substr($s, 12, 4) . '-' .
                substr($s, 16, 4) . '-' .
                substr($s, 20);
        return $guidText;
    }

    public function randomPassword()
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 4; $i++)
        {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function sendemail($emails,$sub,$msg)
  {
    $message = '';
        $message = '<html><head><title>Virtual Dentist</title></head><body><div style="overflow: hidden;" class="a3s" id=":16z"><div class="adM"></div><div><div class="adM"></div><div style="border-left: 1px solid #ddd;min-height: 52px;border-right: 1px solid #ddd;border-top: 1px solid #ddd;color: #666;margin: 0 auto;padding: 11px 0 10px 12px;"><div class="adM"> </div><a target="_blank" href="'.base_url().'"><img style="border:0 none;color:#666; vertical-align:middle;" src="'.base_url().'assets/images/logo.png" alt="virtualdentist" class="CToWUd"></a>  </div><div style="border:1px solid #ddd;color:#666;margin:0 auto;padding:0 20px 20px"><div style="color:#666"><h4 style="font-family:arial!important"> </h4><div style="font-size:12px;line-height:20px;font-family:arial!important">'.$msg.' </div></div></div><div><div style="color:#606060;font-family:Helvetica,Arial,sans-serif;font-size:11px;line-height:150%; padding-right:20px;padding-bottom:5px;padding-left:20px;text-align:center">This email is sent to you as your request for subscription of <span class="il">VirtualDentist</span>.</div><div style="background-color:rgb(51,51,51)!important;margin:0px auto;padding:8px 46px;min-height:50px;clear:both"><div style="width:60%;float:left;color:#fff;padding-top:10px;font-size:12px"> <span>Copyright &copy; 2020 Virtual Dentist - All Rights Reserved.</span></div><div style="float:right"><div style="overflow:hidden;padding-top:5px"><a target="_blank" href="https://www.facebook.com/virtualdentistindia/" style="color:#666;display:block;float:left;margin-left:15px"><img src="https://img.icons8.com/fluent/48/000000/facebook-new.png"/><a target="_blank" href="" style="color:#666;display:block;float:left;margin-left:8px"><img src="https://img.icons8.com/fluent/48/000000/twitter.png"/></a><a target="_blank" style="color:#666;display:block;float:left;margin-left:12px" href=""><img src="https://img.icons8.com/color/48/000000/pinterest--v1.png"/></a></div></div></div></div></div><div class="yj6qo"></div><div class="adL"></div></div></body></html>';
    $this->email->set_mailtype("html");
    $this->email->from('info@virtualdentist.in', 'Virtual Dentist');
    $this->email->to($emails);
    $this->email->subject($sub);
    $this->email->message($message);
    $this->email->send();
            
        return 1;
  }

    public function sendmessage($msg,$phone)
    { 
        $curl = curl_init();
        $msg = str_replace("₹","Rs.",$msg);

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.msg91.com/api/v2/sendsms?country=91&sender=&route=&mobiles=&authkey=&encrypt=&message=&flash=&unicode=&schtime=&afterminutes=&response=&campaign=",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{ \"sender\": \"VIRDEN\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"".$msg."\", \"to\": [ \"". $phone."\" ] } ] }",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTPHEADER => array(
            "authkey: 327543AbIU7KVStyD5ea6c82bP1",
            "content-type: application/json"
          ),
        ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
        return 1;

    }

    public function sendflowmessage($phone,$flowId,$content) {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.msg91.com/api/v5/flow/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n  \"flow_id\": \"".$flowId."\",\n  \"mobiles\": \"91".$phone."\"".$content."}",
        CURLOPT_HTTPHEADER => array(
          "authkey: 327543AbIU7KVStyD5ea6c82bP1",
          "content-type: application/JSON"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      // if ($err) {
      //   echo "cURL Error #:" . $err;
      // } else {
      //   echo $response;
      // }
      return 1;
  }

    public function row_array_value($row)
   {
    foreach($row as $key => $value) {  
        if ($value === null) {
            $row[$key] = 'N/A';
          }
        if (isset($row['password'])) {
          $row['password'] = '$$$';
          $row['auth_token'] = '$$$'; 
      }
    }
    return $row;
   }

  public function result_array_value($row)
   {
    foreach($row as $keyy => $values) {
    
      foreach($row[$keyy] as $key => $value) {  
          if ($value === null) {
              $row[$keyy]->$key = '***';
            }
            if (isset($row[$keyy]->password)) {
              $row[$keyy]->password = '$$$';
              $row[$keyy]->auth_token = '$$$';
            }
            if (isset($row[$keyy]->space_cart_id)) {
              $row[$keyy]->this_is = 'space';
            }elseif (isset($row[$keyy]->service_cart_id)){
              $row[$keyy]->this_is = 'service';
            }
      } 

    }
    
    return $row;
   }


    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
  

     public function get_subscription($obj){

        $reg=$obj['reg_no'];
        $email=$obj['email'];
        $check_reg = $this->db->query("SELECT reg_number FROM vd_doctors WHERE reg_number = '".$reg."'  AND is_verified = '1' ")->result();
         $check_email = $this->db->query("SELECT email_id FROM vd_doctors WHERE email_id = '".$email."'  AND is_verified = '1' ")->result();
        if(count($check_reg) > 0){
            $json = array('status' => false , 'msg' => 'Registration Number Already Exists.' );
        }else if(count($check_email) > 0){

          $json = array('status' => false , 'msg' => 'Email-id Already Exist.' );

        }else{

                $clinic_name = isset($obj['clinic_name']) ? $obj['clinic_name']:"";
                $establish_year = isset($obj['clinic_year']) ? $obj['clinic_year']:"";
                $address = isset($obj['clinic_add']) ? $obj['clinic_add']:"";
                $address1 = isset($obj['add']) ? $obj['add']:"";
                $country = isset($obj['country1']) ? $obj['country1']:"";
                $state = isset($obj['state1']) ? $obj['state1']:"";
                $city = isset($obj['city1']) ? $obj['city1']:"";
                $pincode = isset($obj['zipcode']) ? $obj['zipcode']:"";
                $type=isset($obj['type']) ? $obj['type']:"";
                $reg=isset($obj['reg_no']) ? $obj['reg_no']:"";
                $name=isset($obj['name']) ? $obj['name']:"";
                $email=isset($obj['email']) ? $obj['email']:"";
                $phone=isset($obj['phone']) ? $obj['phone']:"";
                $specialization =isset($obj['specialization']) ? $obj['specialization']:"";
                $pass_y =isset($obj['pass_y']) ? $obj['pass_y']:"";
                $plan =isset($obj['plan']) ? $obj['plan']:"";
                $url = "0";
                $url1 = "0";
                $url2 = "0";
                        //log_message('error','profile path' . $profile);
                        
                        // if(!empty($profile)){
                        //         $file_name = basename($profile);
                        //         $url = base_url().'uploads/profile_pics/'.$file_name;
                        //         $file = 'uploads/profile_pics/'.$file_name;
                        //        file_put_contents($file, $profile);  
                        //     }
                        // if(!empty($degree)){
                        //         //$profile_pic_decode = base64_decode($degree);
                        //         $file_name = basename($degree);
                        //         $url1 = base_url().'uploads/degree/'.$file_name;
                        //         $file = 'uploads/degree/'.$file_name;
                        //         file_put_contents($file, $degree);    
                        //     }
                        // if(!empty($cert)){
                        //         //$profile_pic_decode = base64_decode($cert);
                        //         $file_name = basename($cert);
                        //         $url2 = base_url().'uploads/degree/'.$file_name;
                        //         $file = 'uploads/degree/'.$file_name;
                        //         file_put_contents($file, $cert);    
                        //     }
                    $fee = isset($obj['fee']) ? $obj['fee']:"";
                    $date = date('Y-m-d');  


                    if($type == 'doctor'){
                        $data = array(
                                'clinic_name' =>  $clinic_name,
                                'establish_year' => $establish_year,
                                'address' => $address,
                                'country' => $country,
                                'state' => $state,
                                'city' => $city,
                                'pincode' => $pincode,
                                'created_date' => $date,
                        );
                         $this->db->insert("vd_clinic",$data);
                        $insert_id=$this->db->insert_id();


                        $data2 = array( 
                                        'doctor_type'       =>$type, 
                                       'reg_number'     =>$reg, 
                                       'name'           =>$name, 
                                       'email_id'       =>$email,
                                       'password'       =>'',
                                       'mobile_number'  => $phone,
                                       'specialization' =>$specialization,
                                       'c_country'      => $country,
                                       'c_state'      => $state,
                                       'c_city'      => $city,
                                        'c_pincode'      => $pincode,
                                       'passing_year'   => $pass_y,
                                       'profile_pic'    => $url ,
                                       'counsult_fee'   =>$fee,
                                       'degree_cert'    => $url1,
                                       'reg_cert'       => $url2,
                                       'clinic_id'      =>$insert_id,
                                       'plan_id'      => $plan,
                                       'created_date'   =>  $date                  
                                );
                                $this->db->insert("vd_doctors",$data2);
                                $insert_id1 =$this->db->insert_id();


                                $json = array('status' => true, "data"=>$insert_id1 , 'msg' => 'Doctor Registration Successfully' );

                         }else{


                             $data2 = array( 
                                    'doctor_type'       =>$type, 
                                   'reg_number'     =>$reg, 
                                   'name'           =>$name, 
                                   'email_id'       =>$email,
                                   'mobile_number'  => $phone,
                                   'specialization' =>$specialization,
                                   'c_address'      => $address1, 
                                   'c_country'      => $country,
                                   'c_state'        => $state,
                                   'c_city'         => $city,
                                   'c_pincode'      => $pincode,
                                   'passing_year'   => $pass_y,
                                   'profile_pic'    => $url ,
                                   'counsult_fee'   =>$fee,
                                   'degree_cert'    => $url1,
                                   'reg_cert'       => $url2,
                                   'clinic_id'      =>'0',
                                   'plan_id'        => $plan,
                                   'created_date'   =>  $date                  
                            );
                            $this->db->insert("vd_doctors",$data2);
                            $insert_id =$this->db->insert_id();

                             $json = array('status' => true, "data"=>$insert_id , 'msg' => 'Counsultant Registration Successfully' );
                        }
                }

        return $json;
    }


   public function setpassword($obj){

      $reg_num = $obj['reg_num']; 
      $password = md5($obj['password']); 
      $acc_status = $obj['acc_status'];
      $pay_id = $obj['pay_id'];
      $date = date('Y-m-d');

      $date1 = strtotime($date);
      $new_date1 = strtotime('+ 1 year', $date1);
        $new_date = date('Y-m-d', $new_date1);


      $data = array(
                    'password' => $password
                    // 'pay_status' => $acc_status,
                    // 'acc_status' => '1',
                    // 'starts_on' => $date,
                    // 'ends_on' => $new_date,
                    // 'pay_id' => $pay_id
            );

            $this->db->where("reg_number",$reg_num); 
            $this->db->where("is_verified","1");
           $update = $this->db->update("vd_doctors",$data);

           if($update){

            $json = array('status' => true,  'msg' => 'Password added successfully, you can login now!' );

           }else{

            $json = array('status' => false, 'msg' => 'Your Password Not Set' );

           }

           return $json;

    }


    public function update_profile(){

      $doctor_id = $this->input->post('doctor_id');

      $imgname = $this->db->query("SELECT profile_pic From  vd_doctors WHERE doctor_id = $doctor_id")->result();

      $img = $imgname[0]->profile_pic;

       if(file_exists("uploads/profile_pics/".$img)){
                        unlink("uploads/profile_pics/".$img);  
                        log_message('error', 'unlinking file of profile pic '.$img);
                    }


      if (!empty($_FILES['file']['name'])) {
         $profile = time().$_FILES['file']['name'];
          move_uploaded_file($_FILES['file']['tmp_name'], "./uploads/profile_pics/$profile");
          $data = array(
          'profile_pic' => $profile );      
      $this->db->where('doctor_id', $_POST['doctor_id'] );
      $insert_id = $this->db->Update("vd_doctors",$data);

      }
        
        
     //$img =  '<img class="mx-auto img-fluid img-circle d-block" alt="avatar" id="avtar" src="'.base_url();.'"uploads/profile_pics/'.$profile.'">';

     // return  $json = array('status' => true,'msg'=> 'image updated' , 'data'=>$data);
     
     return $profile;


    }

  public function update_document()
  {
    if (!empty($_FILES['profile']['name'])) {
      $profile = time().$_FILES['profile']['name'];
          move_uploaded_file($_FILES['profile']['tmp_name'], "./uploads/profile_pics/$profile");
          $data = array(
          'profile_pic' => $profile );      
      $this->db->where('doctor_id', $_POST['doctor_id'] );
      $insert_id = $this->db->Update("vd_doctors",$data);
    }
    if (!empty($_FILES['degree']['name'])) {
       $degree = time().$_FILES['degree']['name'];
           move_uploaded_file($_FILES['degree']['tmp_name'], "./uploads/degree/$degree");
          $data = array( 
          'degree_cert' => $degree );      
      $this->db->where('doctor_id', $_POST['doctor_id'] );
      $insert_id = $this->db->Update("vd_doctors",$data); 
    }
     if (!empty($_FILES['cert']['name'])) {
       $cert = time().$_FILES['cert']['name'];
           move_uploaded_file($_FILES['cert']['tmp_name'], "./uploads/degree/$cert");
          $data = array( 
          'reg_cert' => $cert );      
      $this->db->where('doctor_id', $_POST['doctor_id'] );
      $insert_id = $this->db->Update("vd_doctors",$data); 
    }
    
    if($_POST['doctor_id']) {
      $result1 = $this->db->query("SELECT name  FROM `vd_doctors` WHERE  `doctor_id` = '".$_POST['doctor_id']."' ")->row();
      if($result1 != "") {
        // $text = "Dr. ".ucfirst($name)." ".$email." has been subscribed to virtual dentist. Kindly approve the subscription.";
        $mobile = '9820104128';
        // $this->sendmessage($text,$mobile);
        $content = ",\n  \"name\": \"".ucfirst($result1->name)."\"\n";
        $this->sendflowmessage($mobile,'611a4923a0546158d04e5ab9',$content);
      }
    }
     return  $json = array('status' => true,'msg'=> 'Thank you! Your details has been submitted successfully. We will verify your details & if it is approved you will get a payment link soon on your registered email Id');
  }

    public function search($obj){
      $city = $obj['city']; 
      $state = $obj['state']; 
      $pincode = $obj['pincode'];
      $specialization = $obj['specialization'];
       $page  = 1;
        $limit = 5;

         if(!empty($obj['page'])){
            $page = $obj['page'];
        }

        if(!empty($obj['limit'])){
            $limit = $obj['limit'];
        }
        $offset = ($page - 1) * $limit;
        $offset2 = $page * $limit;
      
      $data = $this->db->query("SELECT A.*,B.*,(SELECT AVG(rating) FROM vd_feedback WHERE doctor_id = A.doctor_id) as stars FROM `vd_doctors`AS A INNER JOIN `vd_clinic` AS B ON B.`clinic_id` = A.`clinic_id` WHERE  A.`specialization` = '$specialization' AND B.`pincode` = '$pincode' AND A.`is_verified` = '1' AND A.`acc_status` = '1'")->result_array();
      $count = $this->db->query("SELECT A.*,B.*,(SELECT AVG(rating) FROM vd_feedback WHERE doctor_id = A.doctor_id) as stars FROM `vd_doctors`AS A INNER JOIN `vd_clinic` AS B ON B.`clinic_id` = A.`clinic_id` WHERE A.`specialization` = '$specialization' AND B.`pincode` = '$pincode' AND A.`is_verified` = '1'  AND A.`acc_status` = '1' ")->result_array();

    
        if (count($count) == 0) {
          $count = false;
        }else{
          $count = true;
        }

         $json = array('status' => true,'data'=> $data , 'count'=> $count);

         if($data){ 

         return  $json;
       }else{
        return  $json = array('status' => false,'msg'=> 'No record found related to your search');
       }

    }

 public function gettimeslot($obj){


       $doctor_id = $obj['doctor_id'];
       $date = $obj['date'];

        $result1 = $this->db->query("SELECT morningslot  FROM `vd_work_schedule` WHERE  `doctor_id` = '$doctor_id' ")->row();

       
         $result2 = $this->db->query("SELECT eveningslot  FROM `vd_work_schedule` WHERE  `doctor_id` = '$doctor_id' ")->row();

        

         $result3 = $this->db->query("SELECT schedule_id FROM `vd_appointment` WHERE  `doctor_id` = '$doctor_id' AND `appointment_date` = '$date' AND `status` = '1'")->result();
         $result4 = $this->db->query("SELECT schedule_id FROM `vd_appointment` WHERE  `doctor_id` = '$doctor_id' AND `appointment_date` = '$date' AND `status` = '0'")->result();

         $res = array_merge($result3,$result4);

         if($result1){

          $json = array('status' => true,'morning'=> $result1, 'evening'=>$result2 , 'schedule' => $res);

         }else{

           $json = array('status' => false,'msg' => "No time slot added by the doctor");
         }

         
         return  $json;


    }
    
    
     public function doctor_login($obj='') {
        $email=$obj['email'];
         $password=md5($obj['password']);
         $result = $this->db->query("SELECT doctor_id FROM `vd_doctors`  WHERE `email_id` = '".$email."' AND `password` = '".$password."' AND `profile_pic` != '0' ")->row();
         if($result){
            $res1 =  $this->db->query("SELECT * FROM `vd_doctors`  WHERE `doctor_id` = '".$result->doctor_id."' AND `is_verified` = '1' AND `acc_status` = '1'")->result();
            $res = $this->db->query("SELECT doctor_type FROM `vd_doctors` WHERE `email_id` = '".$email."' AND `password` = '".$password."' AND `is_verified` = '1' AND `acc_status` = '1' ")->row();
            if(count($res1)>0){
                $json = array('status' => true, 'data' => $res1 , 'msg' => 'Login Successfully' , 'doctor_type' => $res);
            }else{
                
                $json = array('status' => false, 'msg' => 'Your account has been deactivated, Please contact administrator');
                
            }
         }else {
             $json = array('status' => false, 'msg' => 'Incorrect email ID or password' );
         }
         
       
        return $json;
    }

    public function update_doctor($obj){
        $clinic_id = $obj['clinic_id'];
        $doctor_id = $obj['doctor_id'];
        $type = $obj['type'];
        $clinic_name = $obj['clinic_name'];
        $establish_year = $obj['clinic_year'];
        $address = $obj['clinic_add'];
        $country = $obj['country1'];
        $state = $obj['state1'];
        $city = $obj['city1'];
        $pincode =  $obj['zipcode'];
        $name= $obj['name'];
        $phone= $obj['phone'];
        $specialization =$obj['specialization'];
        $pass_y =$obj['pass_y'];
        $fee =  $obj['fee'];
        $date = date('Y-m-d H:i:s'); 


        if($type == 'doctor'){
            $data = array(
                    'clinic_name' =>  $clinic_name,
                    'establish_year' => $establish_year,
                    'address' => $address,
                    'country' => $country,
                    'state' => $state,
                    'city' => $city,
                    'pincode' => $pincode,
                    'created_date' => $date,
            );

             $this->db->where("clinic_id",$clinic_id); 
            $this->db->update("vd_clinic",$data);
             


            $data2 = array( 
                            'doctor_type'   =>$type, 
                          
                           'name'           =>$name, 
                           
                         
                           'mobile_number'  => $phone,
                           'specialization' =>$specialization,
                           'passing_year'   => $pass_y,
                           'counsult_fee'   =>$fee,
                           'clinic_id'      =>$clinic_id,
                           'created_date'   =>  $date                  
                    );
                   
                    $this->db->where("doctor_id",$doctor_id); 
                    $this->db->update("vd_doctors",$data2);


                    $result1 = $this->db->query("SELECT A.*,B.* FROM `vd_doctors`AS A INNER JOIN `vd_clinic` AS B ON B.`clinic_id` = A.`clinic_id` WHERE  A.`doctor_id` = '$doctor_id' ")->result();
                   

                    $json = array('status' => true, 'data'=> $result1, 'msg' => 'Data updated successfully' );

                 }else{


                     $data2 = array( 
                            'doctor_type'       =>$type, 
                           
                           'name'           =>$name, 
                           
                           'mobile_number'  => $phone,
                           'specialization' =>$specialization,
                           'passing_year'   => $pass_y,
                           'counsult_fee'   =>$fee,
                           'clinic_id'      =>'0',
                           'created_date'   =>  $date                  
                    );
                   $this->db->where("doctor_id",$doctor_id); 
                    $this->db->update("vd_doctors",$data2);
                   // return $insert_id1;

                     $json = array('status' => true,  'msg' => 'Counsultant updated successfully' );
                }

               
          return  $json;      
                
    }
    
    
    public function update_consultant($obj){

        $doctor_id = $obj['doctor_id'];
        $type = $obj['type'];
        $address = $obj['clinic_add'];
        $country = $obj['country1'];
        $state = $obj['state1'];
        $city = $obj['city1'];
        $pincode =  $obj['zipcode'];
        $name= $obj['name'];
        $phone= $obj['phone'];
        $specialization =$obj['specialization'];
        $pass_y =$obj['pass_y'];
      
        $date = date('Y-m-d H:i:s'); 

         $data2 = array( 
                            'doctor_type'       =>$type, 
                           
                           'name'           =>$name, 
                           
                           'mobile_number'  => $phone,
                           'specialization' =>$specialization,
                           'passing_year'   => $pass_y,
                           'c_address' => $address,
                            'c_country' => $country,
                            'c_state' => $state,
                            'c_city' => $city,
                            'c_pincode' => $pincode,
                           'clinic_id'      =>'0',
                           'created_date'   =>  $date                  
                    );
                   $this->db->where("doctor_id",$doctor_id); 
                    $this->db->update("vd_doctors",$data2);
                   // return $insert_id1;

                     $json = array('status' => true,  'msg' => 'Counsultant updated successfully' );

                       return  $json; 


    }

    public function get_doctor($obj){

         $doctor_id = $obj['doctor_id'];

         $result1 = $this->db->query("SELECT A.*,B.* FROM `vd_doctors`AS A INNER JOIN `vd_clinic` AS B ON B.`clinic_id` = A.`clinic_id` WHERE  A.`doctor_id` = '$doctor_id' ")->result();

         $feedback = $this->db->query("SELECT A.*,B.* FROM `vd_feedback`AS A INNER JOIN `vd_patient_details` AS B ON B.`patient_id` = A.`patient_id` WHERE  A.`doctor_id` = '$doctor_id' ")->result();

         $json = array('status' => true,'data'=> $result1, 'feedback'=> $feedback);

         return  $json;

    }
     
     public function request_otp($obj){
        $location = $obj['location'];
        $name = $obj['name'];
        $contact = $obj['contact'];
        $date = date('Y-m-d H:i:s');
        $otp = $this->randomPassword(); 

         $check_patient = $this->db->query("SELECT patient_id  FROM vd_patient_details WHERE mobile = '".$contact."' ")->row();

        if($check_patient){

          $data1 = array(
                    'patient_name' =>  $name,
                    'mobile' => $contact,
                    'location' => $location,
                    'otp' => $otp,
                    'otp_time' => $date,
                    'created_date' => $date
            );

          $this->db->where("patient_id",$check_patient->patient_id); 
          $this->db->update("vd_patient_details",$data1);


        }else{


        $data = array(
                    'patient_name' =>  $name,
                    'mobile' => $contact,
                    'location' => $location,
                    'otp' => $otp,
                    'otp_time' => $date,
                    'created_date' => $date
            );
        $this->db->insert("vd_patient_details",$data);
        $insert_id1 =$this->db->insert_id();

        }

        //$text = "Virtual Dentist: Your one time password (OTP) to verify your mobile number is $otp. This OTP will expire after 15 minutes";
        $mobile = $obj['contact'];
        //$this->sendmessage($text,$mobile);
        $content = ",\n  \"otp\": \"".$otp."\"\n";
        $this->sendflowmessage($mobile,'60c31f401eadfd56541d3f8f',$content);
        $json = array('status' => true,'data'=> $otp,'msg'=>'OTP sent successfully');
        return  $json;
     }

     public function book_appointment($obj){

        $otp = $obj['otp'];
        $doctor_id = $obj['doctor_id'];
        $scedule_id = $obj['scedule_id'];
        $schedule_eve_id = $obj['schedule_eve_id'];
         //$adate = date_create($obj['appointment_date']);
          $appointment_date = $obj['appointment_date'];

         $date = date('Y-m-d H:i:s');
        $check_otp = $this->db->query("SELECT patient_id , mobile FROM vd_patient_details WHERE otp = '".$otp."' AND ('".$date."' < DATE_ADD(otp_time , INTERVAL 15 MINUTE))")->row();
        if($check_otp){

            $data = array(
                    'patient_id' =>  $check_otp->patient_id,
                    'doctor_id' => $doctor_id,
                    'schedule_id' => $scedule_id,
                    'schedule_eve_id'=>$schedule_eve_id,
                    'appointment_date' => $appointment_date,
                    'created_date' => $date
            );
        $this->db->insert("vd_appointment",$data);
        $insert_id1 =$this->db->insert_id();

        

          $data1 = array(
              'otp'=> '',
              'otp_time' =>''
          );

          $this->db->where("patient_id",$check_otp->patient_id); 
        $this->db->update("vd_patient_details",$data1);

             $json = array('status' => true,'msg'=> "Your appointment has been fixed");

       

        }else{

          $json = array('status' => false,'msg'=> "Check your OTP");

        }

        return  $json;

     }

     public function add_appointment($obj){
      $doctor_id = $obj['doctor_id'];
     // $adate = date_create($obj['appointment_date']);
      $name = $obj['name'];
      $location = $obj['location'];
      $mobile = $obj['mobile'];
      $scedule_id = $obj['timeid'];
      $schedule_eve_id = $obj['timeeveid'];
       $date = date('Y-m-d H:i:s');

       $appointment_date = $obj['appointment_date'];

         $check_patient = $this->db->query("SELECT patient_id  FROM vd_patient_details WHERE mobile = '".$mobile."' ")->row();

        if($check_patient){

          $data1 = array(
                    'patient_name' =>  $name,
                    'mobile' => $mobile,
                    'location' => $location,
                    'otp' => '0',
                    'created_date' => $date
            );

          $this->db->where("patient_id",$check_patient->patient_id); 
          $this->db->update("vd_patient_details",$data1);


          $data = array(
                    'patient_id' =>  $check_patient->patient_id,
                    'doctor_id' => $doctor_id,
                    'schedule_id' => $scedule_id,
                    'schedule_eve_id'=>$schedule_eve_id,
                    'appointment_date' => $appointment_date,
                    'created_date' => $date
            );
        $this->db->insert("vd_appointment",$data);
        $insert_id1 =$this->db->insert_id();



        }else{

           $data1 = array(
                    'patient_name' =>  $name,
                    'mobile' => $mobile,
                    'location' => $location,
                    'otp' => '0',
                    'created_date' => $date
            );

          

           $this->db->insert("vd_patient_details",$data1);
        $insert_id =$this->db->insert_id();


          $data = array(
                    'patient_id' => $insert_id,
                    'doctor_id' => $doctor_id,
                    'schedule_id' => $scedule_id,
                    'schedule_eve_id'=>$schedule_eve_id,
                    'appointment_date' => $appointment_date,
                    'created_date' => $date
            );
        $this->db->insert("vd_appointment",$data);
        $insert_id1 =$this->db->insert_id();

        }

         $result1 = $this->db->query("SELECT A.*,B.* FROM `vd_appointment`AS A 
        INNER JOIN `vd_patient_details` AS B ON B.`patient_id` = A.`patient_id`
         
        WHERE  A.`doctor_id` = '$doctor_id' ")->result();

         $json = array('status' => true,'msg'=> "Your appointment has been fixed" ,'data'=> $result1);

         return $json;

     }
     
     public function add_schedule($obj){

        $doctor_id = $obj['doctor_id'];
        $day = '1';
        $mor_start = $obj['mor_start'];
        $mor_end = $obj['mor_end'];
        $eve_start = $obj['eve_start'];
        $eve_end = $obj['eve_end'];
        $time_slote = $obj['time_slote'];
        $date = date('Y-m-d H:i:s');

        $check_doc = $this->db->query("SELECT * FROM `vd_work_schedule` WHERE `doctor_id` = '".$doctor_id."'")->result();
        
        if(count($check_doc) > 0){

           $start = new DateTime($mor_start);
        $end = new DateTime($mor_end);
        $start_time = $start->format('h:i A');
        $end_time = $end->format('h:i A');
        $i=0;
        $array_of_time =  array();

        while(strtotime($start_time) <= strtotime($end_time)){
            $start = $start_time;
            $end = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
            $start_time = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
            $i++;
            if(strtotime($start_time) <= strtotime($end_time)){
                //$time[$i] 
               // $time[$i]['end'] = $end;
              $morningslote[] = $start;
                 
            }
        }

         $start_e = new DateTime($eve_start);
        $end_e = new DateTime($eve_end);
        $start_time_e = $start_e->format('h:i A');
        $end_time_e = $end_e->format('h:i A');
        $i=0;
         $array_of_time2 = array();
        while(strtotime($start_time_e) <= strtotime($end_time_e)){
            $start_e = $start_time_e;
            $end_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
            $start_time_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
            $i++;
            if(strtotime($start_time_e) <= strtotime($end_time_e)){

              $eveningslote[] = $start_e; 
            }
        }

         
          $m = implode(",", $morningslote);
        
          $e = implode(",", $eveningslote);

           $data1 = array(
                'doctor_id' => $doctor_id,
                'day' => $day,
                'mor_start' => $mor_start,
                'mor_end'=>  $mor_end ,
                'morningslot'=> $m,
                'eve_start' => $eve_start,
                'eve_end' => $eve_end,
                'eveningslot' =>$e,
                'time_slote' => $time_slote,
                'created_date' =>$date

        );


            $this->db->where("doctor_id",$doctor_id); 
          $this->db->update("vd_work_schedule",$data1);

          $check_doc = $this->db->query("SELECT * FROM `vd_work_schedule` WHERE `doctor_id` = '".$doctor_id."'")->result();


      $json = array('status' => true,'data'=> $check_doc);

        }else{

         $start = new DateTime($mor_start);
        $end = new DateTime($mor_end);
        $start_time = $start->format('h:i A');
        $end_time = $end->format('h:i A');
        $i=0;
        $array_of_time =  array();
        while(strtotime($start_time) <= strtotime($end_time)){
            $start = $start_time;
            $end = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
            $start_time = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
            $i++;
            if(strtotime($start_time) <= strtotime($end_time)){
                
              $morningslote[] = $start;
                 
            }
        }

          

         $start_e = new DateTime($eve_start);
        $end_e = new DateTime($eve_end);
        $start_time_e = $start_e->format('h:i A');
        $end_time_e = $end_e->format('h:i A');
        $i=0;
         $array_of_time2 = array();
        while(strtotime($start_time_e) <= strtotime($end_time_e)){
            $start_e = $start_time_e;
            $end_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
            $start_time_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
            $i++;
            if(strtotime($start_time_e) <= strtotime($end_time_e)){

              $eveningslote[] = $start_e; 
            }
        }

          $m = implode(",", $morningslote);      
          $e = implode(",", $eveningslote);


        $data = array(
                'doctor_id' => $doctor_id,
                'day' => $day,
                'mor_start' => $mor_start,
                'mor_end'=>  $mor_end ,
                'morningslot'=> $m,
                'eve_start' => $eve_start,
                'eve_end' => $eve_end,
                'eveningslot' =>$e,
                'time_slote' => $time_slote,
                'created_date' =>$date
        );

        $this->db->insert("vd_work_schedule",$data);
         $insert_id =$this->db->insert_id();

       
         
        $check_doc1 = $this->db->query("SELECT * FROM `vd_work_schedule` WHERE `doctor_id` = '".$doctor_id."'")->result();
       
        $json = array('status' => true,'data'=> $check_doc1);


      }

        return  $json;
     }


    //  public function add_schedule($obj){

    //     $doctor_id = $obj['doctor_id'];
    //     $day = '1';
    //     $mor_start = $obj['mor_start'];
    //     $mor_end = $obj['mor_end'];
    //     $eve_start = $obj['eve_start'];
    //     $eve_end = $obj['eve_end'];
    //     $time_slote = $obj['time_slote'];
    //     $date = date('Y-m-d H:i:s');

    //     $check_doc = $this->db->query("SELECT * FROM `vd_work_schedule` WHERE `doctor_id` = '".$doctor_id."'")->result();

    //     if(count($check_doc) > 0){

    //       $data1 = array(
    //             'doctor_id' => $doctor_id,
    //             'day' => $day,
    //             'mor_start' => $mor_start,
    //             'mor_end'=>  $mor_end ,
    //             'eve_start' => $eve_start,
    //             'eve_end' => $eve_end,
    //             'time_slote' => $time_slote,
    //             'created_date' =>$date

    //     );


    //         $this->db->where("doctor_id",$doctor_id); 
    //       $this->db->update("vd_work_schedule",$data1);


    //       $this->db->where("doctor_id",$doctor_id); 
    //      $this->db->delete('vd_timeslot_morning');


    //     $start = new DateTime($mor_start);
    //     $end = new DateTime($mor_end);
    //     $start_time = $start->format('h:i A');
    //     $end_time = $end->format('h:i A');
    //     $i=0;
    //     while(strtotime($start_time) <= strtotime($end_time)){
    //         $start = $start_time;
    //         $end = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
    //         $start_time = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
    //         $i++;
    //         if(strtotime($start_time) <= strtotime($end_time)){
    //             //$time[$i]['start'] = $start;
    //           // $time[$i]['end'] = $end;
    //           $array_of_time[] = $start;
    //              $data = array(             
    //             'mor_time' => $start,
    //             'doctor_id' => $doctor_id
    //             );

    //              $this->db->insert("vd_timeslot_morning",$data);
    //               $insert_id1 =$this->db->insert_id();
    //         }
    //     }


    //      $this->db->where("doctor_id",$doctor_id); 
    //      $this->db->delete('vd_timeslot_evening');



    //      $start_e = new DateTime($eve_start);
    //     $end_e = new DateTime($eve_end);
    //     $start_time_e = $start_e->format('h:i A');
    //     $end_time_e = $end_e->format('h:i A');
    //     $i=0;
    //     while(strtotime($start_time_e) <= strtotime($end_time_e)){
    //         $start_e = $start_time_e;
    //         $end_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
    //         $start_time_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
    //         $i++;
    //         if(strtotime($start_time_e) <= strtotime($end_time_e)){
    //           $array_of_time_e[] = $start_e;
    //           // $time[$i]['start'] = $start_e;
    //             //$time[$i]['end'] = $end_e;

    //              $data = array(             
    //             'eve_time'=>  $start_e,
    //             'doctor_id' => $doctor_id
    //             );
    //              $this->db->insert("vd_timeslot_evening",$data);
    //               $insert_id1 =$this->db->insert_id();
    //         }
    //     }




    //   $json = array('status' => true,'data'=> $check_doc);

    //     }else{

    //     $data = array(
    //             'doctor_id' => $doctor_id,
    //             'day' => $day,
    //             'mor_start' => $mor_start,
    //             'mor_end'=>  $mor_end ,
    //             'eve_start' => $eve_start,
    //             'eve_end' => $eve_end,
    //             'time_slote' => $time_slote,
    //             'created_date' =>$date

    //     );

    //     $this->db->insert("vd_work_schedule",$data);
    //      $insert_id =$this->db->insert_id();

    //     $start = new DateTime($mor_start);
    //     $end = new DateTime($mor_end);
    //     $start_time = $start->format('h:i A');
    //     $end_time = $end->format('h:i A');
    //     $i=0;
    //     while(strtotime($start_time) <= strtotime($end_time)){
    //         $start = $start_time;
    //         $end = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
    //         $start_time = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time)));
    //         $i++;
    //         if(strtotime($start_time) <= strtotime($end_time)){
    //             //$time[$i]['start'] = $start;
    //           // $time[$i]['end'] = $end;
    //           $array_of_time[] = $start;
    //              $data = array(             
    //             'mor_time' => $start,
    //             'doctor_id' => $doctor_id
    //             );
    //              $this->db->insert("vd_timeslot_morning",$data);
    //               $insert_id1 =$this->db->insert_id();
    //         }
    //     }


    //      $start_e = new DateTime($eve_start);
    //     $end_e = new DateTime($eve_end);
    //     $start_time_e = $start_e->format('h:i A');
    //     $end_time_e = $end_e->format('h:i A');
    //     $i=0;
    //     while(strtotime($start_time_e) <= strtotime($end_time_e)){
    //         $start_e = $start_time_e;
    //         $end_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
    //         $start_time_e = date('h:i A',strtotime('+'.$time_slote.' minutes',strtotime($start_time_e)));
    //         $i++;
    //         if(strtotime($start_time_e) <= strtotime($end_time_e)){
    //           $array_of_time_e[] = $start_e;
    //           // $time[$i]['start'] = $start_e;
    //             //$time[$i]['end'] = $end_e;

    //              $data = array(             
    //             'eve_time'=>  $start_e,
    //             'doctor_id' => $doctor_id
    //             );
    //              $this->db->insert("vd_timeslot_evening",$data);
    //               $insert_id1 =$this->db->insert_id();
    //         }
    //     }

    //     $check_doc1 = $this->db->query("SELECT * FROM `vd_work_schedule` WHERE `doctor_id` = '".$doctor_id."'")->result();
       
    //     $json = array('status' => true,'data'=> $check_doc1);


    //   }

    //     return  $json;
    //  }

    

      public function get_appointment($obj){
       $doctor_id = $obj['doctor_id'];

       $result1 = $this->db->query("SELECT A.*,B.* FROM `vd_appointment`AS A 
        INNER JOIN `vd_patient_details` AS B ON B.`patient_id` = A.`patient_id`
        WHERE  A.`doctor_id` = '$doctor_id' AND A.`status` ='0' ")->result();
        
        $result2 = $this->db->query("SELECT A.*,B.* FROM `vd_appointment`AS A 
        INNER JOIN `vd_patient_details` AS B ON B.`patient_id` = A.`patient_id`
        WHERE  A.`doctor_id` = '$doctor_id' AND A.`status` ='1' ")->result();
        
        $result3 = $this->db->query("SELECT A.*,B.* FROM `vd_appointment`AS A 
        INNER JOIN `vd_patient_details` AS B ON B.`patient_id` = A.`patient_id`
        WHERE  A.`doctor_id` = '$doctor_id' AND A.`status` ='3' ")->result();

            
            $res = array_merge($result1,$result2,$result3); 
      
       

         $json = array('status' => true,'data'=> $res);
       
       return  $json;

     }


     public function get_schedule($obj){

       $doctor_id = $obj['doctor_id'];

        $result1 = $this->db->query("SELECT *  FROM `vd_work_schedule` WHERE  `doctor_id` = '$doctor_id' ")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;

     }

     // public function edit_appointment_status($obj){

     //   $appointment_id = $obj['appointment_id'];
     //   $st = $obj['status'];

     //   $check_staus = $this->db->query("SELECT patient_id FROM vd_appointment WHERE appointment_id = '".$appointment_id."'")->row();
     //     if($check_staus->patient_id){

     //      $patient = $this->db->query("SELECT mobile FROM vd_patient_details WHERE patient_id = '".$check_staus->patient_id."'")->row();
        
     //           $data = array(
     //                  'appointment_id'=>$appointment_id,
     //                  'status' =>$st
     //           );

     //            $this->db->where("appointment_id",$appointment_id); 
     //            $this->db->update("vd_appointment",$data);


     //            if($st == 1){
     //            $text = "your appointment is fix";
     //           // $mobile = $obj['contact'];
     //            $this->sendmessage($text,$patient->mobile);
     //            }

     //             $json = array('status' => true,'data'=> "Status updated Successfully");

     //     }
     //     //else{
          
     //    //          $data = array(
     //    //                 'status' =>'1'
     //    //          );

     //    //           $this->db->where("appointment_id",$appointment_id); 
     //    //           $this->db->update("vd_appointment",$data);

     //    //            $json = array('status' => true,'data'=> "Status updated Successfully");

     //    // }

     //        return  $json;

     // }

  public function edit_appointment_status($obj){

       $appointment_id = $obj['appointment_id'];


       $check_staus = $this->db->query("SELECT patient_id,status,appointment_date,schedule_id FROM vd_appointment WHERE appointment_id = '".$appointment_id."' ")->row();

       if($check_staus->patient_id){

         $patient = $this->db->query("SELECT mobile FROM vd_patient_details WHERE patient_id = '".$check_staus->patient_id."'")->row();


        if($check_staus->status == 1){
        
               $data = array(
                      'status' =>'0'
               );

                $this->db->where("appointment_id",$appointment_id); 
                $this->db->update("vd_appointment",$data);

                //$text = "Virtual Dentist: Your request for an appointment has been declined. Please contact to book the next available date & time";
               
                //$this->sendmessage($text,$patient->mobile);
                $content = "\n";
                $this->sendflowmessage($patient->mobile,'611a48e5cd63d658076b586a',$content);
                $json = array('status' => true,'data'=> "Status updated successfully");

        }else{
          
                 $data = array(
                        'status' =>'1'
                 );

                  $this->db->where("appointment_id",$appointment_id); 
                  $this->db->update("vd_appointment",$data);

                  //$text = "Virtual Dentist: Your appointment on ".$check_staus->appointment_date." at ".$check_staus->schedule_id." has been confirmed, please adhere to the given scheduled time";
               
                  //$this->sendmessage($text,$patient->mobile);
                  $content = ",\n  \"date\": \"".date("d-m-Y", strtotime($check_staus->appointment_date))."\",\n  \"schedule_id\": \"".$check_staus->schedule_id."\"\n";
                  $this->sendflowmessage($patient->mobile,'610a2824fd0adb12013208b3',$content);
                  $json = array('status' => true,'data'=> "Status updated successfully");

        }
      }

            return  $json;

     }




     public function decline_status($obj){

       $appointment_id = $obj['appointment_id'];


      
       $check_staus = $this->db->query("SELECT patient_id,status,appointment_date,schedule_id FROM vd_appointment WHERE appointment_id = '".$appointment_id."' ")->row();

       if($check_staus->patient_id){

         $patient = $this->db->query("SELECT mobile FROM vd_patient_details WHERE patient_id = '".$check_staus->patient_id."'")->row();


          $data = array(
                        'status' =>'3'
                 );

                  
                  $this->db->where("appointment_id",$appointment_id); 
                $this->db->update("vd_appointment",$data);

                // $text = "Virtual Dentist: Your request for an appointment has been declined. Please contact to book the next available date & time";
               
                // $this->sendmessage($text,$patient->mobile);
                $content = "\n";
                $this->sendflowmessage($patient->mobile,'611a48e5cd63d658076b586a',$content);

                 $json = array('status' => true,'data'=> "Status updated successfully");



       }

        return  $json;

     }


    public function attend_patient($obj){

         $appointment_id = $obj['appointment_id'];
         $check_staus = $this->db->query("SELECT status, patient_id FROM vd_appointment WHERE appointment_id = '".$appointment_id."' ")->row();

         if($check_staus->status == 1){
        
               $data = array(
                      'status' =>'2'
               );

                $this->db->where("appointment_id",$appointment_id); 
                $this->db->update("vd_appointment",$data);



                 $json = array('status' => true,'data'=> "Attended successfully");

        }


         return  $json;

     }



    public function add_feedback($obj){

    $doctor_id = $obj['doctor_id'];
    $contact = $obj['mobile'];
    $feed_desc = $obj['feed_desc'];
    $rating = $obj['rating'];
    $date = date('Y-m-d H:i:s');

    $check_patient = $this->db->query("SELECT patient_id  FROM vd_patient_details WHERE mobile = '".$contact."' ")->row();
    if($check_patient){
        $check_appointment =  $this->db->query("SELECT appointment_id FROM vd_appointment WHERE `patient_id` = '".$check_patient->patient_id."' AND `doctor_id` = '".$doctor_id."' AND `status` = '1' ")->row();

            if($check_appointment){

                  $data = array(
                  'doctor_id' =>  $doctor_id,
                  'patient_id' => $check_patient->patient_id,
                  'feed_desc' =>  $feed_desc,
                  'rating' => $rating,
                  'created_date'=>$date
                 );

             $this->db->insert("vd_feedback",$data);
              $insert_id1 =$this->db->insert_id();

              $json = array('status' => true,'data'=> " Feedback added successfully");

            }else{

             $json = array('status' => false,'data'=> "You haven't taken any appointment with this doctor");

            }


        }else{

           $json = array('status' => false,'data'=> "You haven't registered yet");

        }


        return  $json;

     }

     public function get_feedback($obj){
      $doctor_id = $obj['doctor_id'];

       $result1 = $this->db->query("SELECT *  FROM `vd_feedback` WHERE  `doctor_id` = '$doctor_id' ")->result();
       
       if($result1 > 0){
             $json = array('status' => true,'data'=> $result1);
       }else{
          
           $json = array('status' => false,'data'=> 'No feedback yet'); 
           
       }
         return  $json;

     }

     public function get_patient($obj)
     {

      $doctor_id = $obj['doctor_id'];

         $result1 = $this->db->query("SELECT A.*,B.* FROM `vd_patient_details`AS A INNER JOIN `vd_appointment` AS B ON B.`patient_id` = A.`patient_id` WHERE  B.`doctor_id` = '$doctor_id' AND B.`status` = '1' OR B.`status` = '2'")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;

     }

      public function get_counsultant(){

      //$counsultant = $obj['counsultant'];

         $result1 = $this->db->query("SELECT * FROM vd_doctors WHERE doctor_type = 'Counsultation' AND is_verified = '1' AND acc_status = '1'")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;

     }

     public function get_all_doctors($obj){

      // $doctor = $obj['doctor'];


         $result1 = $this->db->query("SELECT * FROM vd_doctors WHERE doctor_type = 'doctor' AND is_verified = '1' AND acc_status = '1' ")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;


     }

     public function get_all_subscriber(){

      // $doctor = $obj['doctor'];


         $result1 = $this->db->query("SELECT * FROM vd_doctors WHERE profile_pic != '0' ")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;


     }

      public function get_all_subscribed(){

      // $doctor = $obj['doctor'];


         $result1 = $this->db->query("SELECT * FROM vd_doctors WHERE is_verified = '1' AND plan_id = '0' AND pay_status = 'success' AND profile_pic != '0' ")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;


     }

     public function add_holiday($obj)
     {

      $doctor_id = $obj['doctor_id'];
      $holiday_date = $obj['holiday_date'];
       $date = date('Y-m-d H:i:s');
       
       $ckk = $this->db->query("SELECT * FROM vd_holiday_schedule WHERE doctor_id = '".$doctor_id."' AND holiday_date = '".$holiday_date."' ")->result();
        
        if(count($ckk)>0){
             $json = array('status' => false,'msg'=> "Date already added into holiday's ");
        }else{
           $data = array(
            'doctor_id' =>  $doctor_id,
            'holiday_date' => $holiday_date,
            'created_date'=>$date
           );

       $this->db->insert("vd_holiday_schedule",$data);
        $insert_id1 =$this->db->insert_id();

        $result1 = $this->db->query("SELECT * FROM vd_holiday_schedule WHERE doctor_id = '".$doctor_id."' ")->result();

         $json = array('status' => true,'data'=> $result1);
        }

        return  $json;

     }

     public function get_holiday($obj){
      $doctor_id = $obj['doctor_id'];

       $result1 = $this->db->query("SELECT * FROM vd_holiday_schedule WHERE doctor_id = '".$doctor_id."' ")->result();

         $json = array('status' => true,'data'=> $result1);

         return  $json;

     }

     public function update_holiday($obj){

      $doctor_id = $obj['doctor_id'];
       $holiday_id = $obj['holiday_id'];
      $holiday_date = $obj['holiday_date'];
       $date = date('Y-m-d H:i:s');

           $data = array(
            'doctor_id' =>  $doctor_id,
            'holiday_date' => $holiday_date,
            'created_date'=>$date
           );

      
       $this->db->where("holiday_id",$holiday_id); 
        $this->db->update("vd_holiday_schedule",$data);
        $json = array('status' => true,'data'=> $holiday_date );

        return  $json;

     }

     public function delete_doctor($obj){

      $doctor_id = $obj['doctor_id'];

      $this->db->where('doctor_id', $doctor_id);
     $deleted_id = $this->db->delete('vd_doctors');
     $json = array('status' => true,'data'=> " Doctor data deleted Successfully");

     return $json;

     }
      public function delete_patient($obj){

      $patient_id = $obj['patient_id'];

      $this->db->where('patient_id', $patient_id);
     $deleted_id = $this->db->delete('vd_patient_details');
     $json = array('status' => true,'data'=> " Patient's data deleted Successfully");

     return $json;

     }

      public function delete_holiday($obj){

      $holiday_id = $obj['holiday_id'];

      $this->db->where('holiday_id', $holiday_id);
     $deleted_id = $this->db->delete('vd_holiday_schedule');
     $json = array('status' => true,'data'=> " Holiday's data deleted Successfully");

     return $json;

     }

     public function delete_schedule($obj){

      $schedule_id = $obj['schedule_id'];

      $this->db->where('schedule_id', $schedule_id);
     $deleted_id = $this->db->delete('vd_work_schedule');
     $json = array('status' => true,'data'=> " Schedule's data deleted Successfully");

     return $json;

     }

     public function approved($obj){

       $doctor_id = $obj['doctor_id'];
      

        $check_staus = $this->db->query("SELECT is_verified ,name,email_id FROM vd_doctors WHERE doctor_id = '".$doctor_id."' ")->row();
        if($check_staus->is_verified == 0){
        
               $data = array(
                      'is_verified' =>'1'
               );

                $this->db->where("doctor_id",$doctor_id); 
                $this->db->update("vd_doctors",$data);

                $sub = 'Email Verification';
                $msg = 'Hello <b>Dr. '.ucfirst($check_staus->name).'</b>,<br>
                    You have just registered an account on VirtualDentist.<br>
                    Please click the link below to pay for subscription<br>
                    <a href="https://virtualdentist.in/home/pay_now/'.$doctor_id.'" target="_blank">click here</a><br>
                    This step is required to confirm the email you entered is valid.<br>
                    If you did not sign up for this account you can ignore this email.<br><br>
                    <b>Thank You!!</b><br>
                    <b>Team VirtualDentist!</b>';
                  $this->api_model->sendemail($check_staus->email_id,$sub,$msg); 

                
        }
        
         $json = array('status' => true,'data'=> "Subscriber has been approved successfully" , 'is_verified'=> "1");


        return $json; 

     }


     public function active($obj){

       $doctor_id = $obj['doctor_id'];
      

        $check_staus = $this->db->query("SELECT acc_status FROM vd_doctors WHERE doctor_id = '".$doctor_id."' ")->row();
        if($check_staus->acc_status == 0){
        
               $data = array(
                      'acc_status' =>'1'
               );

                $this->db->where("doctor_id",$doctor_id); 
                $this->db->update("vd_doctors",$data);

               

                 $json = array('status' => true,'data'=> "Status updated successfully" , 'is_verified'=> "1");

        }else{
          
                 $data = array(
                    'acc_status' =>'0'
                 );

                   $this->db->where("doctor_id",$doctor_id); 
                $this->db->update("vd_doctors",$data);

                  $json = array('status' => true,'data'=> "Status updated successfully" , 'is_verified'=> "0");

        }

        return $json; 

     }



     public function send_notification($obj){

      $doctor_id = $obj['doctor_id'];
      
       $check_staus = $this->db->query("SELECT email_id, name FROM vd_doctors WHERE doctor_id = '".$doctor_id."' ")->row();
      $email = $check_staus->email_id;
        $name = $check_staus->name;

       $data = array(
                      'is_verified' =>'0',
                     'acc_status' =>'0'
               );

                $this->db->where("doctor_id",$doctor_id); 
                $this->db->update("vd_doctors",$data);

                $sub = 'Renewal Verification';
                $msg = 'Hello Dr. '.$name.',<br>
                      Your subscription has been expired, please subscribe again.<br>
                    Please click the link below to pay for subscription<br>
                     <a href="https://virtualdentist.in/home/renew_now/'.$doctor_id.'" target="_blank">click here</a><br>
                    This step is required .<br>
                    If you did not sign up for this account you can ignore this email.<br><br>
                    <b>Thank You!!</b><br>
                    <b>Team VirtualDentist!</b>';
                  $this->api_model->sendemail($email,$sub,$msg); 

                 $json = array('status' => true,'data'=> "Renewal notification sent successfully" );

                  return $json;

     }

     public function getlink($obj){

       $email = $obj['email'];
        $date = date('Y-m-d H:i:s');
        
        $check_email = $this->db->query("SELECT email_id FROM vd_doctors WHERE email_id = '".$email."'  AND is_verified = '1' ")->result();
        
        if(count($check_email) > 0){
        

        $data = array(
                      'token' =>$date
               );

                $this->db->where("email_id",$email); 
                $this->db->update("vd_doctors",$data);

        $sub = 'Reset Password';
                $msg = 'Hello ,<br>
                      
                    A request was submitted to change your account credentials. If this was you, click on the link below to reset them.<br>
                    <a href="'.base_url().'home/changepassword/'.$email.'" target="_blank">click here</a><br>
                    This link will expire within 5 minutes.<br>
                    If you dont want to reset your credentials, just ignore this email and nothing will be changed..<br>
                    <b>Thank You!!</b><br>
                    <b>Team VirtualDentist!</b>';
                  $this->api_model->sendemail($email,$sub,$msg); 

                 $json = array('status' => true,'data'=> "Password reset link has been sent to your email Id " );

                 
                  
        }else{
            
             $json = array('status' => false,'data'=> "No registration with this email iD " );
        
        }
         return $json;
     }

     public function changepass($obj){
      $email = $obj['email'];
      $pass = md5($obj['pass']);



       $data = array(
                      'password' =>$pass
               );

                $this->db->where("email_id",$email); 
                $this->db->update("vd_doctors",$data);

                 $json = array('status' => true,'data'=> "Your password has been changed successfully" );

                  return $json;


     }
     
     public function c_changepass($obj){
      $doctor_id = $obj['doctor_id'];
      $pass = md5($obj['pass']);

       $data = array(
                      'password' =>$pass
               );

                $this->db->where("doctor_id",$doctor_id); 
                $this->db->update("vd_doctors",$data);

                 $json = array('status' => true,'data'=> "Your password has been changed successfully" );

                  return $json;


     }
     
     public function admin_change_pass($obj){
         
          $pass =$obj['pass'];



       $data = array(
                      'password' =>$pass
               );

                $this->db->where("username",'admin'); 
                $this->db->update("vd_admin",$data);

                 $json = array('status' => true,'data'=> "Your password has been changed successfully" );

                  return $json;
         
         
     }
     
       public function getcounsultant($obj){
            $doctor_id = $obj['doctor_id'];

       // $result1 = $this->db->query("SELECT A.*,B.* FROM `vd_doctors`AS A INNER JOIN `vd_clinic` AS B ON B.`clinic_id` = A.`clinic_id` WHERE  A.`doctor_id` = '$doctor_id' AND A.`doctor_type` = 'doctor' ")->result();

          $result2 = $this->db->query("SELECT * FROM `vd_doctors` WHERE `doctor_id` = '$doctor_id' AND `doctor_type` = 'Counsultation' AND `profile_pic` != '0'")->result();

          

            $json = array('status' => true,'data'=> $result2 );

                  return $json;
     }
     
     
     public function lastlogout($obj){
         
          $doctor_id = $obj['doctor_id'];
          
          $date = date('Y-m-d H:i:s'); 
          
           $data = $this->db->query("UPDATE `vd_doctors` SET `last_logged_out`='$date' WHERE `doctor_id` = '$doctor_id'");

               if($data){ 
                   $json = array('status' => true,'data'=> "Successfully logged out" );
                    }else{
                        
                       $json = array('status' => false,'data'=> "Error In Logged Out" ); 
                    }

                

                  return $json;
         
     }


     public function get_payinfo($obj){

       $doctor_id = $obj['doctor_id'];

       $result = $this->db->query("SELECT * FROM vd_doctors WHERE doctor_id = '".$doctor_id."' ")->row();
        $product_info =  $result->reg_number;
        $customer_emial =  $result->email_id;
        $customer_name =  $result->name;
        $mobile =  $result->mobile_number;
        $plan_id =  $result->plan_id;
        if($plan_id == 1) {
            $amount = '5000';
        }else {
            $amount = '2000';
        }

        $PAYU_BASE_URL = "";

      $action = "https://sandboxsecure.payu.in/_payment";

      $MERCHANT_KEY = "rLtGWaHK"; //change  merchant with yours
          $SALT = "SiKMPQ8ykc";  //change salt with yours 


          $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
          //optional udf values 
          $udf1 = '';
          $udf2 = '';
          $udf3 = '';
          $udf4 = '';
          $udf5 = '';
          
          $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|||||||||||' . $SALT;
           $hash = strtolower(hash('sha512', $hashstring));
           
         $success = base_url() . 'Status';  
          $fail = base_url() . 'Status';
          $cancel = base_url() . 'Status';
          
          
           $data = array(
              'mkey' => $MERCHANT_KEY,
              'tid' => $txnid,
              'hash' => $hash,
              'amount' => $amount,
               'name' => $customer_name,
              'reg_number' => $product_info,
              'email' => $customer_emial,
              'mobile' => $mobile,           
              'action' => $action, 
              'sucess' => $success,
              'failure' => $fail,
              'cancel' => $cancel            
          );


           $json = array('status' => true,'data'=> $data );

                  return $json;


     }

}