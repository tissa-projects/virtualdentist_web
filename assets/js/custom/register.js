/* register action */
$('#registerform').submit(function(event) {
    event.preventDefault();
    var site_url = $("#site_url").val();
    var fullname = $("#fullname").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var mobile = $("#mobile").val();
    $.ajax({
        url:site_url+"onlinecourses_Api/register",
        type:"POST",
        // enctype: 'multipart/form-data',
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: JSON.stringify({fullname:fullname,email:email,password:password,mobile:mobile,user_role:'Learner'}),
        success:function(data,status,xhr) {
            var json_text = JSON.stringify(data, null, 2);
            var obj = JSON.parse(json_text);
            if(obj.success == 1) {
                $("#finalmsg").html('<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+obj.message+'</div>');
                setTimeout(function() {
                    $("#finalmsg").html('');
                    location.href = site_url+"onlinecourses/login";
                },4000);
                return false;
            } else if(obj.success == 2){
                $("#mobileerror").html(obj.message);
                setTimeout(function() {
                    $("#mobileerror").html('');
                },4000);
                $("#mobile").focus();
                return false;
            } else {
                $("#emailerror").html(obj.message);
                setTimeout(function() {
                    $("#emailerror").html('');
                },4000);
                $("#email").focus();
                return false;
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $("#finalmsg").html('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+errorMessage+'</div>');
            setTimeout(function() {
                $("#finalmsg").html('');
            },4000);
            return false;
        },
    });
});