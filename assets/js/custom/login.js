/* login action */
$('#loginform').submit(function(event) {
    event.preventDefault();
    var site_url = $("#site_url").val();
    var email = $("#email").val();
    var password = $("#password").val();
    $.ajax({
        url:site_url+"onlinecourses_Api/login",
        type:"POST",
        // enctype: 'multipart/form-data',
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: JSON.stringify({email:email,password:password}),
        success:function(data,status,xhr) {
            var json_text = JSON.stringify(data, null, 2);
            var obj = JSON.parse(json_text);
            if(obj.success == 1) {
                location.href = site_url+"onlinecourses";
                // $("#finalmsg").html('<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+obj.message+'</div>');
                // setTimeout(function() {
                //     $("#finalmsg").html('');
                //     location.href = site_url+"onlinecourses";
                // },4000);
            } else if(obj.success == 2){
                $("#passerror").html(obj.message);
                setTimeout(function() {
                    $("#passerror").html('');
                },4000);
                $("#password").focus();
            } else if(obj.success == 3) {
                $("#emailerror").html(obj.message);
                setTimeout(function() {
                    $("#emailerror").html('');
                },4000);
                $("#email").focus();
            } else {
                $("#finalmsg").html('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+obj.message+'</div>');
                setTimeout(function() {
                    $("#finalmsg").html('');
                },4000);
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $("#finalmsg").html('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+errorMessage+'</div>');
            setTimeout(function() {
                $("#finalmsg").html('');
            },4000);
        },
    });
});

/* forgot action */
$('#forgotform').submit(function(event) {
    event.preventDefault();
    var site_url = $("#site_url").val();
    var email = $("#email").val();
    $.ajax({
        url:site_url+"onlinecourses_Api/forgotpassword",
        type:"POST",
        // enctype: 'multipart/form-data',
        processData:false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: JSON.stringify({email:email}),
        success:function(data,status,xhr) {
            var json_text = JSON.stringify(data, null, 2);
            var obj = JSON.parse(json_text);
            if(obj.success == 1) {
                $("#finalmsg").html('<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+obj.message+'</div>');
                setTimeout(function() {
                    $("#finalmsg").html('');
                    location.href = site_url+"onlinecourses";
                },4000);
            } else {
                $("#emailerror").html(obj.message);
                setTimeout(function() {
                    $("#emailerror").html('');
                },4000);
                $("#email").focus();
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $("#finalmsg").html('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+errorMessage+'</div>');
            setTimeout(function() {
                $("#finalmsg").html('');
            },4000);
        },
    });
});

$(function() {
    var site_url = $("#site_url").val();
    $("form[name='changepassform']").validate({
        ignore: [],
        debug: false,
        rules: {
            password: {
                required: true,
                minlength: 8
            },
            conf_password: {
                required: true,
                minlength: 8,
                equalTo : "#password"
            }
        },
        messages: {
            password: {
                required: "Please enter password",
                minlength: "Please enter at least 8 characters"
            },
            conf_password: {
                required: "Please enter confirm password",
                minlength: "Please enter at least 8 characters",
                equalTo: "Password and confirm password should be same"
            }
        },  
        submitHandler: function(form) {
            var email = $("#email").val();
            var password = $("#password").val();
            var conf_password = $("#conf_password").val();
            $.ajax({
                url:site_url+"onlinecourses_Api/changeforgotpassword",
                type:"POST",
                // enctype: 'multipart/form-data',
                processData:false,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                data: JSON.stringify({email:email,password:password,conf_password:conf_password}),
                success:function(data,status,xhr) {
                    var json_text = JSON.stringify(data, null, 2);
                    var obj = JSON.parse(json_text);
                    if(obj.success == 1) {
                        $("#finalmsg").html('<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+obj.message+'</div>');
                        setTimeout(function() {
                            $("#finalmsg").html('');
                            location.href = site_url+"onlinecourses";
                        },4000);
                    } else {
                        $("#finalmsg").html('<div class="alert alert-danger alert-dismissible bg-success text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+obj.message+'</div>');
                        setTimeout(function() {
                            $("#finalmsg").html('');
                            location.href = site_url+"onlinecourses";
                        },4000);
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $("#finalmsg").html('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'+errorMessage+'</div>');
                    setTimeout(function() {
                        $("#finalmsg").html('');
                    },4000);
                },
            });
        }
    });
});