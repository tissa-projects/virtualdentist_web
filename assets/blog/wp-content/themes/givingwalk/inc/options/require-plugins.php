<?php
add_action( 'tgmpa_register', 'givingwalk_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
*/
function givingwalk_theme_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     * 
     */
    $plugins = array(
        array(
            'name'               => esc_html__('EF4-Framework','givingwalk'),
            'slug'               => 'ef4-framework',
            'source'             => 'charitywalk/ef4-framework.zip',
            'required'           => true,
        ),
        array(
            'name'               => esc_html__('Visual Composer','givingwalk'),
            'slug'               => 'js_composer',
            'source'             => 'js_composer.zip',
            'required'           => true,
        ),
        array(
            'name'               => esc_html__('Revolution Slider','givingwalk'),
            'slug'               => 'revslider',
            'source'             => 'revslider.zip',
            'required'           => true,
        ),
        array(
            'name'               => esc_html__('Custom Post Type UI','givingwalk'),
            'slug'               => 'custom-post-type-ui',
            'required'           => true,
        ),
        array(
            'name'               => esc_html__('Contact Form 7','givingwalk'),
            'slug'               => 'contact-form-7',
            'required'           => false,
        ),
        array(
            'name'               => esc_html__('WooCommerce','givingwalk'),
            'slug'               => 'woocommerce',
            'required'           => false,
        ),
        array(
            'name'               => esc_html__('WP User Avatar','givingwalk'),
            'slug'               => 'wp-user-avatar',
            'required'           => false,
        ),
        array(
            'name'               => esc_html__('EF4 Import/Export','givingwalk'),
            'slug'               => 'ef4-import-and-export',
            'source'             => 'ef4-import-and-export.zip',
            'required'           => true,
        ),
        array(
            'name'               => esc_html__('Newsletter','givingwalk'),
            'slug'               => 'newsletter',
            'required'           => false,
        ),
        array(
            'name'               => esc_html__('The Events Calendar','givingwalk'),
            'slug'               => 'the-events-calendar',
            'required'           => true,
        ),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
    */
    $config = array(
        'default_path' => 'http://spyropress.com/plugins/',                   // Default absolute path to pre-packaged plugins.
    );

    tgmpa( $plugins, $config );

}