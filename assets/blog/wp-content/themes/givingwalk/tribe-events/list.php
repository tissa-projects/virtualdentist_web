<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$show_top_bar = givingwalk_opt_events_top_bar_view();

do_action( 'tribe_events_before_template' );

if( in_array('list', $show_top_bar))
	tribe_get_template_part( 'modules/bar' );

tribe_get_template_part( 'list/content' );

do_action( 'tribe_events_after_template' );